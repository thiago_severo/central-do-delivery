package br.com.serveapp.serveapp.adapt;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineDetailItemCartAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;

    public LineDetailItemCartAdapter( JSONArray listViewItem) {
        this.listViewItem = listViewItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
           View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_item_detalhe_confim_pedido, parent, false);
             lineSimpleItemHolder = new LineSimpleItemHolder(v);
            return lineSimpleItemHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            ((LineSimpleItemHolder)holder).txt_nome_item.setText(listViewItem.getString(position));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public interface OnItemClickListner{
        void onItemClick(JSONObject item);
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        public TextView txt_nome_item;
        public LineSimpleItemHolder(View itemView) {
            super(itemView);
            txt_nome_item = itemView.findViewById(R.id.itemPedidoDetalheText);
        }
    }

}