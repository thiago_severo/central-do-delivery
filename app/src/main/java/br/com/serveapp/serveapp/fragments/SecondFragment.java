package br.com.serveapp.serveapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import br.com.serveapp.serveapp.R;

public class SecondFragment extends Fragment {


    TextView textView;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toast.makeText(getContext(),"Segunddaa fragment", Toast.LENGTH_LONG).show();
        return inflater.inflate(R.layout.fragment2, container, false);

    }




 }
