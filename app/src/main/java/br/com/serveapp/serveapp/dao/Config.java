package br.com.serveapp.serveapp.dao;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

public class Config {

    private static JSONObject config;
    private static final String fileName = "config";

    private static JSONObject getConfig(Context context) {
        if (config == null) {
            try {
                config = new JSONObject(Dao.read(fileName, context));
            } catch (JSONException e) {
                config = new JSONObject();
            }
        }
        return config;
    }

    public static String get(Context context, String key) {
        try {
            return getConfig(context).getString(key);
        } catch (JSONException e) {
            return null;
        }
    }

    public static void put(Context context, String key, String value) {
        try {
            getConfig(context).put(key, value);
            Dao.save(getConfig(context).toString(), fileName, context);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
