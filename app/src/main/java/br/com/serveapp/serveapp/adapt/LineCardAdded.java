package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class LineCardAdded extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListner onItemClickListner;
    Context context;

    public LineCardAdded(JSONArray listViewItem, Context context) {
        this.listViewItem = listViewItem;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_card_credit, parent, false);
        lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {

            //Toast.makeText(context, "bandeira " , Toast.LENGTH_LONG).show();

            String bandeira = listViewItem.getJSONObject(position).getString("bandeira");
            new ListSetImage(context, ((LineSimpleItemHolder) holder).imageCardCredit).execute("card/" + bandeira.replace(" ", "") + ".png");
//            new ListSetImage(context, ((LineSimpleItemHolder)holder).imageCardCredit).execute( "card/amex.png");

            String number = listViewItem.getJSONObject(position).getString("number");

            ((LineSimpleItemHolder) holder).imageCardCredit.setImageBitmap(null);
            ((LineSimpleItemHolder) holder).numCard.setText(number);
            //((LineSimpleItemHolder) holder).numCard.setText(bandeira.toUpperCase() + " **** " + number.trim().substring(number.length() - 4, number.length()));
            //((LineSimpleItemHolder) holder).nameCard.setText("Cartão " + listViewItem.getJSONObject(position).getString("cardType"));

            //      System.out.println("bandeira2: " + bandeira);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public interface OnItemClickListner {
        void onItemClick(JSONObject item);
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        public TextView numCard;
        public ImageView imageCardCredit;
        //public TextView nameCard;

        public LineSimpleItemHolder(View itemView) {
            super(itemView);

            //nameCard = itemView.findViewById(R.id.nameCard);
            imageCardCredit = itemView.findViewById(R.id.imageCardCredit);
            numCard = itemView.findViewById(R.id.numCard);
            // numCard.setText("card");
            imageCardCredit.setImageBitmap(null);



        }

    }

    public void setOnItemClickListner(OnItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

}