package br.com.serveapp.serveapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceLoginAPI;
import br.com.serveapp.serveapp.util.MaskEditUtil;

public class ConfirmCodeSms extends AppCompatActivity {

    TextInputEditText textCodeGenerated;
    private FirebaseAuth mAuth;
    public ProgressDialog mProgressCreateAddress;
    public TextView sendCodeNow;
    public TextView changeNumber;
    public TextView phoneAlready;
    public ProgressBar progressLoadCode;
    String mVerificationId = null;
    public TextView alertAlreadyCodeSms;
    public TextView textViewMessage;
    private Request requestLoginApi;
    boolean auth = false;
    boolean authF = false;
    boolean authN = false;
    String codeGenerated;
    PhoneAuthProvider phoneAuthProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_code_sms);

        mAuth = FirebaseAuth.getInstance();

        progressLoadCode = findViewById(R.id.progressLoadCode);
        textViewMessage = findViewById(R.id.textViewMessage);
        textCodeGenerated = findViewById(R.id.textCodeGenerated);
        sendCodeNow = findViewById(R.id.sendCodeNow);
        changeNumber = findViewById(R.id.changeNumber);
        phoneAlready = findViewById(R.id.phoneAlready);
        alertAlreadyCodeSms = findViewById(R.id.alertAlreadyCodeSms);
        //   mVerificationId = getIntent().getStringExtra("VerificationId");
        phoneAlready.setText(getIntent().getStringExtra("phoneNumber").replace("+55", ""));

        mProgressCreateAddress = new ProgressDialog(ConfirmCodeSms.this);
        String titleCreateAddress = "Verifcando...";
        mProgressCreateAddress.setTitle(titleCreateAddress);
        mProgressCreateAddress.setMessage("Aguarde...");
        requestLoginApi = new Request(getApplicationContext(), new ServiceLoginAPI(null, this));

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);
            }
        });

        alertAlreadyCodeSms.setVisibility(View.GONE);
        Selection.setSelection(textCodeGenerated.getText(), 0);
        textCodeGenerated.setCursorVisible(false);


        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                getIntent().getStringExtra("phoneNumber"),        // Phone number to verify
                30,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                ConfirmCodeSms.this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks

        sendingCode();

        textCodeGenerated.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {

                if (textCodeGenerated.getText().toString().length() > 5) {

                    PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, textCodeGenerated.getText().toString());

                  /*  ((InputMethodManager) ConfirmCodeSms.this.getSystemService(ConfirmCodeSms.this.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                            textCodeGenerated.getWindowToken(), 0);*/
                    mProgressCreateAddress.setTitle("Verificando...");
                    mProgressCreateAddress.setMessage("aguarde");
                    mProgressCreateAddress.show();

                    if (signInWithPhoneAuthCredential(credential, textCodeGenerated.getText().toString()) == false) {

                       /* dimissAlertAlreadyCodeSms("Código inváliooooooooooo! solicite um novo...");
                        textCodeGenerated.setText("");
                        int position = textCodeGenerated.length();
                        Editable etext = textCodeGenerated.getText();
                        Selection.setSelection(etext, position);
                        textCodeGenerated.setCursorVisible(false);
                        mProgressCreateAddress.dismiss();*/
                    }

                   /* if (codeGenerated != null) {
                        if (codeGenerated.equals(textCodeGenerated.getText().toString())) {

                            final Map<String, String> paramLogin = new HashMap<>();
                            paramLogin.put("email", "");
                            paramLogin.put("nome", "");
                            paramLogin.put("sobre_nome", "");
                            paramLogin.put("instructionId", "");
                            paramLogin.put("token", CreateAccountUserActivity.getToken(getApplicationContext()));
                            paramLogin.put("phone", getIntent().getStringExtra("phoneNumber").replace("+55", ""));
                            requestLoginApi.request(paramLogin);

                        } else {

                            dimissAlertAlreadyCodeSms("Código inválido! solicite um novo...");
                            textCodeGenerated.setText("");
                            int position = textCodeGenerated.length();
                            Editable etext = textCodeGenerated.getText();
                            Selection.setSelection(etext, position);
                            textCodeGenerated.setCursorVisible(false);
                            mProgressCreateAddress.dismiss();

                        }
                    }else{

                        dimissAlertAlreadyCodeSms("Código inválido! solicite um novo...");
                        textCodeGenerated.setText("");
                        int position = textCodeGenerated.length();
                        Editable etext = textCodeGenerated.getText();
                        Selection.setSelection(etext, position);
                        textCodeGenerated.setCursorVisible(false);
                        mProgressCreateAddress.dismiss();

                    }
*/
                }

            }
        });

        changeNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {

           /* codeSent();
            codeGenerated = credential.getSmsCode();
            //  signInWithPhoneAuthCredential(credential);
            mProgressCreateAddress.dismiss();
         */
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            if (e instanceof FirebaseAuthInvalidCredentialsException) {

                generateNewSend();
                mProgressCreateAddress.dismiss();
                dimissAlertAlreadyCodeSms("Um código de verificação já foi enviado, Tente Novamente, mais tarde!");
                codeSent();

            } else if (e instanceof FirebaseTooManyRequestsException) {

                mProgressCreateAddress.dismiss();
                generateNewSend();
                dimissAlertAlreadyCodeSms("Um código de verificação já foi enviado, Tente Novamente, mais tarde!");
                codeSent();
            } else {

                generateNewSend();
                dimissAlertAlreadyCodeSms("Não conseguimos enviar o código, verifique sua conexão com a internet!");
                codeSent();
                mProgressCreateAddress.dismiss();

            }

        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {

            generateNewSend();
            codeSent();
            mProgressCreateAddress.dismiss();
            PhoneAuthProvider.ForceResendingToken mResendToken = token;
            mVerificationId = verificationId;
      //      Toast.makeText(getApplicationContext(), "CODE SENT " + verificationId, Toast.LENGTH_LONG).show();

        }

    };

    public void sendingCode() {
        textCodeGenerated.setVisibility(View.GONE);
        alertAlreadyCodeSms.setVisibility(View.GONE);
        textViewMessage.setVisibility(View.GONE);
        sendCodeNow.setVisibility(View.GONE);
    }

    public void codeSent() {

        sendCodeNow.setVisibility(View.VISIBLE);
        progressLoadCode.setVisibility(View.GONE);
        textCodeGenerated.setVisibility(View.VISIBLE);
        textViewMessage.setVisibility(View.VISIBLE);
    }

    public void dimissAlertAlreadyCodeSms(String text) {

        alertAlreadyCodeSms.setText(text);
        alertAlreadyCodeSms.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            public void run() {
                alertAlreadyCodeSms.setVisibility(View.GONE);
            }
        }, 5000);

    }

    public void generateNewSend() {

        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                sendCodeNow.setText("Seu código não chegou? você poderá reenvia-lo em: " + millisUntilFinished / 1000);
                sendCodeNow.setClickable(false);
            }

            public void onFinish() {

                sendCodeNow.setClickable(true);
                sendCodeNow.setText("reenviar código");
                sendCodeNow.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mProgressCreateAddress.setTitle("Gerando novo código");
                        mProgressCreateAddress.setMessage("Aguarde...");
                        mProgressCreateAddress.show();

                        PhoneAuthProvider.getInstance().verifyPhoneNumber(getIntent().getStringExtra("phoneNumber"),        // Phone number to verify
                                30,
                                TimeUnit.SECONDS,
                                ConfirmCodeSms.this,
                                mCallbacks);

                    }
                });
            }

        }.start();

    }

    @Override
    public void onBackPressed() {

        setResult(RESULT_CANCELED);
        super.onBackPressed();
        overridePendingTransition(R.anim.entrer_from_right,
                R.anim.exit_to_left);

    }


    private boolean signInWithPhoneAuthCredential(final PhoneAuthCredential credential, String codeInsert) {

        String smsCode = credential.getSmsCode();

        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {

                            auth = true;
                            FirebaseUser user = task.getResult().getUser();
                            final Map<String, String> paramLogin = new HashMap<>();
                            paramLogin.put("email", "");
                            paramLogin.put("nome", "");
                            paramLogin.put("sobre_nome", "");
                            paramLogin.put("instructionId", "");
                            paramLogin.put("token", CreateAccountUserActivity.getToken(getApplicationContext()));
                            paramLogin.put("phone", getIntent().getStringExtra("phoneNumber").replace("+55", ""));
                            requestLoginApi.request(paramLogin);
                            //mProgressCreateAddress.dismiss();

                        } else {

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                mProgressCreateAddress.dismiss();
                                dimissAlertAlreadyCodeSms("Código inválido! solicite um novooooooo...");
                                textCodeGenerated.setText("");
                                int position = textCodeGenerated.length();
                                Editable etext = textCodeGenerated.getText();
                                Selection.setSelection(etext, position);
                                textCodeGenerated.setCursorVisible(false);
                                //         Toast.makeText(getApplicationContext(), "ff"+((FirebaseAuthInvalidCredentialsException) task.getException()).getErrorCode(), Toast.LENGTH_LONG).show();

                            }

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {

                                          @Override
                                          public void onFailure(@NonNull Exception e) {
                                              mProgressCreateAddress.dismiss();
                                              dimissAlertAlreadyCodeSms("Erro na verificação... Reveja se o código está correto e se você está conectado à internet.");
                                              textCodeGenerated.setText("");
                                              int position = textCodeGenerated.length();
                                              Editable etext = textCodeGenerated.getText();
                                              Selection.setSelection(etext, position);
                                              textCodeGenerated.setCursorVisible(false);
                                          }

                                      }
                );

        return auth;
    }
}



