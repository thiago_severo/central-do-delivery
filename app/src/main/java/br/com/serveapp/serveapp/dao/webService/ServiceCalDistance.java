package br.com.serveapp.serveapp.dao.webService;

import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCalDistance extends OnPostResponse {

    private RecyclerView.ViewHolder linetHolder;
    public ServiceCalDistance(RecyclerView.ViewHolder linetHolder){
        this.linetHolder = linetHolder;
        this.setAutoReconnect(true);
        this.setSaveCache(true);
        this.script = "select_and_calc_distance.php";
    }

    @Override
    public void onResponse(String response) {
        try {

            JSONObject object = new JSONObject(response);
            JSONObject result = object.getJSONObject("result");

            ((LineFavoritEstabelecimentAdapter.LineFavoritHolder) linetHolder).txt_distancia.setText(result.getString("distancia"));
            ((LineFavoritEstabelecimentAdapter.LineFavoritHolder) linetHolder).textValueFrete.setText(result.getString("frete"));
            ((LineFavoritEstabelecimentAdapter.LineFavoritHolder) linetHolder).textTime.setText(result.getString("temp_min") + "/" + result.getString("temp_max"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}