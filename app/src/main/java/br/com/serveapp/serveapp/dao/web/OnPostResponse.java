package br.com.serveapp.serveapp.dao.web;

import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.Map;

public abstract class OnPostResponse {

  //  private static final String url = "http://serveapp.000webhostapp.com/";
   // private static final String url = "http://172.20.255.181/";
//    private static final String folder = "script/";
   // private static final String folder = "serve-admin/public/script/";

    public String script;

    private String id;
    private boolean autoReconnect;
    private boolean saveCache;

    public void setAutoReconnect(boolean autoReconnect) {
        this.autoReconnect = autoReconnect;
    }

    public boolean isAutoReconnect() {
        return autoReconnect;
    }


    public void setSaveCache(boolean saveCache) {
        this.saveCache = saveCache;
    }

    public boolean isSaveCache() {
        return saveCache;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, String> getParam() {
        return null;
    }

    public String getScript(){
        return Script.getUrl()+Script.getFolder()+script;
    }

    public String getFileCacheName(){
        String retorno = getScript().replace("/","");
        retorno = retorno.replace(":","");
        retorno = ((id == null)? (retorno) : (retorno+"."+id));
        return retorno;
    }

    public String getId() {
        return id;
    }

    public void onUpdate(String response) {}

    public abstract void onResponse(String response);

    public void onErrorResponse(VolleyError error){

    }

    public void onCache(JSONObject response){

    }

    public void onStateConnection(StateConnection stateConnection){

    }


}
