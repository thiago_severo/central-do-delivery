package br.com.serveapp.serveapp.dao.webService;

import android.view.View;

import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.SplashScreenHomeApp;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceSystem extends OnPostResponse {

    SplashScreenHomeApp homeApp;

    public ServiceSystem(SplashScreenHomeApp homeApp) {
        this.homeApp = homeApp;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "serve_details.php";
    }

    @Override
    public Map<String, String> getParam() {
        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(homeApp.getApplicationContext()));
        return paramLogin;
    }


    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try{
            homeApp.update(jsonObject);
            homeApp.getProgressBar().setVisibility(View.GONE);
        }catch (NullPointerException e){}
    }


    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                homeApp.update(jsonObject);
            } catch (NullPointerException|JSONException e) {}
        }
    }

}
