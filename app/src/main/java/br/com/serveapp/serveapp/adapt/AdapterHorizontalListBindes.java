package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdapterHorizontalListBindes extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public ArrayList<Categoria> categoriaList = new ArrayList<>();
    public JSONArray brindes;
    public JSONArray cupons;
    Context context;
    private AdapterHorizontalListBindes.onItemClickListener listener;
    int mPosition;

     final static int TypePercent = 1;
     final static int TypeItem = 2;

    public AdapterHorizontalListBindes(Context context,  JSONArray brindes, JSONArray cupons){
         this.brindes = brindes;
         this.cupons = cupons;
        this.context = context;
    }


    public JSONObject getObject(int possition) {
       try{
           if(possition<cupons.length()){
               return cupons.getJSONObject(possition);
           }else{
               possition-= cupons.length();
               return brindes.getJSONObject(possition);
           }
       }   catch (JSONException e){
           return null;
       }
    }

    public boolean isCupom(int position){
        return position < cupons.length();
    }


    @Override
    public int getItemViewType(int position) {
        if(isCupom(position))
            return 1;
        else
            return 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder lineSimpleItemHolder = null;

        if(viewType == 1) { // cupom
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_brinde_cupom, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolderCupom(v);
        }
        else { // brinde
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_brinde, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolder(v);
        }
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        try {

            int progress = getObject(position).getInt("progress");

            if(isCupom(position)) {
             //   ((LineSimpleItemHolderCupom) holder).textNamDestaqueHome.setText(getObject(position).getString("titulo"));
                ((LineSimpleItemHolderCupom) holder).dateBrinde.setText(getObject(position).getString("validade"));
                ((LineSimpleItemHolderCupom) holder).valueCupom.setText(getObject(position).getString("ref"));
                ((LineSimpleItemHolderCupom) holder).mProgress.setProgress(progress);

                if(progress==100){
                    ((LineSimpleItemHolderCupom) holder).mProgress.setVisibility(View.GONE);
                    ((LineSimpleItemHolderCupom) holder).btnAddCupom.setVisibility(View.VISIBLE);
                    ((LineSimpleItemHolderCupom) holder).itemView.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onItemClick(getObject(position), true);
                                }
                            }
                    );
                    ((LineSimpleItemHolderCupom) holder).btnAddCupom.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onItemClick(getObject(position), true);
                                }
                            }
                    );
                }else{
                    ((LineSimpleItemHolderCupom) holder).btnAddCupom.setVisibility(View.GONE);
                }
            }
            else{
                new ListSetImage(context, ((LineSimpleItemHolder) holder).imagemDestaqueItemListHome).execute(getObject(position).getString("foto"));
                ((LineSimpleItemHolder) holder).dateBrinde.setText(getObject(position).getString("validade"));
                ((LineSimpleItemHolder) holder).textNamDestaqueHome.setText(getObject(position).getString("nome"));
                ((LineSimpleItemHolder) holder).mProgress.setVisibility(View.VISIBLE);
                ((LineSimpleItemHolder) holder).mProgress.setProgress(progress);
                ((LineSimpleItemHolder) holder).textProgressLabel.setText(getObject(position).getString("progress_label"));
                ((LineSimpleItemHolder) holder).qtd.setText(getObject(position).getString("qtd")+"x");
                if(progress==100){
                    ((LineSimpleItemHolder) holder).textProgressLabel.setVisibility(View.GONE);
                    ((LineSimpleItemHolder) holder).mProgress.setVisibility(View.GONE);
                    ((LineSimpleItemHolder) holder).btnAddCupom.setVisibility(View.VISIBLE);
                    ((LineSimpleItemHolder) holder).itemView.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onItemClick(getObject(position), false);
                                }
                            }
                    );
                    ((LineSimpleItemHolder) holder).btnAddCupom.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    listener.onItemClick(getObject(position), false);
                                }
                            }
                    );

                }else{
                    ((LineSimpleItemHolder) holder).btnAddCupom.setVisibility(View.GONE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return brindes.length()+cupons.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

            public TextView textNamDestaqueHome;
            public TextView dateBrinde;
            public View view;
            Button btnAddCupom;
            ImageView imagemDestaqueItemListHome;
            public int id;
            ProgressBar mProgress;
            TextView progressLabel;
            TextView qtd;
            TextView textProgressLabel;
            View itemView;
            public LineSimpleItemHolder(View itemView) {
                super(itemView);
                textNamDestaqueHome = itemView.findViewById(R.id.textNamDestaqueHome);
                dateBrinde = itemView.findViewById(R.id.dateBrinde);
                btnAddCupom = itemView.findViewById(R.id.btnAddCupom);
                imagemDestaqueItemListHome = itemView.findViewById(R.id.image);
                progressLabel = itemView.findViewById(R.id.progressLabel);
                qtd = itemView.findViewById(R.id.quantidade);
             //   btnAddCupom.setVisibility(View.GONE);
                textProgressLabel = itemView.findViewById(R.id.progressLabel);
                mProgress = itemView.findViewById(R.id.progressBarBrinde);
                mProgress.setMax(100);
                mProgress.setProgress(70);
               this. itemView = itemView;
            }
        }


    class LineSimpleItemHolderCupom extends RecyclerView.ViewHolder {

        public TextView valueCupom;
        public TextView dateBrinde;
        Button btnAddCupom;
        TextView textNamDestaqueHome;
        public int id;
        ProgressBar mProgress;
        TextView textProgressLabel;
        View itemView;
        public LineSimpleItemHolderCupom(View itemView) {
            super(itemView);

            textNamDestaqueHome = itemView.findViewById(R.id.textNamDestaqueHome);
            valueCupom = itemView.findViewById(R.id.valueCupom);
            dateBrinde = itemView.findViewById(R.id.dateBrinde);
            btnAddCupom = itemView.findViewById(R.id.btnAddCupom);
            textProgressLabel = itemView.findViewById(R.id.progressLabel);
            btnAddCupom.setVisibility(View.GONE);
            this.itemView = itemView;
            mProgress = itemView.findViewById(R.id.progressBarBrinde);
            mProgress.setMax(100);
            mProgress.setProgress(80);

           /* itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if(mProgress.getProgress()>=100)
                  //          listener.onItemClick(getObject(getAdapterPosition()), true);
                        }
                    }
            );*/

            }
    }

   public interface onItemClickListener{
       void onItemClick(JSONObject brinde, boolean isCupom);
   }

    public void setListener(AdapterHorizontalListBindes.onItemClickListener listener) {
        this.listener = listener;
    }

   public  void setList(JSONArray brindes, JSONArray cupons){
        this.cupons=cupons;
        this.brindes = brindes;
   }

}

