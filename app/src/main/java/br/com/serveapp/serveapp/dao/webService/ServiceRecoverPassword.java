package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.LoginActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceRecoverPassword extends OnPostResponse {

   private LoginActivity loginActivity;
   private  View mView;
    public ServiceRecoverPassword(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "redefinir_senha.php";
    }
    public void setmView(View mView) {
        this.mView = mView;
    }

    @Override
    public Map<String, String> getParam() {
        final EditText textRecouverEmail = mView.findViewById(R.id.textSendRecouverEmail);
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("instructionId",  "redefinir");
        paramLogin.put("email", textRecouverEmail.getText().toString());
        return paramLogin;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
       // loginActivity.mProgress.dismiss();

        Toast.makeText(loginActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        loginActivity.mProgresRecouverPass.dismiss();

    }

    @Override
    public void onResponse(String response) {
        if(!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject user = null;

                if (!jsonObject.isNull("result")) {
                    JSONObject result = jsonObject.getJSONObject("result");
                    loginActivity.buildConfirmDialog(result.getString("titulo"), result.getString("msg"));
                    loginActivity.mProgresRecouverPass.dismiss();
                }

            } catch (JSONException e) {
                e.printStackTrace();

                loginActivity.mProgress.dismiss();
                Toast.makeText(loginActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();

            }
    }


}
