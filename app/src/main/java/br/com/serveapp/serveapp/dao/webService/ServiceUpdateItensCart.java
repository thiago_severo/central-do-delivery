package br.com.serveapp.serveapp.dao.webService;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceUpdateItensCart extends OnPostResponse {

    TextView valueItemSelected;
    TextView valueCart;
    TextView TextQtdItens;
    JSONObject obj;
    Context context;
    ProgressBar progressBar;
    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;

    public ServiceUpdateItensCart(ConfirmItensPedidoActiviy confirmItensPedidoActiviy, Context context, TextView valueItemSelected, TextView valueCart, TextView TextQtdItens, JSONObject obj, ProgressBar progressBar) {
        setId(Repository.idEstabelecimento);
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.context = context;
        this.progressBar = progressBar;
        this.valueItemSelected = valueItemSelected;
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.valueCart = valueCart;
        this.obj = obj;
        this.TextQtdItens = TextQtdItens;
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        // loginActivity.mProgress.dismiss();
        Toast.makeText(context, "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {

        if (response != null && !response.equals("null"))

            try {
                confirmItensPedidoActiviy.reload(new JSONObject(response));
                progressBar.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }


}