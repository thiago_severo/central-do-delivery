package br.com.serveapp.serveapp.fragments;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.serveapp.serveapp.HomeCategoryEstabelecimentActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.SplashScreenEstabeleciment;
import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.entidades.Estabelecimento;
import br.com.serveapp.serveapp.entidades.HorarioFuncionamento;
import br.com.serveapp.serveapp.enun.TipoAtendimento;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static br.com.serveapp.serveapp.dao.web.Script.list_estabelecimento;

@SuppressLint("ValidFragment")
public class FragmentHome extends RequestActivity implements ZXingScannerView.ResultHandler{

    public FragmentTransaction fragmentTransaction;
    public int fragmentContainer;

    RecyclerView listAllEstabeleciments;
    ZXingScannerView scannerView;
    View view;

    private static List<Estabelecimento> estabelecimentoList = null;


    private static RecyclerView listFilterEstabeleciments;
    private static LineFavoritEstabelecimentAdapter lineAdapter;

    public FragmentHome() {

    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment2);

        listFilterEstabeleciments = findViewById(R.id.recyclerFilterEstabeleciments);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listFilterEstabeleciments.setLayoutManager(llm);



        final SearchView searchView = findViewById(R.id.searchEstabeleciements);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //      lineAdapter.getFilter().filter(newText);
                return true;
            }
        });

            final Map<String, String> paramLogin = new HashMap<>();
            paramLogin.put("id_end_cliente", "1");
            paramLogin.put("function_method", "list");
            request(list_estabelecimento, paramLogin);
    }


    private void initializeAdapter(JSONArray listEstabeleciment) {

        listEstabeleciment.length();
        lineAdapter = new LineFavoritEstabelecimentAdapter(getApplicationContext(), listEstabeleciment);
        listFilterEstabeleciments.setAdapter(lineAdapter);
        lineAdapter.setOnItemClickListener(new LineFavoritEstabelecimentAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JSONObject estabelecimento) {
                startActivity(new Intent(getApplicationContext(), HomeCategoryEstabelecimentActivity.class));
                startActivity(new Intent(getApplicationContext(), SplashScreenEstabeleciment.class));
                overridePendingTransition(0,0);
            }
        });

    }


    private List<Estabelecimento> initializeDataFavorits() {

        estabelecimentoList = new ArrayList<>();

        Estabelecimento estabelecimento3 = new Estabelecimento();

        HorarioFuncionamento  horarioFuncionamentoDelivery = new HorarioFuncionamento();
        horarioFuncionamentoDelivery.setAbertura("17:00");
        horarioFuncionamentoDelivery.setFechamento("23:00");
        List<HorarioFuncionamento>horarioFuncionamentosDelivery = new ArrayList<>();
        horarioFuncionamentosDelivery.add(horarioFuncionamentoDelivery);

        HorarioFuncionamento horarioFuncionamento = new HorarioFuncionamento();
        horarioFuncionamento.setAbertura("17:00");
        horarioFuncionamento.setFechamento("23:00");
        List<HorarioFuncionamento> horarioFuncionamentos = new ArrayList<>();
        horarioFuncionamentos.add(horarioFuncionamento);


        estabelecimento3.setDesc("O melhor hamburguer da região. O verdadeiro sabor do hambuerger");
        estabelecimento3.setNome("CREPERIA XAXADO");
        estabelecimento3.setLogo("creperia");
        estabelecimento3.setTipoAtendimento(TipoAtendimento.DELIVERY);
        estabelecimento3.setTempMedEntrega(60);
        estabelecimento3.setValorEntrega(2.5f);
        estabelecimento3.setHorariosDelivery(horarioFuncionamentosDelivery);
        estabelecimento3.setHorariosFuncionamento(horarioFuncionamentos);
        estabelecimentoList.add(estabelecimento3);


        Estabelecimento estabelecimento = new Estabelecimento();
        estabelecimento.setDesc("O melhor hamburguer da região. O verdadeiro sabor do hambuerger");
        estabelecimento.setNome("CANTINA BURGUERIA");
        estabelecimento.setLogo("ic_burgeria");
        estabelecimento.setTipoAtendimento(TipoAtendimento.TODOS);
        estabelecimentoList.add(estabelecimento);



        Estabelecimento estabelecimento2 = new Estabelecimento();
        estabelecimento2.setDesc("O melhor pastel da cidade. Venha conferir ");
        estabelecimento2.setNome("DOÇURA CASEIRA ");
        estabelecimento2.setLogo("ic_pastelaria_docura");
        estabelecimento2.setTipoAtendimento(TipoAtendimento.LOCAL);

        HorarioFuncionamento horarioFuncionamento2 = new HorarioFuncionamento();
        horarioFuncionamento2.setAbertura("17:00");
        horarioFuncionamento2.setFechamento("23:00");
        List<HorarioFuncionamento> horarioFuncionamentos2 = new ArrayList<>();
        horarioFuncionamentos2.add(horarioFuncionamento2);
        estabelecimento2.setHorariosFuncionamento(horarioFuncionamentos2);


        estabelecimentoList.add(estabelecimento2);


        Estabelecimento estabelecimento4 = new Estabelecimento();
        estabelecimento4.setDesc("O melhor pastel da cidade. Venha conferir ");
        estabelecimento4.setNome("RESTAURANTE CATULÉ ");
        estabelecimento4.setLogo("catule");
        estabelecimento4.setTipoAtendimento(TipoAtendimento.LOCAL);

        HorarioFuncionamento horarioFuncionamento4 = new HorarioFuncionamento();
        horarioFuncionamento4.setAbertura("17:00");
        horarioFuncionamento4.setFechamento("21:00");
        List<HorarioFuncionamento> horarioFuncionamentos4 = new ArrayList<>();
        horarioFuncionamentos4.add(horarioFuncionamento4);
        estabelecimento4.setHorariosFuncionamento(horarioFuncionamentos4);

        estabelecimentoList.add(estabelecimento4);



        return estabelecimentoList;
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //Save the fragment's state here
    }


    @SuppressLint("ResourceType")
    public void add() {
        TreeFragment secondFragment = new TreeFragment();
        //      fragmentTransaction.add(fragmentContainer, secondFragment,"secondfragment");
        //      fragmentTransaction.addToBackStack("fragment stack 1");
    /*    MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.fragmentListHome.add(secondFragment);
        fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
*/
        //mainActivity.fragmentListHome.add()
        fragmentTransaction.setCustomAnimations(R.anim.slide_in_up, R.anim.slide_out_up);
        fragmentTransaction.add(fragmentContainer, secondFragment, "secondfragment");


     //   fragmentTransaction.replace(id.fragment_container_favorits, mainActivity.fragmentListHome.get(mainActivity.fragmentListHome.size() - 1));
        fragmentTransaction.commit();


    }


    @Override
    public void handleResult(Result result) {

    }


    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(String response) {
        if(!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                initializeAdapter(jsonObject.getJSONArray("result"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onResponseServe(String response) {

    }

    @Override
    public void onCache(String response) {

    }

}