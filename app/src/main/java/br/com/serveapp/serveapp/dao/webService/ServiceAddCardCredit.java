package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.AddCardPaymentActivity;
import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.CreateAddressPopUpActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceAddCardCredit extends OnPostResponse {

    AddCardPaymentActivity addCardPaymentActivity;

    public ServiceAddCardCredit(AddCardPaymentActivity addCardPaymentActivity) {

        this.addCardPaymentActivity = addCardPaymentActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "add_card_cliente.php";

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);

            Toast.makeText(addCardPaymentActivity.getApplicationContext(), "erro ao inserir cartão", Toast.LENGTH_LONG).show();
            addCardPaymentActivity.mProgress.dismiss();

    }


    @Override
    public void onResponse(String response) {

        if (!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);

                if (!jsonObject.isNull("result")) {

                    addCardPaymentActivity.setResult(1);
                    addCardPaymentActivity.finish();

                }

            } catch (JSONException e) {

            }
    }


}