package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AdapterHorizontalListCategoryItens extends RecyclerView.Adapter<AdapterHorizontalListCategoryItens.LineSimpleItemHolder> {

    public ArrayList<Categoria> categoriaList = new ArrayList<>();
    public JSONArray categoriaListObjects;

    private AdapterHorizontalListCategoryItens.onItemClickListener listener;
    private Context context;
    int mPosition;

    public AdapterHorizontalListCategoryItens(int position, Context context, JSONArray categoriaListObjects){
        this.context = context;
        this.categoriaListObjects = categoriaListObjects;
        this.mPosition = position;

        for(int i=0; i<categoriaListObjects.length(); i++){
           Categoria categoria = new Categoria();
           categoria.setLogo(null);
           categoriaList.add(categoria);
       }
    }

    @NonNull
    @Override
    public LineSimpleItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_categorie_itens_scroll_menu, parent, false);
        AdapterHorizontalListCategoryItens.LineSimpleItemHolder lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LineSimpleItemHolder holder, int position) {
        try {
            holder.txt_nome_categ.setText(categoriaListObjects.getJSONObject(position).getString("nome"));
           if( position == mPosition){
               holder.view.setVisibility(View.VISIBLE);
               holder.txt_nome_categ.setTypeface(null, Typeface.BOLD);
               holder.txt_nome_categ.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 15);
           }
           else{
                holder.view.setVisibility(View.GONE);
                holder.txt_nome_categ.setTypeface(null, Typeface.NORMAL);
                holder.txt_nome_categ.setTextColor(context.getResources().getColor(R.color.color));
                holder.txt_nome_categ.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return categoriaListObjects.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

            public TextView txt_nome_categ;
            public View view;
            public int id;

            public LineSimpleItemHolder(View itemView) {
                super(itemView);
                txt_nome_categ = itemView.findViewById(R.id.textMenuRecyclerCategories);
                view = itemView.findViewById(R.id.viewMenuCategorieRec);
                view.setVisibility(View.GONE);
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mPosition = getAdapterPosition();
                        int position = getAdapterPosition();
                        if (listener != null && position != RecyclerView.NO_POSITION) {
                            try {
                                notifyDataSetChanged();
                                listener.onItemClick(categoriaListObjects.getJSONObject(position));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        }

   public interface onItemClickListener{
       void onItemClick(JSONObject categoria);
   }

    public void setListener(AdapterHorizontalListCategoryItens.onItemClickListener listener) {
        this.listener = listener;
    }


    public void setSelectItem(int idCategory) {

        List<Categoria> categorias = categoriaList;
        int posCat = 0;
        try {
            posCat = getPositionCategoty(idCategory);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        categorias.get(posCat).setLogo("ok");

        for (int j = 0; j < categorias.size(); j++) {
            if (j != posCat) {
                categorias.get(j).setLogo(null);
            }
        }
    }


    private int getPositionCategoty(int cdCategory) throws JSONException {
        int pos = 0;
        for(int i=0; i<categoriaListObjects.length(); i++) {
            String val = categoriaListObjects.getJSONObject(i).getString("_id");
            if (cdCategory == Integer.parseInt(val)) {
                pos = i;
            }
        }
        return  pos;
    }

}

