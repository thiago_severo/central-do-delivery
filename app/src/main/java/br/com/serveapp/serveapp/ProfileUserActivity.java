package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceUpdateRegister;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.MaskEditUtil;
import br.com.serveapp.serveapp.util.ValidationUtilField;
import br.com.serveapp.serveapp.R;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import org.json.JSONException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileUserActivity extends AppCompatActivity {


    TextInputEditText text_edit_nome;
    TextInputEditText text_cd_sobre_nome;
    TextInputEditText text_email_edit;
    TextInputEditText text_phone_edit;
    TextInputLayout textInputLayoutEditPhone;
    TextInputEditText text_cpf;
    Request requestUpdateUser;
    TextInputLayout textInputLayoutName;
    TextInputLayout textInputLayoutLastName;
    CardView cardConfirmDataUser;
    public ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if(Repository.getUser(getApplicationContext())==null){
            finish();
        }

        setContentView(R.layout.activity_profile_user);

        progressBar = findViewById(R.id.progressBar5);
        cardConfirmDataUser = findViewById(R.id.cardConfirmDataUser);
        text_edit_nome = findViewById(R.id.text_edit_nome);
        text_cd_sobre_nome = findViewById(R.id.text_cd_sobre_nome);
        text_email_edit = findViewById(R.id.text_email_edit);
        text_phone_edit = findViewById(R.id.text_phone_edit);
        text_cpf = findViewById(R.id.text_cpf_edit);
        textInputLayoutName = findViewById(R.id.textInputLayoutName);
        textInputLayoutLastName = findViewById(R.id.textInputLayoutSobreNome);
        textInputLayoutEditPhone = findViewById(R.id.textInputLayoutEditPhone);

        text_email_edit.setEnabled(false);
        progressBar.setVisibility(View.GONE);

        text_phone_edit.addTextChangedListener(MaskEditUtil.mask(text_phone_edit, MaskEditUtil.FORMAT_FONE, getApplicationContext()));
        text_phone_edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePhone();
                textInputLayoutEditPhone.setError(null);
            }
        });

        try {

            if(Repository.getUser(getApplicationContext()).getString("sobrenome").length()<1) { text_cd_sobre_nome.setText(" "); }
            else{ text_cd_sobre_nome.setText(Repository.getUser(getApplicationContext()).getString("sobrenome"));}

            text_edit_nome.setText(Repository.getUser(getApplicationContext()).getString("nome"));
            text_email_edit.setText(Repository.getUser(getApplicationContext()).getString("email"));
            text_phone_edit.setText(ifNull(Repository.getUser(getApplicationContext()).getString("telefone"),""));
            text_cpf.setText(ifNull(Repository.getUser(getApplicationContext()).getString("cpf"),""));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        requestUpdateUser = new Request(getApplicationContext(), new ServiceUpdateRegister(this));

        cardConfirmDataUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> param = new HashMap<>();

               if( validateName() && validateLastName() && validatePhone()) {

                   param.put("id", Repository.getIdUsuario(getApplicationContext()));
                   param.put("nome", text_edit_nome.getText().toString());
                   param.put("sobrenome", text_cd_sobre_nome.getText().toString());
                   param.put("telefone", text_phone_edit.getText().toString());
                   param.put("cpf", text_cpf.getText().toString());
                   progressBar.setVisibility(View.VISIBLE);
                   requestUpdateUser.request(param);

               }
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    String usernameInput;

    private String ifNull(String text,String s){
        if(text!=null && !text.equals("null"))
            return text;
        else
            return s;
    }

    private boolean validatePhone() {

        String usernameInput = text_phone_edit.getText().toString().trim();
        if (text_phone_edit.getText().toString().isEmpty()){
            textInputLayoutEditPhone.setError("O campo não pode ficar vazio");
            return false;
        }else if (text_phone_edit.getText().toString().trim().length() < 13) {
            textInputLayoutEditPhone.setError("número de telefone inválido");
            return false;
        }
        else if (text_phone_edit.getText().toString().trim().length() > 14) {
            textInputLayoutEditPhone.setError("número de telefone muito longo");
            return false;
        }
        else {
            textInputLayoutEditPhone.setError(null);
            return true;
        }

    }


    private boolean validateName() {
        usernameInput   = text_edit_nome.getText().toString().trim();
        usernameInput = usernameInput.replace(" ", "");

        if (usernameInput.isEmpty()) {
            textInputLayoutName.setError("O campo não pode ficar vazio");
            return false;
        }else if (ValidationUtilField.isValidName(usernameInput)==false) {
            textInputLayoutName.setError("Nome inválido, insira apenas letras");
            return false;
        } else {
            textInputLayoutName.setError(null);
            return true;
        }

    }

    private boolean validateLastName() {
        usernameInput = text_cd_sobre_nome.getText().toString().trim();
        usernameInput = usernameInput.replace(" ", "");

        if (usernameInput.isEmpty()) {
            textInputLayoutLastName.setError("O campo não pode ficar vazio");
            return false;
        }else if (ValidationUtilField.isValidName(usernameInput)==false) {
            textInputLayoutLastName.setError("Nome inválido, insira apenas letras");
            return false;
        } else {
            textInputLayoutLastName.setError(null);
            return true;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.top:
                //action
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void share(){
        String urlToShare = "https://stackoverflow.com/questions/7545254";
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
// intent.putExtra(Intent.EXTRA_SUBJECT, "Foo bar"); // NB: has no effect!
        intent.putExtra(Intent.EXTRA_TEXT, urlToShare);

// See if official Facebook app is found
        boolean facebookAppFound = false;
        List<ResolveInfo> matches = getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.facebook.katana")) {
                intent.setPackage(info.activityInfo.packageName);
                facebookAppFound = true;
                break;
            }
        }

// As fallback, launch sharer.php in a browser
        if (!facebookAppFound) {
            String sharerUrl = "https://www.facebook.com/sharer/sharer.php?u=" + urlToShare;
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharerUrl));
        }
        startActivity(intent);
    }

}
