package br.com.serveapp.serveapp.fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import br.com.serveapp.serveapp.fragments.ifood.Ifood;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.LineProductsForEstabelecimentsAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceCheckIn;
import br.com.serveapp.serveapp.repository.objects.Repository;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class FragmentReadQrCode extends Fragment implements ZXingScannerView.ResultHandler {
    View view;
    private LineProductsForEstabelecimentsAdapter lineAdapter;
    public static int cdCategory;
    public Ifood ifood;
    public static JSONArray listRecyclerHorizontalCategories;
    public ZXingScannerView scannerView;
    ListProductsStablishmentDeliveryActivity activity;
    Button insertCodeMesa;
    public AlertDialog dialog;
    Request requestCheckIn;
    ServiceCheckIn serviceCheckIn;

    ImageView imageFlash;
    boolean flash;


    public FragmentReadQrCode(ListProductsStablishmentDeliveryActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_read_qr_code, container, false);

        scannerView = view.findViewById(R.id.z_xing_scannerr);
        List list = new ArrayList<BarcodeFormat>();
        list.add(BarcodeFormat.QR_CODE);
        scannerView.setFormats(list);
        scannerView.setAutoFocus(true);

        imageFlash = view.findViewById(R.id.imageFlash3);
        insertCodeMesa = view.findViewById(R.id.insertCodeMesa2);
        requestCheckIn = new Request(activity.getApplicationContext(), new ServiceCheckIn(this, activity));

        insertCodeMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        imageFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flash == false) {
                    flash = true;
                    scannerView.setFlash(true);
                    imageFlash.setImageResource(R.drawable.ic_serve_flash_on);
                } else if (flash == true) {
                    flash = false;
                    scannerView.setFlash(false);
                    imageFlash.setImageResource(R.drawable.ic_serve_flash_off);
                }

            }

        });

        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            scannerView.setResultHandler(this);
            scannerView.startCamera();
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, 1024);
        }

    }

    public void resumeReadQrCode() {

    }

    @Override
    public void handleResult(Result result) {

        String titleId = "Entrando..";
        activity.mProgress.setTitle(titleId);
        activity.mProgress.setMessage("verificando conexão com a internet...");
        activity.mProgress.show();

        String id_cliente = Repository.getIdUsuario(activity);
        final Map<String, String> param = new HashMap<>();
        param.put("function_method", "check-in");
        param.put("id_mesa", result.getText().toString());
        param.put("instructionId", "check-in");
        param.put("id_cliente", id_cliente);
        requestCheckIn.request(param);
        // ---------------------------------------

    }


    public void showDialog() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
        View mView = getLayoutInflater().inflate(R.layout.dialog_input_code_mesa, null);

        Button btnCancelCodeMesa = mView.findViewById(R.id.btnCancelCodeMesa);
        Button btnConfirmCodeMesa = mView.findViewById(R.id.btnConfirmCodeMesa);
        final EditText inputCodeMesa = mView.findViewById(R.id.inputCodeMesa);

        mBuilder.setView(mView);
        dialog = mBuilder.create();
        dialog.show();

        btnCancelCodeMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirmCodeMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                activity.mProgress.show();
                String titleId = "Entrando..";
                activity.mProgress.setTitle(titleId);
                activity.mProgress.setMessage("verificando conexão com a internet...");

                String id_cliente = null;
                id_cliente = Repository.getIdUsuario(activity);
                final Map<String, String> param = new HashMap<>();
                param.put("function_method", "check-in-cod");
                param.put("id_mesa", inputCodeMesa.getText().toString());
                param.put("id_cliente", id_cliente);
                requestCheckIn.request(param);

            }
        });

        dialog.show();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {
            activity.cardFab.setVisibility(View.GONE);
        } else {
            activity.cardFab.setVisibility(View.VISIBLE);
        }

    }
}