package br.com.serveapp.serveapp.fragments.ifood;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.synnapps.carouselview.CarouselView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.CarouselAdapter;
import br.com.serveapp.serveapp.dao.web.Script;

public class LineVerticalProduct extends RecyclerView.ViewHolder implements View.OnClickListener{

    private TextView txt_nome_item;
    private TextView txt_value_item;
    private ImageView imageItemProducts;
    private TextView txt_descricao;
    private TextView txt_medida;
    private RatingBar avaliacao;
    public ImageView imageCupomDesconto;
    public TextView txt_perc_descont;
    public TextView tempPreparo;
    public TextView txtParcela;
    public ProgressBar progressImage;

    JSONObject item;
    LinearLayoutManager layoutManager;
    Fragment fragment;
    Activity activity;

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setLayoutManager(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    private void clean(View itemView){
        itemView.findViewById(R.id.btn_accrease_item_demand).setVisibility(View.GONE);
        itemView.findViewById(R.id.btn_decrease_item).setVisibility(View.GONE);
        itemView.findViewById(R.id.textQtdItemDemandFinalize).setVisibility(View.GONE);
        itemView.findViewById(R.id.optionsText).setVisibility(View.GONE);
        itemView.findViewById(R.id.imageIsBrinde).setVisibility(View.GONE);
    }

    public LineVerticalProduct(View itemView) {
        super(itemView);
        clean(itemView);
        imageItemProducts = itemView.findViewById(R.id.imageItemProducts);
        txt_nome_item = itemView.findViewById(R.id.nomeItemCardapio);
        txt_value_item = itemView.findViewById(R.id.txtValorItemMenu);
        txt_descricao = itemView.findViewById(R.id.textViewDesc);
        txt_medida = itemView.findViewById(R.id.textMedida);
        avaliacao = itemView.findViewById(R.id.ratingBarLikedListProduct);
        txt_perc_descont = itemView.findViewById(R.id.textCupomDescont2);
        imageCupomDesconto = itemView.findViewById(R.id.imageCupomDescont3);
        tempPreparo = itemView.findViewById(R.id.tempPreparo);
        txtParcela = itemView.findViewById(R.id.txtParcela);
        progressImage = itemView.findViewById(R.id.progressImage);
        progressImage.setVisibility(View.VISIBLE);
        //tempPreparo.setVisibility(View.GONE);

        ImageView imageEstabeleciment = itemView.findViewById(R.id.imageEstabeleciment);
        imageEstabeleciment.setVisibility(View.GONE);
        add_expand_image();
        itemView.setOnClickListener(this);
    }

    private void add_expand_image(){

        imageItemProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(fragment.getContext());
                View mView = ((Activity)fragment.getContext()).getLayoutInflater().inflate(R.layout.detail_product, null);
                final CarouselView carouselView = (CarouselView) mView.findViewById(R.id.carouselView);

                final TextView textViewAddressEvent = mView.findViewById(R.id.textViewAddressEvent);
                final ImageButton imageButtonCloseEvent = mView.findViewById(R.id.imageButtonCloseEvent);
                final Button buttonOkEvent = mView.findViewById(R.id.buttonOkEvent);

                try{
                    JSONArray banners = new JSONArray(item.getString("fotos"));
                    carouselView.setPageCount(banners.length());
                    CarouselAdapter adapter = new CarouselAdapter(fragment.getActivity(), banners);
                    carouselView.setViewListener(adapter);
                }catch (Exception e){

                }

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                imageButtonCloseEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                buttonOkEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
            }
        });

    }

    public void initialize(final JSONObject item) {
        this.item = item;
        try {

            txt_perc_descont.setText(item.getString("desconto"));
            if(item.isNull("desconto") ||
                    item.getString("desconto").equals("null")){
                imageCupomDesconto.setVisibility(View.GONE);
            }
            else {
                imageCupomDesconto.setVisibility(View.VISIBLE);
            }

            tempPreparo.setText(item.getString("tempPreparo"));
            txt_nome_item.setText(item.getString("nome"));
            txt_value_item.setText(item.getString("valor"));
            txt_descricao.setText(item.getString("descricao"));
            txt_medida.setText(item.getString("medida"));
            txtParcela.setText(item.getString("parcela"));
            avaliacao.setRating(Float.parseFloat(((String) item.getString("avaliacao") + "").substring(0, 1)));
            //new ListSetImage(itemView.getContext(), imageItemProducts).execute(item.getString("foto"));
            String imageURL = Script.image+item.getString("foto");
            Picasso.get().load(imageURL).fit()
                    .centerInside()
                    .into(imageItemProducts, new Callback() {

                        @Override
                        public void onSuccess() {
                            (imageItemProducts).setAlpha(0f);
                            (imageItemProducts).animate().setDuration(500).alpha(1f).start();
                            progressImage.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                        }
                    });
            ;
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onClick(View view) {


        /*WindowManager.LayoutParams layoutParams = activity.getWindow().getAttributes();
        layoutParams.dimAmount = 0.75f;
        activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        activity.getWindow().setAttributes(layoutParams);*/

        Intent intent = new Intent(view.getContext(), DetalheItemPopUpActivity.class);
        try {
            intent.putExtra("foto", item.getString("foto"));
            intent.putExtra("value", item.getDouble("value"));
            intent.putExtra("valor", item.getString("valor"));
            //intent.putExtra("nome", item.getString("nome"));
            String name = item.getString("cat_name")+"-"+item.getString("nome");
            intent.putExtra("nome", name);
            intent.putExtra("descricao", item.getString("descricao"));
            intent.putExtra("id_produto", item.getString("_id"));
            intent.putExtra("tempoPreparo",  item.getString("tempPreparo"));
            intent.putExtra("avaliacao",  item.getString("avaliacao"));
            if(item.has("fotos")){
                intent.putExtra("fotos",  item.getString("fotos"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        //fragment.startActivityForResult(intent, 1);

        new DetalheItemPopUpActivity(activity,intent).show();

        //fragment.startActivity(intent);


    }
}
