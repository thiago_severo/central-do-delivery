package br.com.serveapp.serveapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.android.volley.VolleyError;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.LinePaymentFormListAdapterView;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.dao.webService.ServiceCompleteEnd;
import br.com.serveapp.serveapp.dao.webService.ServiceLoadDealhesPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceRealizarPedido;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.DownloadDadosPOST;
import okhttp3.MediaType;
import okhttp3.RequestBody;

public class ConfirmDataDemandActivity extends RequestActivity {

    TextView textEnderecoEntrega;
    TextView paymentFormConfirm;
    ImageView btEditAddress;
    ImageView editPAymentForm;
    JSONObject alreadyAddress;
    public ProgressDialog mProgress;
    ProgressBar progressBar9;

    AlertDialog dialogEnd;
    EditText edtCliente;
    TextInputEditText edtReferencia;
    ProgressBar progressBar;
    Button btnConfirmEnd;

    String idEndSelected;
    String descEndSelected;
    String valueCart;
    String cepSelected = null;
    ImageView icon_pagamento2;
    int isLocal;
    boolean needMoney = false;
    public CardView confirmPedido;
    public CardView cardSelectDebit;

    String typeCard = "CreditCard";
    Request requestRelizarPedido;
    String bandeira, cvv, idCardCredit, token, nameHolder;


    public void responseCompleteEnd() {
        progressBar.setVisibility(View.GONE);
        confirmarPedido();
        dialogEnd.dismiss();
    }


    public void errorCompleteEnd() {
        progressBar.setVisibility(View.GONE);
        btnConfirmEnd.setEnabled(true);
        Toast.makeText(this, "Erro de conexão", Toast.LENGTH_LONG).show();
    }

    RadioButton cartaoDebito;
    RadioButton cartaoCredito;
    boolean isPaymentOnline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_demand);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressBar9 = findViewById(R.id.progressBar9);
        textEnderecoEntrega = findViewById(R.id.textEndereco);
        paymentFormConfirm = findViewById(R.id.paymentFormConfirm);
        btEditAddress = findViewById(R.id.btEditAddress);
        editPAymentForm = findViewById(R.id.editPAymentForm);
        icon_pagamento2 = findViewById(R.id.icon_pagamento2);
        cardSelectDebit = findViewById(R.id.cardSelectDebit);

        mProgress = new ProgressDialog(this);
        String titleId = "Aguarde";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Estamos enviando o seu pedido...");

        idEndSelected = getIntent().getStringExtra("idEndereco");
        descEndSelected = getIntent().getStringExtra("endereco");
        valueCart = getIntent().getStringExtra("valueToPay");
        textEnderecoEntrega.setText(descEndSelected);
        paymentFormConfirm.setText(getIntent().getStringExtra("formaPagamento"));
        isLocal = getIntent().getIntExtra("isLocal", 0);

        if(getIntent().hasExtra("isPaymentOnline")) {

            isPaymentOnline = getIntent().getBooleanExtra("isPaymentOnline", false);
            cvv = getIntent().getStringExtra("cvv");
            bandeira = getIntent().getStringExtra("bandeira");
            idCardCredit = getIntent().getStringExtra("idCardCredit");
            token = getIntent().getStringExtra("token");
            nameHolder = getIntent().getStringExtra("nameHolder");

            //Toast.makeText(getApplicationContext(), "cvv  "+ cvv, Toast.LENGTH_LONG).show();

        }

        if(getIntent().getBooleanExtra("isMultipleCard", false) == false){
            cardSelectDebit.setVisibility(View.GONE);
        }

        if (!getIntent().getStringExtra("formaPagamento").contains("dinheiro")) {
            icon_pagamento2.setImageResource(0);
            icon_pagamento2.setImageBitmap(null);
            new ListSetImage(getApplicationContext(), icon_pagamento2).execute(LinePaymentFormListAdapterView.iconSelected);
        } else {
            icon_pagamento2.setImageResource(0);
            icon_pagamento2.setImageBitmap(null);
            new ListSetImage(getApplicationContext(), icon_pagamento2).execute("card/money_icon.png");
        }

        cartaoCredito = findViewById(R.id.cartaoCredito);
        cartaoDebito = findViewById(R.id.cartaoDebito);
        cartaoCredito.setChecked(true);

        requestRelizarPedido = new Request(getApplicationContext(), new ServiceRealizarPedido(null, this));
        confirmPedido = findViewById(R.id.confirmPedido);
        progressBar9.setVisibility(View.GONE);

        confirmPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().hasExtra("revise_dados") && getIntent().getStringExtra("revise_dados").equals("true")) {
                    confirmarDados();
                } else {
                    confirmarPedido();
                }
            }
        });

        editPAymentForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FormaPagamentoActivity.class);
                startActivityForResult(intent, 66);
            }
        });

        btEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent openAdddress = new Intent(getApplicationContext(), ChangeAddressPopUpActivity.class);
                openAdddress.putExtra("id_endereco", idEndSelected);
                openAdddress.putExtra("text_endereco", descEndSelected);
                openAdddress.putExtra("updateCart", true);
                openAdddress.putExtra("openByCart", "ok");
                startActivityForResult(openAdddress, 77);

                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);

            }
        });

    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(getApplicationContext(), ConfirmItensPedidoActiviy.class);
        intent.putExtra("titulo", descEndSelected);
        intent.putExtra("_id", idEndSelected);
        if (cepSelected != null) {
            intent.putExtra("cep", cepSelected);
        }

        setResult(Activity.RESULT_OK, intent);

        super.onBackPressed();
        overridePendingTransition(R.anim.entrer_from_leftt,
                R.anim.exit_to_right);

    }

    public void confirmarDados() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ConfirmDataDemandActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.complemento_dados, null);

        edtCliente = mView.findViewById(R.id.edtCliente);
        edtReferencia = mView.findViewById(R.id.edtReferencia);
        btnConfirmEnd = mView.findViewById(R.id.btnConfirm);
        progressBar = mView.findViewById(R.id.progressBar);

        progressBar.setVisibility(View.GONE);
        edtCliente.setText(getIntent().getStringExtra("cliente"));
        edtReferencia.setText(getIntent().getStringExtra("reference"));

        final Request request = new Request(ConfirmDataDemandActivity.this, new ServiceCompleteEnd(ConfirmDataDemandActivity.this));
        mBuilder.setView(mView);
        dialogEnd = mBuilder.create();
        dialogEnd.show();

        btnConfirmEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateField(edtCliente, 3, "Nome curto") &&
                        validateField(edtReferencia, 10, "Referencia muito curta")) {
                    progressBar.setVisibility(View.VISIBLE);
                    btnConfirmEnd.setEnabled(false);
                    final Map<String, String> param = new HashMap<>();
                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    param.put("id_estabelecimento", Repository.idEstabelecimento);
                    param.put("nome", edtCliente.getText().toString());
                    param.put("reference", edtReferencia.getText().toString());
                    request.request(param);
                }
            }
        });
    }

    private boolean validateField(EditText editText, int minLength, String msg) {
        String str = editText.getText().toString().trim();
        if (editText.length() < minLength) {
            editText.setError(msg);
            return false;
        } else {
            editText.setError(null);
            return true;
        }
    }

    public void confirmarPedido() {
        boolean troco = LinePaymentFormListAdapterView.id == 0;



        if(true) {
            if (isPaymentOnline) {

                Double val = (getIntent().getDoubleExtra("valorCompra", 0) * 100);
                Integer valStr = val.intValue();
                //Integer valStr = val.intValue() * 100;
                MediaType mediaType = MediaType.parse("application/json");

/*            RequestBody body = RequestBody.create(mediaType, "{  \r\n   \"MerchantOrderId\":\"2014111706\",\r\n   " +
                    "\"Customer\":{  \r\n      " +
                    "\"Name\":\"Romeryto V Nogueira\"\r\n   " + "},\r\n   " +
                    "\"Payment\":{  \r\n     " +
                    "\"Type\":\"CreditCard\",\r\n     " +
                    "\"Amount\":1,\r\n     " +
                    "\"Installments\":1,\r\n     " +
                    "\"SoftDescriptor\":\"123456789ABCD\",\r\n     " +
                    "\"CreditCard\":{  \r\n         " +
                    "\"CardToken    \":\"" + token  +   "\",\n" +
                    "\"SecurityCode \": \""+    cvv +   "\",\n" +
                    "\"Brand\"        :  \""+  bandeira     +"\"}\n " +
                    "}\r\n   " +
                    "}\r\n" +
                    "}");*/

/*                RequestBody body = RequestBody.create(mediaType, "{  \r\n   \"MerchantOrderId\":\"2014111706\",\r\n   " +
                        "\"Customer\":{  \r\n      " +
                        "\"Name        \" : \"" + nameHolder + "\"\r\n   " + "},\r\n   " +
                        "\"Payment\":{  \r\n     " +
                        "\"Type\":\"CreditCard\",\r\n     " +
                        "\"Amount\":"+valStr+",\r\n     " +
                        "\"Installments\":1,\r\n     " +
                        "\"SoftDescriptor\":\" \",\r\n     " +
                        "\"CreditCard\":{  \r\n         " +
                        "\"CardToken\":\"" + token + "\",\r\n " +
                        "\"SecurityCode \": \"" + cvv + "\",\n" +
                        "\"Brand\"        :  \"" + bandeira + "\"}\n " +
                        "}\r\n   " +
                        "}\r\n" +
                        "}");
*/

                //  6fb7a669aca457a9e43009b3d66baef8bdefb49aa85434a5adb906d3f920bfeA
                //  Toast.makeText(this, "cvv: "+ cvv , Toast.LENGTH_SHORT).show();


/*                DownloadDadosPOST downloadDadosPOST = new DownloadDadosPOST(body);
                downloadDadosPOST.searchDataCard = "https://api.cieloecommerce.cielo.com.br/1/sales/";
*/

                //     downloadDadosPOST.searchDataCard = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/";

                        runOnUiThread(new Runnable() {
                            public void run() {

                                   //Toast.makeText(getApplicationContext(), "response " + response, Toast.LENGTH_LONG).show();

                                    Map<String, String> paramUpdate = new HashMap<>();
                                    paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                                    paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);

                                    if (isPaymentOnline) {
                                        paramUpdate.put("is_payment_online", "1");
                                        paramUpdate.put("forma_pagamento", idCardCredit);
                                    }


                                requestRelizarPedido.request(paramUpdate);
                                        mProgress.show();

                                }

                        });

            } else {
                if (troco == true && !idEndSelected.equals("0")) {
                    showNeedMoney();
                } else {
                    Map<String, String> paramUpdate = new HashMap<>();
                    paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);
                    paramUpdate.put("forma_pagamento", LinePaymentFormListAdapterView.idSaved + "");
                 //   paramUpdate.put("valor_total", valueCart);

                    requestRelizarPedido.request(paramUpdate);
                    mProgress.show();
                    // confirmPedido.setEnabled(false);
                }

            }

        }else{

            showMsg("Estabelecimento Fechadoooo!", "O pedido não pode ser realizado no momento.");

        }

    }

    public void showMsg(String titulo, String mensagem) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView textView = mView.findViewById(R.id.textMainMsg);
        TextView textViewSucess = mView.findViewById(R.id.textSucessMessage);

        textView.setText(titulo);
        textViewSucess.setText(mensagem);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Intent intent = new Intent(getApplicationContext(), FormaPagamentoActivity.class);
                startActivityForResult(intent, 66);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 77) {

            JSONObject jsonObject = null;
            try {

                jsonObject = new JSONObject(data.getStringExtra("endereco"));
                idEndSelected = jsonObject.getString("_id");
                cepSelected = jsonObject.getString("cep");
                if (idEndSelected.equals("0")) {
                    //  textEnderecoEntrega.setText("idEndSelected " + idEndSelected + "  " + descEndSelected);
                } else {
                    //    descEndSelected = jsonObject.getString("desc");
                    //     textEnderecoEntrega.setText(descEndSelected + " - " + jsonObject.getString("cep"));
                }

                Map<String, String> paramUpdate = new HashMap<>();
                paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                paramUpdate.put("id_endereco", idEndSelected);
                paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);
                paramUpdate.put("function_method", "set_end");
                request(Script.update_address_cart, paramUpdate);
                progressBar9.setVisibility(View.VISIBLE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (requestCode == 66) {

            if (resultCode == RESULT_CANCELED) {

            }
            else if(resultCode==11){

                isPaymentOnline = true;

                nameHolder = data.getStringExtra("nameHolder");
                cvv = data.getStringExtra("cvv");
                bandeira = data.getStringExtra("bandeira");
                token = data.getStringExtra("token");
                idCardCredit = data.getStringExtra("idCardCredit");
                paymentFormConfirm.setText(valueCart + "\nPagamento pelo app com: " + " " + bandeira.toUpperCase() + " **** " + data.getStringExtra("card"));
                new ListSetImage(getApplicationContext(), icon_pagamento2).execute("card/" + data.getStringExtra("bandeira").replace(" ", "") + ".png");
                LinePaymentFormListAdapterView.iconSelected = "card/" + data.getStringExtra("bandeira").replace(" ", "") + ".png";

            }
            else {

                if (Repository.tipoPagamentoSelecionado != null && !Repository.tipoPagamentoSelecionado.equals("dinheiro")) {

                    isPaymentOnline = false;
                    paymentFormConfirm.setText(valueCart + " [" + Repository.tipoPagamentoSelecionado + "]" + " - Pagamento na entrega");
                    icon_pagamento2.setImageResource(0);
                    icon_pagamento2.setImageBitmap(null);
                    new ListSetImage(getApplicationContext(), icon_pagamento2).execute(data.getStringExtra("icon"));
                    LinePaymentFormListAdapterView.iconSelected = data.getStringExtra("icon");

                } else {
                    if (LinePaymentFormListAdapterView.idSaved == 0) {

                        isPaymentOnline = false;
                        paymentFormConfirm.setText(valueCart + " " + "[dinheiro]" + " - Pagamento na entrega");
                        icon_pagamento2.setImageResource(0);
                        icon_pagamento2.setImageBitmap(null);
                        new ListSetImage(getApplicationContext(), icon_pagamento2).execute("card/money_icon.png");

                    }
                }

            }

        }

        //Toast.makeText(getApplicationContext(), "isPaymentOnline  " + isPaymentOnline, Toast.LENGTH_LONG).show();

    }


    public void showNeedMoney() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);

        View mView = getLayoutInflater().inflate(R.layout.need_money_to_pay, null);
        final TextView textTotal = mView.findViewById(R.id.totalPriceMoney);
        final EditText needValue = mView.findViewById(R.id.needValue);
        final CheckBox checkBoxNotNeed = mView.findViewById(R.id.checkBoxNotNeed);
        Button btnCancelMoney = mView.findViewById(R.id.btnRecuseCancel);
        Button btnFinishMoney = mView.findViewById(R.id.btnConfirmCancel);
        textTotal.setText(valueCart);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        //mProgress.dismiss();
        dialog.show();

        btnCancelMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnFinishMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Double valorTotal = Double.parseDouble(textTotal.getText().toString().replace("R$", "").replace(",", "."));
                Double valorQuePreciso;
                String need = needValue.getText().toString();
                if (need == null || need.equals("")) {
                    valorQuePreciso = 0.0;
                } else {
                    valorQuePreciso = Double.parseDouble(needValue.getText().toString());
                }

                if (valorQuePreciso > (valorTotal) || checkBoxNotNeed.isChecked()) {

                    Map<String, String> param = new HashMap<>();
                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    param.put("id_estabelecimento", Repository.idEstabelecimento);
                    param.put("forma_pagamento", "0");
               //     param.put("valor_total", getIntent().getStringExtra("valorCompra"));
                    param.put("troco", needValue.getText().toString());

                   /* if (idEndSelected.equals("0") && isLocal == 1) {
                        requestRelizarPedido.request(param);
                        mProgress.show();
                        confirmPedido.setEnabled(false);
                    } else {
                        confirmDemand(param);
                    }*/

                    requestRelizarPedido.request(param);
                    mProgress.show();
                    dialog.dismiss();

                } else {

                    if (need == null || need.equals("")) {
                        Toast.makeText(getApplicationContext(), "Insira o valor!", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "O valor inserido deve ser maior que o total do carrinho!", Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        checkBoxNotNeed.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                needMoney = b;
            }
        });

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        progressBar9.setVisibility(View.GONE);
        Toast.makeText(this, "erro ao atualizar!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponseServe(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONObject result = jsonObject.getJSONObject("result");
            descEndSelected = result.getJSONObject("end").getString("desc");

            if(descEndSelected==null || descEndSelected.equals("null")){
                descEndSelected = result.getJSONObject("end").getString("titulo");
            }

            textEnderecoEntrega.setText(descEndSelected);
            valueCart = result.getJSONObject("cart").getJSONObject("total").getString("label");

            //   Toast.makeText(getApplicationContext(), "tipoPagamentoSelecionado "+ Repository.tipoPagamentoSelecionado, Toast.LENGTH_LONG).show();
            new ListSetImage(getApplicationContext(), icon_pagamento2).execute("card/" + Repository.tipoPagamentoSelecionado + ".png");
            paymentFormConfirm.setText(valueCart + " [" + Repository.tipoPagamentoSelecionado + "]");
            progressBar9.setVisibility(View.GONE);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onCache(String response) {

    }

    public void receiveSeucessResponsePedido(String idPedido) {

        DetailDemand.idPedido = idPedido;

        Request requestDetailPedido = new Request(getApplicationContext(), new ServiceLoadDealhesPedido(this, idPedido));
        requestDetailPedido.request();

    }

    public void stabelecimentoClosed(String textMain) {

        mProgress.dismiss();

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ConfirmDataDemandActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView text = mView.findViewById(R.id.textSucessMessage);
        TextView textViewMain = mView.findViewById(R.id.textMainMsg);

        text.setVisibility(View.GONE);
        textViewMain.setText(textMain);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        confirmPedido.setEnabled(true);

    }


}