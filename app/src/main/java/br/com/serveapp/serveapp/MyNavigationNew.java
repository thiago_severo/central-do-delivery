package br.com.serveapp.serveapp;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import br.com.serveapp.serveapp.repository.objects.Repository;

public class MyNavigationNew{

    final static int cor = Color.rgb(200, 0, 0);

    public static void setButtonsAction(final Activity c){
        final View menuHome = c.findViewById(R.id.menuHome);
        final View menuFav = c.findViewById(R.id.menuFav);
        final View menuCart = c.findViewById(R.id.menuCart);
        final View menuEvent = c.findViewById(R.id.menuEvent);
        final View menuProduto = c.findViewById(R.id.menuProduto);

        TextView badge = c.findViewById(R.id.badge);

        if(Repository.getQtdCart()>0) {
            badge.setText(Repository.getQtdCart()+"");
            badge.setVisibility(View.VISIBLE);
        }else{
            badge.setText("");
            badge.setVisibility(View.GONE);
        }

        //cor = R.color.colorDark;
        if(c.getClass() == NavigationDrawerActivity.class){
            ((ImageButton)menuHome).setColorFilter(cor); // White Tint
        }else if(c.getClass() == FavoritsActivity.class){
            ((ImageButton)menuFav).setColorFilter(cor); // White Tint
        }else if(c.getClass() == ConfirmItensPedidoActiviy.class){
            badge.setText("");
            badge.setVisibility(View.GONE);
            ((ImageButton)menuCart).setColorFilter(cor); // White Tint
        }else if(c.getClass() == AtracoesActivity.class){
            ((ImageButton)menuEvent).setColorFilter(cor); // White Tint
        }else if(c.getClass() == ListAllProducts.class){
            ((ImageButton)menuProduto).setColorFilter(cor); // White Tint
        }

        menuHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(NavigationDrawerActivity.class, menuHome, c);
            }
        });

        menuFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(FavoritsActivity.class, menuFav, c);
            }
        });

        menuCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ConfirmItensPedidoActiviy.class, menuCart, c);
            }
        });

        menuEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(AtracoesActivity.class, menuEvent, c);
            }
        });

        menuProduto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ListAllProducts.class, menuProduto, c);
            }
        });
    }

    private static void startActivity(Class<?> activityClass, View view, Activity context){
        ((ImageButton)view).setColorFilter(cor); // White Tint
        Repository.idEstabelecimento = "0";
        if(context.getClass() != activityClass){
            context.startActivity(new Intent(context.getApplicationContext(), activityClass));
            context.overridePendingTransition(0, 0);
            context.finish();
        }
    }
}
