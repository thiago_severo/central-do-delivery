package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class AdapterHorizontalCategories extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public ArrayList<String> categoriaList = new ArrayList<>();
    public JSONArray categorias;
    Context context;
    private ListenerClickCategories listenerClickCategories;

    public void setListenerClickCategories(ListenerClickCategories listenerClickCategories) {
        this.listenerClickCategories = listenerClickCategories;
    }

    private AdapterHorizontalCategories.onItemClickListener listener;
    int mPosition;

    final static int TypePercent = 1;
    final static int TypeItem = 2;

    public AdapterHorizontalCategories(Context context, JSONArray categorias) {
        this.categorias = categorias;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder lineSimpleItemHolder = null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_categorie_estab, parent, false);
        lineSimpleItemHolder = new LineSimpleCategorie(v);

        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        try {
            JSONObject cat = categorias.getJSONObject(position);
            ((LineSimpleCategorie) holder).nameCategorie.setText(cat.getString("nome"));
            ((LineSimpleCategorie) holder).id = cat.getString("_id");

            ImageView img = ((LineSimpleCategorie) holder).imagemDestaqueItemListHome;
            ProgressBar progress = ((LineSimpleCategorie) holder).progressImage;
            new ListSetImage(holder.itemView.getContext(), img).execute(cat.getString("foto"), progress);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return categorias.length();
    }

    class LineSimpleCategorie extends RecyclerView.ViewHolder {

        public String id;
        public TextView nameCategorie;
        public ImageView imagemDestaqueItemListHome;
        public ProgressBar progressImage;
        public CardView cardViewInfoEstabeleciment;

        public LineSimpleCategorie(View itemView) {
            super(itemView);

            imagemDestaqueItemListHome = itemView.findViewById(R.id.image);
            nameCategorie = itemView.findViewById(R.id.textNamDestaqueHome);
            progressImage = itemView.findViewById(R.id.progressImage);

            progressImage.setVisibility(View.VISIBLE);

            //cardViewInfoEstabeleciment = itemView.findViewById(R.id.cardViewInfoEstabeleciment);
            //cardViewInfoEstabeleciment.setRadius(15);
            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            listenerClickCategories.clickCategorie(id);
                        }
                    }
            );

        }
    }

    public interface onItemClickListener {
        void onItemClick(JSONObject brinde, boolean isCupom);
    }

    public void setListener(AdapterHorizontalCategories.onItemClickListener listener) {
        this.listener = listener;
    }

    public interface ListenerClickCategories {
        public void clickCategorie(String categorie);
    }


}

