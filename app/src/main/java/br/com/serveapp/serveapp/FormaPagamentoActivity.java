package br.com.serveapp.serveapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.LinePaymentFormListAdapterView;
import br.com.serveapp.serveapp.adapt.LineSelectCard;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.dao.webService.ServiceFormaPagamento;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class FormaPagamentoActivity extends RequestActivity {

    Pedido pedido;
    Button btn_finish_money_demand;
    RecyclerView recyclerViewPAymentForm;
    RecyclerView recyclePagamentoDebito;
    RecyclerView listMyCards;
    TextView textValToPayment;
    private static final int CONFIRM_PAYMENT = 11;
    private Button changeAddress;
    Button btnCancelSelectPgmt;
    CardView buttonSelectMoney;
    View borderMoney;
    Request serviceFormaPagamento;
    String tipoPagamentoSelecionado;
    String imagemSelecionada;
    CardView cardCredito;
    CardView cardDebito;
    Intent resultIntent;
    public static boolean is_payment_online = false;
    public static boolean select_online = false;

    public ProgressDialog mProgress;

    CardView cardSelectDebit;
    RadioButton cartaoCredito;
    RadioButton cartaoDebito;

    AlertDialog dialog;
    Button btnAddNewCard;
    TextView cardSubTipoPagamentoTitleApp;

    LineSelectCard LineSelectCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        try {
            if (Repository.idEstabelecimento == null) {
                finish();
            }
        } catch (Exception e) {
            finish();
        }

        mProgress = new ProgressDialog(this);
        String titleId = "Excluindo...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Aguardando uma resposta do servidor...");

        setContentView(R.layout.activity_forma_pagamento);
        getSupportActionBar().hide();
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        tipoPagamentoSelecionado = Repository.tipoPagamentoSelecionado;
        imagemSelecionada = LinePaymentFormListAdapterView.iconSelected;

        btnAddNewCard = findViewById(R.id.end_add2);
        listMyCards = findViewById(R.id.listMyCards);
        recyclePagamentoDebito = findViewById(R.id.recyclePagamentoDebito);
        recyclerViewPAymentForm = findViewById(R.id.recyclerViewPAymentFormCredit);
        cardCredito = findViewById(R.id.cardCredito);
        cardDebito = findViewById(R.id.cardDebito);
        LinearLayoutManager lm = new LinearLayoutManager(getApplicationContext());
        LinearLayoutManager lmDeb = new LinearLayoutManager(getApplicationContext());
        recyclerViewPAymentForm.setLayoutManager(lm);
        recyclePagamentoDebito.setLayoutManager(lmDeb);

        cardSubTipoPagamentoTitleApp = findViewById(R.id.cardSubTipoPagamentoTitleA);
        btnAddNewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(), AddCardPaymentActivity.class), 1);
            }
        });

        btnCancelSelectPgmt = findViewById(R.id.btnCancelSelectPgmt);
        btnCancelSelectPgmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                setResult(RESULT_CANCELED);
            }
        });

        btn_finish_money_demand = findViewById(R.id.btn_finish_money_demand);
        btn_finish_money_demand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                select_online = is_payment_online;

                if(is_payment_online){

                    Repository.tipoPagamentoSelecionado = tipoPagamentoSelecionado;
                    LinePaymentFormListAdapterView.idSaved = LinePaymentFormListAdapterView.id;
                    setResult(11, resultIntent);
                    finish();

                }else{

                    resultIntent = new Intent();
                    resultIntent.putExtra("icon", imagemSelecionada);
                    Repository.tipoPagamentoSelecionado = tipoPagamentoSelecionado;
                    LinePaymentFormListAdapterView.idSaved = LinePaymentFormListAdapterView.id;

                    if (tipoPagamentoSelecionado != null) {
                        setResult(66, resultIntent);
                    } else {
                        setResult(66);
                    }
                    finish();
                }
            }
        });

        buttonSelectMoney = findViewById(R.id.buttonSelectMoney);
        borderMoney = findViewById(R.id.borderMoney);

        if (LinePaymentFormListAdapterView.idSaved == 0) {

            LinePaymentFormListAdapterView.setSelected(borderMoney, LinePaymentFormListAdapterView.id);

            /// pay

            enabledPayment();
        }

        serviceFormaPagamento = new Request(getApplicationContext(), new ServiceFormaPagamento(this));
        serviceFormaPagamento.request();

        buttonSelectMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tipoPagamentoSelecionado = "dinheiro";
                is_payment_online = false;
                LinePaymentFormListAdapterView.setSelected(borderMoney, 0);
                enabledPayment();
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listMyCards.setLayoutManager(new GridLayoutManager(this, 4));
        //listMyCards.setLayoutManager(llm);

      /*
        LineSelectCard = new LineSelectCard(Repository.listCards, getApplicationContext());
        LineSelectCard.setOnItemClickListner(new LineSelectCard.OnItemClickListner() {
            @Override
            public void onItemClick(boolean selectOnline, JSONObject jsonObject) {
                if (selectOnline == true && jsonObject.has("number")) {

                    Intent resultIntent = new Intent();
                    try {

                        resultIntent.putExtra("number", jsonObject.getString("number"));
                        resultIntent.putExtra("numeroCartao", jsonObject.getString("numeroCartao"));
                        resultIntent.putExtra("bandeira", jsonObject.getString("bandeira").toLowerCase());
                        resultIntent.putExtra("cvv", jsonObject.getString("cvv"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    setResult(11, resultIntent);
                    finish();

                } else {

                    Toast.makeText(getApplicationContext(), "not", Toast.LENGTH_LONG).show();
                    //  setResult(11);
                    finish();
                }
            }
        });

        listMyCards.setAdapter(LineSelectCard);

*/



    }

        @Override
    public void onResponseServe(String response) {

            if (response != null && !response.equals("null"))
                try {
                    mProgress.dismiss();
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject result = jsonObject.getJSONObject("result");
                    initializeFromasPagamento(result.getJSONArray("credito"), result.getJSONArray("debito"),   result.getJSONArray("cards_cliente"), result.getString("central_pay"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }


    @Override
    public void onCache(String response) {

    }

    public void unselectMoney() {
        buttonSelectMoney.setBackgroundResource(0);
    }

    public void enabledPayment() {
        btn_finish_money_demand.setBackground(getDrawable(R.drawable.borda_arredondada_color_black));
        btn_finish_money_demand.setEnabled(true);
    }

    public void initializeFromasPagamento(JSONArray resultCredito, JSONArray resultDebito, JSONArray cards_cliente, String central_pay ) {

        if(central_pay!=null && central_pay.equalsIgnoreCase("0")) {
            listMyCards.setVisibility(View.GONE);
            btnAddNewCard.setVisibility(View.GONE);
            cardSubTipoPagamentoTitleApp.setVisibility(View.GONE);
        }

        if (resultCredito.length() == 0) {
            cardCredito.setVisibility(View.GONE);
        } else {
            cardCredito.setVisibility(View.VISIBLE);
        }
        if (resultDebito.length() == 0) {
            cardDebito.setVisibility(View.GONE);
        } else {
            cardDebito.setVisibility(View.VISIBLE);
        }

        LinePaymentFormListAdapterView linePaymentFormListAdapterViewCredit = new LinePaymentFormListAdapterView(this, getApplicationContext(), resultCredito);
        linePaymentFormListAdapterViewCredit.setListner(new LinePaymentFormListAdapterView.OnItemClickListnerPayment() {
            @Override
            public void onItemClick(JSONObject item) {
                try {

                    //buttonSelectMoney.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.borda_transparente));
                    tipoPagamentoSelecionado = item.getString("nome");
                    imagemSelecionada = item.getString("icon");
                    is_payment_online = false;
                    LinePaymentFormListAdapterView.unSelected(borderMoney);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        LineSelectCard = new LineSelectCard(cards_cliente, getApplicationContext());
        LineSelectCard.setOnItemClickListner(new LineSelectCard.OnItemClickListner() {
            @Override
            public void onItemClick(boolean selectOnline, JSONObject jsonObject, View selected) {

                if (selectOnline == true && jsonObject.has("card")) {

                    resultIntent = new Intent();
                    is_payment_online = true;
                    try {
                        LinePaymentFormListAdapterView.setSelected(selected,jsonObject.getInt("_id"));
   ////////      //     LinePaymentFormListAdapterView.idSaved = jsonObject.getInt("_id");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LinePaymentFormListAdapterView.unSelected(borderMoney);
                       /* LinePaymentFormListAdapterView.id = 0;
                        //View selected = LinePaymentFormListAdapterView.selected;
                        if (LinePaymentFormListAdapterView.selected != null) {
                            LinePaymentFormListAdapterView.selected.setBackground(selected.getResources().getDrawable(R.color.background));
                        }

                        selected.setBackground(selected.getResources().getDrawable(R.color.colorPrimary));
                        LinePaymentFormListAdapterView.selected = selected;
*/

                    try {
//                        Toast.makeText(getApplicationContext(), "erro"  , Toast.LENGTH_LONG).show();

                        resultIntent.putExtra("card", jsonObject.getString("card"));
                        resultIntent.putExtra("numeroCartao", jsonObject.getString("number"));
                        resultIntent.putExtra("bandeira", jsonObject.getString("bandeira").toLowerCase());
               //         resultIntent.putExtra("isMultipleCard", jsonObject.getBoolean("isMultiple"));
                        resultIntent.putExtra("idCardCredit", jsonObject.getString("_id"));
                        resultIntent.putExtra("cvv", jsonObject.getString("cvv"));
                        resultIntent.putExtra("token", jsonObject.getString("token"));
                        resultIntent.putExtra("nameHolder", jsonObject.getString("nameHolder"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

               //     Toast.makeText(getApplicationContext(), "not", Toast.LENGTH_LONG).show();
                    //  setResult(11);
                    finish();
                }
            }

            @Override
            public void onItemClickDelete(boolean selectOnline, JSONObject card) {

                stabelecimentoClosed(card);

            }
        });

        listMyCards.setAdapter(LineSelectCard);

        recyclerViewPAymentForm.setAdapter(linePaymentFormListAdapterViewCredit);
        recyclerViewPAymentForm.setLayoutManager(new GridLayoutManager(this, 4));
        LinePaymentFormListAdapterView linePaymentFormListAdapterViewDebit = new LinePaymentFormListAdapterView(this, getApplicationContext(), resultDebito);
        linePaymentFormListAdapterViewDebit.setListner(new LinePaymentFormListAdapterView.OnItemClickListnerPayment() {
            @Override
            public void onItemClick(JSONObject item) {
                try {
                    tipoPagamentoSelecionado = item.getString("nome");
                    imagemSelecionada = item.getString("icon");
                    is_payment_online = false;
                    LinePaymentFormListAdapterView.unSelected(borderMoney);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        recyclePagamentoDebito.setAdapter(linePaymentFormListAdapterViewDebit);
        recyclePagamentoDebito.setLayoutManager(new GridLayoutManager(this, 4));

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 1) {
            serviceFormaPagamento.request();
        }

    }



    public void stabelecimentoClosed(final JSONObject card) {

    //    mProgress.dismiss();
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(FormaPagamentoActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.delete_credit_card, null);

        mBuilder.setView(mView);
        dialog = mBuilder.show();

        Button btnCancel = mView.findViewById(R.id.btnCancelDeleteCardCredit);
        Button btnConfirmDelete = mView.findViewById(R.id.btnConfirmDeleteCardCredit);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirmDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Map<String, String> paramLogin = new HashMap<>();
                paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
                try {
                    paramLogin.put("id_card", card.getString("_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                paramLogin.put("function_method", "remove");
                paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));

                request(Script.select_forma_pagamento, paramLogin);
                dialog.dismiss();
                mProgress.show();
            }
        });

   //     confirmPedido.setEnabled(true);
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        mProgress.dismiss();
    }
}
