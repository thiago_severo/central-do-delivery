package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.NavigationDrawerActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceEstabeleciment extends OnPostResponse {

    NavigationDrawerActivity navigationDrawerActivity;
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressBar progressBar;

    public ServiceEstabeleciment(ProgressBar progressBar, NavigationDrawerActivity navigationDrawerActivity, SwipeRefreshLayout swipeRefreshLayout) {
        this.progressBar = progressBar;
        this.navigationDrawerActivity = navigationDrawerActivity;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "estabelecimento_new.php";
    }

    @Override
    public void onResponse(String response) {
        progressBar.setVisibility(View.GONE);
        try{
            progressBar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
    }

        @Override
    public void onCache(JSONObject jsonObject) {
        try {

            JSONArray result = jsonObject.getJSONObject("result").getJSONArray("list");
            JSONArray categorias = jsonObject.getJSONObject("result").getJSONArray("categorias");
            JSONObject promotions = jsonObject.getJSONObject("result").getJSONObject("destaque");
            String id = jsonObject.getJSONObject("result").getJSONObject("end").getString("_id");
            String titulo = jsonObject.getJSONObject("result").getJSONObject("end").getString("titulo");
            Repository.fav.reset(result);
            if(result==null || result.length()<=0){
                navigationDrawerActivity.textBodyEmptyEstab.setVisibility(View.VISIBLE);
                navigationDrawerActivity.textTitleEmptyEstab.setVisibility(View.VISIBLE);
            }

            if(Repository.qtdCart < 0){
                Repository.qtdCart = jsonObject.getJSONObject("result").getInt("qtd_card");
            }

            navigationDrawerActivity.initializeAdapter(result, categorias, promotions);
            navigationDrawerActivity.updateAddress(titulo, id);
            navigationDrawerActivity.setQtdCart();



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            swipeRefreshLayout.setRefreshing(false);
             }catch (NullPointerException e){}
        super.onErrorResponse(error);
        progressBar.setVisibility(View.GONE);
        Toast.makeText(navigationDrawerActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONObject("result").getJSONArray("list");
                JSONArray categorias = jsonObject.getJSONObject("result").getJSONArray("categorias");
                JSONObject promotions = jsonObject.getJSONObject("result").getJSONObject("destaque");

                if(result==null || result.length()<=0){
                    navigationDrawerActivity.textBodyEmptyEstab.setVisibility(View.VISIBLE);
                    navigationDrawerActivity.textTitleEmptyEstab.setVisibility(View.VISIBLE);
                }

                String id = jsonObject.getJSONObject("result").getJSONObject("end").getString("_id");
                String titulo = jsonObject.getJSONObject("result").getJSONObject("end").getString("titulo");

                if(!jsonObject.getJSONObject("result").isNull("qtd_card"))
                Repository.qtdCart = jsonObject.getJSONObject("result").getInt("qtd_card");

                navigationDrawerActivity.setQtdCart();

                Repository.fav.reset(result);
                navigationDrawerActivity.initializeAdapter(result, categorias, promotions);
                navigationDrawerActivity.updateAddress(titulo, id);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
