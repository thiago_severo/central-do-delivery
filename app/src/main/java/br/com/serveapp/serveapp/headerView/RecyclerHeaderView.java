package br.com.serveapp.serveapp.headerView;

import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class RecyclerHeaderView extends RecyclerView.OnScrollListener {

    RecyclerView bodyView;
    LinearLayoutManager bodyLayout;
    RecyclerView headerView;
    LinearLayoutManager headerLayout;
    HeaderLineAdapter adapter;
    Activity activity;

    public RecyclerHeaderView(RecyclerView bodyView, RecyclerView headerView, Activity activity) {
        this.activity = activity;
        this.bodyView = bodyView;
        this.headerView = headerView;
        setupRecycler();
        setupRecyclerTitle();
    }

    private void setupRecycler() {
        // Configurando o gerenciador de layout para ser uma lista.
        bodyLayout = new LinearLayoutManager(activity);
        bodyView.setLayoutManager(bodyLayout);
        bodyView.addOnScrollListener(this);
    }


    public void setAdapter(List<String> result) {
        headerView.setAdapter(new HeaderLineAdapter(new JSONArray(result), bodyLayout, headerLayout));
    }

    public void setAdapter(JSONArray result) {
        headerView.setAdapter(new HeaderLineAdapter(result, bodyLayout, headerLayout));
    }

    public void setAdapter(JSONArray result, String attr) {
        try {
            JSONArray temp = new JSONArray();
            if (result != null) {
                for (int i = 0; i < result.length(); i++) {
                    temp.put(result.getJSONObject(i).getString(attr));
                }
            }
            adapter = new HeaderLineAdapter(temp, bodyLayout, headerLayout);
            headerView.setAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setupRecyclerTitle() {
        // Configurando o gerenciador de layout para ser uma lista.
        headerLayout = new LinearLayoutManager(activity);
        headerLayout.setOrientation(LinearLayoutManager.HORIZONTAL);
        headerView.setLayoutManager(headerLayout);
        //headerView.setAdapter(new HeaderLineAdapter(titles, bodyLayout,headerLayout));
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        //final FloatingActionButton fab = activity.findViewById(R.id.floatingActionButton);
        if (recyclerView == bodyView) {
            HeaderLineGroup.selected(bodyLayout.findFirstVisibleItemPosition());
            headerLayout.scrollToPositionWithOffset(bodyLayout.findFirstVisibleItemPosition(), 0);
        }
        /*if (newState == RecyclerView.SCROLL_STATE_IDLE){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    fab.show();
                }
            }, 100);
        }*/

    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        /*FloatingActionButton fab = activity.findViewById(R.id.floatingActionButton);
        if (dy > 0 || dy < 0 && fab.isShown())
            fab.hide(); */
    }
}
