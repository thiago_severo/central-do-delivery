package br.com.serveapp.serveapp;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.VolleyError;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import br.com.serveapp.serveapp.activity.local.ConfirmItensDemandLocal;
import br.com.serveapp.serveapp.activity.local.TabsPagerAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.dao.webService.ServiceAddToFavorit;
import br.com.serveapp.serveapp.dao.webService.ServiceCallAtendente;
import br.com.serveapp.serveapp.dao.webService.ServiceCheckOut;
import br.com.serveapp.serveapp.dao.webService.ServiceLoadCarValue;
import br.com.serveapp.serveapp.dao.webService.ServiceRemoveToFavorit;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.drawer.MyBounceInterpolator;
import br.com.serveapp.serveapp.entidades.Estabelecimento;
import br.com.serveapp.serveapp.entidades.cardapio.GroupItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.entidades.pedido.PedidoLocal;
import br.com.serveapp.serveapp.enun.StatusPedido;
import br.com.serveapp.serveapp.fragments.FragmentComanda;
import br.com.serveapp.serveapp.fragments.FragmentListDemandsEstablishment;
import br.com.serveapp.serveapp.fragments.FragmentListProductsEstablishment;
import br.com.serveapp.serveapp.fragments.FragmentReadQrCode;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.RefreshProducts;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ListProductsStablishmentDeliveryActivity extends BaseEstabelecimentActivity implements ZXingScannerView.ResultHandler, RefreshProducts {

    private TabsPagerAdapter tabePageAdapter;
    public ViewPager viewPager;
    private List<ItemPedido> itemPedidoList = new ArrayList<>();
    Request requestCall;

    private JSONObject jsonEstab;
    public CardView cardFab;
    TextView textValTotalDemand;

    TextView mainStatusText;
    TextView timeEntrega;
    TextView mainTaxaEntrega;

    TextView textTotalItensAdd;
    TextView textNameToolbarEstab;
    CardView cardFinishDemand;
    TextView textFinishDemand;
    TextView textTotalDemandinCar;
    ImageView btnShowDetail;
    ImageView btnClickFavorit;
    ConstraintLayout constransCallAtendente;
    ConstraintLayout constraintLayoutLeft;
    Request requestCheckIn;
    Request requestCheckOut;
    public ProgressDialog mProgress;

    public static final int ITEM_ADD = 5;
    public static final int GET_DEMAND = 6;
    public static final int RESULT_GET_DEMAND_SUCESS = 7;
    public static final int RESULT_UPDATE_CAR_VALUE = 8;
    public static final int RESULT_SCROLLING_ACTIVITY = 9;

    private boolean setDemand;
    public static boolean isLocal = false;

    public static ListProductsStablishmentDeliveryActivity refference;

    Request requestAddtoFavorits;
    Request requestRemovetoFavorits;
    public double valTotal = 0;
    public static double qtdItem = 0;
    public ImageView floatingActionButtonCallQr;
    public static boolean isOpen;

    boolean blockBtnFavorit = false;
    BottomNavigationView bottomNavigationView;
    FragmentManager fragmentManager = getSupportFragmentManager();
    private BottomNavigationView.OnNavigationItemSelectedListener listener =

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    switch (menuItem.getItemId()) {
                        case R.id.navigation_left:
                            finish();
                            overridePendingTransition(0, 0);
                            break;

                        case R.id.nav_carrinho_local:
                            startActivity(new Intent(getApplicationContext(), ConfirmItensDemandLocal.class));
                            overridePendingTransition(0, 0);
                            break;
                    }

                    return true;
                }
            };


    Request requestServiceLoadCarValue;
    static FragmentListDemandsEstablishment tabPedido;
    static FragmentComanda tabComanda;
    static FragmentListProductsEstablishment tabListProducts;

    @SuppressLint("WrongViewCast")
    TabLayout tabLayout;

    public void setJsonEstab(JSONObject jsonEstab) {
        try {
            this.jsonEstab = jsonEstab;
            mainTaxaEntrega.setText(jsonEstab.getString("frete"));
            String tempoEntrega = jsonEstab.getString("temp_min")+" - "+jsonEstab.getString("temp_max")+" min";
            timeEntrega.setText(tempoEntrega);
            mainStatusText.setText(jsonEstab.getString("status").toUpperCase());

            if(jsonEstab.getJSONObject("delivery").getString("aberto").equals("0")){
                mainStatusText.setText("FECHADO");
                isOpen = false;
            }
            else{
                isOpen = true;
                mainStatusText.setText("ABERTO");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public static void showIn(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(0f);
        v.setTranslationY(v.getHeight());
        v.animate()
                .setDuration(200)
                .translationY(0)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                    }
                })
                .alpha(1f)
                .start();
    }

    public static void showOut(final View v) {
        v.setVisibility(View.VISIBLE);
        v.setAlpha(1f);
        v.setTranslationY(0);
        v.animate()
                .setDuration(200)
                .translationY(v.getHeight())
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setVisibility(View.GONE);
                        super.onAnimationEnd(animation);
                    }
                }).alpha(0f)
                .start();
    }

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            doRestart();
        }

    };

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        //Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        try {
            if (Repository.getIdUsuario(getApplicationContext()) == null || Repository.idEstabelecimento == null) {
                finish();
            }
        } catch (Exception e) {
            finish();
        }

        setContentView(R.layout.activity_list_products_establishment);
        Toolbar toolbar = findViewById(R.id.toolbarMainProductsDelivery);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(0, 0);
            }
        });

        cardFab = findViewById(R.id.cardFab);
        floatingActionButtonCallQr = findViewById(R.id.floatingActionButtonCallQr);
        textNameToolbarEstab = findViewById(R.id.textNameToolbarEstab);

        if (getIntent().hasExtra("nome")) {
            textNameToolbarEstab.setText((getIntent().getStringExtra("nome")).toUpperCase());
        }

        btnShowDetail = findViewById(R.id.btnRightDetail);

        mProgress = new ProgressDialog(this);
        String titleId = "Entrando..";
        mProgress.setTitle(titleId);
        mProgress.setMessage("verificando conexão com a internet...");

// ----------

        mainStatusText = findViewById(R.id.mainStatusText);
        timeEntrega = findViewById(R.id.timeEntrega);
        mainTaxaEntrega = findViewById(R.id.mainTaxaEntrega);

        textValTotalDemand = findViewById(R.id.textTotalDemandinCar);
        textTotalItensAdd = findViewById(R.id.textCountItens);
        textFinishDemand = findViewById(R.id.textFinishDemand);
        textTotalDemandinCar = findViewById(R.id.textTotalDemandinCar);

        constransCallAtendente = findViewById(R.id.constransCallAtendente);
        constransCallAtendente.setVisibility(View.GONE);
        constraintLayoutLeft = findViewById(R.id.constraintLayoutLeft);
        constraintLayoutLeft.setVisibility(View.GONE);

        requestCheckOut = new Request(getApplicationContext(), new ServiceCheckOut(this));
        requestCall = new Request(getApplicationContext(), new ServiceCallAtendente(this));

        btnShowDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toolbarDetail = new Intent(getApplicationContext(), ScrollingActivity.class);

                try {

                    toolbarDetail.putExtra("nome", getIntent().getStringExtra("nome"));
                    toolbarDetail.putExtra("slogan", getIntent().getStringExtra("slogan"));
                    toolbarDetail.putExtra("logo", getIntent().getStringExtra("logo"));


                    toolbarDetail.putExtra("distancia", jsonEstab.getString("distancia"));
                    toolbarDetail.putExtra("rating", jsonEstab.getString("avaliacao"));
                    toolbarDetail.putExtra("frete", jsonEstab.getString("frete"));

                    startActivityForResult(toolbarDetail, GET_DEMAND);
                    overridePendingTransition(R.anim.entrer_from_right,
                            R.anim.exit_to_left);
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),"Não foi possível carregar os dados no momennto :/", Toast.LENGTH_LONG).show();
                }




            }
        });

        cardFinishDemand = findViewById(R.id.card_item_add_to_car);
        cardFinishDemand.setVisibility(View.GONE);

        cardFinishDemand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cardFinishDemand.setEnabled(false);
                Intent intent = new Intent(getApplicationContext(), ConfirmItensPedidoActiviy.class);
                intent.putExtra("nome", getIntent().getStringExtra("nome"));
                startActivityForResult(intent, GET_DEMAND);
                overridePendingTransition(R.anim.slide_in_up,
                        R.anim.slide_out_up);
            }
        });

        btnClickFavorit = findViewById(R.id.favorit_btn);
        requestServiceLoadCarValue = new Request(getApplicationContext(), new ServiceLoadCarValue(this));
        requestAddtoFavorits = new Request(getApplicationContext(), new ServiceAddToFavorit(btnClickFavorit, this));
        requestRemovetoFavorits = new Request(getApplicationContext(), new ServiceRemoveToFavorit(btnClickFavorit, this));

        if (getIntent().hasExtra("demandFinalized")) {
            cardFinishDemand.setVisibility(View.GONE);
            qtdItem = 0;
            viewPager.setCurrentItem(tabePageAdapter.fragments.size());
        }

        if (Repository.fav.contains(Repository.idEstabelecimento)) {
            animAddFavorit();
        }

        btnClickFavorit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnClickFavorit.getDrawable().getConstantState() != getResources().getDrawable(R.drawable.icon_favorit_clicked).getConstantState()) {
                    requestAddtoFavorits.request();
                } else {
                    requestRemovetoFavorits.request();
                }
            }
        });

        requestServiceLoadCarValue.request();
        if (getIntent().hasExtra("id_produto")) {
            Intent intent = new Intent(getApplicationContext(), DetalheItemPopUpActivity.class);
            intent.putExtra("foto", "");
            intent.putExtra("value", 1);
            intent.putExtra("valor", "");
            intent.putExtra("nome", "name");
            intent.putExtra("descricao", "descricao");
            intent.putExtra("id_produto", getIntent().getStringExtra("id_produto"));
            intent.putExtra("tempoPreparo",  "0");
            intent.putExtra("avaliacao",  "5.0");
            //startActivityForResult(intent, 1);
            new DetalheItemPopUpActivity(this,intent).show();


        }

    }

    @SuppressLint("RestrictedApi")
    public void dontShowAcessCart() {
        cardFinishDemand.setVisibility(View.GONE);
        //  floatingActionButtonCallQr.setVisibility(View.GONE);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    public void addTabComandaDelivery() {


        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

        floatingActionButtonCallQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(0);
            }
        });

        tabePageAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        tabLayout = findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.pagerScrolling);
        tabListProducts = new FragmentListProductsEstablishment();

        tabPedido = new FragmentListDemandsEstablishment(this);
        tabePageAdapter.adicionar(new FragmentReadQrCode(this), "CHECK-IN");
        tabePageAdapter.adicionar(tabListProducts, "Cardápio");
        tabePageAdapter.adicionar(tabPedido, "Pedidos");

        floatingActionButtonCallQr.setImageResource(R.drawable.qrcode);
        floatingActionButtonCallQr.setVisibility(View.VISIBLE);

        constransCallAtendente.setVisibility(View.GONE);
        constraintLayoutLeft.setVisibility(View.GONE);


        if (getIntent().hasExtra("serveapp.notifyId")) {

            viewPager.setAdapter(tabePageAdapter);
            viewPager.setCurrentItem(2);
            tabLayout.setupWithViewPager(viewPager);

        } else {

            viewPager.setAdapter(tabePageAdapter);
            viewPager.setCurrentItem(1);
            tabLayout.setupWithViewPager(viewPager);

        }

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    dontShowAcessCart();
                } else {
                    if (qtdItem > 0) {
                        showCardDemand();
                        floatingActionButtonCallQr.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    public void addTabComandaLocal(String urlImageAtendente) {

        requestServiceLoadCarValue.request();
        cardFab.setVisibility(View.VISIBLE);

        if (urlImageAtendente != null && !urlImageAtendente.equals("null")) {
            new ListSetImage(getApplicationContext(), floatingActionButtonCallQr).execute(urlImageAtendente);
        } else {
            floatingActionButtonCallQr.setImageResource(R.drawable.garcon);
        }

        floatingActionButtonCallQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (constraintLayoutLeft.getVisibility() == View.VISIBLE) {
                    showOut(constraintLayoutLeft);
                    showOut(constransCallAtendente);
                } else {
                    showIn(constraintLayoutLeft);
                    showIn(constransCallAtendente);
                }

            }
        });

        constransCallAtendente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> paramLogin = new HashMap<>();

                paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                paramLogin.put("type", "1");

                requestCall.request(paramLogin);
            }
        });

        constraintLayoutLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> paramLogin = new HashMap<>();

                paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                paramLogin.put("type", "2");

                requestCall.request(paramLogin);
            }
        });

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }

        tabePageAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        tabLayout = findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.pagerScrolling);

        tabPedido = new FragmentListDemandsEstablishment(this);
        tabComanda = new FragmentComanda(this);

        tabComanda.setLeftTable(new FragmentComanda.LeftTableListener() {
            @Override
            public void leftTable() {

                String id_cliente = null;

                mProgress = new ProgressDialog(ListProductsStablishmentDeliveryActivity.this);
                String titleId = "Saindo...";
                mProgress.setTitle(titleId);
                mProgress.setMessage("A comanda será enviada para o seu email...");

                id_cliente = Repository.getIdUsuario(getApplicationContext());
                final Map<String, String> param = new HashMap<>();
                param.put("function_method", "check-out");
                param.put("id_cliente", id_cliente);
                requestCheckOut.request(param);
                mProgress.show();

            }
        });

        tabComanda.updateList();
        tabListProducts = new FragmentListProductsEstablishment();

        tabePageAdapter.adicionar(tabComanda, "Comanda");
        tabePageAdapter.adicionar(tabListProducts, "Cardápio");
        tabePageAdapter.adicionar(tabPedido, "Pedidos");

        viewPager.setAdapter(tabePageAdapter);
        viewPager.setCurrentItem(1);
        tabLayout.setupWithViewPager(viewPager);



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        // super.onActivityResult(requestCode, resultCode, data);


        if (resultCode == Activity.RESULT_OK) {
            qtdItem = Double.parseDouble(data.getStringExtra("qtd"));
            if (qtdItem > 0)
                showBtnGoToCar(data.getStringExtra("label"), data.getStringExtra("qtd"));
        }
        if (resultCode == Activity.RESULT_CANCELED) {

        }
        if (resultCode == ITEM_ADD) {
            showCardDemand();
        };

        if (resultCode == RESULT_GET_DEMAND_SUCESS) {

            Repository.pedidoToTabLocal = buildDemand(Repository.listOfItensPedidosToTabLocal);
            Repository.listGroupPedidosToTabLocal.addAll(Repository.listOfItensPedidosToTabLocal);
            Repository.removelAllItensPedidosToTabLocal();
            Repository.listOfPedidosToTabLocal.add(Repository.pedidoToTabLocal);

            itemPedidoList.removeAll(itemPedidoList);
            cardFinishDemand.setVisibility(View.GONE);
            viewPager.setCurrentItem(tabePageAdapter.getCount());
        }

        if (requestCode == GET_DEMAND) {

            if (resultCode == RESULT_UPDATE_CAR_VALUE) {

                if (data.hasExtra("qtd")) {
                    qtdItem = Double.parseDouble(data.getStringExtra("qtd"));
                    if (qtdItem > 0) {
                        showBtnGoToCar(data.getStringExtra("cartValue"), data.getStringExtra("qtd"));
                    } else {
                        cardFinishDemand.setVisibility(View.GONE);
                    }
                }

            }else if(resultCode == RESULT_SCROLLING_ACTIVITY){
                setDemand = true;
            }
            else {


                cardFinishDemand.setEnabled(true);
                requestServiceLoadCarValue.request();
                cardFinishDemand.setVisibility(View.GONE);

            }

        }

        if (resultCode == 99) {
            requestServiceLoadCarValue.request();
        }

        if (resultCode == 65) {
            requestServiceLoadCarValue.request();
        }

        cardFinishDemand.setEnabled(true);

    }


    public static void updateLineDemand(int position) {

        try {
            tabPedido.lineItemDemandEstabelishment.listItensDemand.getJSONObject(position).put("status", "preparando");
            tabPedido.lineItemDemandEstabelishment.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Pedido buildDemand(List<GroupItemPedido> groupItemPedidos) {

        Pedido pedido = new PedidoLocal();
        List<ItemPedido> itemPedidos = new ArrayList<>();

        for (GroupItemPedido groupItemPedido : groupItemPedidos) {
            for (int i = 0; i < groupItemPedido.getCountIntPedido(); i++) {
                itemPedidos.add(groupItemPedido.getItemPedido());
            }
        }

        pedido.setId(new Random().nextInt());
        Estabelecimento estabelecimento2 = new Estabelecimento();
        estabelecimento2.setNome(" Cantina Burgueria ");
        pedido.setEstabelecimento(estabelecimento2);
        pedido.setItens(itemPedidos);
        pedido.setStatus(StatusPedido.RECEBIDO);
        return pedido;
    }

    public void changeFragment(JSONArray arrayCategories, JSONObject categoryIniti) throws JSONException {
        FragmentListProductsEstablishment fragmentListProductsEstablishment = new FragmentListProductsEstablishment();
        fragmentListProductsEstablishment.listRecyclerHorizontalCategories = arrayCategories;
        fragmentListProductsEstablishment.cdCategory = categoryIniti.getInt("_id");
        tabePageAdapter.fragments.set(0, fragmentListProductsEstablishment);
        tabePageAdapter.notifyDataSetChanged();
    }

    public void animAddFavorit() {
        final Animation myAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        btnClickFavorit.setImageResource(R.drawable.icon_favorit_clicked);
        btnClickFavorit.startAnimation(myAnim);
    }

    public void showBtnGoToCar(String label, String qtd) {

        Repository.qtdCart = Integer.parseInt(qtd);
        qtdItem = Double.parseDouble(qtd);
        textTotalDemandinCar.setText(label);
        textTotalItensAdd.setText(qtd);

        if (Integer.parseInt(qtd) > 0) {
            showCardDemand();
        } else {
            cardFinishDemand.setVisibility(View.GONE);
        }

        Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_up);
        Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_out_up);

        textFinishDemand.startAnimation(slide_up);
        textFinishDemand.startAnimation(slide_down);
    }

    public void showMsg(String mensagem, String titulo) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView textView = mView.findViewById(R.id.textMainMsg);
        TextView textViewSucess = mView.findViewById(R.id.textSucessMessage);
        textView.setText(titulo);
        textViewSucess.setText(mensagem);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        tabComanda.updateList();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        refference = this;

        if (tabPedido != null)
            tabPedido.updateList();

        if (tabComanda != null)
            tabComanda.updateList();

        Repository.activityVisible = this;

    }

    @Override
    public void onPause() {
        if (Repository.activityVisible == this) {
            Repository.activityVisible = null;
        }
        super.onPause();
    }

    public void updateFragemnts() {
        if (tabPedido != null)
            tabPedido.updateList();
        if (tabComanda != null)
            tabComanda.updateList();
    }

    @Override
    public void handleResult(Result scanResult) {

        mProgress.show();
        String id_cliente = Repository.getIdUsuario(getApplicationContext());
        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("function_method", "check-in");
        paramLogin.put("id_mesa", scanResult.getText().toString());
        paramLogin.put("instructionId", "check-in");
        paramLogin.put("id_cliente", id_cliente);
        request(Script.check_in, paramLogin);
        // ---------------------------------------

    }

    @Override
    public void onResponseServe(String response) {

        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                Repository.idEstabelecimento = result.getString("id_estabelecimento");
                Repository.atendimentoLocal = true;
                mProgress.dismiss();
                addTabComandaLocal(null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Verifique sua conexão com a internet", Toast.LENGTH_LONG).show();
        mProgress.dismiss();
    }

    public void updateImageAtendente(String urlImageAtendente) {
        new ListSetImage(getApplicationContext(), floatingActionButtonCallQr).execute(urlImageAtendente);
    }

    public void receiveSucessResponse(String label, String quantidade) {
        textTotalDemandinCar.setText(label);
        textTotalItensAdd.setText(quantidade);
        cardFinishDemand.setVisibility(View.VISIBLE);
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(2000);
        cardFinishDemand.setAnimation(fadeIn);
        //showCardDemand();
    }

    private void showCardDemand(){
        /*
        cardFinishDemand.setVisibility(View.VISIBLE);
        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator()); //add this
        fadeIn.setDuration(2000);
        cardFinishDemand.setAnimation(fadeIn);*/

        cardFinishDemand.setVisibility(View.VISIBLE);
        cardFinishDemand.setY(cardFinishDemand.getY()+200);
        cardFinishDemand.setAlpha(0.0f);

// Start the animation
        cardFinishDemand.animate().setDuration(1500)
                .translationY(0)
                .alpha(1.0f)
                .setListener(null);
    }

    @Override
    public void refresh(JSONObject obj) {
        onResume();
        requestServiceLoadCarValue.request();
    }
}
