package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

import static android.view.View.GONE;

public class LineItemComanda extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    LayoutInflater inflater;
    public JSONArray listItensCarrinho;
    TextView textOptions;
    private Context context;
    public onClickPedirOutro onClickPedirOutroListener;

    public LineItemComanda(Context context, JSONArray jsonArray) {
        this.textOptions = textOptions;
        this.context = context;
        this.listItensCarrinho = jsonArray;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_item_comanda, parent, false);
        viewHolder = new ItemCart(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {

            JSONObject itemCar = listItensCarrinho.getJSONObject(position);
            ((ItemCart) holder).nome.setText(itemCar.getString("nome"));
            ((ItemCart) holder).valor.setText(itemCar.getString("valor"));
            ((ItemCart) holder).qtd.setText(itemCar.getString("qtd"));
            ((ItemCart) holder).ratting_avaliation.setRating(itemCar.getInt("avaliacao"));

            if(itemCar.getInt("ped_status") == 4){
                ((ItemCart) holder).cardComanda.setAlpha(1.0f);
                ((ItemCart) holder).naoEngue.setVisibility(GONE);
            }else{
                ((ItemCart) holder).naoEngue.setVisibility(View.VISIBLE);
                ((ItemCart) holder).cardComanda.setAlpha(0.3f);
            }

            new ListSetImage(context, ((ItemCart) holder).foto).execute(itemCar.getString("foto"));
            ((ItemCart) holder).hora.setText(itemCar.getString("hora"));

            if (itemCar.getJSONArray("itens").length() <= 0) {
                ((ItemCart) holder).btn_down_detail.setVisibility(GONE);
            } else {
                ((ItemCart) holder).recyclerDetailItemComand.setAdapter(new LineDetailItemCartAdapter(itemCar.getJSONArray("itens")));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listItensCarrinho.length();
    }


    class ItemCart extends RecyclerView.ViewHolder {

        public CardView cardComanda;
        public TextView naoEngue;
        public TextView nome;
        public TextView valor;
        public TextView qtd;
        public TextView hora;
        public RatingBar ratting_avaliation;
        public ImageView foto;
        public RecyclerView recyclerDetailItemComand;
        public Button buttonPedirOutro;
        ImageView btn_down_detail;
        CardView cardView20;
        CardView cardExpandableItemPedido;

        public ItemCart(View itemView) {
            super(itemView);
            recyclerDetailItemComand = itemView.findViewById(R.id.recyclerDetailItemCar);
            cardExpandableItemPedido = itemView.findViewById(R.id.cardExpandableItemPedido);
            cardView20 = (CardView) itemView.findViewById(R.id.cardView20);
            btn_down_detail = itemView.findViewById(R.id.btn_down_detail);
            recyclerDetailItemComand = itemView.findViewById(R.id.recyclerDetailItemCar);
            nome = (TextView) itemView.findViewById(R.id.nome);
            valor = (TextView) itemView.findViewById(R.id.valor);
            qtd = (TextView) itemView.findViewById(R.id.quantidade);
            hora = (TextView) itemView.findViewById(R.id.hora);
            ratting_avaliation = itemView.findViewById(R.id.ratting_avaliation);
            foto = itemView.findViewById(R.id.foto);
            buttonPedirOutro = itemView.findViewById(R.id.buttonPedirOutro);
            cardComanda = itemView.findViewById(R.id.cardComanda);
            naoEngue = itemView.findViewById(R.id.txNaoEntregue);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            recyclerDetailItemComand.setLayoutManager(linearLayoutManager);

            cardView20.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    downUpCard(recyclerDetailItemComand);
                }
            });

            buttonPedirOutro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        onClickPedirOutroListener.clickPedirOutro(listItensCarrinho.getJSONObject(getAdapterPosition()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            });


        }

        private void downUpCard(final View view) {
            /*if (view.getVisibility() == View.GONE) {
                ObjectAnimator.ofFloat(btn_down_detail, "rotation", 180, 360).start();
                view.setVisibility(View.VISIBLE);
                view.animate()
                        .alpha(1f)
                        .setDuration(500)
                        .setListener(null);

            } else if (view.getVisibility() == View.VISIBLE) {
                ObjectAnimator.ofFloat(btn_down_detail, "rotation", 0, 180).start();
                view.animate()
                        .alpha(0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                view.setVisibility(View.GONE);
                            }
                        });
            }*/
        }

    }

    interface onClickPedirOutro {
        public void clickPedirOutro(JSONObject jsonObject);
    }

    public void setLickOutroListener(onClickPedirOutro onClickPedirOutro) {
        this.onClickPedirOutroListener = onClickPedirOutro;
    }
}
