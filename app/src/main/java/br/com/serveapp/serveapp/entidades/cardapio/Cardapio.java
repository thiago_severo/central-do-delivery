package br.com.serveapp.serveapp.entidades.cardapio;

import java.util.List;

public class Cardapio {

    private long id;
    private List<ItemCardapio> destaque;
    private List<Categoria> categorias;

    public List<ItemCardapio> getDestaque() {
        return destaque;
    }
    public void setDestaque(List<ItemCardapio> destaque) {
        this.destaque = destaque;
    }
    public List<Categoria> getCategorias() {
        return categorias;
    }
    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

}
