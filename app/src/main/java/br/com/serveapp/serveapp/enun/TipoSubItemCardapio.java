package br.com.serveapp.serveapp.enun;

public enum TipoSubItemCardapio {
    RadioButton, CheckBox, Spinner, Increment;
}
