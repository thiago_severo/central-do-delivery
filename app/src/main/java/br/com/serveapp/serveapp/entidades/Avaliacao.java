package br.com.serveapp.serveapp.entidades;

public class Avaliacao {

    private long id;
    private int numAvaliacoes, pontuacoes;

    public void addPontuacao(int pontuacao) {
        numAvaliacoes++;
        pontuacoes += pontuacao;
    }

    public void removePontuacao(int pontuacao) {
        numAvaliacoes--;
        pontuacoes-=pontuacao;
    }

    public void reset() {
        numAvaliacoes = 0;
        pontuacoes = 0;
    }

    public float getAvaliacao() {
        if(numAvaliacoes>0) {
            return pontuacoes / (float) numAvaliacoes;
        }else {
            return 0;
        }
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
