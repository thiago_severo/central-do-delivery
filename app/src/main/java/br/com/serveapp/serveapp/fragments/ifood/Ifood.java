package br.com.serveapp.serveapp.fragments.ifood;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.fragments.FragmentListProductsEstablishment;

public class Ifood extends RecyclerView.OnScrollListener {

    RecyclerView bodyView;
    RecyclerView headerView;
    LinearLayoutManager bodyLayout;
    LinearLayoutManager headerLayout;
    ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity;

    JSONArray categorias;
    JSONArray jsonFilter;

    View activity;
    Fragment fragment;
    Request request;
    public Ifood(ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity, View activity, FragmentListProductsEstablishment fragment) {
        this.activity = activity;
        this.fragment = fragment;
        this.listProductsStablishmentDeliveryActivity = listProductsStablishmentDeliveryActivity;
        bodyView = activity.findViewById(R.id.recycleListItensCardapioEstabeleciment);
        headerView = activity.findViewById(R.id.titleListEmpty);
        setupRecycler();
        setupRecyclerTitle();
  }

    private void setupRecycler() {
        // Configurando o gerenciador de layout para ser uma lista.
        bodyLayout = new LinearLayoutManager(activity.getContext());
        bodyView.setLayoutManager(bodyLayout);
        final List<Item> mUsers = new ArrayList<>();
        bodyView.addOnScrollListener(this);
       // bodyView.setAdapter(new GroupLineAdapter(Result.getResult(), bodyLayout, fragment));
    }

    public void setAdapter(JSONArray result, int isLocal){
        this.categorias = result;
        updateList(result);
    }

    public void updateList(JSONArray result){
        bodyView.setAdapter(new GroupLineAdapter(result, bodyLayout, fragment, listProductsStablishmentDeliveryActivity));
        headerView.setAdapter(new HeaderLineAdapter(result, bodyLayout, headerLayout));
    }

    private void setupRecyclerTitle() {
        // Configurando o gerenciador de layout para ser uma lista.
        headerLayout = new LinearLayoutManager(activity.getContext());
        headerLayout.setOrientation(LinearLayoutManager.HORIZONTAL);
        headerView.setLayoutManager(headerLayout);
        List<String> mUsers = new ArrayList<>();
        //headerView.setAdapter(new HeaderLineAdapter(Result.getResult(), bodyLayout, headerLayout));
    }

    @Override
    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
        if (recyclerView == bodyView) {
            HeaderLineGroup.selected(bodyLayout.findFirstVisibleItemPosition());
            headerLayout.scrollToPositionWithOffset(bodyLayout.findFirstVisibleItemPosition(), 0);
        }
        CardView fab = listProductsStablishmentDeliveryActivity.findViewById(R.id.cardFab);

    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        CardView fab = listProductsStablishmentDeliveryActivity.findViewById(R.id.cardFab);
        ConstraintLayout fab1 = listProductsStablishmentDeliveryActivity.findViewById(R.id.constransCallAtendente);
        ConstraintLayout fab2 = listProductsStablishmentDeliveryActivity.findViewById(R.id.constraintLayoutLeft);
        //if(dy>0 || dy<0 && fab.isShown()){
        //    fab.hide();
       // }
        if(dy>0){
            fab.setVisibility(View.GONE);
            fab1.setVisibility(View.GONE);
            fab2.setVisibility(View.GONE);
        }
        if(dy<0){
            fab.setVisibility(View.VISIBLE);
        }

    }

    public void filterProducts(String filter) {
        try {
            jsonFilter = new JSONArray();
            for (int i = 0; i < categorias.length(); i++) {
                JSONObject tempCat = categorias.getJSONObject(i);
                JSONArray products = tempCat.getJSONArray("itens");
                JSONArray filterProducts = new JSONArray();
                for (int j = 0; j < products.length(); j++) {
                    JSONObject tempProduct = products.getJSONObject(j);
                    if(filter(filter, tempProduct)){
                        filterProducts.put(tempProduct);
                    }
                }
                if (filterProducts.length() > 0) {
                    JSONObject categoria = new JSONObject();
                    categoria.put("nome", tempCat.getString("nome"));
                    categoria.put("id", tempCat.getString("id"));
                    categoria.put("orientation", tempCat.getString("orientation"));
                    categoria.put("itens", filterProducts);
                    jsonFilter.put(categoria);
                }
            }
            updateList(jsonFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean filter(String filter, JSONObject product) {
        double value = 0;
        try {
            filter = filter.toUpperCase();
            String[] f = filter.split(" ");
            //String temp = (product.getString("nome") + " " + product.getString("cat_name")).toUpperCase();
            String temp = (product.getString("nome") + " "
                    + product.getString("cat_name")).toUpperCase();
            temp = temp;
            for (String s : f) {
                if (!temp.contains(s))
                    return false;
                else {
                    temp = temp.replaceFirst(s, "");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }
}
