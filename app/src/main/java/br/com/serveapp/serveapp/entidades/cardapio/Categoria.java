package br.com.serveapp.serveapp.entidades.cardapio;

import java.util.List;

public class Categoria {

    private long id;
    private String nome;
    private String logo;
    private List<ItemCardapio> itens;


    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }
    public List<ItemCardapio> getItens() {
        return itens;
    }
    public void setItens(List<ItemCardapio> itens) {
        this.itens = itens;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }


}
