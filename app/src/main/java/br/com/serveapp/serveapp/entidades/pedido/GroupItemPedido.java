package br.com.serveapp.serveapp.entidades.pedido;

import org.json.JSONArray;
import org.json.JSONObject;

public class GroupItemPedido {

    public String idGroupItem;
    public int qtdMax;
    public int qtdMin;
    public JSONArray itemSelectedsCheck = new JSONArray();
    public JSONObject itemSelectedRadio;
    public String typeComponent;
    public String erro;
    public double valorInicial=0;
    public double valorRadio;
    public boolean enabled;

}
