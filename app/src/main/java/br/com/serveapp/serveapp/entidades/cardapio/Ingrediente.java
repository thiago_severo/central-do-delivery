package br.com.serveapp.serveapp.entidades.cardapio;

import android.os.Parcel;

import java.io.Serializable;


public class Ingrediente  implements Serializable {


    private long id;
    private String nome;
    private float valorExtra;


    public Ingrediente(Parcel in) {
        id = in.readLong();
        nome = in.readString();
        valorExtra = in.readFloat();
    }



    public Ingrediente() {

    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public float getValorExtra() {
        return valorExtra;
    }
    public void setValorExtra(float valorExtra) {
        this.valorExtra = valorExtra;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }



}
