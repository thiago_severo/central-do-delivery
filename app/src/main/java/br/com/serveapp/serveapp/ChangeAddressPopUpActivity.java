package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.LineAddressListAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListAddress;
import br.com.serveapp.serveapp.dao.webService.ServiceRemoveEndCliente;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.AddressBottomSheetDialog;
import br.com.serveapp.serveapp.util.CheckGroup;


public class ChangeAddressPopUpActivity extends AppCompatActivity {

    Button btnCloseItem;

    RecyclerView listAddress;
    Button btnConfirmAddress;
    Button end_add;
    TextView end_main;
    public ProgressBar progressDeleteAddress;
    NestedScrollView nestedScrollView;
    CardView cardLocation;

    Request requestServiceListAddress;
    Request requestServiceDeleteAddress;
    AddressBottomSheetDialog bottomSheet = null;
    LineAddressListAdapter lineAddressListAdapter;
    JSONObject alreadyAddress;
    LinearLayoutManager llm;

    String value;

    @SuppressLint("WrongViewCast")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (Repository.getUser(getApplicationContext()) == null) {
            finish();
        }

        setContentView(R.layout.end);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getIntent().hasExtra("updateCart")) {
            requestServiceListAddress = new Request(getApplicationContext(), new ServiceListAddress(Repository.idEstabelecimento, this));
        } else {
            requestServiceListAddress = new Request(getApplicationContext(), new ServiceListAddress("0", this));
        }

        requestServiceDeleteAddress = new Request(getApplicationContext(), new ServiceRemoveEndCliente(this));
        progressDeleteAddress = findViewById(R.id.progressIncrement2);

        listAddress = findViewById(R.id.end_list);
        llm = new LinearLayoutManager(getApplicationContext());
        listAddress.setLayoutManager(llm);
        listAddress.setNestedScrollingEnabled(false);
        listAddress.setHasFixedSize(false);

        end_add = findViewById(R.id.end_add);
        end_main = findViewById(R.id.end_main);
        end_main.setText(getIntent().getStringExtra("text_endereco"));

        end_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                end_add.setEnabled(false);
                startActivityForResult(new Intent(getApplicationContext(), SearchAddressActivity.class), 0);
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);

            }
        });

        requestServiceListAddress.request();
        progressDeleteAddress.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if(bottomSheet!=null){
            bottomSheet.dismiss();
        }
        end_add.setEnabled(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case Activity.RESULT_OK:
                if(bottomSheet!=null) {
                    bottomSheet.dismiss();
                }
                requestServiceListAddress.request();
                progressDeleteAddress.setVisibility(View.VISIBLE);
                end_add.setEnabled(true);
                break;
        }
    }


    public void initializeAdapterListAddress(JSONArray itens, String idSelected) {

        for (int i = 0; i < itens.length(); i++) {
            try {
                if (itens.getJSONObject(i).getString("_id").equals(idSelected)) {
                    alreadyAddress = itens.getJSONObject(i);
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (getIntent().hasExtra("openByCart")) {
            lineAddressListAdapter = new LineAddressListAdapter(new CheckGroup(1, 1), getApplicationContext(), itens, idSelected, true);
            lineAddressListAdapter.disbleDelete = true;
        } else {
            lineAddressListAdapter = new LineAddressListAdapter(new CheckGroup(1, 1), getApplicationContext(), itens, idSelected, false);
        }

        lineAddressListAdapter.setOnClickListenerAddress(new LineAddressListAdapter.OnItemClickListnerAddress() {
            @Override
            public void onItemClick(JSONObject item) {
                try {
                    value = null;
               //     Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_LONG).show();
                    alreadyAddress = item;
                    end_main.setText(String.valueOf(alreadyAddress.getString("titulo")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        lineAddressListAdapter.setOnClickListenerDeleteAddress(new LineAddressListAdapter.OnItemClickListnerDeleteAddress() {
            @Override
            public void onItemClickDelete(JSONObject item) {
          /*      Map<String, String> param = new HashMap<>();
                try {
                    progressDeleteAddress.setVisibility(View.VISIBLE);
                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    param.put("id_endereco", item.getString("_id"));
                    param.put("function_method", "delete");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestServiceDeleteAddress.request(param);
            */

                try {

                    bottomSheet = new AddressBottomSheetDialog(item.getString("_id"), ChangeAddressPopUpActivity.this, item);
                    bottomSheet.show(getSupportFragmentManager(), "opções de endereço");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        });


        listAddress.setAdapter(lineAddressListAdapter);

        final int i = getPosition();
        listAddress.scrollToPosition(i);

    }

    public void deleteByBottomSheet(String idEndereco) {

        Map<String, String> param = new HashMap<>();
        bottomSheet.dismiss();
        progressDeleteAddress.setVisibility(View.VISIBLE);
        param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
        param.put("id_endereco", idEndereco);
        param.put("function_method", "delete");
        requestServiceDeleteAddress.request(param);

    }

    public void openToEditAddress(JSONObject adddress) {

        Intent intent = new Intent(getApplicationContext(), CreateAddressPopUpActivity.class);
        intent.putExtra("editAddress", true);

        try {

//
            String idEndereco = adddress.getString("_id");

            String array[] = new String[2];
            array = adddress.getString("cidade").split("-");

            if(!adddress.isNull("complemento" )) {
                intent.putExtra("complemento", adddress.getString("complemento"));
            }
            if(!adddress.isNull("ponto_ref" )) {
                intent.putExtra("ponto_ref", adddress.getString("ponto_ref"));
            }


            intent.putExtra("bairro", adddress.getString("bairro"));
            intent.putExtra("rua", adddress.getString("logradouro"));
            intent.putExtra("titulo", adddress.getString("titulo"));
            intent.putExtra("numero", adddress.getString("num"));
            intent.putExtra("cep", adddress.getString("cep"));
            intent.putExtra("cidade", array[0]);
            intent.putExtra("uf", array[1]);
            intent.putExtra("id_endereco", idEndereco);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        startActivityForResult(intent, 1);

    }

    private int getPosition() {
        int temp = 0;
        for (int i = 0; i < lineAddressListAdapter.listViewItem.length(); i++) {
            try {
                if (lineAddressListAdapter.listViewItem.getJSONObject(i).getString("_id").equals(getIntent().getStringExtra("id_endereco"))) {
                    temp = i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return temp;
    }

    public void dimissProgress() {
        progressDeleteAddress.setVisibility(View.GONE);
        if (bottomSheet != null) {
            bottomSheet.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), ConfirmItensPedidoActiviy.class);
        intent.putExtra("endereco", String.valueOf(alreadyAddress));
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }


}
