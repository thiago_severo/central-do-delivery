package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.serveapp.serveapp.adapt.LineMensagemAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListMensagens;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.widget.LinearLayout.VERTICAL;

public class ChatActivity extends AppCompatActivity {

    Request requestListMensagens;
    RecyclerView recyclerMensagens;
    public static String idPedido;
    ProgressBar progressBar ;
    ImageView imageButtonSend;
    ImageView imageItemEstChat;
    EditText textMessage;
    TextView nome_estabelecimento_chat;
    ServiceListMensagens serviceListMensagens;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            finish();
        }

    };

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        if(getIntent().hasExtra("id_pedido")) {
            idPedido = getIntent().getStringExtra("id_pedido");
        }

        try{
            if(idPedido==null){
                finish();
            }
        }catch (Exception e){
            finish();
        }

        setContentView(R.layout.activity_chat);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        imageItemEstChat = findViewById(R.id.imageItemEstChat);
        nome_estabelecimento_chat = findViewById(R.id.nome_estabelecimento_chat);
        textMessage = findViewById(R.id.textMessage);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        imageButtonSend = findViewById(R.id.imageButtonSend);
        recyclerMensagens = findViewById(R.id.recyclerMensagens);
        LinearLayoutManager layoutManager  = new LinearLayoutManager(getApplicationContext(), VERTICAL, true);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        layoutManager.setSmoothScrollbarEnabled(true);
        layoutManager.setReverseLayout(true);
         recyclerMensagens.setLayoutManager(layoutManager);
        recyclerMensagens.scrollToPosition(0);

        Map<String, String> param = new HashMap<>();
        serviceListMensagens = new ServiceListMensagens(idPedido,this);
        requestListMensagens = new Request(getApplicationContext(), serviceListMensagens);
        param.put("id_pedido", serviceListMensagens.getId());
        requestListMensagens.request(param);

        imageButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                textMessage.setEnabled(false);
                sendMsg();
            }
        });

    }

    public void updateListMessages(){
        Map<String, String> param = new HashMap<>();
        param.put("id_pedido", serviceListMensagens.getId());
        requestListMensagens.request(param);
    }

    public void initializeChat(JSONObject result) {

        try {
            new ListSetImage(getApplicationContext(), imageItemEstChat).execute(result.getJSONObject("est").getString("foto"));
            nome_estabelecimento_chat.setText(result.getJSONObject("est").getString("nome").toUpperCase());
            recyclerMensagens.setAdapter(new LineMensagemAdapter(getApplicationContext(),result.getJSONArray("msg")));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        recyclerMensagens.scrollToPosition(0);
    }

    public void sucessSend(){
        progressBar.setVisibility(View.GONE);
        textMessage.setEnabled(true);
        textMessage.setText("");
    }

    public void erroEnvio(){
        progressBar.setVisibility(View.GONE);
        textMessage.setEnabled(true);
    }

    public void sendMsg(){
        Map<String, String> param = new HashMap<>();
        param.put("function_method", "sent");
        param.put("id_pedido", serviceListMensagens.getId());
        param.put("mensagem",  textMessage.getText().toString());
        requestListMensagens.request(param);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Repository.activityChatVisible = this;
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (Repository.activityChatVisible == this) {
            Repository.activityChatVisible = null;
        }
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_OK);
        if(getIntent().hasExtra("id_pedido")) {
            Intent intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        super.onBackPressed();
    }

}
