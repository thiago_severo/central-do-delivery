package br.com.serveapp.serveapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.StrictMode;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceUserRegister;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.ValidationUtilField;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;


import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class CreateAccountUserActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    TextInputEditText cepText;
    TextInputEditText ruaText;
    JSONObject obj;
    public Button btn_create_acc;

    private RequestQueue mQueue;
    private Button btnCancel;
    private TextInputEditText textCelular;
    boolean isNome, isSegundoNome, isEmail, isSenha;

    Toolbar toolbar;
    public ProgressDialog mProgress;

    private TextInputEditText txtNome;
    private TextInputEditText txtSobrenome;
    private TextInputEditText txtEmail;
    private TextInputEditText txtSenha;
    private String txtTelefone;
    Request requestRegiterUser;


    private TextInputLayout textInputCdFirstname;
    private TextInputLayout textInputCdSenha;
    private TextInputLayout textInputLayoutEmail;
    private TextInputLayout textInputCdLastname;

    View.OnFocusChangeListener listenerChengeFocus = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if (!hasFocus && v.getId()== R.id.lg_cd_first_name) {
                if(validateName()==false){
                    btn_create_acc.setEnabled(false);
                };
            }else if(!hasFocus && v.getId()==R.id.lg_cd_last_name){
                if(validateLastName()==false){
                    btn_create_acc.setEnabled(false);
                };
            }
            else if(!hasFocus && v.getId()==R.id.cd_dgt_verificador){
                if(validateEmail()==false){
                    btn_create_acc.setEnabled(false);
                }
            }
            else if(!hasFocus && v.getId()==R.id.lg_cd_pass){
                if(validateSenha()==false){
                    btn_create_acc.setEnabled(false);
                }
            }

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account_user);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mProgress =new ProgressDialog(this);
        String titleId="Enviando dados...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Please Wait...");

        txtNome = findViewById(R.id.lg_cd_first_name);
        txtSobrenome = findViewById(R.id.lg_cd_last_name);
        txtEmail = findViewById(R.id.cd_dgt_verificador);
        txtSenha = findViewById(R.id.lg_cd_pass);

        txtNome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputCdFirstname.setError(null);
            }
        });




        txtSobrenome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputCdLastname.setError(null);
            }
        });

        txtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutEmail.setError(null);
            }
        });

        txtSenha.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                textInputCdSenha.setError(null);
            }
        });

        textInputCdFirstname = findViewById(R.id.textInputCdFirstname);
        textInputCdSenha = findViewById(R.id.textInputCdSenha);
        textInputLayoutEmail = findViewById(R.id.textInputLayoutCdVerificador);
        textInputCdLastname = findViewById(R.id.textInputCdLastname);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);
            }
        });

        if(getIntent().hasExtra("num_telefone")){
            txtTelefone =  getIntent().getStringExtra("num_telefone");
        }

        requestRegiterUser = new Request(getApplicationContext(), new ServiceUserRegister(this));

        btn_create_acc = findViewById(R.id.btn_cd_create_account);

        btn_create_acc.setOnClickListener(new View.OnClickListener() {



             @Override
             public void onClick(View v) {

                 btn_create_acc.setEnabled(false);

               if(validateName()==true && validateEmail()==true && validateLastName()==true  && validateSenha()==true ) {
                   mProgress.show();
                   //startCreateAddressActivity();
                   requestRegiterUser.request();
               }else{
                   Toast.makeText(getApplicationContext(), "campos inválidos", Toast.LENGTH_SHORT).show();
                   btn_create_acc.setEnabled(true);
               }
             }
         });

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mQueue = Volley.newRequestQueue(this);
    }

    public final String getHttpGET(String urlToRead) {
        StringBuilder result = new StringBuilder();

        try {
            URL url = new URL(urlToRead);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

        } catch (MalformedURLException | ProtocolException ex) {
            // verifica os Eventos
            ex.printStackTrace();
        } catch (IOException ex) {
            // verifica os Eventos
            ex.printStackTrace();
        }

        return result.toString();
    }

    public static String getToken(Context context) {
        if(Repository.token!=null){
            return Repository.token;
        }
        else {
            Repository.token = Dao.read("token", context);
            return getToken(context);
        }
    }

    public void  buildConfirmDialog(String textMain,  String texto){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView text = mView.findViewById(R.id.textSucessMessage);
        TextView textViewMain = mView.findViewById(R.id.textMainMsg);
        text.setText(texto);
        textViewMain.setText(textMain);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.show();
        btn_create_acc.setEnabled(true);
        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public TextInputEditText getTxtNome() {
        return txtNome;
    }
    public TextInputEditText getTxtSobrenome() {
        return txtSobrenome;
    }
    public TextInputEditText getTxtEmail() {
        return txtEmail;
    }
    public TextInputEditText getTxtSenha() {
        return txtSenha;
    }
    public String getTxtTelefone() {
        return txtTelefone;
    }
    public void startCreateAddressActivity() {

        Intent intent = new Intent(getApplicationContext(), SearchAddressActivity.class);
        intent.putExtra("createUser", "y");
        startActivity(intent);
    }

    private CreateAccountUserActivity getThis(){
        return this;
    }

    private boolean validateName() {
        String usernameInput = txtNome.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputCdFirstname.setError("O campo não pode ficar vazio");
            return false;
        }else if (ValidationUtilField.isValidName(usernameInput)==false) {
            textInputCdFirstname.setError("Nome inválido, insira apenas letras");
            return false;
        } else {
            textInputCdFirstname.setError(null);
            return true;
        }
    }

    private boolean validateLastName() {
        String usernameInput = txtSobrenome.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputCdLastname.setError("O campo não pode ficar vazio");
            return false;
        }else if (ValidationUtilField.isValidName(usernameInput)==false) {
            textInputCdLastname.setError("Nome inválido, insira apenas letras");
            return false;
        } else {
            textInputCdLastname.setError(null);
            return true;
        }
    }

    private boolean validateEmail() {
        String usernameInput = txtEmail.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputLayoutEmail.setError("O campo não pode ficar vazio");
            return false;
        }else if (ValidationUtilField.isValidEmailAddressRegex(usernameInput)==false) {
            textInputLayoutEmail.setError("Email inválido");
            return false;
        } else {
            textInputLayoutEmail.setError(null);
            return true;
        }
    }

    private boolean validateSenha() {
        String usernameInput = txtSenha.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputCdSenha.setError("O campo não pode ficar vazio");
            return false;
        }else if (usernameInput.length()<4) {
            textInputCdSenha.setError("insira pelo menos 4 caracteres");
            return false;
        } else {
            textInputCdSenha.setError(null);
            return true;
        }
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus && v.getId()==R.id.lg_cd_first_name) {
            if(validateName()==false){
                isNome=false;
            }else {
                isNome=true;
            };
        }else if(!hasFocus && v.getId()==R.id.lg_cd_last_name){
            if(validateLastName()==false){
                isSegundoNome = false;
            }else {
                isSegundoNome = true;
            };
        }
        else if(!hasFocus && v.getId()==R.id.cd_dgt_verificador){
            if(validateEmail()==false){
              isEmail = false;
            }else {
                isEmail = true;
            }
        }
        else if(!hasFocus && v.getId()==R.id.lg_cd_pass){
            if(validateSenha()==false){
               isSenha = false;
            }else {
                isSenha = true;
            }
        }
    }
}
