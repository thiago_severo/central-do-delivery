package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.LineAddressListAdapter;
import br.com.serveapp.serveapp.adapt.LineCardAdded;
import br.com.serveapp.serveapp.adapt.LineSelectCard;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.AddressBottomSheetDialog;
import br.com.serveapp.serveapp.util.CheckGroup;


public class MyCardsPaymentActivity extends RequestActivity {

    Button btnCloseItem;

    RecyclerView listMyCards;
    Button btnConfirmAddress;
    Button end_add;
    TextView end_main;
    public ProgressBar progressDeleteAddress;
    NestedScrollView nestedScrollView;
    CardView cardLocation;

    Request requestServiceListAddress;
    Request requestServiceDeleteAddress;
    AddressBottomSheetDialog bottomSheet = null;
    LineAddressListAdapter lineAddressListAdapter;
    LineCardAdded lineCardAdded;
    JSONObject alreadyAddress;
    LinearLayoutManager llm;
    Request serviceFormaPagamento;

    String value;
    private LineSelectCard LineSelectCard;

    AlertDialog dialog;
    public ProgressDialog mProgress;


    @SuppressLint("WrongViewCast")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        if (Repository.getUser(getApplicationContext()) == null) {
            finish();
        }


        setContentView(R.layout.list_user_form_payment);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        progressDeleteAddress = findViewById(R.id.progressIncrement2);
        progressDeleteAddress.setVisibility(View.GONE);

        mProgress = new ProgressDialog(this);
        String titleId = "Excluindo...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Aguardando uma resposta do servidor...");

        listMyCards = findViewById(R.id.end_list);
        llm = new LinearLayoutManager(getApplicationContext());
        listMyCards.setLayoutManager(llm);
        listMyCards.setNestedScrollingEnabled(false);
        listMyCards.setHasFixedSize(false);

        end_add = findViewById(R.id.end_add);
        end_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                end_add.setEnabled(false);
                startActivityForResult(new Intent(getApplicationContext(), AddCardPaymentActivity.class), 0);
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);

            }
        });

        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));

        request(Script.select_forma_pagamento, paramLogin);
        lineCardAdded = null;

        //     lineCardAdded = new LineCardAdded( Repository.listCards, getApplicationContext());
   //     listAddress.setAdapter(lineCardAdded);

    }

    @Override
    public void onResponseServe(String response) {

        mProgress.dismiss();

        if (response != null && !response.equals("null"))
            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                initializeFromasPagamento(result.getJSONArray("credito"), result.getJSONArray("debito"),   result.getJSONArray("cards_cliente"), result.getString("central_pay"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    private void initializeFromasPagamento(JSONArray credito, JSONArray debito, JSONArray cards_cliente, String central_pay) {

        LineSelectCard = new LineSelectCard(cards_cliente, getApplicationContext());
        LineSelectCard.setOnItemClickListner(new LineSelectCard.OnItemClickListner() {
            @Override
            public void onItemClick(boolean selectOnline, JSONObject jsonObject, View selected) {
            }

            @Override
            public void onItemClickDelete(boolean selectOnline, JSONObject card) {
                stabelecimentoClosed(card);
            }
        });

        listMyCards.setAdapter(LineSelectCard);


    }

    private void stabelecimentoClosed(final JSONObject card) {

        //    mProgress.dismiss();
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(MyCardsPaymentActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.delete_credit_card, null);

        mBuilder.setView(mView);
        dialog = mBuilder.show();

        Button btnCancel = mView.findViewById(R.id.btnCancelDeleteCardCredit);
        Button btnConfirmDelete = mView.findViewById(R.id.btnConfirmDeleteCardCredit);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirmDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Map<String, String> paramLogin = new HashMap<>();
                paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
                try {
                    paramLogin.put("id_card", card.getString("_id"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                paramLogin.put("function_method", "remove");
                paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));

                request(Script.select_forma_pagamento, paramLogin);
                dialog.dismiss();
                mProgress.show();
            }
        });

    }


    @Override
    public void onCache(String response) {

        if (response != null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                initializeFromasPagamento(result.getJSONArray("credito"), result.getJSONArray("debito"),   result.getJSONArray("cards_cliente"), result.getString("central_pay"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomSheet != null) {
            bottomSheet.dismiss();
        }
        end_add.setEnabled(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==1){

            Map<String, String> paramLogin = new HashMap<>();

            paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);

            request(Script.select_forma_pagamento, paramLogin);

            //listMyCards.setAdapter(lineCardAdded);
        }

    }


    public void initializeAdapterListAddress(JSONArray itens, String idSelected) {

        for (int i = 0; i < itens.length(); i++) {
            try {
                if (itens.getJSONObject(i).getString("_id").equals(idSelected)) {
                    alreadyAddress = itens.getJSONObject(i);
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        if (getIntent().hasExtra("openByCart")) {
            lineAddressListAdapter = new LineAddressListAdapter(new CheckGroup(1, 1), getApplicationContext(), itens, idSelected, true);
            lineAddressListAdapter.disbleDelete = true;
        } else {
            lineAddressListAdapter = new LineAddressListAdapter(new CheckGroup(1, 1), getApplicationContext(), itens, idSelected, false);
        }

        lineAddressListAdapter.setOnClickListenerAddress(new LineAddressListAdapter.OnItemClickListnerAddress() {
            @Override
            public void onItemClick(JSONObject item) {
                try {
                    value = null;
                    //     Toast.makeText(getApplicationContext(), item.toString(), Toast.LENGTH_LONG).show();
                    alreadyAddress = item;
                    end_main.setText(String.valueOf(alreadyAddress.getString("titulo")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        lineAddressListAdapter.setOnClickListenerDeleteAddress(new LineAddressListAdapter.OnItemClickListnerDeleteAddress() {
            @Override
            public void onItemClickDelete(JSONObject item) {
          /*      Map<String, String> param = new HashMap<>();
                try {
                    progressDeleteAddress.setVisibility(View.VISIBLE);
                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    param.put("id_endereco", item.getString("_id"));
                    param.put("function_method", "delete");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                requestServiceDeleteAddress.request(param);
            */

               /* try {

                    bottomSheet = new AddressBottomSheetDialog(item.getString("_id"), MyCardsPaymentActivity.this, item);
                    bottomSheet.show(getSupportFragmentManager(), "opções de endereço");

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

            }

        });

        listMyCards.setAdapter(lineAddressListAdapter);

        final int i = getPosition();
        listMyCards.scrollToPosition(i);

    }

    public void deleteByBottomSheet(String idEndereco) {

        Map<String, String> param = new HashMap<>();
        bottomSheet.dismiss();
        progressDeleteAddress.setVisibility(View.VISIBLE);
        param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
        param.put("id_endereco", idEndereco);
        param.put("function_method", "delete");
        requestServiceDeleteAddress.request(param);

    }

    public void openToEditAddress(JSONObject adddress) {

        Intent intent = new Intent(getApplicationContext(), CreateAddressPopUpActivity.class);
        intent.putExtra("editAddress", true);

        try {

//
            String idEndereco = adddress.getString("_id");
            String array[] = new String[2];
            array = adddress.getString("cidade").split("-");

            if (!adddress.isNull("complemento")) {
                intent.putExtra("complemento", adddress.getString("complemento"));
            }
            if (!adddress.isNull("ponto_ref")) {
                intent.putExtra("ponto_ref", adddress.getString("ponto_ref"));
            }

            intent.putExtra("bairro", adddress.getString("bairro"));
            intent.putExtra("rua", adddress.getString("logradouro"));
            intent.putExtra("titulo", adddress.getString("titulo"));
            intent.putExtra("numero", adddress.getString("num"));
            intent.putExtra("cep", adddress.getString("cep"));
            intent.putExtra("cidade", array[0]);
            intent.putExtra("uf", array[1]);
            intent.putExtra("id_endereco", idEndereco);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        startActivityForResult(intent, 1);

    }

    private int getPosition() {
        int temp = 0;
        for (int i = 0; i < lineAddressListAdapter.listViewItem.length(); i++) {
            try {
                if (lineAddressListAdapter.listViewItem.getJSONObject(i).getString("_id").equals(getIntent().getStringExtra("id_endereco"))) {
                    temp = i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return temp;
    }

    public void dimissProgress() {
        progressDeleteAddress.setVisibility(View.GONE);
        if (bottomSheet != null) {
            bottomSheet.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        // Intent intent = new Intent(getApplicationContext(), ConfirmItensPedidoActiviy.class);
        // intent.putExtra("endereco", String.valueOf(alreadyAddress));
        // setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }


    @Override
    public void onErrorResponse(VolleyError error) {

        mProgress.dismiss();

    }
}
