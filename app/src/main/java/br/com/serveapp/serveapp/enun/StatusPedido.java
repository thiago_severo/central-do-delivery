package br.com.serveapp.serveapp.enun;

public enum StatusPedido {
    RECEBIDO, PREPARANDO, PRONTO, LEVANDO, ENTREGUE, AVALIADO;
}
