package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentCategoriesProductEstablishment;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListCategoryEstabeleciment extends OnPostResponse {

    FragmentCategoriesProductEstablishment fragmentCategoriesProductEstablishment;
    public ServiceListCategoryEstabeleciment(String id, FragmentCategoriesProductEstablishment fragmentCategoriesProductEstablishment){
        this.setId(id);
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.fragmentCategoriesProductEstablishment = fragmentCategoriesProductEstablishment;
        this.script = "select_categorias_estabelecimento.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(fragmentCategoriesProductEstablishment.getContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
            try {
                JSONArray result = jsonObject.getJSONArray("result");
                fragmentCategoriesProductEstablishment.initializeAdapterCategories(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if(!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                fragmentCategoriesProductEstablishment.initializeAdapterCategories(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}
