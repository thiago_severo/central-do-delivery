package br.com.serveapp.serveapp.dao.webService;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.adapt.LineItemListConfirmPedidoAdapter;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.BaseEstabelecimentActivity;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceRemoveItemCart extends OnPostResponse {

    BaseEstabelecimentActivity listProductsStablishmentDeliveryActivity;
    ImageView imageView;
    Context context;
    TextView textView;
    public JSONArray listItens;
    private int id_item;
    LineItemListConfirmPedidoAdapter lineItemListConfirmPedidoAdapter;
    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;
    public ServiceRemoveItemCart(ConfirmItensPedidoActiviy confirmItensPedidoActiviy, Context context, TextView textView,  int position, LineItemListConfirmPedidoAdapter lineItemListConfirmPedidoAdapter){
      setId(Repository.idEstabelecimento);
        this.context = context;
        this.textView = textView;
        this.lineItemListConfirmPedidoAdapter = lineItemListConfirmPedidoAdapter;
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.id_item = position;
       this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_produtos_do_carrinho_new.php";
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(context, "erro de conexão", Toast.LENGTH_LONG).show();
        confirmItensPedidoActiviy.progressIncrement.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(String response) {
        if (response != null && !response.equals("null"))

            try {
                confirmItensPedidoActiviy.reload(new JSONObject(response));
                confirmItensPedidoActiviy.progressIncrement.setVisibility(View.GONE);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

}