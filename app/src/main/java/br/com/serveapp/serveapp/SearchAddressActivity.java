package br.com.serveapp.serveapp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.android.volley.VolleyError;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.drawer.ListSetImage2;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static androidx.core.os.HandlerCompat.postDelayed;

public class SearchAddressActivity extends RequestActivity implements GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks, View.OnClickListener {

    PlacesClient placesClient;

    private LocationManager locationManager;
    private LocationListener listener;
    private FusedLocationProviderClient fusedLocationClient;
    private ImageView serveLogo;

    LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            useLocation(location);
            //  locationManager.removeUpdates(locationListener);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    LocationListener locationListenerEmpty = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            useLocation(location);
            //  locationManager.removeUpdates(locationListener);

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    int number = 0;
    TextView textLocationGeoCode;
    private LocationRequest mLocationRequest;
    ProgressBar progressIncrement;
    AutocompleteSupportFragment autocompleteSupportFragment;
    CardView cardLocation;
    TextView InsertAddressLater;
    CardView cardInsertLatter;
    boolean alreadyLocation;

    final static int CreateAddressCityActivity = 8;
    final static int CreateAddressPopUpActivity = 9;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_address);

        InsertAddressLater = findViewById(R.id.InsertAddressLater);
        textLocationGeoCode = findViewById(R.id.textLocationGeoCode);
        progressIncrement = findViewById(R.id.progressIncrement);
        cardInsertLatter = findViewById(R.id.cardInsertLatter);
        String apikey = "AIzaSyD2BX1ownrgJDRM7dPDWFbfmkNaQuo60p4";
        cardLocation = findViewById(R.id.cardLocation);
        cardLocation.setEnabled(false);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apikey);
        }

        placesClient = Places.createClient(this);
        autocompleteSupportFragment = (AutocompleteSupportFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentSearch);
        ((EditText) autocompleteSupportFragment.getView().findViewById(R.id.places_autocomplete_search_input)).setTextSize(16.1f);

        autocompleteSupportFragment.setHint("Pesquise por rua, bairro, cep ...");
        autocompleteSupportFragment.setCountry("BR");
        autocompleteSupportFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.ADDRESS, Place.Field.LAT_LNG, Place.Field.NAME));

        autocompleteSupportFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NonNull Place place) {
            }

            @Override
            public void onError(@NonNull Status status) {
            }
        });

        if (autocompleteSupportFragment.isAdded()) {

        }

        if (getIntent().hasExtra("createUser")) {
            InsertAddressLater.setVisibility(View.VISIBLE);
        } else {

            InsertAddressLater.setVisibility(View.GONE);

            Toolbar toolbar = findViewById(
                    R.id.toolbar1);
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

        }

        cardInsertLatter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(), CreateAddressCityAccount.class), CreateAddressCityActivity);
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);
            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (Build.VERSION.SDK_INT >= 24) {
            InsertAddressLater.setText(Html.fromHtml("<u>Inserir um endereço mais tarde</u>", Html.FROM_HTML_MODE_LEGACY));
        } else {
            InsertAddressLater.setText(Html.fromHtml("<u>Inserir um endereço mais tarde</u>"));
        }

        serveLogo = findViewById(R.id.serveLogo);
      //  new ListSetImage2(getApplicationContext(), serveLogo).execute("system/toolbar.png");

        knowNetwork();

    }

    @Override
    public void onResponseServe(String response) {

        pedirPermissao();

    }

    @Override
    public void onCache(String response) {

    }

    @SuppressLint("MissingPermission")
    public void configurarServico() {

        Location network_loc = null;
        Location gps_loc = null;
        Location final_loc;

        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
            //     gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        /*if(!network_enabled) {
           locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListenerEmpty);
        }*/

       /* if(gps_enabled){
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, locationListener);
        }*/

        try {
            network_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            //   gps_loc = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (network_loc != null) {
            final_loc = network_loc;
            useLocation(final_loc);
            //      Toast.makeText(getApplicationContext(), "use last network location", Toast.LENGTH_LONG).show();
        } else {
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, locationListener);
        }

       /* else if (gps_loc != null) {
            final_loc = network_loc;
            useLocation(final_loc);
        //    Toast.makeText(getApplicationContext(), "use last network location", Toast.LENGTH_LONG).show();
        }*/

    }

    public void knowNetwork() {

        final Map<String, String> paramLogin = new HashMap<>();

        paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
        request(Script.list_chat, paramLogin);

    }

    public void pedirPermissao() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, 33);

            } else {
                // Toast.makeText(getApplicationContext(), "serviço conf", Toast.LENGTH_LONG).show();
                //configurarServico();
                userLocation2();
            }
        }

    }

    private void userLocation2() {

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION}, 33);
                    } else {
                        configurarServico();
                    }
                }
            }

        });

        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(SearchAddressActivity.this, 22);
                    } catch (IntentSender.SendIntentException ignored) {
                    }
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        locationManager = null;

        if (getIntent().hasExtra("requestForAddressNull")) {
            finish();
            NavigationDrawerActivity.refference.finish();
        }

        super.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    textLocationGeoCode.setText("a permissão para o acesso à localização foi negada");
                    progressIncrement.setVisibility(View.GONE);
                } else {
                    userLocation2();
                }
            }
        } else {

            textLocationGeoCode.setText("A permissão para o acesso à localização foi negada!");
            progressIncrement.setVisibility(View.GONE);

        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == CreateAddressCityActivity) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    finish();
                    break;
            }
        } else if (requestCode == 22) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    configurarServico();
                    break;
                case Activity.RESULT_CANCELED:
                    progressIncrement.setVisibility(View.GONE);
                    textLocationGeoCode.setText("O acesso à localização está desabilitado!");
                    break;
            }

        } else if (requestCode == CreateAddressPopUpActivity) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    try {
                        data.putExtra("id_endereco", Repository.getUser(getApplicationContext()).getString("id_endereco"));
                        setResult(Activity.RESULT_OK, data);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    finish();
                    break;
                case Activity.RESULT_CANCELED:
                    break;

            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);

            if (resultCode == Activity.RESULT_OK) {

                Place place = Autocomplete.getPlaceFromIntent(data);
                br.com.serveapp.serveapp.util.Address address = br.com.serveapp.serveapp.util.Address.factory(place.getAddress());
                parseLatLgn(address);

            } else if (resultCode == Activity.RESULT_CANCELED) {

            }

        }

    }

    public void parseLatLgn(br.com.serveapp.serveapp.util.Address address) {

        Intent intentAddAddress = null;

        if (getIntent().hasExtra("createUser")) {
            intentAddAddress = new Intent(getApplicationContext(), CreateAddressAccountActivity.class);
        } else {
            intentAddAddress = new Intent(getApplicationContext(), CreateAddressPopUpActivity.class);
        }

        intentAddAddress.putExtra("rua", address.getRua());
        intentAddAddress.putExtra("bairro", address.getBairro());
        intentAddAddress.putExtra("cidade", address.getCidade());
        intentAddAddress.putExtra("cep", address.getCep());
        intentAddAddress.putExtra("uf", address.getUf());

        startActivityForResult(intentAddAddress, CreateAddressPopUpActivity);
        overridePendingTransition(R.anim.entrer_from_right,
                R.anim.exit_to_left);

    }


    private void useLocation(Location location) {

        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null) {

            JSONObject user = Repository.getUser(getApplicationContext());
            JSONObject lastKnowLocation = new JSONObject();

            try {

                lastKnowLocation.put("rua", addresses.get(0).getThoroughfare());
                lastKnowLocation.put("numero", addresses.get(0).getFeatureName());
                lastKnowLocation.put("bairro", addresses.get(0).getSubLocality());
                lastKnowLocation.put("cidade", addresses.get(0).getSubAdminArea());
                lastKnowLocation.put("estado", addresses.get(0).getAdminArea());
                lastKnowLocation.put("cep", addresses.get(0).getPostalCode());

                user.put("lastKnowLocation", lastKnowLocation);
                Dao.save(user.toString(), FilesName.user, getApplicationContext());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (alreadyLocation == false) {

                textLocationGeoCode.setText(
                        getStringLocation(addresses));
                alreadyLocation = true;
                progressIncrement.setVisibility(View.GONE);
                cardLocation.setEnabled(true);
                final List<Address> finalAddresses = addresses;

                cardLocation.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (finalAddresses != null) {

                            Intent intentAddAddress = null;

                            if (getIntent().hasExtra("createUser")) {
                                intentAddAddress = new Intent(getApplicationContext(), CreateAddressAccountActivity.class);
                            } else {
                                intentAddAddress = new Intent(getApplicationContext(), CreateAddressPopUpActivity.class);
                            }

                            intentAddAddress.putExtra("rua", finalAddresses.get(0).getThoroughfare());
                            intentAddAddress.putExtra("bairro", finalAddresses.get(0).getSubLocality());
                            intentAddAddress.putExtra("cidade", finalAddresses.get(0).getSubAdminArea());
                            intentAddAddress.putExtra("cep", finalAddresses.get(0).getPostalCode());
                            intentAddAddress.putExtra("uf", finalAddresses.get(0).getAdminArea());

                            startActivityForResult(intentAddAddress, CreateAddressPopUpActivity);
                            overridePendingTransition(R.anim.entrer_from_right,
                                    R.anim.exit_to_left);

                        }


                    }
                });

            }

        } else {

            if (alreadyLocation == false) {

                if (!Repository.getUser(getApplicationContext()).isNull("lastKnowLocation")) {

                    try {

                        cardLocation.setEnabled(true);
                        textLocationGeoCode.setText(getStringLocationJson(Repository.getUser(getApplicationContext()).getJSONObject("lastKnowLocation")));
                        progressIncrement.setVisibility(View.GONE);

                        cardLocation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                Intent intentAddAddress = null;

                                if (getIntent().hasExtra("createUser")) {
                                    intentAddAddress = new Intent(getApplicationContext(), CreateAddressAccountActivity.class);
                                } else {
                                    intentAddAddress = new Intent(getApplicationContext(), CreateAddressPopUpActivity.class);
                                }

                                try {

                                    intentAddAddress.putExtra("rua", Repository.getUser(getApplicationContext()).getJSONObject("lastKnowLocation").getString("rua"));
                                    intentAddAddress.putExtra("bairro", Repository.getUser(getApplicationContext()).getJSONObject("lastKnowLocation").getString("bairro"));
                                    intentAddAddress.putExtra("cidade", Repository.getUser(getApplicationContext()).getJSONObject("lastKnowLocation").getString("cidade"));
                                    intentAddAddress.putExtra("cep", Repository.getUser(getApplicationContext()).getJSONObject("lastKnowLocation").getString("cep"));
                                    intentAddAddress.putExtra("uf", Repository.getUser(getApplicationContext()).getJSONObject("lastKnowLocation").getString("estado"));

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                startActivityForResult(intentAddAddress, CreateAddressPopUpActivity);
                                overridePendingTransition(R.anim.entrer_from_right,
                                        R.anim.exit_to_left);

                            }

                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    textLocationGeoCode.setText("Não foi possivel obter sua localização. Pesquise por rua bairro ou cep");
                    progressIncrement.setVisibility(View.GONE);
                    cardLocation.setEnabled(false);

                }

            }

        }

        if (locationManager != null)
            locationManager.removeUpdates(locationListener);

    }

    private String getStringLocation(List<Address> addresses) {

        String locationAll = "";

        String rua = addresses.get(0).getThoroughfare();
        String numero = addresses.get(0).getFeatureName();
        String bairro = addresses.get(0).getSubLocality();
        String cidade = addresses.get(0).getSubAdminArea();
        String estado = addresses.get(0).getAdminArea();
        String cep = addresses.get(0).getPostalCode();

        if (rua != null && !rua.equals("null")) {
            locationAll += rua + ", ";
        }
        if (numero != null && !numero.equals("null")) {
            locationAll += numero + " - ";
        }
        if (bairro != null && !bairro.equals("null")) {
            locationAll += bairro + ", ";
        }
        if (cidade != null && !cidade.equals("null")) {
            locationAll += cidade + " - ";
        }
        if (estado != null && !estado.equals("null")) {
            locationAll += estado + ", ";
        }
        if (cep != null && !cep.equals("null")) {
            locationAll += cep + ".";
        }

        return locationAll;
    }


    private String getStringLocationJson(JSONObject addresses) {

        String locationAll = "";

        String rua = null;
        String numero = null;
        String bairro = null;
        String cidade = null;
        String estado = null;
        String cep = null;

        try {
            rua = addresses.getString("rua");
            numero = addresses.getString("numero");
            bairro = addresses.getString("bairro");
            cidade = addresses.getString("cidade");
            estado = addresses.getString("estado");
            cep = addresses.getString("cep");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (rua != null && !rua.equals("null")) {
            locationAll += rua + ", ";
        }
        if (numero != null && !numero.equals("null")) {
            locationAll += numero + " - ";
        }
        if (bairro != null && !bairro.equals("null")) {
            locationAll += bairro + ", ";
        }
        if (cidade != null && !cidade.equals("null")) {
            locationAll += cidade + " - ";
        }
        if (estado != null && !estado.equals("null")) {
            locationAll += estado + ", ";
        }
        if (cep != null && !cep.equals("null")) {
            locationAll += cep + ".";
        }

        return locationAll;
    }


    @Override
    public void onClick(View view) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {

        super.onResume();
        number = 0;
    }


    @Override
    public void onErrorResponse(VolleyError error) {

        textLocationGeoCode.setText("Erro de conexão. Não foi possivel obter sua localização. Pesquise por rua bairro ou cep");
        progressIncrement.setVisibility(View.GONE);
        cardLocation.setEnabled(false);

    }
}
