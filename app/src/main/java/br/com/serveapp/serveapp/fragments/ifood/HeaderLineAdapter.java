package br.com.serveapp.serveapp.fragments.ifood;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;

public class HeaderLineAdapter extends RecyclerView.Adapter<HeaderLine> {

    private JSONArray mHeades;
    LinearLayoutManager bodyLayout;
    LinearLayoutManager headerLayout;

    public HeaderLineAdapter(JSONArray mHeades, LinearLayoutManager bodyLayout, LinearLayoutManager headerLayout) {
        this.mHeades = mHeades;
        this.bodyLayout = bodyLayout;
        this.headerLayout = headerLayout;
        HeaderLineGroup.inicialize();
    }

    @Override
    public HeaderLine onCreateViewHolder(ViewGroup parent, int viewType) {

        return new HeaderLine(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_categorie_itens_scroll_menu, parent, false), bodyLayout, headerLayout);
    }

    @Override
    public void onBindViewHolder(HeaderLine holder, final int position) {
        try {
            holder.initialize(mHeades.getJSONObject(position), position);
            HeaderLineGroup.add(holder);
            if(position == 0){
                holder.selected();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        mHeades = mHeades;
        return mHeades != null ? mHeades.length() : 0;
    }

}
