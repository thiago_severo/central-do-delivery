package br.com.serveapp.serveapp;

import android.os.Bundle;

import br.com.serveapp.serveapp.activity.local.TabsPagerAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceDeatlhesEstabelecimento;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.enun.SeloCliente;
import br.com.serveapp.serveapp.fragment.local.FragmentListDemandsLocal;
import br.com.serveapp.serveapp.fragments.FragmentListHorarios;
import br.com.serveapp.serveapp.fragments.FragmentMap;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;
import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import static br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity.RESULT_SCROLLING_ACTIVITY;
import static br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity.RESULT_UPDATE_CAR_VALUE;

public class ScrollingActivity extends AppCompatActivity {

    Request request;
    private TextView textNameScroll;
    private ImageView ImgSeloClient;
    private TextView textCategoriaEstab;
    private CardView cardDetalheFrete;
    private TextView textStatusLocal;
    private TextView textViewTelefoneEstab;
    private TextView txt_status_estabelecimento_delivery;
    private TextView textCliente;
    private CardView card_aceita_cartao;
    private ImageView logoScroll;
    private ImageView imageFreteScroll;
    private TabsPagerAdapter tabePageAdapter;
    public ViewPager viewPager;
    TextView textDistancia;
    ImageView imgLogoEstabelecimentoScroll;
    RatingBar ratingBar;
    TextView textViewDescricaoEst;
    TextView frete_detalhado;
    TextView frete_valor;
    TextView textNIvelDesc;
    TextView QtdItensLocal;
    TextView QtdItensDelivery;
    TextView qtdPontos;
    CardView cardSelo;

    FragmentListHorarios tabListHorarios;
    TabLayout tabLayout;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            finish();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        try{
            if(Repository.getIdUsuario(getApplicationContext())==null || Repository.idEstabelecimento==null){
                finish();
            }
        }catch (Exception e){
            finish();
        }

        setContentView(R.layout.activity_scrolling);

        Toolbar toolbar = (Toolbar) findViewById(R.id.collapsing_toolbar);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_SCROLLING_ACTIVITY);
                onBackPressed();
            }
        });


        imageFreteScroll = findViewById(R.id.imageFreteScroll);
        textNIvelDesc = findViewById(R.id.textNIvelDesc);
        card_aceita_cartao = findViewById(R.id.card_aceita_cartao);
        imgLogoEstabelecimentoScroll = findViewById(R.id.ImgSeloClient);
        textDistancia = findViewById(R.id.distancia);
        textCliente = findViewById(R.id.textCliente);
        QtdItensDelivery = findViewById(R.id.QtdItensDelivery);
        QtdItensLocal = findViewById(R.id.QtdItensLocal);
        textCategoriaEstab = findViewById(R.id.textCategoriaEstab);

        ImgSeloClient = findViewById(R.id.ImgSelo);
        cardSelo = findViewById(R.id.cardSelo);
        qtdPontos = findViewById(R.id.qtdPontos);
        frete_valor = findViewById(R.id.val_frete);
        cardDetalheFrete = findViewById(R.id.cardDetalheFrete);
        frete_detalhado = findViewById(R.id.detalheFrete);
        textViewDescricaoEst = findViewById(R.id.desc_estab2);
        textStatusLocal = findViewById(R.id.textStatusLocal);
        txt_status_estabelecimento_delivery = findViewById(R.id.txt_status_estabelecimento_delivery);
        ratingBar = findViewById(R.id.ratingEstabeleciomento);
        textNameScroll = findViewById(R.id.textScrolName);
        textViewTelefoneEstab = findViewById(R.id.textViewTelefoneEstab);

        tabePageAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        tabLayout = findViewById(R.id.tabScroll);
        viewPager = (ViewPager) findViewById(R.id.pagerScroll);

        request = new Request(getApplicationContext(), new ServiceDeatlhesEstabelecimento(this));
        request.request();

    }

    public void setEstabelecimento(JSONObject estabelecimento) {
        try {

            tabePageAdapter = new TabsPagerAdapter(getSupportFragmentManager());
            textNameScroll.setText(estabelecimento.getString("nome"));
            textCategoriaEstab.setText(estabelecimento.getString("tipo"));
            textViewTelefoneEstab.setText(estabelecimento.getString("telefone"));

            new ListSetImage(getApplicationContext(), imgLogoEstabelecimentoScroll).execute(estabelecimento.getString("foto"));

            if (!estabelecimento.isNull("avaliacao")) {
                ratingBar.setRating(Float.parseFloat(estabelecimento.getString("avaliacao")));
                ratingBar.setAlpha(1.0f);
            } else {
                ratingBar.setRating(5);
                ratingBar.setAlpha(0.5f);
            }

            if (!estabelecimento.isNull("ped_local"))
                QtdItensLocal.setText(estabelecimento.getString("ped_local"));

            if (!estabelecimento.isNull("ped_delivery"))
                QtdItensDelivery.setText(estabelecimento.getString("ped_delivery"));

            qtdPontos.setText(estabelecimento.getString("pontos"));
            textViewDescricaoEst.setText(estabelecimento.getString("descricao"));

            if (estabelecimento.getString("distancia") == null || estabelecimento.getString("distancia").equals("null")) {
                textDistancia.setVisibility(View.GONE);
            } else
                textDistancia.setText(estabelecimento.getString("distancia"));


            frete_valor.setText(estabelecimento.getString("frete"));
            frete_detalhado.setText(estabelecimento.getString("frete_detalhado"));

            if (estabelecimento.getInt("tipoAtendimento") == 2) {
                textStatusLocal.setVisibility(View.GONE);
            } else {
                if (estabelecimento.getJSONObject("status_local").getString("aberto").equals("0")) {
                    textStatusLocal.setText("LOCAL : FECHADO");
                }
            }

            if (estabelecimento.getInt("tipoAtendimento") == 1) {

                txt_status_estabelecimento_delivery.setVisibility(View.GONE);
                frete_valor.setVisibility(View.GONE);
                cardDetalheFrete.setVisibility(View.GONE);
                imageFreteScroll.setVisibility(View.GONE);

            } else {
                if (estabelecimento.getJSONObject("status_delivery").getString("aberto").equals("0")) {
                    txt_status_estabelecimento_delivery.setText("DELIVERY : FECHADO");
                }
            }

            if (estabelecimento.getInt("tipoAtendimento") == 3) {
                if (estabelecimento.getJSONObject("status_local").getString("aberto").equals("0")) {
                    textStatusLocal.setText("LOCAL : FECHADO");
                }
                if (estabelecimento.getJSONObject("status_delivery").getString("aberto").equals("0")) {
                    txt_status_estabelecimento_delivery.setText("DELIVERY : FECHADO");
                }
            }

            if (estabelecimento.getString("aceita_cartao").equals("0")) {
                card_aceita_cartao.setVisibility(View.GONE);
            }

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                getSupportFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
            }

            Repository.listHorarios = estabelecimento.getJSONArray("horarios");
            tabListHorarios = new FragmentListHorarios(this, Repository.listHorarios, estabelecimento.getInt("tipoAtendimento"));
            FragmentListDemandsLocal tabPedido1 = new FragmentListDemandsLocal(this);
            FragmentMap fragmentMap = new FragmentMap(estabelecimento.getString("latitude"), estabelecimento.getString("longitude"), estabelecimento.getString("endereco"));


            tabePageAdapter.adicionar(fragmentMap, "Endereço");
            tabePageAdapter.adicionar(tabListHorarios, "Horários");
            viewPager.setAdapter(tabePageAdapter);
            tabLayout.setupWithViewPager(viewPager);

            if (!estabelecimento.isNull("n_cliente")) {

                if (estabelecimento.getJSONObject("n_cliente").getString("descricao").equals(SeloCliente.PRATA.toString().toLowerCase()))
                    ImgSeloClient.setBackgroundResource(R.drawable.silver_icon);
                if (estabelecimento.getJSONObject("n_cliente").getString("descricao").equals(SeloCliente.BRONZE.toString().toLowerCase()))
                    ImgSeloClient.setBackgroundResource(R.drawable.bronze_icon);
                if (estabelecimento.getJSONObject("n_cliente").getString("descricao").equals(SeloCliente.OURO.toString().toLowerCase()))
                    ImgSeloClient.setBackgroundResource(R.drawable.gold_icon);
                if (estabelecimento.getJSONObject("n_cliente").getString("descricao").equals(SeloCliente.DIAMANTE.toString().toLowerCase()))
                    ImgSeloClient.setBackgroundResource(R.drawable.diamante_icon);

                textNIvelDesc.setText(estabelecimento.getJSONObject("n_cliente").getString("descricao"));

            } else {
                cardSelo.setVisibility(View.GONE);
                textNIvelDesc.setVisibility(View.GONE);
                textCliente.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}