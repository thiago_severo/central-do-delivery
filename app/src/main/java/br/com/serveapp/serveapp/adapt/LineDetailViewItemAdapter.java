package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.pedido.GroupItemPedido;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.CheckGroup;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static br.com.serveapp.serveapp.adapt.DetalheItemAdapter.VIEW_TYPE_CHECK_BOX;
import static br.com.serveapp.serveapp.adapt.DetalheItemAdapter.VIEW_TYPE_RADIO;

public class LineDetailViewItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    private LineDetailViewItemAdapter.onItemClickListener listener;
    private LineDetailViewItemAdapter.onItemClickListenerCheck listenerCheck;
    private Context context;
    private int qtdMax;
    private int qtdMin;
    public int mSelectedItem = -1;
    int count =0;

    public interface onItemClickListener {
        void onItemClick(JSONObject categoria);
    }

    public interface onItemClickListenerCheck {
        void onItemClickCheck(JSONObject categoria, boolean check, int count);
    }
    GroupItemPedido groupItemPedido;
    CheckGroup checkGroup;
    String option;

    int countItensPreSelected = 0;

    JSONArray listTotalItensSelecteds;
    JSONArray listItensPreSelecteds;

    public LineDetailViewItemAdapter(String option, CheckGroup checkGroup, GroupItemPedido groupItemPedido, int qtdMax, int qtdMin, Context context, JSONArray listViewItem, JSONArray listItensPreSelecteds,
            JSONArray listTotalItensSelecteds
    ){

        this.listTotalItensSelecteds = listTotalItensSelecteds;
        this.listItensPreSelecteds = listItensPreSelecteds;
        this.option = option;
        this.groupItemPedido = groupItemPedido;
        this.checkGroup = checkGroup;
        this.qtdMax =qtdMax;
        this.qtdMin =qtdMin;
        this.context = context;
        this.listViewItem = listViewItem;

        if(option==null)
        for(int i =0; i<listViewItem.length(); i++) {
            try {
                if (listViewItem.getJSONObject(i).getInt("def") == 1) {
                    Repository.itens.add(listViewItem.getJSONObject(i).getInt("id_item"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        int returnType = 0;

            if (qtdMax == 1 && qtdMin == 1){ returnType = VIEW_TYPE_RADIO ; }
            else { returnType =  VIEW_TYPE_CHECK_BOX ;
                groupItemPedido.itemSelectedsCheck = null;
            }
        return  returnType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
        if(viewType== VIEW_TYPE_CHECK_BOX) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_item_product_check, parent, false);
             lineSimpleItemHolder = new LineSimpleItemHolderCheck(v);
        }else if(viewType== VIEW_TYPE_RADIO) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_item_product_radio, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolderRadio(v);
       }
            return lineSimpleItemHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if(holder instanceof LineSimpleItemHolderCheck) {
                Integer id = listViewItem.getJSONObject(position).getInt("id_item");
                if(checkGroup!=null){
                    checkGroup.add(((LineSimpleItemHolderCheck) holder).checkBox);
                }
                if(option==null) {
                    if (listViewItem.getJSONObject(position).getInt("def") == 1) {
                        ((LineSimpleItemHolderCheck) holder).checkBox.setChecked(true);
                        checkGroup.setChecked(((LineSimpleItemHolderCheck) holder).checkBox);
                        Repository.itens.add(id);
                    }
                }
                else{
                    if ( Repository.itens.contains(listViewItem.getJSONObject(position).getInt("id_item"))) {
                        ((LineSimpleItemHolderCheck) holder).checkBox.setChecked(true);
                        checkGroup.setChecked(((LineSimpleItemHolderCheck) holder).checkBox);
                        Repository.itens.add(id);
                    }
                }

                if(listViewItem.getJSONObject(position).getInt("disponivel")==0){
                    ((LineSimpleItemHolderCheck) holder).checkBox.setEnabled(false);
                }

                ((LineSimpleItemHolderCheck) holder).txt_nome_item.setText(listViewItem.getJSONObject(position).getString("nome"));
                ((LineSimpleItemHolderCheck) holder).lineExtra.setText(listViewItem.getJSONObject(position).getString("extra"));
                ((LineSimpleItemHolderCheck) holder).txt_nome_item.setTextColor(context.getResources().getColor(R.color.color));

                if( listViewItem.getJSONObject(position).isNull("detalhe") || listViewItem.getJSONObject(position).getString("detalhe").trim().length()==0  ){
                    ((LineSimpleItemHolderCheck) holder).txt_detalhe.setVisibility(View.GONE);
                }
                else{
                   // ((LineSimpleItemHolderCheck) holder).txt_detalhe.setVisibility(View.VISIBLE);
                    ((LineSimpleItemHolderCheck) holder).txt_detalhe.setText(listViewItem.getJSONObject(position).getString("detalhe")); }

            }
            else if(holder instanceof LineSimpleItemHolderRadio){

                if(checkGroup!=null){
                    checkGroup.add(((LineSimpleItemHolderRadio) holder).radioButton);
                }
                if(option==null) {
                    if (listViewItem.getJSONObject(position).getInt("def") == 1) {
                        ((LineSimpleItemHolderRadio) holder).radioButton.setChecked(true);
                        checkGroup.setChecked(((LineSimpleItemHolderRadio) holder).radioButton);
                    }
                }else{
                    if ( Repository.itens.contains(listViewItem.getJSONObject(position).getInt("id_item"))) {
                        ((LineSimpleItemHolderRadio) holder).radioButton.setChecked(true);
                        checkGroup.setChecked(((LineSimpleItemHolderRadio) holder).radioButton);
                    }
                }

                if(listViewItem.getJSONObject(position).getInt("disponivel")==0){
                    ((LineSimpleItemHolderRadio) holder).radioButton.setEnabled(false);
                }

                ((LineSimpleItemHolderRadio) holder).txt_nome_item.setText(listViewItem.getJSONObject(position).getString("nome"));
                ((LineSimpleItemHolderRadio) holder).lineExtra.setText(listViewItem.getJSONObject(position).getString("extra"));
                ((LineSimpleItemHolderRadio) holder).txt_nome_item.setTextColor(context.getResources().getColor(R.color.color));

                if( listViewItem.getJSONObject(position).isNull("detalhe") || listViewItem.getJSONObject(position).getString("detalhe").trim().length()==0  ){
                    ((LineSimpleItemHolderRadio) holder).txt_detalhe.setVisibility(View.GONE);
                } else{ ((LineSimpleItemHolderRadio) holder).txt_detalhe.setText(listViewItem.getJSONObject(position).getString("detalhe")); }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public void setListener(LineDetailViewItemAdapter.onItemClickListener listener) {
        this.listener = listener;
    }

    public void setListenerCheck(LineDetailViewItemAdapter.onItemClickListenerCheck listenerCheck) {
        this.listenerCheck = listenerCheck;
    }

    public interface OnItemClickListner{
        void onItemClick(JSONObject item);
    }

    class LineSimpleItemHolderCheck extends RecyclerView.ViewHolder {

    public TextView txt_nome_item;
    public TextView lineExtra;
    public CheckBox checkBox;
    public Integer id;
    public TextView txt_detalhe;


    public LineSimpleItemHolderCheck(final View itemView) {
        super(itemView);
        txt_nome_item = itemView.findViewById(R.id.lineTextValueCupom);
        lineExtra = itemView.findViewById(R.id.lineExtra);
        checkBox = itemView.findViewById(R.id.lineCheck);
        txt_detalhe = itemView.findViewById(R.id.detalheItem);

        checkBox.setOnCheckedChangeListener(
              new CompoundButton.OnCheckedChangeListener() {
                  @Override
                  public void onCheckedChanged(CompoundButton compoundButton, boolean check) {
                      final int pos = getAdapterPosition();
                      try {
                          if(check==false) {
                              listenerCheck.onItemClickCheck(listViewItem.getJSONObject(pos), false, count );
                              Repository.itens.remove(listViewItem.getJSONObject(pos).getInt("id_item"));
                          }
                          else if(check==true){
                              count++;
                              listenerCheck.onItemClickCheck(listViewItem.getJSONObject(pos), true, count);
                              Repository.itens.add(listViewItem.getJSONObject(pos).getInt("id_item"));
                              listTotalItensSelecteds.put(listViewItem.getJSONObject(pos));
                          }
                      }catch (JSONException jsonException){

                      }

              }

        }
        );

    }

    }

    class LineSimpleItemHolderRadio extends RecyclerView.ViewHolder {

        public boolean checked =false;
        public TextView txt_nome_item;
        public TextView lineExtra;
        public RadioButton radioButton;
        public TextView txt_detalhe;

        public LineSimpleItemHolderRadio(View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.lineRadio);
            txt_nome_item = itemView.findViewById(R.id.lineTextValueCupom);
            lineExtra = itemView.findViewById(R.id.lineExtra);
            txt_detalhe = itemView.findViewById(R.id.detalheItem);
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean check) {
                    final int pos = getAdapterPosition();
                    try {
                        if(check==false) {
                           }
                        else if(check==true){
                            listener.onItemClick(listViewItem.getJSONObject(pos));
                        }
                    }catch (JSONException jsonException){

                    }
                }
            });

        }
    }

}