package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import br.com.serveapp.serveapp.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;

public class SheetBehavior extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private Button mButton1, mButton2, mButton3;
    private BottomSheetBehavior mBottomSheetBehavior1, mBottomSheetBehavior2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sheet_behavior);

            View bottomSheet = findViewById(R.id.bottom_sheet1);
            mBottomSheetBehavior1 = BottomSheetBehavior.from(bottomSheet);

            mButton1 = (Button) findViewById(R.id.button_1);
            mButton1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mBottomSheetBehavior1.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_EXPANDED);
                        mButton1.setText("collapse_button1");
                    }
                    else {
                        mBottomSheetBehavior1.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        mButton1.setText("button1");
                    }
                }
            });

            final View bottomSheet2 = findViewById(R.id.bottom_sheet2);
            mBottomSheetBehavior2 = BottomSheetBehavior.from(bottomSheet2);
            mBottomSheetBehavior2.setHideable(true);
            mBottomSheetBehavior2.setPeekHeight(300);
            mBottomSheetBehavior2.setState(BottomSheetBehavior.STATE_HIDDEN);

            mButton2 = (Button) findViewById(R.id.button_2);
            mButton2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mBottomSheetBehavior2.getState() == BottomSheetBehavior.STATE_EXPANDED) {
                        mBottomSheetBehavior2.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        mButton2.setText("button2_hide");
                    }
                    else if(mBottomSheetBehavior2.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
                        mBottomSheetBehavior2.setState(BottomSheetBehavior.STATE_HIDDEN);
                        mButton2.setText("R.string.button2");
                    }
                    else if(mBottomSheetBehavior2.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                        mBottomSheetBehavior2.setState(BottomSheetBehavior.STATE_EXPANDED);
                        mButton2.setText("R.string.button2_peek");
                    }
                }
            });

            mBottomSheetBehavior2.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        mButton2.setText("R.string.button2_peek");
                    }
                    else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                        mButton2.setText("R.string.button2_hide");
                    }
                    else if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        mButton2.setText("R.string.button2");
                    }
                }

                @Override
                public void onSlide(View bottomSheet, float slideOffset) {
                }
            });


        }

    }

