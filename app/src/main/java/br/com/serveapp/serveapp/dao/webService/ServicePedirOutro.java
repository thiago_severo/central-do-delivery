package br.com.serveapp.serveapp.dao.webService;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentComanda;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServicePedirOutro extends OnPostResponse {

    FragmentComanda fragmentComanda;
    Context context;

    public ServicePedirOutro(Context context) {
        this.context = context;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "pedir_outro.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(context, "erro de conexão", Toast.LENGTH_LONG).show();
    }

        @Override
        public void onResponse (String response){
            if (response != null && !response.equals("null"))
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject result = jsonObject.getJSONObject("result");
                    ListProductsStablishmentDeliveryActivity.refference.showMsg(result.getString("msg"), result.getString("titulo"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

        @Override
        public void onUpdate (String response){

        }

    }
