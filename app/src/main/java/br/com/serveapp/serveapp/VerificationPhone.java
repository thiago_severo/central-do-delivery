package br.com.serveapp.serveapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import br.com.serveapp.serveapp.util.MaskEditUtil;

public class VerificationPhone extends AppCompatActivity {

    TextInputEditText textPhoneInput;
    TextInputEditText textCodeGenerated;
    Button genCode;
    String mVerificationId;

    //Add it below the lines where you declared the fields
    private FirebaseAuth mAuth;
    private Button buttonVerifiyCode;

    TextInputEditText maskedEditText;
    TextInputLayout textInputLayoutPhone;
    String phoneNumber;

    public ProgressDialog mProgressCreateAddress;

//Add it in the onCreate method, after calling method initFields()

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_phone);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        textInputLayoutPhone = findViewById(R.id.textInputLayoutPhone);
        genCode = findViewById(R.id.genCode);
        textPhoneInput = findViewById(R.id.textPhoneInput);
        mAuth = FirebaseAuth.getInstance();

        mProgressCreateAddress = new ProgressDialog(VerificationPhone.this);
        String titleCreateAddress = "Enviando código via SMS...";
        mProgressCreateAddress.setTitle(titleCreateAddress);
        mProgressCreateAddress.setMessage("Aguarde...");

        maskedEditText = findViewById(R.id.textPhoneInput);

        if(getIntent().hasExtra("phoneInput")){
            maskedEditText.setText(getIntent().getStringExtra("phoneInput"));
        }

        int position = maskedEditText.length();
        Editable etext = maskedEditText.getText();
        Selection.setSelection(etext, position);

        maskedEditText.addTextChangedListener(MaskEditUtil.mask(maskedEditText, MaskEditUtil.FORMAT_FONE, getApplicationContext()));
        maskedEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
              //    validatePhone();
                  textInputLayoutPhone.setError(null);
            }
        });

        genCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validatePhone() == true) {

                    genCode.setEnabled(false);
                    mProgressCreateAddress.show();

                    phoneNumber = "+55" + textPhoneInput.getText().toString();
                    PhoneAuthProvider.getInstance().verifyPhoneNumber(
                            "+55" + textPhoneInput.getText().toString(),        // Phone number to verify
                            60,                 // Timeout duration
                            TimeUnit.SECONDS,   // Unit of timeout
                            VerificationPhone.this,               // Activity (for callback binding)
                            mCallbacks);        // OnVerificationStateChangedCallbacks



                }

            }
        });

      /*  buttonVerifiyCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, textCodeGenerated.getText().toString());
                signInWithPhoneAuthCredential(credential);

            }
        });*/

    }

    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {

            Toast.makeText(getApplicationContext(), "onVerificationCompleted", Toast.LENGTH_LONG).show();

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {


            if (e instanceof FirebaseAuthInvalidCredentialsException) {

                genCode.setEnabled(true);
                mProgressCreateAddress.dismiss();

            } else if (e instanceof FirebaseTooManyRequestsException) {

                genCode.setEnabled(true);
                mProgressCreateAddress.dismiss();
                Toast.makeText(getApplicationContext(), "Um código de verificação já foi enviado, Tente Novamente, mais tarde!", Toast.LENGTH_LONG).show();

            }else{

                Toast.makeText(getApplicationContext(), "verifique sua conexão com a internet", Toast.LENGTH_LONG).show();
                genCode.setEnabled(true);
                mProgressCreateAddress.dismiss();
            }

        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {

            mVerificationId = verificationId;
            PhoneAuthProvider.ForceResendingToken mResendToken = token;

            mProgressCreateAddress.dismiss();

            phoneNumber = "+55" + textPhoneInput.getText().toString();

            Intent intent = new Intent(getApplicationContext(), ConfirmCodeSms.class);
            intent.putExtra("VerificationId", mVerificationId );
            intent.putExtra("phoneNumber", phoneNumber);

            startActivityForResult(intent , 1);

        }

    };

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                            Toast.makeText(getApplicationContext(), "signIn:success", Toast.LENGTH_LONG).show();
                            FirebaseUser user = task.getResult().getUser();

                        } else {

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                Toast.makeText(getApplicationContext(), "Código invalido! aguarde para solictar um novo!", Toast.LENGTH_LONG).show();
                            }

                        }
                    }
                });
    }

    private boolean validatePhone() {

        String usernameInput = maskedEditText.getText().toString().trim();
        if (maskedEditText.getText().toString().isEmpty()) {
            textInputLayoutPhone.setError("O campo não pode ficar vazio");
            return false;
        } else if (maskedEditText.getText().toString().trim().length() < 14) {
            textInputLayoutPhone.setError("número de telefone inválido");
            return false;
        } else if (maskedEditText.getText().toString().trim().length() > 18) {
            textInputLayoutPhone.setError("número de telefone muito longo");
            return false;
        }
        else {
            textInputLayoutPhone.setError(null);
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_CANCELED){
            genCode.setEnabled(true);
        }

        if(resultCode==99){
            setResult(99);
            finish();
        }

    }




}
