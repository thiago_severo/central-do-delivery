package br.com.serveapp.serveapp.util;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CieloRequest {

    public String stringRequest;

    public static String getRequest(String url){

        String response = null;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();
        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .addHeader("MerchantId", "f207cb7a-e1eb-4896-ac08-4a8ae717e061")
                .addHeader("Content-Type", "text/json")
                .addHeader("MerchantKey", "SKCGJKUWPZTDAPLUZNKQRSJLOXEJKXHGGBPMUACC")
                .build();

        try {
            Response response1 = client.newCall(request).execute();
            response = response1.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    };

    public static String postRequest(String url, RequestBody requestBody){

        String response = null;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        Request request = new Request.Builder()
                .url(url)
                .method("POST", requestBody)
                .addHeader("MerchantId", "f207cb7a-e1eb-4896-ac08-4a8ae717e061")
                .addHeader("Content-Type", "text/json")
                .addHeader("MerchantKey", "SKCGJKUWPZTDAPLUZNKQRSJLOXEJKXHGGBPMUACC")
                .build();

        try {
            Response response1 = client.newCall(request).execute();
            response = response1.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;

    }


}
