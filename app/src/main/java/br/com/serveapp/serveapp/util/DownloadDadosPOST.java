package br.com.serveapp.serveapp.util;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DownloadDadosPOST extends AsyncTask<Void, Void, String> {

    public static String searchDataCard = "https://api.cieloecommerce.cielo.com.br/1/card/";
    public static String url = "\"{\\r\\n    \\\"CustomerName\\\": \\\"Comprador Teste Cielo\\\",\\r\\n    \\\"CardNumber\\\":\\\"4716854277892656\\\",\\r\\n    \\\"Holder\\\":\\\"Comprador T Cielo\\\",\\r\\n    \\\"ExpirationDate\\\":\\\"12/2030\\\",\\r\\n    \\\"Brand\\\":\\\"Visa\\\"\\r\\n}\"";
    ;
    // public static String searchDataCard = "https://apisandbox.cieloecommerce.cielo.com.br/1/sales/";

    MediaType mediaType = MediaType.parse("text/json");
    RequestBody body = RequestBody.create(mediaType, "{\r\n    \"CustomerName\": \"Comprador Teste Cielo\",\r\n    \"CardNumber\":\"5231117787524117\",\r\n    \"Holder\":\"Comprador T Cielo\",\r\n    \"ExpirationDate\":\"12/2030\",\r\n    \"Brand\":\"Visa\"\r\n}");

    ResponseRequest responseRequest;

    public DownloadDadosPOST(RequestBody requestBody){

    this.body = requestBody;

    }

    public DownloadDadosPOST(){

    }

    @Override
    protected String doInBackground(Void... params) {

        StringBuilder resposta = new StringBuilder();

        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        /*MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType,  this.url);
        Request request = new Request.Builder()
                .url(searchDataCard)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .addHeader("MerchantId", "f207cb7a-e1eb-4896-ac08-4a8ae717e061")
                .addHeader("MerchantKey", "SKCGJKUWPZTDAPLUZNKQRSJLOXEJKXHGGBPMUACC")
                .build();
*/

        MediaType mediaType = MediaType.parse("text/json");
        //RequestBody body = RequestBody.create(mediaType, "{\r\n    \"CustomerName\": \"Comprador Teste Cielo\",\r\n    \"CardNumber\":\"5231117787524117\",\r\n    \"Holder\":\"Comprador T Cielo\",\r\n    \"ExpirationDate\":\"12/2030\",\r\n    \"Brand\":\"Visa\"\r\n}");

      //  RequestBody body = RequestBody.create(mediaType, url);

        Request request = new Request.Builder()
                .url(searchDataCard)
                .method("POST", body)

             //   .addHeader("MerchantId", "65cb874f-87ae-4333-83a3-cbe2c7f23172")
                .addHeader("MerchantId", "dfbe2589-9e71-499c-8669-ba9bd411e303")
                .addHeader("Content-Type", "text/json")
                .addHeader("MerchantKey", "7EyMYnvKHirU6Xq1mi4Wn4qcBkMWnt35qN6ktNfx")
              //  .addHeader("MerchantKey", "HVWEDXTORNPHZYIBHFTNZFAGDCAIMXEHXFXNZRAW")
                .build();

        String response = null;

        try {
            Response response1 = client.newCall(request).execute();
            response = response1.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        /*try {

           // URL url = new URL("https://apiquerysandbox.cieloecommerce.cielo.com.br/1/RecurrentPayment/c30f5c78-fca2-459c-9f3c-9c4b41b09048");

            String urlStr = "https://apiquerysandbox.cieloecommerce.cielo.com.br/1/RecurrentPayment/c30f5c78-fca2-459c-9f3c-9c4b41b09048";
            URL url = new URL(urlStr);
            URI uri = null;
            try {
                uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            url = uri.toURL();*/


        //URL url = new URL("https://reqres.in/api/users?page=2");

       /*     HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            connection.setRequestProperty("MerchantId", "f207cb7a-e1eb-4896-ac08-4a8ae717e061");
            connection.setRequestProperty("MerchantKey", "SKCGJKUWPZTDAPLUZNKQRSJLOXEJKXHGGBPMUACC");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.connect();

            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()) {
                resposta.append(scanner.next());

                System.out.println("resposta: "+ resposta);

            }



        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        responseRequest.setResponse(response);

        return response;

    }
    @Override
    protected void onPostExecute(String dados) {
        // Faça alguma coisa com os dados
    }

    public interface ResponseRequest{
        public void setResponse(String response);
    }

    public void setRequestResponse(ResponseRequest responseRequest){
        this.responseRequest = responseRequest;
    }

}
