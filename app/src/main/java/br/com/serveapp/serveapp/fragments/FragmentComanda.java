package br.com.serveapp.serveapp.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.AdapterHorizontalListCategoryItens;
import br.com.serveapp.serveapp.adapt.LineComanda;
import br.com.serveapp.serveapp.adapt.LineDemandGeral;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListComanda;
import br.com.serveapp.serveapp.dao.webService.ServicePedirOutro;
import br.com.serveapp.serveapp.repository.objects.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FragmentComanda extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Request requestListDemand;
    private Button leftTable;
    LeftTableListener leftTableListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    Request requestCancelDemand;
    public static long time;
    View view;
    public static int cdCategory;

    TextView textTitleComandaEmpty;
    TextView textBodyComandaEmpty;


    TextView textTotal;
    TextView textCouvert;
    TextView textTaxaServicoValue;
    TextView textTaxaServicoCouvertPercent;
    TextView textMesa;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser) {

            if (requestListDemand != null)
                requestListDemand.request();
            else {
                requestListDemand = new Request(getApplicationContext(), new ServiceListComanda(this, swipeRefreshLayout));
                requestListDemand.request();
            }

        } else {
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    private static RecyclerView recyclerComanda;
    public static JSONArray listRecyclerHorizontalCategories;
    RecyclerView horizontalRecyclerCategories;
    AdapterHorizontalListCategoryItens adapterHorizontalListCategoryItens;
    public LineDemandGeral lineItemDemandEstabelishment;
    ListProductsStablishmentDeliveryActivity activity;

    Request requestPedirOutro;

    public FragmentComanda(ListProductsStablishmentDeliveryActivity activity) {
        this.activity = activity;
        requestPedirOutro =
                new Request(activity.getApplicationContext(), new ServicePedirOutro(activity.getApplicationContext()));

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (requestListDemand != null)
            requestListDemand.request();
        else {
            requestListDemand = new Request(getApplicationContext(), new ServiceListComanda(this, swipeRefreshLayout));
            requestListDemand.request();
        }
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_comanda, container, false);
        recyclerComanda = view.findViewById(R.id.listComanda);
        textMesa = view.findViewById(R.id.textMesa);
        textTotal = view.findViewById(R.id.totalPedidos);
        textCouvert = view.findViewById(R.id.couvert);
        textTaxaServicoValue = view.findViewById(R.id.valor);
        textTaxaServicoCouvertPercent = view.findViewById(R.id.percent);
        leftTable = view.findViewById(R.id.leftTable);
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        textTitleComandaEmpty = view.findViewById(R.id.textTitleComandaEmpty);
        textBodyComandaEmpty = view.findViewById(R.id.textBodyComandaEmpty);

        leftTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leftTableListener.leftTable();
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerComanda.setLayoutManager(llm);
        requestListDemand = new Request(getApplicationContext(), new ServiceListComanda(this, swipeRefreshLayout));

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            requestListDemand.request();
                        } catch (NullPointerException e) {
                            requestListDemand = new Request(getApplicationContext(), new ServiceListComanda(FragmentComanda.this, swipeRefreshLayout));
                            requestListDemand.request();
                        }
                    }
                }
        );

        recyclerComanda.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (activity instanceof ListProductsStablishmentDeliveryActivity) {
                    CardView fab = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.cardFab);

                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (activity instanceof ListProductsStablishmentDeliveryActivity) {
                    CardView fab = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.cardFab);
                    ConstraintLayout fab1 = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.constransCallAtendente);
                    ConstraintLayout fab2 = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.constraintLayoutLeft);

                    if (dy > 0) {
                        fab.setVisibility(View.GONE);
                        fab1.setVisibility(View.GONE);
                        fab2.setVisibility(View.GONE);
                    }
                    if (dy < 0) {
                        fab.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        requestListDemand.request();

        return view;
    }

    public void initializeAdapterListCategories() {
        try {
            adapterHorizontalListCategoryItens = new AdapterHorizontalListCategoryItens(getPositionCategoty(cdCategory), getApplicationContext(), listRecyclerHorizontalCategories);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        horizontalRecyclerCategories.setAdapter(adapterHorizontalListCategoryItens);
        horizontalRecyclerCategories.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        int position = 0;
        try {
            int pos = data.getIntExtra("position", 0);
            lineItemDemandEstabelishment.listItensDemand.getJSONObject(pos).put("status", "entregue");
            lineItemDemandEstabelishment.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private int getPositionCategoty(int cdCategory) throws JSONException {
        int pos = 0;
        for (int i = 0; i < listRecyclerHorizontalCategories.length(); i++) {
            String val = listRecyclerHorizontalCategories.getJSONObject(i).getString("_id");

            if (cdCategory == Integer.parseInt(val)) {
                pos = i;
                break;
            }

        }
        return pos;
    }


    public void showMsg(String mensagem) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity().getApplicationContext());
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView textView = mView.findViewById(R.id.textSucessMessage);
        TextView textViewSucess = mView.findViewById(R.id.textSucessMessage);
        textView.setText("OPS :(");
        textViewSucess.setText(mensagem);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

    }

    public void initializeAdapterListProducts(JSONArray result) {
        if(result.length()>0){
            textBodyComandaEmpty.setVisibility(View.GONE);
            textTitleComandaEmpty.setVisibility(View.GONE);
        }
        LineComanda lineComanda = new LineComanda(this, getApplicationContext(), result);
        lineComanda.setPedirOutroListener(new LineComanda.PedirOutroListener() {
            @Override
            public void clickPedirOutro(JSONObject item) {
                showDialogPedirOutro(item);
            }
        });
        recyclerComanda.setAdapter(lineComanda);
    }

    public void showDialogPedirOutro(final JSONObject item) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);

        View mView = getLayoutInflater().inflate(R.layout.dialog_pedir_outro, null);
        Button btnCancel = mView.findViewById(R.id.btnCancelCancel);
        Button btnConfirmCancel = mView.findViewById(R.id.btnConfirmCancel);

        mBuilder.setView(mView);
        final AlertDialog dialogi = mBuilder.create();
        dialogi.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogi.dismiss();
            }
        });

        btnConfirmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> paramLogin = new HashMap<>();
                try {
                    paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
                    paramLogin.put("id_produtoSelecionado", item.getString("id"));
                    requestPedirOutro.request(paramLogin);
                    dialogi.dismiss();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    public void initializeHeader(String textTotal, String textCouvert, String textTaxaServicoValue, String textTaxaServicoCouvertPercent, String mesa) {
        this.textTotal.setText(textTotal);
        this.textCouvert.setText(textCouvert);
        this.textTaxaServicoCouvertPercent.setText(textTaxaServicoCouvertPercent);
        this.textTaxaServicoValue.setText(textTaxaServicoValue);
        this.textMesa.setText("TOTAL DA MESA (" + mesa + ")");
    }

    public void updateList() {
        if (requestListDemand != null)
            requestListDemand.request();
        else {
            requestListDemand = new Request(getApplicationContext(), new ServiceListComanda(this, swipeRefreshLayout));
            requestListDemand.request();
        }
    }

    public interface LeftTableListener {
        public void leftTable();
    }

    public void setLeftTable(LeftTableListener leftTableListener) {
        this.leftTableListener = leftTableListener;
    }

}