package br.com.serveapp.serveapp.navigationListener;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;

import br.com.serveapp.serveapp.AtracoesActivity;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.FavoritsActivity;
import br.com.serveapp.serveapp.ListAllProducts;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.repository.objects.Repository;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class BottomNavigationHomeListener {

    public static boolean changeNavigation(Activity activity, MenuItem menuItem){
        switch (menuItem.getItemId()) {
            case R.id.nav_fav_home:
                break;
            case R.id.navigation_fav_fav:
                Intent intent = new Intent(activity.getApplicationContext(), FavoritsActivity.class).setFlags(FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.overridePendingTransition(0, 0);
                break;
            case R.id.nav_fav_carrinho:
                Repository.idEstabelecimento = "0";
                Intent intent_carrinho = new Intent(activity.getApplicationContext(), ConfirmItensPedidoActiviy.class).setFlags(FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent_carrinho);
                activity.overridePendingTransition(0, 0);

                break;
            case R.id.nav_fav_produtos:
                Intent intent_produtos = new Intent(activity.getApplicationContext(), ListAllProducts.class).setFlags(FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent_produtos);
                activity.overridePendingTransition(0, 0);
                break;
            case R.id.nav_atracoes:
                Intent intent_events = new Intent(activity.getApplicationContext(), AtracoesActivity.class).setFlags(FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent_events);
                activity.overridePendingTransition(0, 0);
                break;
        }

        return true;
    }



}
