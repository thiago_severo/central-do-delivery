package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.FormaPagamentoActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceFormaPagamento extends OnPostResponse {

    FormaPagamentoActivity formaPagamentoActivity;



    public ServiceFormaPagamento(FormaPagamentoActivity formaPagamentoActivity) {

        this.formaPagamentoActivity = formaPagamentoActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_forma_pagamento.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(formaPagamentoActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        paramLogin.put("id_cliente", Repository.getIdUsuario(formaPagamentoActivity.getApplicationContext()));

        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {

        try {
            JSONObject result = jsonObject.getJSONObject("result");

            result.getJSONArray("cards_cliente");
            result.getString("central_pay");

            formaPagamentoActivity.initializeFromasPagamento(result.getJSONArray("credito"), result.getJSONArray("debito"),    result.getJSONArray("cards_cliente"), result.getString("central_pay"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                formaPagamentoActivity.initializeFromasPagamento(result.getJSONArray("credito"), result.getJSONArray("debito"),   result.getJSONArray("cards_cliente"), result.getString("central_pay"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}