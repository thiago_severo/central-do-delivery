package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.CreateAddressPopUpActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceEditAddress extends OnPostResponse {

    CreateAddressPopUpActivity createAddressPopUpActivity;

    public ServiceEditAddress(CreateAddressPopUpActivity createAddressPopUpActivity) {

        this.createAddressPopUpActivity = createAddressPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_ends_cliente.php";

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
            Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
            createAddressPopUpActivity.mProgressCreateAddress.dismiss();
            createAddressPopUpActivity.btn_cadastrar.setEnabled(true);
        }

    @Override
    public Map<String, String> getParam() {

        Map<String, String> paramLogin = new HashMap<>();

        paramLogin.put("id_cliente", Repository.getIdUsuario(createAddressPopUpActivity));
        paramLogin.put("titulo", createAddressPopUpActivity.txtCdTitleAddress.getText().toString());
        paramLogin.put("cep", createAddressPopUpActivity.textInputCep.getText().toString());
        paramLogin.put("logradouro", createAddressPopUpActivity.text_cd_rua.getText().toString());
        paramLogin.put("num", createAddressPopUpActivity.text_cd_numero.getText().toString());
        paramLogin.put("complemento", createAddressPopUpActivity.text_cd_complemento.getText().toString());
        paramLogin.put("bairro", createAddressPopUpActivity.text_cd_bairro.getText().toString());
        paramLogin.put("cidade", createAddressPopUpActivity.text_cd_cidade.getText().toString());
        paramLogin.put("estado", createAddressPopUpActivity.spinner.getSelectedItem().toString());
        paramLogin.put("_id", createAddressPopUpActivity.idEndereco );
        paramLogin.put("ponto_ref", createAddressPopUpActivity.descAvaliacao.getText().toString());

        paramLogin.put("function_method", "edit");

        return paramLogin;

    }

    @Override
    public void onResponse(String response) {


        if (!response.equals("null"))

            try {

                JSONObject jsonObject = new JSONObject(response);

                if (!jsonObject.isNull("result")) {
                    JSONObject user = Repository.getUser(createAddressPopUpActivity);
                        createAddressPopUpActivity.mProgressCreateAddress.dismiss();
                        createAddressPopUpActivity.loadFinish();
                }

            } catch (JSONException e) {

                    Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                    createAddressPopUpActivity.mProgressCreateAddress.dismiss();

            }

    }

}