package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.CreateAddressPopUpActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceGetAddressByCep extends OnPostResponse {

    CreateAddressAccountActivity createAddressAccountActivity;
    CreateAddressPopUpActivity createAddressPopUpActivity;
    public ServiceGetAddressByCep(CreateAddressPopUpActivity createAddressPopUpActivity, CreateAddressAccountActivity createAddressAccountActivity){
        this.createAddressAccountActivity = createAddressAccountActivity;
        this.createAddressPopUpActivity = createAddressPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "get_endereco_by_cep.php";
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        if(createAddressAccountActivity!=null) {
            paramLogin.put("cep", createAddressAccountActivity.textInputCep.getText().toString().replace("-", ""));
        }else{
            paramLogin.put("cep", createAddressPopUpActivity.textInputCep.getText().toString().replace("-", ""));
        }
        return paramLogin;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        createAddressAccountActivity.mProgress.dismiss();
        Toast.makeText(createAddressAccountActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        if(!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject address = null;

                if (!jsonObject.isNull("result") && !jsonObject.getJSONObject("result").isNull("cep")) {
                    address = jsonObject.getJSONObject("result");
                    if(createAddressAccountActivity!=null) {
                        createAddressAccountActivity.mProgress.dismiss();
                        createAddressAccountActivity.text_cd_bairro.setText(address.getString("bairro"));
                        createAddressAccountActivity.text_cd_cidade.setText(address.getString("cidade"));
                        //createAddressAccountActivity.text_cd_cidade.setEnabled(false);
                        createAddressAccountActivity.text_cd_rua.setText(address.getString("logradouro"));
                        createAddressAccountActivity.setSelectSpiner(address.getString("uf"));
                        //createAddressAccountActivity.spinner.setEnabled(false);
                        //createAddressAccountActivity.text_cd_complemento.setText(" ");
                        //createAddressAccountActivity.text_cd_numero.setText(" ");
                        createAddressAccountActivity.mProgress.dismiss();
                        //createAddressAccountActivity.getIsValid();
                        createAddressAccountActivity.btn_cadastrar.setEnabled(true);
                    }
                    else{
                        createAddressPopUpActivity.mProgress.dismiss();
                        createAddressPopUpActivity.text_cd_bairro.setText(address.getString("bairro"));
                        createAddressPopUpActivity.text_cd_cidade.setText(address.getString("cidade"));
                        //createAddressPopUpActivity.text_cd_cidade.setEnabled(false);
                        createAddressPopUpActivity.text_cd_rua.setText(address.getString("logradouro"));
                        createAddressPopUpActivity.setSelectSpiner(address.getString("uf"));
                        //createAddressPopUpActivity.spinner.setEnabled(false);
                        //createAddressPopUpActivity.text_cd_complemento.setText(" ");
                        //createAddressPopUpActivity.text_cd_numero.setText(" ");
                        createAddressPopUpActivity.mProgress.dismiss();
                        //createAddressPopUpActivity.getIsValid();
                        createAddressPopUpActivity.btn_cadastrar.setEnabled(true);
                    }

                } else {
                    if(createAddressAccountActivity!=null) {
                        createAddressAccountActivity.mProgress.dismiss();
                        createAddressAccountActivity.btn_cadastrar.setEnabled(true);
                        Toast.makeText(createAddressAccountActivity.getApplicationContext(), "CEP não encontrado", Toast.LENGTH_LONG).show();
                    }else{
                        createAddressPopUpActivity.btn_cadastrar.setEnabled(true);
                        createAddressPopUpActivity.mProgress.dismiss();
                        Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "CEP não encontrado", Toast.LENGTH_LONG).show();

                    }

                    }

            } catch (JSONException e) {
                e.printStackTrace();

                if(createAddressAccountActivity!=null) {
                    createAddressAccountActivity.mProgress.dismiss();
                    createAddressAccountActivity.text_cd_bairro.setText("");
                    createAddressAccountActivity.text_cd_cidade.setText("");
                    createAddressAccountActivity.text_cd_cidade.setEnabled(true);
                    createAddressAccountActivity.text_cd_rua.setText("");
                    createAddressAccountActivity.spinner.setEnabled(true);
                    createAddressAccountActivity.text_cd_complemento.setText("");
                    createAddressAccountActivity.text_cd_numero.setText("");
                    createAddressAccountActivity.mProgress.dismiss();
                    Toast.makeText(createAddressAccountActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                }else{
                    createAddressPopUpActivity.mProgress.dismiss();
                    Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                }
            }
    }



}
