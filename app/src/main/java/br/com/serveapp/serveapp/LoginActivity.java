package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginBehavior;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceLogin;
import br.com.serveapp.serveapp.dao.webService.ServiceLoginAPI;
import br.com.serveapp.serveapp.dao.webService.ServiceRecoverPassword;
import br.com.serveapp.serveapp.util.MaskEditUtil;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient googleApiCLient;
    //private com.google.android.gms.common.SignInButton singinButton;
    private Button singinButton;
    private final int SIGN_IN_CODE = 777;
    public static LoginActivity refference;

    private String verificationId;

    private LoginButton singinButtonFace;
    private CallbackManager callbackManager;
    private GraphRequest request;

    private Request requestLogin;
    private Request requestLoginAPI;
    private Request requestRecoverPassword;

    private TextInputLayout textInputUsername;
    private TextInputLayout textInputPassword;
    public TextInputEditText textEditUsername;
    public TextInputEditText textEditPassword;

   /* View.OnFocusChangeListener listenerChengeFocus = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if (!hasFocus && v.getId() == R.id.lg_email) {
                validateUsername();
            } else if (!hasFocus && v.getId() == R.id.lg_pass) {
                validatePassword();
            }

        }
    };*/

    public Button btnCreateAccount;
    public Button btnAccess;
    public ProgressBar progressBar;
    public ProgressDialog mProgress;
    public ProgressDialog mProgresRecouverPass;

    FirebaseAuth mAuth;

    @SuppressLint("ResourceAsColor")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        requestLogin = new Request(getApplicationContext(), new ServiceLogin(this));
        requestLoginAPI = new Request(getApplicationContext(), new ServiceLoginAPI(this, null));
        requestRecoverPassword = new Request(getApplicationContext(), new ServiceRecoverPassword(this));

        mProgress = new ProgressDialog(this);
        String titleId = "Entrando...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("por favor, aguarde...");

        mProgresRecouverPass = new ProgressDialog(this);
        String titleRecouver = "Enviando dados...";
        mProgresRecouverPass.setTitle(titleRecouver);
        mProgresRecouverPass.setMessage("Por favor, aguarde...");

        textInputUsername = findViewById(R.id.textInputLayoutlgUser);

        textEditUsername = findViewById(R.id.lg_email);
        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        //singinButtonFace = findViewById(R.id.login_button_fc);
        singinButtonFace = new LoginButton(this);

        singinButtonFace.setText("facebook");
        singinButtonFace.setReadPermissions(Arrays.asList("public_profile", "email"));

        singinButtonFace.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK);
        singinButtonFace.setLoginBehavior(LoginBehavior.NATIVE_ONLY);
        singinButtonFace.setLoginBehavior(LoginBehavior.WEB_ONLY);

        Button btFacebook = findViewById(R.id.fb);
        btFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                singinButtonFace.callOnClick();
            }
        });

        mAuth = FirebaseAuth.getInstance();

        singinButtonFace.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        final Map<String, String> paramLogin = new HashMap<>();
                        try {
                            if(object.has("email")){
                                paramLogin.put("email", object.getString("email"));
                            }else if(object.has("id")){
                                paramLogin.put("email", object.getString("id")+"@facebook");
                            }
                            paramLogin.put("nome", object.getString("first_name"));
                            paramLogin.put("sobre_nome", object.getString("last_name"));
                            paramLogin.put("instructionId", "facebook");
                            paramLogin.put("token", CreateAccountUserActivity.getToken(getApplicationContext()));

                            requestLoginAPI.request(paramLogin);
                            LoginManager.getInstance().logOut();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "first_name,last_name,email,id");
                request.setParameters(parameters);
                request.executeAsync();
                mProgress.show();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Cancelado!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Falha ao logar!", Toast.LENGTH_SHORT).show();
            }
        });

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        googleApiCLient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso).build();

        final GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, gso);

        ConstraintLayout layout_login = findViewById(R.id.layout_login);
        //layout_login.setBackgroundColor(getResources().getColor(R.color.colorDark));

        singinButton = findViewById(R.id.google_button);
        singinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = googleSignInClient.getSignInIntent();
                startActivityForResult(intent, SIGN_IN_CODE);
            }

        });

        btnCreateAccount = findViewById(R.id.btn_lg_create_account);
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CadastroTelefoneActivity.class);
                startActivity(intent);
            }
        });

        textEditUsername.addTextChangedListener(MaskEditUtil.mask(textEditUsername, MaskEditUtil.FORMAT_FONE, getApplicationContext()));

        btnAccess = findViewById(R.id.btn_acessar);

        btnAccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((InputMethodManager) LoginActivity.this.getSystemService(LoginActivity.this.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                        textEditUsername.getWindowToken(), 0);
                if (validatePhone()) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.confirm_number, null);
                    final TextView number = mView.findViewById(R.id.txt_number);
                    final ImageButton imageButtonCloseEvent = mView.findViewById(R.id.imageButtonCloseEvent);
                    final Button buttonOkEvent = mView.findViewById(R.id.buttonOkEvent);

                    number.setText(textEditUsername.getText().toString());
                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    imageButtonCloseEvent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    buttonOkEvent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            if (validatePhone() == true) {

                                dialog.dismiss();
                                progressBar.setVisibility(View.VISIBLE);
                                progressBar.setVisibility(View.GONE);

                                String phoneNumber = "+55" + textEditUsername.getText().toString();

                                Intent intent = new Intent(getApplicationContext(), ConfirmCodeSms.class);
                                intent.putExtra("VerificationId", "" );
                                intent.putExtra("phoneNumber", phoneNumber);

                                startActivityForResult(intent , 1);


                            }

                        }
                    });

                }

            }
        });

    }


    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential credential) {

            progressBar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), "onVerificationCompleted", Toast.LENGTH_LONG).show();

        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

            //Toast.makeText(getApplicationContext(), "Falha ao enviar SMS", Toast.LENGTH_LONG).show();
            progressBar.setVisibility(View.GONE);
            if (e instanceof FirebaseAuthInvalidCredentialsException) {

            } else if (e instanceof FirebaseTooManyRequestsException) {
                //Toast.makeText(getApplicationContext(), "Um código de verificação já foi enviado, Tente Novamente, mais tarde!", Toast.LENGTH_LONG).show();
                verificationCod();
            }

        }

        @Override
        public void onCodeSent(@NonNull String verificationId,
                               @NonNull PhoneAuthProvider.ForceResendingToken token) {

            PhoneAuthProvider.ForceResendingToken mResendToken = token;
            LoginActivity.this.verificationId = verificationId;

        }

    };

    private void verificationCod(){
        if(verificationId!=null)
        progressBar.setVisibility(View.GONE);
        String phoneNumber = "+55" + textEditUsername.getText().toString();
        Intent intent = new Intent(getApplicationContext(), ConfirmCodeSms.class);
        intent.putExtra("VerificationId", verificationId );
        intent.putExtra("phoneNumber", phoneNumber);
        startActivityForResult(intent , 1);
    }

    private boolean validatePhone() {
        String fone = textEditUsername.getText().toString().replaceAll("[^0-9]+", "");
        if(fone.length() == 11){
            textEditUsername.setError(null);
            return true;
        }else if(fone.length() == 10){
            fone = "("+fone.substring(0,2)+") 9"+fone.substring(2,6)+"-"+fone.substring(6,10);
            textEditUsername.setText(fone);
            textEditUsername.setError(null);
            return true;
        }else if(fone.isEmpty()){
            textEditUsername.setError("informe um telefone");
            return false;
        }else{
            textEditUsername.setError("número inválido");
            return false;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    protected void onResume() {
        super.onResume();
        refference = this;
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {

        final AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if (task.isSuccessful()) {
                             handleSignInResult(acct);
                        } else {
                            Toast.makeText(getApplicationContext(), "Falha na autenticação.", Toast.LENGTH_LONG).show();
                        }

                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_CODE) {
           /* GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);*/

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                Toast.makeText(getApplicationContext(),"Google sign in failed" + e.getMessage().toString(), Toast.LENGTH_LONG).show();
            }

        }

        if(resultCode==99){
            finish();
        }

    }

    private void handleSignInResult(GoogleSignInAccount acct) {

            Map<String, String> paramLogin = new HashMap<>();
            paramLogin.put("email", acct.getEmail());
            paramLogin.put("nome", acct.getGivenName());
            paramLogin.put("sobre_nome", acct.getFamilyName());
            paramLogin.put("instructionId", "google");
            paramLogin.put("token", CreateAccountUserActivity.getToken(getApplicationContext()));
            requestLoginAPI.request(paramLogin);
            mProgress.show();
            signOutGoogleAccount();

        }



    private boolean validateUsername() {
        String usernameInput = textInputUsername.getEditText().getText().toString().trim();

        if (usernameInput.isEmpty()) {
            textInputUsername.setError("Campo não pode ficar vazio");
            return false;
        } else if (usernameInput.length() > 50) {
            textInputUsername.setError("email muito longo");
            return false;
        } else {
            textInputUsername.setError(null);
            return true;
        }
    }

    private void signOutGoogleAccount() {
        if (googleApiCLient.isConnected()) {
            Auth.GoogleSignInApi.signOut(googleApiCLient);
            googleApiCLient.disconnect();
        }
    }

    public void buildConfirmDialog(String textMain, String texto) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(LoginActivity.this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView text = mView.findViewById(R.id.textSucessMessage);
        TextView textViewMain = mView.findViewById(R.id.textMainMsg);

        text.setText(texto);
        textViewMain.setText(textMain);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

}