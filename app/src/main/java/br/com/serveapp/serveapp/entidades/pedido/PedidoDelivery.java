package br.com.serveapp.serveapp.entidades.pedido;

import br.com.serveapp.serveapp.entidades.Endereco;
import br.com.serveapp.serveapp.enun.FormaPag;

public class PedidoDelivery extends Pedido{

    private FormaPag formaPag;
    private Endereco endEntrega;
    private float taxaEntrega;

    public FormaPag getFormaPag() {
        return formaPag;
    }
    public void setFormaPag(FormaPag formaPag) {
        this.formaPag = formaPag;
    }
    public Endereco getEndEntrega() {
        return endEntrega;
    }
    public void setEndEntrega(Endereco endEntrega) {
        this.endEntrega = endEntrega;
    }
    public float getTaxaEntrega() {
        return taxaEntrega;
    }
    public void setTaxaEntrega(float taxaEntrega) {
        this.taxaEntrega = taxaEntrega;
    }

}
