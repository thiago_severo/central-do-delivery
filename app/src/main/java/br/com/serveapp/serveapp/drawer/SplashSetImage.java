package br.com.serveapp.serveapp.drawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.LoginActivity;
import br.com.serveapp.serveapp.NavigationDrawerActivity;
import br.com.serveapp.serveapp.SplashScreenHomeApp;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.repository.objects.Repository;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SplashSetImage extends SetImageWebBipMap {

    Intent intent;
    ImageView imageView;
    ProgressBar progressBar;
    Context context;
    SplashScreenHomeApp splashScreenHomeApp;

    public SplashSetImage(SplashScreenHomeApp splashScreenHomeApp, Context context, ImageView imageView, ProgressBar progressBar) {
        super(context);
        this.context = context;
        this.imageView = imageView;
        this.progressBar = progressBar;
        this.splashScreenHomeApp = splashScreenHomeApp;
    }

    public void loadLocal(){

    }


    public void loadWeb(){

        new Handler(Looper.getMainLooper()).post(new Runnable(){
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    protected void onPostExecute(final Bitmap result) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                imageView.setImageBitmap(result);
            }
        });
        final String returnUser = Dao.read(FilesName.user, context);

        try {
            JSONObject jsonObject = new JSONObject(returnUser);
            Repository.setUser(jsonObject);
            if(Repository.getUser(context).isNull("id_endereco")&&Repository.getUser(context).isNull("id_municipio")){
                intent = new Intent(context, CreateAddressAccountActivity.class);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
            }else {
                intent = new Intent(context, NavigationDrawerActivity.class);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
            }
        } catch (JSONException e) {
            intent = new Intent(context, LoginActivity.class);
            intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
         }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //SplashScreenHomeApp splashScreenHomeApp = (SplashScreenHomeApp) context;
                context.startActivity(intent);
                splashScreenHomeApp.finish();
            }
        }, 2000);

    }

}