package br.com.serveapp.serveapp.dao.web;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import br.com.serveapp.serveapp.dao.Dao;

import java.util.Map;

public abstract class RequestActivity extends AppCompatActivity implements Response.Listener<String>, Response.ErrorListener{

    StringRequest stringRequest;
    RequestQueue requestQueue;
    String script;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestQueue = Volley.newRequestQueue(this);
    }

    private void setScriptName(String script){
        script = script.replace("/","");
        script = script.replace(":","");
        this.script = script;
    }

    private String getScriptName(){
        return this.script;
    }

    public void request(String script){
        request(script, null);
    }

    public void request(String script, final Map<String, String> p) {
        if(p!=null)p.put("api_key","tpeVo2SZCALxTjlrYimU13psgHVUNc7YB");
        final Map<String, String> params = p;
        setScriptName(script);
        // verificar antes o arquivo local
        cache(getScriptName());
        stringRequest = new StringRequest(Request.Method.POST, script, this, this) {
            protected Map<String, String> getParams() {
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    @Override
    public void onResponse(String response) {
        Dao.saveCache(response, getScriptName(), this);
        onResponseServe(response);
    }

    private void cache(String scriptName){
        String cache = Dao.readCache(scriptName, this);
        onCache(cache);
    }

    public abstract void onResponseServe(String response);
    public abstract void onCache(String response);

}