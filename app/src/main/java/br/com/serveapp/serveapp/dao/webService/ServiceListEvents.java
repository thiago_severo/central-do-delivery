package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.AtracoesActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListEvents extends OnPostResponse {

    AtracoesActivity atracoesActivity;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ProgressBar progressBar;
    public ServiceListEvents(ProgressBar progressBar, AtracoesActivity atracoesActivity, SwipeRefreshLayout swipeRefreshLayout) {
        this.progressBar = progressBar;
        this.atracoesActivity = atracoesActivity;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_events.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
        progressBar.setVisibility(View.GONE);
        Toast.makeText(atracoesActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(atracoesActivity));

        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONObject result = jsonObject.getJSONObject("result");
            atracoesActivity.initializeAdapterListAddress(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                atracoesActivity.initializeAdapterListAddress(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}