package br.com.serveapp.serveapp.adapt;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class LineProductsForEstabelecimentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private Activity myActivity;
    private OnItemClickListner listener;

    JSONArray itemListProduct;

    public LineProductsForEstabelecimentsAdapter(Context context, JSONArray itemListProduct) {
        this.itemListProduct = itemListProduct;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_product, parent, false);
        LineProductItem lineProductItem = new LineProductItem(v);
        return lineProductItem;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        LineProductItem line = (LineProductItem) holder;
        try {
            JSONObject prod = itemListProduct.getJSONObject(position);
            line.tempPreparo.setText(prod.getString("tempPreparo"));
            line.txt_nome_item.setText(prod.getString("nome"));
            line.txtParcela.setText(prod.getString("parcela"));
            line.txt_value_item.setText(prod.getString("valor"));
            line.txt_descricao.setText(prod.getString("descricao"));
            line.txt_medida.setText(prod.getString("medida"));



            //if (!prod.isNull("est_name"))
             //   line.textViewNameEstab.setText(prod.getString("est_name"));
            line.avaliacao.setRating(Float.parseFloat((prod.getString("avaliacao") + "").substring(0, 1)));
            if (!prod.getString("desconto").equals("null")) {
                line.imageCupomDescont3.setVisibility(View.VISIBLE);
                line.textCupomDescont2.setVisibility(View.VISIBLE);
                line.textCupomDescont2.setText(prod.getString("desconto"));
            }

            if (!prod.isNull("est_foto"))
                new ListSetImage(context, line.imageEstabeleciment).execute(prod.getString("est_foto"));
            new ListSetImage(context, line.imageItemProducts).execute(prod.getString("foto"), line.progressImage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return itemListProduct.length();
    }


    class LineProductItem extends RecyclerView.ViewHolder {

        private TextView txt_nome_item;
        private TextView txt_value_item;
        private ImageView imageItemProducts;
        private ProgressBar progressImage;
        private TextView txt_descricao;
        private TextView txt_medida;
        private ImageView imageEstabeleciment;

        private TextView textCupomDescont2;
        private ImageView imageCupomDescont3;
        private TextView tempPreparo;
        private RatingBar avaliacao;
        private TextView txtParcela;



        public LineProductItem(@NonNull View itemView) {
            super(itemView);

            txtParcela = itemView.findViewById(R.id.txtParcela);
            imageItemProducts = itemView.findViewById(R.id.imageItemProducts);
            txt_nome_item = itemView.findViewById(R.id.nomeItemCardapio);
            txt_value_item = itemView.findViewById(R.id.txtValorItemMenu);
            txt_descricao = itemView.findViewById(R.id.textViewDesc);
            txt_medida = itemView.findViewById(R.id.textMedida);
            imageEstabeleciment = itemView.findViewById(R.id.imageEstabeleciment);
            avaliacao = itemView.findViewById(R.id.ratingBarLikedListProduct);
            progressImage = itemView.findViewById(R.id.progressImage);

            tempPreparo = itemView.findViewById(R.id.tempPreparo);
            textCupomDescont2 = itemView.findViewById(R.id.textCupomDescont2);
            imageCupomDescont3 = itemView.findViewById(R.id.imageCupomDescont3);

            imageCupomDescont3.setVisibility(View.GONE);
            textCupomDescont2.setVisibility(View.GONE);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        try {
                            listener.onItemClick(itemListProduct.getJSONObject(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
            clean(itemView);
        }

        private void clean(View itemView){
            itemView.findViewById(R.id.btn_accrease_item_demand).setVisibility(View.GONE);
            itemView.findViewById(R.id.btn_decrease_item).setVisibility(View.GONE);
            itemView.findViewById(R.id.textQtdItemDemandFinalize).setVisibility(View.GONE);
            itemView.findViewById(R.id.optionsText).setVisibility(View.GONE);
            itemView.findViewById(R.id.imageIsBrinde).setVisibility(View.GONE);
        }
    }

    public interface OnItemClickListner {
        void onItemClick(JSONObject itemCardapio);
    }

    public void setOnItemClickListener(OnItemClickListner listener) {
        this.listener = listener;
    }

}
