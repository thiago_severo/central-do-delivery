package br.com.serveapp.serveapp.fragments.ifood;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import br.com.serveapp.serveapp.R;

public class LineVerticalProductAdapter extends RecyclerView.Adapter<LineVerticalProduct>{

    private JSONArray itens;
    LinearLayoutManager layoutManager;
    Fragment fragment;
    Activity activity;

    public LineVerticalProductAdapter(JSONArray itens, LinearLayoutManager layoutManager, Fragment fragment, Activity activity) {
        this.itens = itens;
        this.fragment = fragment;
        this.activity = activity;
        this.layoutManager = layoutManager;
    }

    @Override
    public LineVerticalProduct onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_product, parent, false);
        return new LineVerticalProduct(view);
    }

    @Override
    public void onBindViewHolder(LineVerticalProduct holder, final int position) {
        try {
            holder.initialize(itens.getJSONObject(position));
            holder.setFragment(fragment);
            holder.setActivity(activity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.setActivity(activity);
        holder.setLayoutManager(layoutManager);
    }

    @Override
    public int getItemCount() {
        return itens != null ? itens.length() : 0;
    }

}
