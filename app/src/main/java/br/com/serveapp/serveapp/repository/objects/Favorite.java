package br.com.serveapp.serveapp.repository.objects;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Favorite {

    private List<JSONObject> list = new ArrayList<>();
    private Map<String, JSONObject> mapFav = new HashMap<>();
    private Map<String, JSONObject> mapCache = new HashMap<>();

    public Favorite(){
        reset();
    }

    public void reset(){
        list = new ArrayList<>();
        mapFav = new HashMap<>();
        mapCache = new HashMap<>();
    }

    public void reset(JSONArray array){
        try {
            reset();
            for (int i=0; i < array.length(); i++){
                add(array.getJSONObject(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void add(JSONObject obj){
        if(isFav(obj)){
            mapFav.put(getId(obj), obj);
            list.add(obj);
        }else{
            mapCache.put(getId(obj), obj);
        }
    }

    public void add(String id){
        JSONObject obj = mapCache.remove(id);
        list.add(obj);
        mapFav.put(id,obj);
    }

    public boolean contains(JSONObject obj){
        return list.contains(obj);
    }

    public boolean contains(String id_obj){
        return mapFav.containsKey(id_obj);
    }

    public JSONArray list(){
        return new JSONArray(list);
    }

    public void remove(String id){
        JSONObject obj = mapFav.remove(id);
        list.remove(obj);
        mapCache.put(id, obj);
    }



    private String getId(JSONObject obj){
        try {
            return obj.getString("_id");
        } catch (JSONException e) {
            return null;
        }
    }

    private boolean isFav(JSONObject obj){
        try {
            return obj.getString("fav").equals("1");
        } catch (JSONException e) {
            return false;
        }
    }
}
