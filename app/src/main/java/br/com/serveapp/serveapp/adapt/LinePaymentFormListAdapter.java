package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.FormaPagamentoActivity;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LinePaymentFormListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListnerAddress listner;
    Context context;
    View v;
    int mPosition = -1;
    FormaPagamentoActivity formaPagamentoActivity;

    public LinePaymentFormListAdapter(FormaPagamentoActivity formaPagamentoActivity,  Context context, JSONArray listViewItem) {
        this.context=context;
        this.listViewItem = listViewItem;
        this.formaPagamentoActivity = formaPagamentoActivity;
       }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_payment_type, parent, false);
             lineSimpleItemHolder = new LineSimpleItemHolder(v);
            return lineSimpleItemHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            ((LineSimpleItemHolder)holder).textPayment.setText(listViewItem.getJSONObject(position).getString("nome"));
            ((LineSimpleItemHolder)holder).iconPayment  = v.findViewById(context.getResources().getIdentifier(listViewItem.getJSONObject(position).getString("foto"), "drawable", context.getPackageName()));
            ((LineSimpleItemHolder)holder).imageIconcConfirm.setVisibility(View.GONE);

            if(mPosition == position){
                ((LineSimpleItemHolder)holder).imageIconcConfirm.setVisibility(View.VISIBLE);
            }
            //       new ListSetImage(context, ((LineSimpleItemHolder)holder).iconPayment).execute(listViewItem.getJSONObject(position).getString("foto"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listViewItem.length();
    }



    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        public ImageView iconPayment;
        public ImageView  imageIconcConfirm;
        public TextView  textPayment;

        public LineSimpleItemHolder(View itemView) {
            super(itemView);

            iconPayment = itemView.findViewById(R.id.iconPayment);
            textPayment = itemView.findViewById(R.id.textPayment);
         //   imageIconcConfirm = itemView.findViewById(R.id.imageIconcConfirm);
            imageIconcConfirm.setVisibility(View.GONE);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    formaPagamentoActivity.enabledPayment();
                    final int position = getAdapterPosition();
                    mPosition = getAdapterPosition();
                    notifyDataSetChanged();
                }
            });

        }
    }

    public void setOnClickListenerAddress(OnItemClickListnerAddress listenere){
        this.listner = listenere;
    }
    public interface OnItemClickListnerAddress{
        void onItemClick(JSONObject item);
    }

}