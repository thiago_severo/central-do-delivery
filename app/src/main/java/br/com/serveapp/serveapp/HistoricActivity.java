package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import br.com.serveapp.serveapp.adapt.HistoricDemandAdapter;
import br.com.serveapp.serveapp.fragments.FragmentListDemandsEstablishment;
import br.com.serveapp.serveapp.repository.objects.Repository;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HistoricActivity extends AppCompatActivity {

    RecyclerView recycleListHistoricDemand;
    JSONArray listDemand = new JSONArray();
    HistoricDemandAdapter adapter;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            finish();
        }

    };

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        if(Repository.getIdUsuario(getApplicationContext())==null ){
            finish();
        }

        setContentView(R.layout.activity_historic);

        Toolbar toolbarHistoricDemand = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbarHistoricDemand);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }


        toolbarHistoricDemand.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        replaceFragment(new FragmentListDemandsEstablishment(this));

    }

    public void setAdapter(){

        JSONObject jsonObjectCantina = new JSONObject();
        JSONObject jsonObjectCantina1 = new JSONObject();
        try {
            jsonObjectCantina.put("nomeEstabeleciment", "Açaí clube");
            jsonObjectCantina1.put("nomeEstabeleciment", "Matutu's espeteria");
            jsonObjectCantina.put("status", "entregue");
            jsonObjectCantina1.put("status", "preparando");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        listDemand.put(jsonObjectCantina);
        listDemand.put(jsonObjectCantina1);

        adapter = new HistoricDemandAdapter(getApplicationContext(), listDemand );
        recycleListHistoricDemand.setAdapter(adapter);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra("result");

        }
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
        }

    }


    public void replaceFragment(Fragment destFragment)
    {
        // First get FragmentManager object.
        FragmentManager fragmentManager = this.getSupportFragmentManager();

        // Begin Fragment transaction.
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        // Replace the layout holder with the required Fragment object.
        fragmentTransaction.replace(R.id.dynamic_fragment_frame_layout, destFragment);

        // Commit the Fragment replace action.
        fragmentTransaction.commit();
    }





}
