package br.com.serveapp.serveapp.dao.webService;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentCategoriesProductEstablishment;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListDestaquesEstabeleciment extends OnPostResponse {

    FragmentCategoriesProductEstablishment fragmentCategoriesProductEstablishment;

    public ServiceListDestaquesEstabeleciment(String id, FragmentCategoriesProductEstablishment fragmentCategoriesProductEstablishment) {
        this.setId(id);
        this.fragmentCategoriesProductEstablishment = fragmentCategoriesProductEstablishment;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_destaques_estabelecimento.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONArray result = jsonObject.getJSONArray("result");
            fragmentCategoriesProductEstablishment.initializeAdapterDestaques(result);
            fragmentCategoriesProductEstablishment.reloadLayout();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if (!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                fragmentCategoriesProductEstablishment.initializeAdapterDestaques(result);
                fragmentCategoriesProductEstablishment.reloadLayout();
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}