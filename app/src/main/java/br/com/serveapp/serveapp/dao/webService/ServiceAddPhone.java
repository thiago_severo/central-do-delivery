package br.com.serveapp.serveapp.dao.webService;

import android.content.Intent;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.CadastroTelefoneActivityForLoginApi;
import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.NavigationDrawerActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.SearchAddressActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ServiceAddPhone extends OnPostResponse {

    CadastroTelefoneActivityForLoginApi cadastroTelefoneActivityForLoginApi;
    ImageView imageView;

    public ServiceAddPhone(CadastroTelefoneActivityForLoginApi cadastroTelefoneActivityForLoginApi) {
        this.cadastroTelefoneActivityForLoginApi = cadastroTelefoneActivityForLoginApi;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "alter_phone.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(cadastroTelefoneActivityForLoginApi.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        cadastroTelefoneActivityForLoginApi.mProgress.dismiss();
    }

    @Override
    public void onResponse(String response) {
        if (!response.equals("null"))

            try {
                cadastroTelefoneActivityForLoginApi.mProgress.dismiss();
                JSONObject user = Repository.getUser(cadastroTelefoneActivityForLoginApi);
                JSONObject jsonObject = new JSONObject(response);
                user.put("telefone", jsonObject.getString("phone"));
                Dao.save(user.toString(), FilesName.user, cadastroTelefoneActivityForLoginApi.getApplicationContext());
                cadastroTelefoneActivityForLoginApi.sucessAddPhone();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(cadastroTelefoneActivityForLoginApi.getApplicationContext(), "verifique sua conexão", Toast.LENGTH_LONG).show();
                cadastroTelefoneActivityForLoginApi.mProgress.dismiss();
            }

    }

}
