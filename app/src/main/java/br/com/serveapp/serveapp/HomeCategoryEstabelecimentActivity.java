package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import br.com.serveapp.serveapp.adapt.AdapterGridCategoryItens;
import br.com.serveapp.serveapp.adapt.PromocaoListAdapter;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.R;

import java.util.ArrayList;
import java.util.List;

public class HomeCategoryEstabelecimentActivity extends AppCompatActivity {

    RecyclerView recycleGridCategoryItensEstabeleciment;
    RecyclerView recycleHorizontalListItensEstabeleciment;
    AdapterGridCategoryItens categorieListAdapter;
    PromocaoListAdapter promocaoListAdapter;

    List<Categoria> categoriaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_category_estabeleciment);

        recycleGridCategoryItensEstabeleciment = findViewById(R.id.recycleGridCategoryItensEstabeleciment);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(this, 2);
        recycleGridCategoryItensEstabeleciment.setLayoutManager(mLayoutManager);

        recycleHorizontalListItensEstabeleciment = findViewById(R.id.recyclePromocoes);
        LinearLayoutManager llmHorizontal = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        recycleHorizontalListItensEstabeleciment.setLayoutManager(llmHorizontal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarHomeCategory);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String[] plants = new String[]{
                "R. Antonio de Melo Lima, 771. N Sra Conceicao",
                "Bolean birch",
                "Canoe birch",
                "Cherry birch",
                "European weeping birch"
        };

        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,plants
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        Spinner spinner = findViewById(R.id.spinnerHomeHestabelecimento);
        spinner.setAdapter(spinnerArrayAdapter);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        initializeDataCategories();
        //initializeAdapterCategories();
     //   initializeAdapterPromo();
     /*   categorieListAdapter.setListener(new AdapterGridCategoryItens.onItemClickListener() {
            @Override
            public void onItemClick(Categoria categoria) {
               Intent intent = new Intent(getApplicationContext(), ListProductsCardapioEstabeleciment.class);
                startActivity(intent);
                finish();
            }
        });
*/




    }


  /*  private void initializeAdapterCategories() {
        categorieListAdapter = new AdapterGridCategoryItens(getApplicationContext(), categoriaList);
        recycleGridCategoryItensEstabeleciment.setAdapter(categorieListAdapter);
    }*/


/*    private void initializeAdapterPromo() {
        promocaoListAdapter = new PromocaoListAdapter(getApplicationContext(), categoriaList);
        recycleHorizontalListItensEstabeleciment.setAdapter(promocaoListAdapter);
    }
*/


    private List<Categoria> initializeDataCategories(){

        categoriaList = new ArrayList<Categoria>();

        Categoria categoria = new Categoria();
        categoria.setId(1);
      //  categoria.setItens(initializeDataItens());
        categoria.setNome("bebidas");
        categoria.setLogo("bebidas_categorie");

        Categoria categoria2 = new Categoria();
        categoria2.setId(2);
     //   categoria2.setItens(initializeDataItens());
        categoria2.setNome("Lanches");
        categoria2.setLogo("lanches");


        Categoria categoria3 = new Categoria();
        categoria3.setId(3);
       // categoria3.setItens(initializeDataItens());
        categoria3.setNome("Petiscos");
        categoria3.setLogo("petisco");

        categoriaList.add(categoria);
        categoriaList.add(categoria2);
        categoriaList.add(categoria3);

        return categoriaList;

    }


}
