package br.com.serveapp.serveapp.entidades;

import java.util.Date;
import java.util.List;

import br.com.serveapp.serveapp.entidades.cardapio.Cardapio;
import br.com.serveapp.serveapp.enun.TipoAtendimento;

public class Estabelecimento {

    private long id;
    private String logo;
    private String nome, desc, telefone;
    private int tempMedEntrega;
    private Date dataCriacao;
    private Date entradaNoApp;
    private String slogan;
    private List<HorarioFuncionamento> horariosDelivery;
    private List<HorarioFuncionamento> horariosFuncionamento;
    private float valorEntrega;
    private Avaliacao avaliacao;
    private TipoAtendimento tipoAtendimento;
    private String tipoEstabelecimento;
    private Cardapio cardapio;
    private Endereco end;
    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getTelefone() {
        return telefone;
    }
    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    public int getTempMedEntrega() {
        return tempMedEntrega;
    }
    public void setTempMedEntrega(int tempMedEntrega) {
        this.tempMedEntrega = tempMedEntrega;
    }
    public List<HorarioFuncionamento> getHorariosDelivery() {
        return horariosDelivery;
    }
    public void setHorariosDelivery(List<HorarioFuncionamento> horariosDelivery) {
        this.horariosDelivery = horariosDelivery;
    }
    public List<HorarioFuncionamento> getHorariosFuncionamento() {
        return horariosFuncionamento;
    }
    public void setHorariosFuncionamento(List<HorarioFuncionamento> horariosFuncionamento) {
        this.horariosFuncionamento = horariosFuncionamento;
    }
    public float getValorEntrega() {
        return valorEntrega;
    }
    public void setValorEntrega(float valorEntrega) {
        this.valorEntrega = valorEntrega;
    }
    public Avaliacao getAvaliacao() {
        return avaliacao;
    }
    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }
    public TipoAtendimento getTipoAtendimento() {
        return tipoAtendimento;
    }
    public void setTipoAtendimento(TipoAtendimento tipoAtendimento) {
        this.tipoAtendimento = tipoAtendimento;
    }
    public String getTipoEstabelecimento() {
        return tipoEstabelecimento;
    }
    public void setTipoEstabelecimento(String tipoEstabelecimento) {
        this.tipoEstabelecimento = tipoEstabelecimento;
    }
    public Cardapio getCardapio() {
        return cardapio;
    }
    public void setCardapio(Cardapio cardapio) {
        this.cardapio = cardapio;
    }
    public Endereco getEnd() {
        return end;
    }
    public void setEnd(Endereco end) {
        this.end = end;
    }
    public String getSlogan() {
        return slogan;
    }
    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }
    public Date getEntradaNoApp() {
        return entradaNoApp;
    }
    public void setEntradaNoApp(Date entradaNoApp) {
        this.entradaNoApp = entradaNoApp;
    }
    public Date getDataCriacao() {
        return dataCriacao;
    }
    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

}
