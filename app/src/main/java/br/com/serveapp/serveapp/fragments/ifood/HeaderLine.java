package br.com.serveapp.serveapp.fragments.ifood;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;

import org.json.JSONException;
import org.json.JSONObject;

public class HeaderLine extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView moreButton;
    JSONObject header;
    View selectHeader;
    int categoria;
    LinearLayoutManager bodyLayout;
    LinearLayoutManager headerLayout;

    public HeaderLine(View itemView, LinearLayoutManager bodyLayout, LinearLayoutManager headerLayout) {
        super(itemView);
        this.bodyLayout = bodyLayout;
        this.headerLayout = headerLayout;
        moreButton = itemView.findViewById(R.id.textMenuRecyclerCategories);
        selectHeader = itemView.findViewById(R.id.viewMenuCategorieRec);
    }

    public void selected(){
        selectHeader.setBackgroundColor(Color.RED);
    }

    public void desselected(){
        selectHeader.setBackgroundColor(Color.WHITE);
    }

    public void initialize(final JSONObject header, int categoria){
        this.header = header;
        this.categoria =categoria;
        try {
            moreButton.setText(header.getString("nome"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        HeaderLineGroup.selected(this);
        bodyLayout.scrollToPositionWithOffset(categoria, 0);
    }
}
