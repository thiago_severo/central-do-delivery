package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.FormaPagamentoActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class LinePaymentFormListAdapterView extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListnerPayment listner;
    Context context;
    View v;

    public static View selected;
    public static View viewSaved;
    public static int idSaved = 0;

    public static String iconSelected;

    public static int id = 0;


    public static void setSelected(View s, int id) {

        LinePaymentFormListAdapterView.id = id;

        unSelected(LinePaymentFormListAdapterView.selected);

        LinePaymentFormListAdapterView.selected = s;
        s.setBackground(s.getResources().getDrawable(R.color.colorPrimary));

    }

    public static void unSelected(View s) {
        if(s!=null){
            s.setBackground(s.getResources().getDrawable(R.color.background));
        }
    }


    public void setListner(OnItemClickListnerPayment listner) {
        this.listner = listner;
    }

    int mPosition = -1;
    FormaPagamentoActivity formaPagamentoActivity;

    public LinePaymentFormListAdapterView(FormaPagamentoActivity formaPagamentoActivity, Context context, JSONArray listViewItem) {
        this.context = context;
        this.listViewItem = listViewItem;
        this.formaPagamentoActivity = formaPagamentoActivity;
        LinePaymentFormListAdapterView.selected = null;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder lineSimpleItemHolder = null;
        v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_payment_type, parent, false);
        lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {

            ((LineSimpleItemHolder) holder).textPayment.setText(listViewItem.getJSONObject(position).getString("nome"));
            ((LineSimpleItemHolder) holder).id = (listViewItem.getJSONObject(position).getInt("_id"));
            new ListSetImage(context, ((LineSimpleItemHolder) holder).iconPayment).execute(listViewItem.getJSONObject(position).getString("icon"));

            if (((LineSimpleItemHolder) holder).id == idSaved && FormaPagamentoActivity.select_online ==false) {
                setSelected(((LineSimpleItemHolder) holder).border, ((LineSimpleItemHolder) holder).id);
            }

            formaPagamentoActivity.enabledPayment();

        //    Toast.makeText(context,"icon: "+ listViewItem.getJSONObject(position).getString("icon"), Toast.LENGTH_LONG).show();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        public ImageView iconPayment;
        public TextView textPayment;
        public int id;
        CardView cardTipoPagamento;
        ConstraintLayout border;
        public View itemView;

        public LineSimpleItemHolder(View itemView) {
            super(itemView);

            this.itemView = itemView;
            iconPayment = itemView.findViewById(R.id.iconPayment);
            textPayment = itemView.findViewById(R.id.textPayment);
            cardTipoPagamento = itemView.findViewById(R.id.cardTipoPagmento);
            border = itemView.findViewById(R.id.border);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPosition = getAdapterPosition();
                    try {
                        listner.onItemClick(listViewItem.getJSONObject(mPosition));
                        setSelected(border, listViewItem.getJSONObject(getAdapterPosition()).getInt("_id"));
                        formaPagamentoActivity.unselectMoney();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    public interface OnItemClickListnerPayment {
        void onItemClick(JSONObject item);
    }

}