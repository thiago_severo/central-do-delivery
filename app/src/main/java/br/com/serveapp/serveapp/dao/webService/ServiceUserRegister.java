package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.CreateAccountUserActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceUserRegister extends OnPostResponse {

    CreateAccountUserActivity createAccountUserActivity;

    public ServiceUserRegister(CreateAccountUserActivity createAccountUserActivity){

        this.createAccountUserActivity = createAccountUserActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "insert_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(createAccountUserActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        createAccountUserActivity.mProgress.dismiss();
        createAccountUserActivity.btn_create_acc.setEnabled(true);
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();

        paramLogin.put("nome", createAccountUserActivity.getTxtNome().getText().toString());
        paramLogin.put("sobrenome", createAccountUserActivity.getTxtSobrenome().getText().toString());
        paramLogin.put("telefone", createAccountUserActivity.getTxtTelefone());
        paramLogin.put("email", createAccountUserActivity.getTxtEmail().getText().toString());
        paramLogin.put("senha", createAccountUserActivity.getTxtSenha().getText().toString());
        paramLogin.put("token", CreateAccountUserActivity.getToken(createAccountUserActivity.getApplicationContext()));

        return paramLogin;
    }


    @Override
    public void onResponse(String response) {

        if(!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject user = null;
                if (!jsonObject.isNull("result")) {
                    user = jsonObject.getJSONObject("result");
                    Repository.setUser(user);
                    Dao.save(user.toString(), FilesName.user, createAccountUserActivity.getApplicationContext());
                    createAccountUserActivity.startCreateAddressActivity();
                    createAccountUserActivity.mProgress.dismiss();
                    createAccountUserActivity.finish();
                }
                else{

                    JSONObject erro = jsonObject.getJSONObject("erro");
                    createAccountUserActivity.mProgress.dismiss();
                    createAccountUserActivity.buildConfirmDialog(erro.getString("titulo"), erro.getString("msg"));

                }
            }catch (JSONException e){

            }

    }


}
