package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ListAllProducts;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListAllProducts extends OnPostResponse {

    ListAllProducts listAllProducts;
    ProgressBar progressBar;
    SwipeRefreshLayout swipeRefreshLayout;
    public ServiceListAllProducts(ProgressBar progressBar, SwipeRefreshLayout swipeRefreshLayout, ListAllProducts listAllProducts){
//        this.setId(Repository.idEstabelecimento);
        this.progressBar = progressBar;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.listAllProducts = listAllProducts;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_produtos.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(listAllProducts.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        progressBar.setVisibility(View.GONE);
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(listAllProducts.getApplicationContext()));
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
            try {
                JSONObject result = jsonObject.getJSONObject("result");
                listAllProducts.initializeListProducts(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onResponse(String response) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onUpdate(String response) {
        if(!response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                listAllProducts.initializeListProducts(result);

            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}
