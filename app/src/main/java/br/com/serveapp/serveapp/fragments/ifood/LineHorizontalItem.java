package br.com.serveapp.serveapp.fragments.ifood;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class LineHorizontalItem extends RecyclerView.ViewHolder implements View.OnClickListener {

    private String id;
    private String estab_id;
    private String estab_foto;
    private TextView name;
    private TextView newPriging;
    private TextView oldPriging;
    private TextView estab_name;
    private TextView textCupomDescont;

    private ImageView imagem;




    //private ImageView imageEstabeleciment;

    JSONObject item;
    LinearLayoutManager layoutManager;
    Fragment fragment;
    Activity activity;

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setLayoutManager(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public LineHorizontalItem(View itemView) {
        super(itemView);
        imagem = itemView.findViewById(R.id.image);
        name = itemView.findViewById(R.id.name);
        newPriging = itemView.findViewById(R.id.newPriging);
        oldPriging = itemView.findViewById(R.id.oldPriging);
        estab_name = itemView.findViewById(R.id.estab_name);
        textCupomDescont = itemView.findViewById(R.id.textCupomDescont);
        itemView.setOnClickListener(this);
    }


    public void initialize(final JSONObject item) {
        this.item = item;
        try {
            name.setText(item.getString("nome"));
            newPriging.setText(item.getString("valor"));
            oldPriging.setText(item.getString("valorBase"));

            if(!item.isNull("estab_name")){
                estab_name.setText(item.getString("estab_name"));
            }else{
                estab_name.setText(item.getString("cat_name"));
            }

            new ListSetImage(itemView.getContext(), imagem).
                    execute(item.getString("foto"));
            if (!item.isNull("desconto")){
                textCupomDescont.setText(item.getString("desconto"));
                itemView.findViewById(R.id.textCupomDescont).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.textOffCupomDesconto).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.imageCupomDescont).setVisibility(View.VISIBLE);

                itemView.findViewById(R.id.view3).setVisibility(View.VISIBLE);
                itemView.findViewById(R.id.oldPriging).setVisibility(View.VISIBLE);

            }
            else{
                itemView.findViewById(R.id.textCupomDescont).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.textOffCupomDesconto).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.imageCupomDescont).setVisibility(View.INVISIBLE);

                itemView.findViewById(R.id.view3).setVisibility(View.INVISIBLE);
                itemView.findViewById(R.id.oldPriging).setVisibility(View.INVISIBLE);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(view.getContext(), DetalheItemPopUpActivity.class);
        try {
            intent.putExtra("foto", item.getString("foto"));
            intent.putExtra("value", item.getDouble("value"));
            intent.putExtra("valor", item.getString("valor"));
            String name = item.getString("cat_name")+"-"+item.getString("nome");
            intent.putExtra("nome", name);
            intent.putExtra("descricao", item.getString("descricao"));
            intent.putExtra("id_produto", item.getString("_id"));
            intent.putExtra("avaliacao",  item.getString("avaliacao"));
            intent.putExtra("tempoPreparo",  item.getString("tempPreparo"));
            if(item.has("fotos")){
                intent.putExtra("fotos",  item.getString("fotos"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            //fragment.startActivityForResult(intent, 1);
            new DetalheItemPopUpActivity(activity,intent).show();

        }catch (NullPointerException e){}

        try{
            activity.startActivityForResult(intent, 1);
        }catch (NullPointerException e){}
    }
}
