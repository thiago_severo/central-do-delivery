package br.com.serveapp.serveapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.LineProductsForEstabelecimentsAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListProductsEstabeleciment;
import br.com.serveapp.serveapp.fragments.ifood.Ifood;


public class FragmentListProductsEstablishment extends Fragment {

    View view;
    public TextView titleListProcutsEmprty;
    public TextView bodyListProductsEmpty;
    public ProgressBar progressBar;
    private LineProductsForEstabelecimentsAdapter lineAdapter;
    public static int cdCategory;
    public Ifood ifood;
    public static JSONArray listRecyclerHorizontalCategories;
    SearchView searchView;
    Request requestListProductsEstabeleciment;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_list_products_establishment, container, false);
        progressBar = view.findViewById(R.id.progressBar3);
        searchView = view.findViewById(R.id.searchView);

        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        titleListProcutsEmprty = view.findViewById(R.id.titleListProcutsEmprty);
        bodyListProductsEmpty = view.findViewById(R.id.bodyListProductsEmpty);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            requestListProductsEstabeleciment.request();
                        } catch (NullPointerException e) {
                            requestListProductsEstabeleciment = new Request(getContext(), new ServiceListProductsEstabeleciment(FragmentListProductsEstablishment.this, swipeRefreshLayout));
                            requestListProductsEstabeleciment.request();
                        }
                    }
                }
        );

        ifood = new Ifood((ListProductsStablishmentDeliveryActivity) getActivity(), view, this);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {
                                                  return true;
                                              }

                                              @Override
                                              public boolean onQueryTextChange(String newText) {
                                                  ifood.filterProducts(newText);
                                                  return true;
                                              }
                                          }
        );

        progressBar.setVisibility(View.VISIBLE);
        titleListProcutsEmprty.setVisibility(View.GONE);
        bodyListProductsEmpty.setVisibility(View.GONE);

        requestListProductsEstabeleciment = new Request(getContext(), new ServiceListProductsEstabeleciment(this, swipeRefreshLayout));
        requestListProductsEstabeleciment.request();
        return view;

    }

    public void update() {

        if (requestListProductsEstabeleciment != null)
            requestListProductsEstabeleciment.request();

    }

    public void setListEmpty(JSONObject jsonObject){

        try{

            JSONArray result = jsonObject.getJSONArray("result");
            JSONObject estab = jsonObject.getJSONObject("estab");
            ListProductsStablishmentDeliveryActivity activity = (ListProductsStablishmentDeliveryActivity) getActivity();

            if(estab!=null && activity!=null)
            activity.setJsonEstab(estab);

            if(result.length()>0){
                titleListProcutsEmprty.setVisibility(View.GONE);
                bodyListProductsEmpty.setVisibility(View.GONE);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}