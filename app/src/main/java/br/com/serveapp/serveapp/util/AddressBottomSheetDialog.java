package br.com.serveapp.serveapp.util;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONObject;

import br.com.serveapp.serveapp.ChangeAddressPopUpActivity;
import br.com.serveapp.serveapp.R;

public class AddressBottomSheetDialog extends BottomSheetDialogFragment {


    private String idEndereco;
    private ChangeAddressPopUpActivity changeAddressPopUpActivity;
    private JSONObject address;

    public AddressBottomSheetDialog(String idEndereco, ChangeAddressPopUpActivity changeAddressPopUpActivity, JSONObject address) {

        this.address = address;
        this.idEndereco = idEndereco;
        this.changeAddressPopUpActivity = changeAddressPopUpActivity;

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = null;

        v = inflater.inflate(R.layout.bottom_sheet_layout, container, false);
        TextView deleteAddress = v.findViewById(R.id.deleteAddressSheet);
        TextView editAddress = v.findViewById(R.id.editAddress);

        deleteAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAddressPopUpActivity.deleteByBottomSheet(idEndereco);
            }
        });

        editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAddressPopUpActivity.openToEditAddress(address);
            }
        });

        return v;
    }

}