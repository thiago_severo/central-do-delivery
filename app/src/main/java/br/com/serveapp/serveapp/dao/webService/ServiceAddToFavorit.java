package br.com.serveapp.serveapp.dao.webService;

import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.BaseEstabelecimentActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceAddToFavorit extends OnPostResponse {

    BaseEstabelecimentActivity listProductsStablishmentDeliveryActivity;
    ImageView imageView;

    public ServiceAddToFavorit(ImageView imageView, BaseEstabelecimentActivity listProductsStablishmentDeliveryActivity) {
        this.listProductsStablishmentDeliveryActivity = listProductsStablishmentDeliveryActivity;
        this.imageView = imageView;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "add_fav_cliente.php";
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(listProductsStablishmentDeliveryActivity.getApplicationContext()));
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        return paramLogin;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        if (!response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("result")) {
                    listProductsStablishmentDeliveryActivity.animAddFavorit(imageView);
                    Repository.fav.add(Repository.idEstabelecimento);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "verifique sua conexão", Toast.LENGTH_LONG).show();
            }
    }

}
