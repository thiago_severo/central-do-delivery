package br.com.serveapp.serveapp.util;
import android.view.View;
import android.widget.CompoundButton;


import java.util.ArrayList;
import java.util.List;

public class CheckGroup {

    private int min, max;
    private List<CompoundButton> selecteds;
    public static final int undefined = 0;


    public CheckGroup(int min, int max) {
        this.max = max;
        this.min = min;
        selecteds = new ArrayList<>();
    }

    public void setChecked(CompoundButton compoundButton){
        selecteds.add(compoundButton);
    }

    public void unChecked(){
        while (selecteds.size()>0){
            CompoundButton cb = selecteds.remove(0);
            cb.setChecked(false);
        }
    }

    public int getMax() {
        return max;
    }
    public int getMin() {
        return min;
    }
    public void setMax(int max) {
        this.max = max;
    }
    public void setMin(int min) {
        this.min = min;
    }

    private void checked(CompoundButton checkBox){
        if(max!=undefined && selecteds.size()>=max){
            CompoundButton temp = selecteds.remove(0);
            temp.setChecked(false);
        }
        selecteds.add(checkBox);
    }

    private void unChecked(CompoundButton checkBox){
        if(min!=undefined && selecteds.size()<= min){
            checkBox.setChecked(true);
        }else{
            checkBox.setChecked(false);
            selecteds.remove(checkBox);
        }
    }

    public void add(final CompoundButton checkBox){
        checkBox.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(!selecteds.contains(checkBox))
                {
                    checked(checkBox);
                }
                else
                {
                    unChecked(checkBox);
                }
            }
        });
    }

}