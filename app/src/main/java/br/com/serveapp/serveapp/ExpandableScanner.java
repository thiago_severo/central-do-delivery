package br.com.serveapp.serveapp;

import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ExpandableScanner extends RequestActivity implements ZXingScannerView.ResultHandler {

    ZXingScannerView scannerView;
    Button insertCodeMesa;
    ImageView imageFlash;
    boolean flash;
    public ProgressDialog mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expandable_scanner);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        insertCodeMesa = findViewById(R.id.insertCodeMesa);
        scannerView = findViewById(R.id.z_xing_scannerr);
        imageFlash = findViewById(R.id.imageFlash);

        mProgress = new ProgressDialog(this);
        String titleId = "Entrando...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("verificando conexão com a internet...");

        imageFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flash == false) {
                    flash = true;
                    scannerView.setFlash(true);
                    imageFlash.setImageResource(R.drawable.ic_serve_flash_on);
                } else if (flash == true) {
                    flash = false;
                    scannerView.setFlash(false);
                    imageFlash.setImageResource(R.drawable.ic_serve_flash_off);
                }

            }

        });

        insertCodeMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        List list = new ArrayList<BarcodeFormat>();
        list.add(BarcodeFormat.QR_CODE);
        scannerView.setFormats(list);
        scannerView.setAutoFocus(true);

    }


    @Override
    public void onCache(String response) {

    }

    @Override
    protected void onResume() {
        super.onResume();


        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            scannerView.setResultHandler(this);
            scannerView.startCamera();
        } else {
            ActivityCompat.requestPermissions(ExpandableScanner.this, new String[]{Manifest.permission.CAMERA}, 1024);
        }

    }

    @Override
    public void handleResult(Result scanResult) {
        // ----------------------------------------
        mProgress.show();
        String id_cliente = Repository.getIdUsuario(getApplicationContext());
        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("function_method", "check-in");
        paramLogin.put("id_mesa", scanResult.getText().toString());
        paramLogin.put("instructionId", "check-in");
        paramLogin.put("id_cliente", id_cliente);
        request(Script.check_in, paramLogin);

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Verifique sua conexão com a internet", Toast.LENGTH_LONG).show();
        mProgress.dismiss();
    }

    public void showDialog() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.dialog_input_code_mesa, null);

        Button btnCancelCodeMesa = mView.findViewById(R.id.btnCancelCodeMesa);
        Button btnConfirmCodeMesa = mView.findViewById(R.id.btnConfirmCodeMesa);
        final EditText inputCodeMesa = mView.findViewById(R.id.inputCodeMesa);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        btnCancelCodeMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirmCodeMesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mProgress.show();
                String id_cliente = null;
                id_cliente = Repository.getIdUsuario(getApplicationContext());
                final Map<String, String> paramLogin = new HashMap<>();
                paramLogin.put("function_method", "check-in-cod");
                paramLogin.put("id_mesa", inputCodeMesa.getText().toString());
                paramLogin.put("id_cliente", id_cliente);
                request(Script.check_in, paramLogin);

            }
        });

        dialog.show();

    }

    @Override
    public void onResponseServe(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                //check-in
                JSONObject result = jsonObject.getJSONObject("result");
                Repository.idEstabelecimento = result.getString("id_estabelecimento");
                Repository.atendimentoLocal = true;
                Intent intentOpenServiceLocal = new Intent(getApplicationContext(), SplashScreenEstabeleciment.class);
                intentOpenServiceLocal.putExtra("foto", result.getString("foto"));
                intentOpenServiceLocal.putExtra("slogan", result.getString("slogan"));
                intentOpenServiceLocal.putExtra("nome", result.getString("nome"));
                intentOpenServiceLocal.putExtra("tipoAtendimento", "local");

                startActivity(intentOpenServiceLocal);
                mProgress.dismiss();
                finish();
            } catch (JSONException e) {
                e.printStackTrace();
                mProgress.dismiss();
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                scannerView.startCamera();
                scannerView.resumeCameraPreview(this);
            }
        }

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        flash = false;
        scannerView.setFlash(false);
    }


}
