package br.com.serveapp.serveapp;

import android.app.AlertDialog;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.drawer.MyBounceInterpolator;

public class BaseEstabelecimentActivity extends RequestActivity {

    public void animAddFavorit(ImageView imageView){

        final Animation myAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        imageView.setImageResource(R.drawable.icon_favorit_clicked);
        imageView.startAnimation(myAnim);

    }
    public void animRemoveFavorit(ImageView imageView){
        final Animation myAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.scale);
        MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
        myAnim.setInterpolator(interpolator);
        imageView.setImageResource(R.drawable.ic_favorite_border_white_24dp);
        imageView.startAnimation(myAnim);

    }

    public void  buildConfirmDialog( String texto){

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView text = mView.findViewById(R.id.textSucessMessage);
        TextView textViewMain = mView.findViewById(R.id.textMainMsg);

        text.setText(texto);
        textViewMain.setText("OK");
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }


    @Override
    public void onResponseServe(String response) {

    }

    @Override
    public void onCache(String response) {

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }
}
