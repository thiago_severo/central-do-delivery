package br.com.serveapp.serveapp.entidades.cardapio;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;

import java.util.List;

public class GroupItemPedido implements ParentObject {

    private ItemPedido itemPedido;
    private int countIntPedido;
    private float valorToal;

    public ItemPedido getItemPedido() {
        return itemPedido;
    }

    public int getCountIntPedido() {
        return countIntPedido;
    }

    public float getValorToal() {
        return valorToal;
    }

    public void setItemPedido(ItemPedido itemPedido) {
        this.itemPedido = itemPedido;
    }

    public void setCountIntPedido(int countIntPedido) {
        this.countIntPedido = countIntPedido;
    }

    public void setValorToal(float valorToal) {
        this.valorToal = valorToal;
    }

    @Override
    public List<Object> getChildObjectList() {
        return null;
    }

    @Override
    public void setChildObjectList(List<Object> list) {

    }
}
