package br.com.serveapp.serveapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

import br.com.serveapp.serveapp.adapt.LineCashback;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceCashBack;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class CashBackActivity extends AppCompatActivity {

    RecyclerView listCashBack;
    public TextView textSaldoCash;
    public ProgressDialog mProgress;
    public Button buttonResgatar;
    Request request;
    Double saldoCashBack;
    Double valorSelecionado;
    NumberFormat nf;
    public ProgressBar progressBar;

    public AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_back);

        nf = NumberFormat.getCurrencyInstance(new Locale("pt", "BR"));

        progressBar = findViewById(R.id.progressIncrement3);
        listCashBack = findViewById(R.id.listCashBack);
        textSaldoCash = findViewById(R.id.textSaldoCash);
        buttonResgatar = findViewById(R.id.buttonResgatar);

        buttonResgatar.setVisibility(View.GONE);
        textSaldoCash.setVisibility(View.GONE);

        //nf = NumberFormat.getCurrencyInstance();
        mProgress = new ProgressDialog(this);
        String titleId = "Deletando cupom...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("aguarde...");

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);
            }
        });

        //   ServiceCashBack serviceCashBack = new ServiceCashBack(this);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listCashBack.setLayoutManager(llm);

        buttonResgatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }
        });

        request = new Request(getApplicationContext(), new ServiceCashBack(this));
        request.request();

    }

    public void showDialog() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.resgatar_cashback, null);
        final EditText needValue = mView.findViewById(R.id.needValue);
        final TextView txValor = mView.findViewById(R.id.txValor);
        // tvValor
        Button btnCancelMoney = mView.findViewById(R.id.btnRecuseCancel);
        Button btnConfirm = mView.findViewById(R.id.btnConfirmCancel);
        SeekBar seekBar = mView.findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                valorSelecionado = saldoCashBack/100*progress;
                txValor.setText(nf.format(valorSelecionado));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mBuilder.setView(mView);
        dialog = mBuilder.create();
        //mProgress.dismiss();
        dialog.show();
        btnCancelMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap param = new HashMap<String, String>();
                //Toast.makeText(getApplicationContext(), "need: "+ needValue.getText() , Toast.LENGTH_SHORT).show();
                    if(valorSelecionado!=null) {
                        if (valorSelecionado > saldoCashBack || valorSelecionado <= 0) {
                            Toast.makeText(getApplicationContext(), "O valor iserido deve ser inferior ou igual ao saldo", Toast.LENGTH_SHORT).show();
                        } else {
                            param.put("function_method", "resgatar");
                            param.put("valor", valorSelecionado.toString());
                            param.put("id_cliente", Repository.getIdUsuario(CashBackActivity.this));
                            request.request(param);
                            progressBar.setVisibility(View.VISIBLE);
                            dialog.dismiss();
                            valorSelecionado = 0.0;
                        }
                    }

            }
        });

    }

    public void initializeList(JSONArray result, Double value) {

        textSaldoCash.setText("Saldo: " + nf.format(value));
        saldoCashBack = value;

        //  Toast.makeText(getApplicationContext(),"ededed"+ value, Toast.LENGTH_LONG).show();

        LineCashback lineCashback = new LineCashback(this, result);
        lineCashback.notifyDataSetChanged();
        listCashBack.setAdapter(lineCashback);
        lineCashback.setListener(new LineCashback.ClickDeletListener() {

            @Override
            public void deleteClick(String id) {

                mProgress.show();
                Request request = new Request(getApplicationContext(), new ServiceCashBack(CashBackActivity.this));
                HashMap param = new HashMap<String, String>();
                param.put("function_method", "remove");
                param.put("id", id);
                param.put("id_cliente", Repository.getIdUsuario(CashBackActivity.this));

                request.request(param);

            }
        });

    }


}
