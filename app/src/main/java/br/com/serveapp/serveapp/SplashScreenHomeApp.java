package br.com.serveapp.serveapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceSystem;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SplashScreenHomeApp extends AppCompatActivity {

    private TextView textViewDesc;
    private ImageView imageViewLogo;
    private ProgressBar progressBar;
    private Request request;

    public void update(JSONObject obj) {
        try {

            textViewDesc.setText("Tudo que você precisa em apenas um clique");
           // textViewDesc.setText(obj.getString("desc"));
            new ListSetImage(getApplicationContext(), imageViewLogo,0).
                    execute(obj.getString("system/central.png"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_home_app);
        textViewDesc = findViewById(R.id.textViewLoginHome);
        imageViewLogo = findViewById(R.id.ImgSeloClient);
        progressBar = findViewById(R.id.progressBarSplash);
        request = new Request(getApplicationContext(), new ServiceSystem(this));
        request.request();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                nextActivity();
            }
        }, 2000);

    }


    private void nextActivity() {

        final String returnUser = Dao.read(FilesName.user, getApplicationContext());
        Intent intent;
        try {
            JSONObject jsonObject = new JSONObject(returnUser);
            Repository.setUser(jsonObject);
            intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
        } catch (JSONException e) {
            intent = new Intent(getApplicationContext(), LoginActivity.class);
        }

        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(intent);
        finish();

    }


}