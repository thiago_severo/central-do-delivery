package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCupom extends OnPostResponse {

    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;

    public ServiceCupom(ConfirmItensPedidoActiviy confirmItensPedidoActiviy) {
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        Toast.makeText(confirmItensPedidoActiviy.getApplicationContext(), "algum erro aconteceu, verifique sua conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        if (response != null && !response.equals("null")) {

            try {
                confirmItensPedidoActiviy.reload(new JSONObject(response));
                ConfirmItensPedidoActiviy.reference.progressIncrement.setVisibility(View.GONE);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

}