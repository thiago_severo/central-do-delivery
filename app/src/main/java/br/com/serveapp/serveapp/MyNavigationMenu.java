package br.com.serveapp.serveapp;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;

import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.repository.objects.Variables;

public class MyNavigationMenu implements NavigationView.OnNavigationItemSelectedListener{

    private Activity context;
    public MyNavigationMenu(Activity context,NavigationView navigationView){
        try {
            View headerView = navigationView.getHeaderView(0);
            TextView navUsername = (TextView) headerView.findViewById(R.id.account_name);
            navUsername.setText(Repository.getUser(context).getString("nome"));
            TextView navUserMail = (TextView) headerView.findViewById(R.id.account_mail);
            navUserMail.setText(Repository.getUser(context).getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.context = context;
    }

    public static void a(BottomNavigationView bottomNavigationView, Activity activity){
      /*  BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(4);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        View badge = LayoutInflater.from(activity)
                .inflate(R.layout.badge, itemView, true);
        TextView tv = badge.findViewById(R.id.notifications_badge);
        tv.setText(Repository.qtdCart);
*/

//        int menuItemId = bottomNavigationView.getMenu().getItem(4).getItemId();


    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_perfil) {
            context.startActivityForResult(new Intent(context.getApplicationContext(), ProfileUserActivity.class), Variables.OPEN_PROFILE);
        } else if (id == R.id.nav_fidelidade) {
            context.startActivityForResult(new Intent(context.getApplicationContext(), FidelidadeCardActivity.class), Variables.OPEN_PROFILE);
        } else if (id == R.id.nav_histórico) {
            context.startActivity(new Intent(context.getApplicationContext(), HistoricActivity.class));
        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=br.com.serveapp.serveapp");
            context.startActivity(Intent.createChooser(intent, "Compartilhar com"));

        } else if (id == R.id.nav_sair) {
            Boolean delete = Dao.delete(FilesName.user, context.getApplicationContext());
            context.startActivity(new Intent(context.getApplicationContext(), LoginActivity.class));
            context.finish();
        } else if (id == R.id.nav_cashback) {
            context.startActivityForResult(new Intent(context.getApplicationContext(), CashBackActivity.class), Variables.OPEN_PROFILE);
        }
        else if (id == R.id.nav_payment_forms) {
            context.startActivity(new Intent(context.getApplicationContext(), MyCardsPaymentActivity.class));
        }

        else if(id == R.id.nav_mensagens) {
            context.startActivity(new Intent(context.getApplicationContext(), ListMessagesChatActivity.class));
        }

        else if(id == R.id.nav_send) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "servetecnologia@gmail.com", null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Fale conosco");
            context.startActivity(Intent.createChooser(emailIntent, null));
        }

        DrawerLayout drawer = context.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
