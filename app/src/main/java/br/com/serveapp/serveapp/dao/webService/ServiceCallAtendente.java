package br.com.serveapp.serveapp.dao.webService;

import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.BaseEstabelecimentActivity;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCallAtendente extends OnPostResponse {

    BaseEstabelecimentActivity listProductsStablishmentDeliveryActivity;
    ImageView imageView;

    public ServiceCallAtendente( ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity) {
        this.listProductsStablishmentDeliveryActivity = listProductsStablishmentDeliveryActivity;
        this.imageView = imageView;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "solicitation_table.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        if (!response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("result")) {
               //    Toast.makeText(listProductsStablishmentDeliveryActivity, jsonObject.getJSONObject("result").getString("msg"), Toast.LENGTH_LONG).show();
                    listProductsStablishmentDeliveryActivity.buildConfirmDialog(jsonObject.getJSONObject("result").getString("msg"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "verifique sua conexão", Toast.LENGTH_LONG).show();
            }
    }

}
