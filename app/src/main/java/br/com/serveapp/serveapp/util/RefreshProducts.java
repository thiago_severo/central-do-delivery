package br.com.serveapp.serveapp.util;

import org.json.JSONObject;

public interface RefreshProducts {

    public void refresh(JSONObject obj);
}
