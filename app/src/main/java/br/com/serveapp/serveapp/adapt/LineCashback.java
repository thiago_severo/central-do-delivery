package br.com.serveapp.serveapp.adapt;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import br.com.serveapp.serveapp.R;

public class LineCashback extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    Context context;
    ClickDeletListener clickDeletListener;


    public LineCashback(Context context, JSONArray listViewItem) {
        this.context = context;
        this.listViewItem = listViewItem;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (listViewItem.getJSONObject(position).getString("resgatado").equals("0")) {
                return 0;
            } else {
                return 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder lineSimpleItemHolder = null;

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_cashback, parent, false);
        lineSimpleItemHolder = new LineSimpleItem(v);

        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        try {
            ((LineSimpleItem) holder).textValue.setText(listViewItem.getJSONObject(position).getString("valor"));
            ((LineSimpleItem) holder).textCodigo.setText(listViewItem.getJSONObject(position).getString("cod_promocional"));
            ((LineSimpleItem) holder).setResgatado(listViewItem.getJSONObject(position).getString("resgatado").equals("1"));
            ((LineSimpleItem) holder).id_cupom = listViewItem.getJSONObject(position).getString("_id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    class LineSimpleItem extends RecyclerView.ViewHolder {

        TextView textValue;
        TextView textCodigo;
        TextView clickDelete;
        ImageView btCopy;
        String id_cupom;
        boolean resgatado;

        public LineSimpleItem(View itemView) {
            super(itemView);
            clickDelete = itemView.findViewById(R.id.clickDelete);
            textCodigo = itemView.findViewById(R.id.codeCashBack);
            textValue = itemView.findViewById(R.id.textValueCode);
            btCopy = itemView.findViewById(R.id.btCopy);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!resgatado) {
                        ClipboardManager clipboard = (ClipboardManager) view.getContext().getSystemService(Context.CLIPBOARD_SERVICE);
                        ClipData clip = ClipData.newPlainText("RANDOM UUID", textCodigo.getText().toString());
                        Toast.makeText(view.getContext(), "Código copiado", Toast.LENGTH_LONG).show();
                        clipboard.setPrimaryClip(clip);
                    }
                }
            });

            clickDelete.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    if(!resgatado){
                        clickDeletListener.deleteClick(id_cupom);
                    }
                }
            });
        }

        public void setResgatado(boolean resgatado) {
            this.resgatado = resgatado;
            if(resgatado){
                textCodigo.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                textValue.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                clickDelete.setEnabled(false);
                int cor = Color.argb(255, 190, 190, 190);
                textCodigo.setTextColor(cor);
                textValue.setTextColor(cor);
                btCopy.setColorFilter(cor);
            }
        }
    }

    public interface ClickDeletListener {
        public void deleteClick(String id);
    }


    public void setListener(ClickDeletListener listener) {
        this.clickDeletListener = listener;
    }

}