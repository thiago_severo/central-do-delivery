package br.com.serveapp.serveapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.AdapterGridCategoryItens;
import br.com.serveapp.serveapp.adapt.LineProductsForEstabelecimentsAdapter;
import br.com.serveapp.serveapp.adapt.LocalDemandAdapter;
import br.com.serveapp.serveapp.adapt.PromocaoListAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListCategoryEstabeleciment;
import br.com.serveapp.serveapp.dao.webService.ServiceListDestaquesEstabeleciment;
import br.com.serveapp.serveapp.entidades.Cliente;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.repository.objects.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static br.com.serveapp.serveapp.repository.objects.Repository.listOfPedidosToTabLocal;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FragmentCategoriesProductEstablishment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Request requestListCategoryEstabeleciment;
    private Request requestListDestaquesEstabeleciment;
    ConstraintLayout constraintLayout;
    LocalDemandAdapter adapter;
    private RecyclerView listDemandOfClient;
    View view;

    private LineProductsForEstabelecimentsAdapter lineAdapter;
   boolean preencher;

    ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity;
    public FragmentCategoriesProductEstablishment(ListProductsStablishmentDeliveryActivity activity ) {
      listProductsStablishmentDeliveryActivity = activity;
   }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    RecyclerView recycleGridCategoryItensEstabeleciment;
    RecyclerView recycleHorizontalListItensEstabeleciment;
    public AdapterGridCategoryItens categorieListAdapter;
    PromocaoListAdapter promocaoListAdapter;
    List<Categoria> categoriaList;

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home_category_estabeleciment, container, false);

            recycleGridCategoryItensEstabeleciment = view.findViewById(R.id.recycleGridCategoryItensEstabeleciment);
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
            recycleGridCategoryItensEstabeleciment.setLayoutManager(mLayoutManager);

            recycleHorizontalListItensEstabeleciment = view.findViewById(R.id.recyclePromocoes);
            LinearLayoutManager llmHorizontal = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
            recycleHorizontalListItensEstabeleciment.setLayoutManager(llmHorizontal);
        String idEstabelecimento = Repository.idEstabelecimento;
        requestListCategoryEstabeleciment = new Request(getApplicationContext(), new ServiceListCategoryEstabeleciment(idEstabelecimento, this));
        requestListCategoryEstabeleciment.request();

        requestListDestaquesEstabeleciment = new Request(getApplicationContext(), new ServiceListDestaquesEstabeleciment(idEstabelecimento, this));
        requestListDestaquesEstabeleciment.request();
        return view;

    }

    public void setAdapter(){
        Cliente cliente = new Cliente();
        cliente.setId(1);

        adapter = new LocalDemandAdapter(getApplicationContext(), listOfPedidosToTabLocal,cliente);
        listDemandOfClient.setAdapter(adapter);

    }

    public void initializeAdapterDestaques( JSONArray categoriaListObjects) {
        promocaoListAdapter = new PromocaoListAdapter(getApplicationContext(), categoriaListObjects);
        recycleHorizontalListItensEstabeleciment.setAdapter(promocaoListAdapter);
        recycleHorizontalListItensEstabeleciment.getAdapter().notifyDataSetChanged();

    }

    public void initializeAdapterCategories(final JSONArray categoriaListObjects) {
        categorieListAdapter = new AdapterGridCategoryItens(getApplicationContext(), categoriaListObjects);
        categorieListAdapter.setListener(new AdapterGridCategoryItens.onItemClickListener() {
            @Override
            public void onItemClick(JSONObject categoria) {
                try {
                    listProductsStablishmentDeliveryActivity.changeFragment(categoriaListObjects, categoria);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        recycleGridCategoryItensEstabeleciment.setAdapter(categorieListAdapter);
        recycleGridCategoryItensEstabeleciment.getAdapter().notifyDataSetChanged();

    }

    public void reloadLayout(){
    }

}
