package br.com.serveapp.serveapp.dao.webService;

import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ScrollingActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceDeatlhesEstabelecimento extends OnPostResponse {

    String id_estabelecimento;
    ScrollingActivity scrollingActivity;

    public ServiceDeatlhesEstabelecimento(ScrollingActivity scrollingActivity) {
        setId(Repository.idEstabelecimento);
        this.scrollingActivity = scrollingActivity;
        this.id_estabelecimento = id_estabelecimento;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_details_estabelecimento.php";
    }

    @Override
    public Map<String, String> getParam() {
        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(scrollingActivity.getApplicationContext()));
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        return paramLogin;
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            scrollingActivity.setEstabelecimento(jsonObject.getJSONObject("result"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                scrollingActivity.setEstabelecimento(jsonObject.getJSONObject("result"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}