package br.com.serveapp.serveapp.dao.webService;

import android.app.Activity;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.AvaliacaoInterface;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceAvaliarPedido extends OnPostResponse {

    AvaliacaoInterface fragmentListDemandsEstablishment;
    Activity activity;
    public static String position;

    public ServiceAvaliarPedido(AvaliacaoInterface fragmentListDemandsEstablishment, Activity activity) {
        this.activity = activity;
        this.fragmentListDemandsEstablishment = fragmentListDemandsEstablishment;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "avaliar_pedido.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(activity, "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        if (response != null && !response.equals("null"))
            try {

                JSONObject jsonObject = new JSONObject(response);
                fragmentListDemandsEstablishment.dimissProgressBarSendFeedBack();
                fragmentListDemandsEstablishment.dimissPopUpAvaliacao();
                fragmentListDemandsEstablishment.showMsg(jsonObject.getJSONObject("result").getJSONObject("msg").getString("title"), jsonObject.getJSONObject("result").getJSONObject("msg").getString("text"));
                position = null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

}