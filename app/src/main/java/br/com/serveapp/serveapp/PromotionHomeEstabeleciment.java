package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import br.com.serveapp.serveapp.R;

public class PromotionHomeEstabeleciment extends AppCompatActivity {

    TextView descText;
    ImageButton plus, minus;
    TextView mItemDescription;

    private ProgressBar progressBar;
    private int progressStatus = 0;
    private TextView textView;
    private Handler handler = new Handler();
    CardView cardView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion_home_estabeleciment);
        cardView = findViewById(R.id.cardDetailDemand);

        LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.line_demand_detail,null);
        View view2 = inflater.inflate(R.layout.line_demand_detail,null);

        LinearLayout linearLayout = findViewById(R.id.linearLayoutCardDemand);
        linearLayout.addView(view);
        linearLayout.addView(view2);

        progressBar = (ProgressBar) findViewById(R.id.progressBarDemandLiked);
        textView = (TextView) findViewById(R.id.textSucessMessage);
        // Start long running operation in a background thread
        progressBar.setProgress(50);

        final CardView card = findViewById(R.id.cardRolation);
        mItemDescription  = findViewById(R.id.mItemDescription);

     /*   mItemDescription.setText("-animate()\n" +
                          "-setListener(null\n"+
                "-animate()");
*/



       /* card.animate()
                .translationY(card.getHeight())
                .alpha(0.0f)
                .setDuration(300);
*/
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            collapseExpandTextView();
              }
        });

    }


    void collapseExpandTextView() {
        if (cardView.getVisibility() == View.GONE) {

            cardView.setVisibility(View.VISIBLE);

            AlphaAnimation animation1 = new AlphaAnimation(0.2f, 1.0f);
            animation1.setDuration(500);
            cardView.setAlpha(1f);
            cardView.startAnimation(animation1);



        } else {

            cardView.setAlpha(0.5f);
            cardView.animate()
                    .alpha(0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            cardView.setVisibility(View.GONE);
                        }
                    });

           }


    }

}
