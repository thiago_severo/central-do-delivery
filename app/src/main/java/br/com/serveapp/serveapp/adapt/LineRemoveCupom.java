package br.com.serveapp.serveapp.adapt;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineRemoveCupom extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListner onItemClickListner;

    public LineRemoveCupom(JSONArray listViewItem) {
        this.listViewItem = listViewItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
           View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_cupom_remove, parent, false);
             lineSimpleItemHolder = new LineSimpleItemHolder(v);
            return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            ((LineSimpleItemHolder)holder).txt_nome_item.setText(listViewItem.getJSONObject(position).getString("ref"));
            ((LineSimpleItemHolder)holder).nameCupom.setText(listViewItem.getJSONObject(position).getString("title"));
            ((LineSimpleItemHolder)holder).textDateValCupom.setText(listViewItem.getJSONObject(position).getString("validade"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public interface OnItemClickListner{
        void onItemClick(JSONObject item);
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        public TextView nameCupom;
        public TextView textDateValCupom;
        public Button btnRemover;
        public TextView txt_nome_item;
        public LineSimpleItemHolder(View itemView) {
            super(itemView);

            textDateValCupom = itemView.findViewById(R.id.textDateValCupom);
            nameCupom = itemView.findViewById(R.id.nameCupom);
            btnRemover = itemView.findViewById(R.id.btnRemoveCupom);
            txt_nome_item = itemView.findViewById(R.id.lineTextValueCupom);

            btnRemover.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(onItemClickListner!=null)
                    try {
                        onItemClickListner.onItemClick(listViewItem.getJSONObject(getAdapterPosition()));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }

    }

    public void setOnItemClickListner(OnItemClickListner onItemClickListner){
        this.onItemClickListner = onItemClickListner;
    }

}