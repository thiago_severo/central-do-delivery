package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.drawer.MyBounceInterpolator;

public class LineAtracaoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListnerAddress listner;
    OnItemClickListnerViewImage listnerViewImage;

    public void setListnerViewImage(OnItemClickListnerViewImage listnerViewImage) {
        this.listnerViewImage = listnerViewImage;
    }

    public void setListner(OnItemClickListnerAddress listner) {
        this.listner = listner;
    }

    Context context;

    public LineAtracaoListAdapter(Context context, JSONArray jsonArray){
        listViewItem = jsonArray;
        this.context = context;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
           View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_atracao, parent, false);
             lineSimpleItemHolder = new LineSimpleItemHolder(v);
            return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {


            JSONObject obj = listViewItem.getJSONObject(position);

            if(obj.getInt("fav")==1){
                ((LineSimpleItemHolder)holder).imageViewLiked.setImageResource(R.drawable.ic_favorite_black_24dp);
                ((LineSimpleItemHolder)holder).favorit = true;
            }else{
                ((LineSimpleItemHolder)holder).imageViewLiked.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                ((LineSimpleItemHolder)holder).favorit = false;
            }
            ((LineSimpleItemHolder)holder).nome_atracao.setText(obj.getString("titulo"));
            ((LineSimpleItemHolder)holder).horario_atracao.setText(obj.getString("hora_inicio")  );
            ((LineSimpleItemHolder)holder).desc_atracao.setText(obj.getString("descricao"));
            ((LineSimpleItemHolder)holder).tipo_cobranca.setText(obj.getString("tipo_cobranca"));
            ((LineSimpleItemHolder)holder).local_atracao.setText(obj.getString("localTitle"));
            ((LineSimpleItemHolder)holder).valor_atracao.setText(obj.getString("valor"));
            ((LineSimpleItemHolder)holder).textEndereco.setText(obj.getString("endereco"));

            ((LineSimpleItemHolder)holder).msg = "***********************\n"+
                    "<<< *"+obj.getString("titulo")+"* >>>\n" +
                    "***********************\n"+
                    obj.getString("hora_inicio")+" "+obj.getString("data")+"\n_"+
                    obj.getString("descricao")+"_\n"+
                    obj.getString("tipo_cobranca")+" *"+obj.getString("valor")+"*\n"+
                    obj.getString("localTitle")+"\n("+
                    "https://serveapp.com.br/images/"+
                    obj.getString("foto")+")\n\n"+
                    "***********************\n"+
                    "*SERVE DELIVERY*\n"+
                    "***********************\n"+
                    "https://play.google.com/store/apps/details?id=br.com.serveapp.serveapp"

            ;

            new ListSetImage(context,  ((LineSimpleItemHolder)holder).img_atracao ).execute(obj.getString("foto"));
            new ListSetImage(context,  ((LineSimpleItemHolder)holder).img_estabelecimentos ).execute(obj.getString("fotoOrganizadora"));
            buildLinePoints(((LineSimpleItemHolder)holder).layoutCountPontos, obj.getJSONArray("atracoes"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }



    class LineSimpleItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView img_estabelecimentos;
        public ImageView img_atracao;
        public TextView  tipo_cobranca;
        public TextView  nome_atracao;
        public TextView  horario_atracao;
        public TextView desc_atracao;
        public TextView local_atracao;
        public TextView valor_atracao;
        public ImageView imageViewLiked;
        public ImageView img_share;
        public String msg;

        public ViewGroup layoutCountPontos;
        public TextView textEndereco;
        boolean favorit;
        public LineSimpleItemHolder(final View itemView) {
            super(itemView);


            textEndereco = itemView.findViewById(R.id.textEndereco);
            img_estabelecimentos = itemView.findViewById(R.id.img_estabelecimentos);
            img_atracao = itemView.findViewById(R.id.img_atracao);
            tipo_cobranca = itemView.findViewById(R.id.tipo_cobranca);
            nome_atracao = itemView.findViewById(R.id.nome_atracao);

            horario_atracao = itemView.findViewById(R.id.horario_atracao);
            desc_atracao = itemView.findViewById(R.id.desc_atracao);
            local_atracao = itemView.findViewById(R.id.local_atracao);
            valor_atracao = itemView.findViewById(R.id.valor_atracao);
            layoutCountPontos = itemView.findViewById(R.id.layoutCountPontos);
            imageViewLiked = itemView.findViewById(R.id.imageViewLiked);
            img_share = itemView.findViewById(R.id.img_share);
            itemView.setOnClickListener(this);

            imageViewLiked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Animation myAnim = AnimationUtils.loadAnimation(context, R.anim.scale);
                    MyBounceInterpolator interpolator = new MyBounceInterpolator(0.2, 20);
                    myAnim.setInterpolator(interpolator);
                    if(favorit==false){
                        favorit=true;
                        imageViewLiked.setImageResource(R.drawable.ic_favorite_black_24dp);
                        final int position = getAdapterPosition();
                        try {
                            listner.onItemClick(listViewItem.getJSONObject(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else if(favorit){
                        favorit=false;
                        imageViewLiked.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                        final int position = getAdapterPosition();
                        try {
                            listner.onItemClickRemove(listViewItem.getJSONObject(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    imageViewLiked.startAnimation(myAnim);
                }
            });

            img_share.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("text/plain");
                    intent.putExtra(Intent.EXTRA_TEXT, msg);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(Intent.createChooser(intent, "Compartilhar com"));
                }
            });

        }

        @Override
        public void onClick(View view) {
            final int position = getAdapterPosition();
            try {
                listnerViewImage.onItemClickViewDetailImage(listViewItem.getJSONObject(position));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    public interface OnItemClickListnerAddress{

        void onItemClick(JSONObject item);
        void onItemClickRemove(JSONObject item);

    }

    public interface OnItemClickListnerViewImage{
        void onItemClickViewDetailImage(JSONObject item);
    }

    public void buildLinePoints(ViewGroup linearLayout, JSONArray jsonArray){
        for ( int i=0; i<jsonArray.length(); i++){
            TextView textView = new TextView(context);
            textView.setTextColor(Color.rgb(255,255,255));
            textView.setPadding(10,2,10,2);


            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(0,0,8,0);
            textView.setLayoutParams(params);

            textView.setSingleLine(true);
            textView.setGravity(Gravity.CENTER);
            textView.setBackground(context.getResources().getDrawable(R.drawable.borda_arredondada_light));
          //  btn.setLayoutParams(new LinearLayout.LayoutParams(70, 20));
            try {
                textView.setText(jsonArray.getString(i));
                textView.setTextSize(10);
                linearLayout.addView(textView);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }


}