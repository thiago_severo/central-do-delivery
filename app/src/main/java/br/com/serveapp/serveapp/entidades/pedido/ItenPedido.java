package br.com.serveapp.serveapp.entidades.pedido;

import java.util.ArrayList;
import java.util.List;

public class ItenPedido {

  public int id_produto;
  public int id_Item_Pedido;
  public String tipo;
  public List<GroupItemPedido> itemGroupPedidos = new ArrayList<>();
  public double valorTotal;
  public String valObservacao="  ";

}
