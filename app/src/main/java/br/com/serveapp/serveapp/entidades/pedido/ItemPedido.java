package br.com.serveapp.serveapp.entidades.pedido;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;
import br.com.serveapp.serveapp.entidades.cardapio.Ingrediente;
import br.com.serveapp.serveapp.entidades.cardapio.ItemCardapio;

public class ItemPedido implements ParentObject, Serializable, Parcelable {

    private long id;
    private List<Ingrediente> ingredientes;
    private float valor;
    private ItemCardapio item;



    private List<Object> mChildrenList;

    public ItemPedido(Parcel in) {
        id = in.readLong();
        valor = in.readFloat();

    }

    public static final Creator<ItemPedido> CREATOR = new Creator<ItemPedido>() {
        @Override
        public ItemPedido createFromParcel(Parcel in) {
            return new ItemPedido(in);
        }

        @Override
        public ItemPedido[] newArray(int size) {
            return new ItemPedido[size];
        }
    };

    public ItemPedido() {

    }

    public List<Ingrediente> getIngredientes() {
        return ingredientes;
    }
    public void setIngredientes(List<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }
    public float getValor() {
        return valor;
    }
    public void setValor(float valor) {
        this.valor = valor;
    }
    public ItemCardapio getItem() {
        return item;
    }
    public void setItem(ItemCardapio item) {
        this.item = item;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    @Override
    public List<Object> getChildObjectList() {
        return mChildrenList;
    }

    @Override
    public void setChildObjectList(List<Object> list) {
        mChildrenList = list;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeFloat(valor);
    }
}
