package br.com.serveapp.serveapp.util;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import br.com.serveapp.serveapp.ChatActivity;
import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.ListMessagesChatActivity;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.repository.objects.Repository;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import static br.com.serveapp.serveapp.repository.objects.Repository.activityChatVisible;
import static br.com.serveapp.serveapp.repository.objects.Repository.activityCupom;
import static br.com.serveapp.serveapp.repository.objects.Repository.activityVisible;
import static br.com.serveapp.serveapp.repository.objects.Repository.listMessagesChatActivity;

public class MyFirebaseService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Repository.token = s;
        Dao.save(s, "token", getApplicationContext());
    }

    private void pushNotification(RemoteMessage.Notification notification, PendingIntent pendingIntent) {

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "tutorialspoint_01";
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            @SuppressLint("WrongConstant") NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications", NotificationManager.IMPORTANCE_MAX);
            notificationChannel.setDescription("Sample Channel description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
        notificationBuilder.setAutoCancel(true).
                setDefaults(Notification.DEFAULT_ALL).
                setWhen(System.currentTimeMillis()).
                setSmallIcon(R.drawable.central_logo).
                setTicker("Serve").
                setPriority(Notification.PRIORITY_MAX).
                setContentTitle(notification.getTitle()).
                setContentIntent(pendingIntent).
                setContentText(notification.getBody()).
                setContentInfo("Information");
        notificationManager.notify(1, notificationBuilder.build());

    }

    public void onMessageReceived(RemoteMessage remoteMessage) {

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        final Map<String, String> data = remoteMessage.getData();

        for (Map.Entry<String, String> entry : data.entrySet()) {
            Object key = entry.getKey();
            Object value = entry.getValue();
            //TODO: other cool stuff
        }

        //id_estabelecimento

       if (remoteMessage.getNotification() != null) {

           if (data.size() > 0) {
                String type = data.get("type");
                String value = data.get("value");

                switch (type) {
                    case "update_order_status": {
                        String id_pedido = data.get("id_estabelecimento");
                        updateOrder(notification, id_pedido);
                        break;
                    }
                    case "new_message":
                        String id_pedido = data.get("id_pedido");
                        notifyMessage(notification, id_pedido);
                        break;
                    case "new_cupom":
                        if (data.get("id_estabelecimento").equalsIgnoreCase(Repository.idEstabelecimento + "") && activityCupom!=null) {
                            activityCupom.update();
                        }
                        break;
                    case "update_atendente_table":{
                        activityVisible.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                activityVisible.updateImageAtendente(data.get("value"));
                            }});

                    }
                }
            }

       }

    }

    private void updateOrder( RemoteMessage.Notification notification, String id_estabelecimento) {

        if (activityVisible != null) {
            Log.d("notifications", "sucesso");
            activityVisible.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activityVisible.updateFragemnts();
                }
            });
            Log.d("notifications", "sucesso depois");

        }

        else if (Repository.activityDetailDemandVisible != null) {

            Log.d("notifications", "sucesso");
            Repository.activityDetailDemandVisible.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Repository.activityDetailDemandVisible.updateDetailPedido();
                }
            });
            Log.d("notifications", "sucesso depois");

        } else if(Repository.activityDetailDemandVisible == null && activityVisible == null){

            Repository.idEstabelecimento = id_estabelecimento;
            Intent intent = new Intent(this, ListProductsStablishmentDeliveryActivity.class);
            intent.putExtra("serveapp.notifyId", "idPedido");
            intent.putExtra("id_pedido", id_estabelecimento);

            PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            pushNotification(notification, pIntent);
            Log.d("notifications", "not fragment");

        }

    }

    private void notifyMessage(RemoteMessage.Notification notification, String idPedido) {

        if (activityChatVisible != null) {

            activityChatVisible.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activityChatVisible.updateListMessages();
                }
            });

        }
        else if (listMessagesChatActivity != null) {

            listMessagesChatActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    listMessagesChatActivity.updateListMessages();
                }
            });

        }
        else if(listMessagesChatActivity == null && activityChatVisible == null){

            Intent intent = new Intent(this, ListMessagesChatActivity.class);
            intent.putExtra("serveapp.notifyId", "idChat");
            intent.putExtra("id_pedido", idPedido);

            PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            pushNotification(notification, pIntent);

        }

    }

    private void notifyCupom() {
        if (activityCupom != null) {
            activityCupom.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activityCupom.update();
                }
            });
        }
    }


}
