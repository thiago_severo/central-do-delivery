package br.com.serveapp.serveapp.fragment.local;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.serveapp.serveapp.entidades.SubItem;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.entidades.cardapio.Ingrediente;
import br.com.serveapp.serveapp.entidades.cardapio.ItemCardapio;
import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.activity.local.LocalMainActivity2;
import br.com.serveapp.serveapp.adapt.CategorieListAdapter;
import br.com.serveapp.serveapp.adapt.LineProductsForEstabelecimentsAdapter;
import br.com.serveapp.serveapp.enun.TipoSubItemCardapio;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;


public class FragmentListProductsLocal extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    LocalMainActivity2 localActivity;
    private static List<Categoria> categoriaList = null;
    private static List<ItemCardapio> listItensMenu = null;

    private RecyclerView listProducts;
    View view;

    private LineProductsForEstabelecimentsAdapter lineAdapter;
    private CategorieListAdapter categorieListAdapter;
    boolean preencher;

    public FragmentListProductsLocal(Activity activity , boolean preencher) {
        // Required empty public constructor
        this.localActivity = (LocalMainActivity2) activity;
        this.preencher = preencher;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_list_products_local, container, false);

        listProducts = view.findViewById(R.id.recyclerListProductslocal);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listProducts.setLayoutManager(llm);


        if(preencher == true) {
            initializeDataItens();
            initializeAdapter();

            lineAdapter.setOnItemClickListener(new LineProductsForEstabelecimentsAdapter.OnItemClickListner() {
                @Override
                public void onItemClick(JSONObject itemCardapio) {
                    Intent intent = new Intent(getApplicationContext(), DetalheItemPopUpActivity.class);
                    try {
                        intent.putExtra("id_produto", itemCardapio.getString("_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                  /*  if(itemCardapio.getItens()!=null) {
                        Repository.itemPedidoOfListProductsToPopUp.addAll(itemCardapio.getItens());
                    }
                    intent.putExtra("precoItem", itemCardapio.getValor()+"");
                    intent.putExtra("objectItemCardapio", itemCardapio);
                */
                    startActivityForResult(intent, 1);
                }
            });


        }



        return view;

    }


    private void initializeAdapter() {

      //  lineAdapter = new LineProductsForEstabelecimentsAdapter(getApplicationContext(), listItensMenu);
        listProducts.setAdapter(lineAdapter);
        listProducts.scrollToPosition(listItensMenu.size()-1);



    }




    private List<ItemCardapio> initializeDataItens() {
        listItensMenu = new ArrayList<>();

        ItemCardapio itemHeader = new ItemCardapio();
        itemHeader.setDescricao("header");
        itemHeader.setNome("Lanches");

        listItensMenu.add(itemHeader);


        ItemCardapio item = new ItemCardapio();
        item.setDescricao("item");
        item.setNome("X-Hambueguer");
        item.setValor(12);

        SubItem adicionais = new SubItem();

        List <Ingrediente> ingredienteAdicionais = new ArrayList<>();
        Ingrediente tomate = new Ingrediente();
        tomate.setNome("tomate");
        ingredienteAdicionais.add(tomate);

        Ingrediente milho = new Ingrediente();
        milho.setNome("milho");
        ingredienteAdicionais.add(milho);

        Ingrediente ervilha = new Ingrediente();
        ervilha.setNome("ervilha");
        ingredienteAdicionais.add(ervilha);

        Ingrediente batataPalha = new Ingrediente();
        batataPalha.setNome("batata palha");
        ingredienteAdicionais.add(batataPalha);

        adicionais.setIngredientes(ingredienteAdicionais);
        adicionais.setTipoSubItemCardapio(TipoSubItemCardapio.CheckBox);
        adicionais.setNome("Adicionais");


        SubItem queijo = new SubItem();

        List <Ingrediente> ingredienteQueijo = new ArrayList<>();
        Ingrediente suico = new Ingrediente();
        suico.setNome("suiço");
        ingredienteQueijo.add(suico);

        Ingrediente cheddar = new Ingrediente();
        cheddar.setNome("cheddar");
        ingredienteQueijo.add(cheddar);

        Ingrediente prato = new Ingrediente();
        prato.setNome("prato");
        ingredienteQueijo.add(prato);


        SubItem pao = new SubItem();

        List <Ingrediente> ingredientePao = new ArrayList<>();

        Ingrediente novegrao = new Ingrediente();
        novegrao.setNome("9 grãos");
        ingredientePao.add(novegrao);

        Ingrediente italiano = new Ingrediente();
        italiano.setNome("italiano");
        ingredientePao.add(italiano);

        Ingrediente quatroqueijos = new Ingrediente();
        quatroqueijos.setNome("4 queijos");
        ingredientePao.add(quatroqueijos);

        pao.setIngredientes(ingredientePao);
        pao.setTipoSubItemCardapio(TipoSubItemCardapio.RadioButton);
        pao.setNome("pão");

        queijo.setIngredientes(ingredienteQueijo);
        queijo.setTipoSubItemCardapio(TipoSubItemCardapio.RadioButton);
        queijo.setNome("queijo");

        adicionais.setIngredientes(ingredienteAdicionais);
        adicionais.setTipoSubItemCardapio(TipoSubItemCardapio.CheckBox);
        adicionais.setNome("Adicionais");

        List<SubItem> subItems = new ArrayList<>();
        subItems.add(adicionais);
        subItems.add(queijo);
        subItems.add(pao);

        item.setItens(subItems);
        item.setLogo("icon_burguer");

        listItensMenu.add(item);

        ItemCardapio estabelecimento2 = new ItemCardapio();
        estabelecimento2.setDescricao("item");
        estabelecimento2.setNome("X-Tudo");
        estabelecimento2.setValor(10);
        estabelecimento2.setLogo("icon_burguer");

        listItensMenu.add(estabelecimento2);


        ItemCardapio estabelecimento4 = new ItemCardapio();
        estabelecimento4.setDescricao("item");
        estabelecimento4.setNome("Pastel de frango");
        estabelecimento4.setValor(4);
        estabelecimento4.setLogo("pastel");

        listItensMenu.add(estabelecimento4);

        ItemCardapio itemHeader2 = new ItemCardapio();
        itemHeader2.setDescricao("header");
        itemHeader2.setNome("Bebidas");

        listItensMenu.add(itemHeader2);


        ItemCardapio estabelecimento3 = new ItemCardapio();
        estabelecimento3.setDescricao("item");
        estabelecimento3.setNome("Água Mineral 200 ml");
        estabelecimento3.setValor(3);
        estabelecimento3.setLogo("agua_mineral");

        listItensMenu.add(estabelecimento3);

        return listItensMenu;
    }


}
