package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ListMessagesChatActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListChat extends OnPostResponse {

    ListMessagesChatActivity atracoesActivity;
    private SwipeRefreshLayout swipeRefreshLayout;

    public ServiceListChat(ListMessagesChatActivity atracoesActivity, SwipeRefreshLayout swipeRefreshLayout) {
        this.atracoesActivity = atracoesActivity;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.setAutoReconnect(true);
        this.setSaveCache(true);
        this.script = "select_chat.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
        Toast.makeText(atracoesActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(atracoesActivity));
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONArray result = jsonObject.getJSONArray("result");
            atracoesActivity.initializeAdapterMsgs(result);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}

    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                atracoesActivity.initializeAdapterMsgs(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}