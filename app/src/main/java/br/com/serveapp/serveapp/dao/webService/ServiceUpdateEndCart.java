package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceUpdateEndCart extends OnPostResponse {

    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;
    public ServiceUpdateEndCart(ConfirmItensPedidoActiviy confirmItensPedidoActiviy){
        setId(Repository.idEstabelecimento);
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(confirmItensPedidoActiviy.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        confirmItensPedidoActiviy.progressIncrement.setVisibility(View.GONE);
        super.onErrorResponse(error);
    }

    @Override
    public void onResponse(String response) {
        if(response!=null && !response.equals("null"))
            try {
                confirmItensPedidoActiviy.progressIncrement.setVisibility(View.GONE);
                confirmItensPedidoActiviy.reload(new JSONObject(response));
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }



}
