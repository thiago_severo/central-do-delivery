package br.com.serveapp.serveapp.entidades.pedido;

public class PedidoLocal extends Pedido{

    private int mesa;

    public int getMesa() {
        return mesa;
    }

    public void setMesa(int mesa) {
        this.mesa = mesa;
    }

}
