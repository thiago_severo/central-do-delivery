package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.ChangeAddressPopUpActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceListAddress extends OnPostResponse {

    ChangeAddressPopUpActivity changeAddressPopUpActivity;

    public ServiceListAddress(String id, ChangeAddressPopUpActivity changeAddressPopUpActivity) {
        this.setId(id);
        this.changeAddressPopUpActivity = changeAddressPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_ends_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(changeAddressPopUpActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
        changeAddressPopUpActivity.progressDeleteAddress.setVisibility(View.GONE);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(changeAddressPopUpActivity));
        if (!getId().equals("0")) {
            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        }

        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONArray result = jsonObject.getJSONArray("result");
            changeAddressPopUpActivity.initializeAdapterListAddress(result, jsonObject.getString("selected"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {
        changeAddressPopUpActivity.progressDeleteAddress.setVisibility(View.GONE);
    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                changeAddressPopUpActivity.initializeAdapterListAddress(result, jsonObject.getString("selected"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}