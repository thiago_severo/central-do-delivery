package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;

import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.fragments.FragmentListDemandsEstablishment;
import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

public class ServiceCancelDetailPedido extends OnPostResponse {

        FragmentListDemandsEstablishment fragmentListDemandsEstablishment;
        DetailDemand activity;

        public ServiceCancelDetailPedido(DetailDemand activity) {
        this.activity = activity;
        this.fragmentListDemandsEstablishment = fragmentListDemandsEstablishment;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "api/home/client_cancel";
        }

        public String getScript(){
                return Script.getUrl()+script;
        }

        @Override
        public void onErrorResponse(VolleyError error) {

        Toast.makeText(activity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();

        }

        @Override
        public void onResponse (String response){

                 if (response != null && !response.equals("null"))
                activity.setResultado();

        }

    }
