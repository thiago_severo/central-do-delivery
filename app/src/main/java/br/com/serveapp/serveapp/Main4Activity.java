package br.com.serveapp.serveapp;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;

import br.com.serveapp.serveapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


public class Main4Activity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main4);

        SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        LatLng m = new LatLng(-8.001162, -38.306532);
        this.googleMap.addMarker(new MarkerOptions().position(m).title("marsh"));
        this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(m));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(m, 17.0f));

    }
}
