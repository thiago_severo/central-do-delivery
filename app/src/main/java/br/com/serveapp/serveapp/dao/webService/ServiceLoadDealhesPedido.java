package br.com.serveapp.serveapp.dao.webService;

import android.app.Activity;
import android.content.Intent;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import java.util.HashMap;
import java.util.Map;

public class ServiceLoadDealhesPedido extends OnPostResponse {

    Activity activity;

    public ServiceLoadDealhesPedido(Activity activity, String idPedido) {
        setId(idPedido);
        this.activity = activity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_detalhes_pedido.php";
    }

    @Override
    public Map<String, String> getParam() {

        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_pedido", getId());
        return paramLogin;
    }

    @Override
    public void onResponse(String response) {
        final Intent intent = new Intent(activity, DetailDemand.class);
        intent.putExtra("nome", activity.getIntent().getStringExtra("nome"));
        intent.putExtra("demandFinalized", true);
        intent.putExtra("id_pedido", getId());
        activity.startActivityForResult(intent, 1);
        activity.setResult(Repository.finalizeCreateDemand);
        activity.finish();

    }


    @Override
    public void onErrorResponse(VolleyError error) {
        // loginActivity.mProgress.dismiss();
        Toast.makeText(activity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);

    }


    @Override
    public void onUpdate(String response) {

    }

}