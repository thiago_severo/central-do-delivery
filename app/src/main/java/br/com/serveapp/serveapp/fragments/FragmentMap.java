package br.com.serveapp.serveapp.fragments;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import br.com.serveapp.serveapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class FragmentMap extends Fragment implements OnMapReadyCallback {

    MapView mMapView;
    private GoogleMap googleMap;
    double lat;
    Double lon;
    private String address;
    public FragmentTransaction fragmentTransaction;
    Button button;

    TextView textView;
    private TextView textAddressEstab;


    public FragmentMap(String lat, String lon, String address){
        this.lat = Double.parseDouble(lat);
        this.lon = Double.parseDouble(lon);
        this.address = address;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }



    @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment3, container, false);

        textAddressEstab = v.findViewById(R.id.textAddressEstab);
        textAddressEstab.setText("Rua "+address);

        SupportMapFragment  mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        return v;
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

            this.googleMap = googleMap;
            LatLng m = new LatLng(lat, lon);
            this.googleMap.addMarker(new MarkerOptions().position(m).title("marsh"));
            this.googleMap.moveCamera(CameraUpdateFactory.newLatLng(m));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(m, 17.0f));

    }
}
