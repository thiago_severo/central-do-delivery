package br.com.serveapp.serveapp.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.entidades.Estabelecimento;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FragmentFavorit extends Fragment {


    private static RecyclerView favoritsEstabelecimentList;
    private static List<Estabelecimento> estabelecimentoList =null;
    private LineFavoritEstabelecimentAdapter lineAdapter;
    public ProgressBar progressBar;

    public FragmentFavorit(){

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorits, container, false);
        favoritsEstabelecimentList = view.findViewById(R.id.listFavorits);
        progressBar = view.findViewById(R.id.progressBar6);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        favoritsEstabelecimentList.setLayoutManager(llm);



        initializeDataFavorits();
        favoritsEstabelecimentList.setItemAnimator(new DefaultItemAnimator());
       initializeAdapter();

        return view;
    }


    private void initializeAdapter(){
       // lineAdapter =   new LineFavoritEstabelecimentAdapter(getApplicationContext(),estabelecimentoList);
        favoritsEstabelecimentList.setAdapter(lineAdapter);
    }


    private List<Estabelecimento> initializeDataFavorits() {
        estabelecimentoList = new ArrayList<>();
        Estabelecimento estabelecimento = new Estabelecimento();
        estabelecimento.setDesc("O melhor hamburguer da região. O verdadeiro sabor do hambuerger");
        estabelecimento.setNome("CANTINA BURGUERIA");
        estabelecimento.setLogo("ic_burgeria");


        estabelecimentoList.add(estabelecimento);

        Estabelecimento estabelecimento2 = new Estabelecimento();
        estabelecimento2.setDesc("O melhor hamburguer da região. O verdadeiro sabor do hambuerger");
        estabelecimento2.setNome("CREPERIA XAXADO");
        estabelecimento2.setLogo("creperia");
        estabelecimentoList.add(estabelecimento2);



        return estabelecimentoList;
    }

}
