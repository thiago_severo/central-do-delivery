package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ProfileUserActivity;
import br.com.serveapp.serveapp.dao.Dao;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceUpdateRegister extends OnPostResponse {

    ProfileUserActivity profileUserActivity;

    public ServiceUpdateRegister(ProfileUserActivity profileUserActivity){

        this.profileUserActivity = profileUserActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "insert_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(profileUserActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        profileUserActivity.progressBar.setVisibility(View.GONE);
        super.onErrorResponse(error);
    }

    @Override
    public void onResponse(String response) {

        if(!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject user = null;
                if (!jsonObject.isNull("result")) {

                    user = jsonObject.getJSONObject("result");
                    Repository.setUser(user);
                    Dao.save(user.toString(), FilesName.user, profileUserActivity.getApplicationContext());
                    Toast.makeText(profileUserActivity.getApplicationContext(), "Usuário atualizado com sucesso", Toast.LENGTH_SHORT).show();
                    profileUserActivity.finish();
                }
                else{
                    Toast.makeText(profileUserActivity.getApplicationContext(), "Erro na atualização do usuário", Toast.LENGTH_SHORT).show();
                }
            }catch (JSONException e){

            }

    }


}
