package br.com.serveapp.serveapp.util;

import android.view.View;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.serveapp.serveapp.R;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ValidationUtilField {

    public static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    //"(?=.*[0-9])" +         //at least 1 digit
                    //"(?=.*[a-z])" +         //at least 1 lower case letter
                    //"(?=.*[A-Z])" +         //at least 1 upper case letter
                   // "(?=.*[a-zA-Z])" +      //any letter
                  //  "(?=.*[@#$%^&+=])"+    //at least 1 special character
                  //  "(?=\\S+$)" +           //no white spaces
                   // ".{4,}" +               //at least 4 characters
                    "$");

    public static final Pattern TELEPHONE_PATTERN = Pattern.compile("[0-9]+");

    View.OnFocusChangeListener listener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus ) {
                 if(v.getId() ==  R.id.text_username && ((TextInputEditText)v).getText().toString().length()>0) {
                    Toast.makeText(getApplicationContext(), "exit focus", Toast.LENGTH_LONG).show();
               }
            }
            else{

            }
        }
    };

    public static  boolean validateCep(TextInputEditText textInputEditText, TextInputLayout textInputLayout) {
        String usernameInput = textInputEditText.getText().toString().trim();
        if (usernameInput.isEmpty()){
            textInputEditText.setError(null);
            return true;
        }
        else if (usernameInput.length()<9 || !ValidationUtilField.TELEPHONE_PATTERN.matcher(usernameInput.replace("+","").replace("-","")).matches()) {
            textInputEditText.setError("Número inválido");
            return false;
        }
        else {
            textInputEditText.setError(null);
            return true;
        }
    }

    private static boolean validatePhone(TextInputEditText text_cd_phone, TextInputLayout textInputLayoutPhone) {
        String usernameInput = text_cd_phone.getText().toString().trim();
        if (usernameInput.isEmpty()){
            textInputLayoutPhone.setError("O campo não pode ficar vazio");
            return false;
        }else if (usernameInput.length() > 12) {
            textInputLayoutPhone.setError("número de telefone muito longo");
            return false;
        }else if (usernameInput.length()<8 || !ValidationUtilField.TELEPHONE_PATTERN.matcher(usernameInput.replace("+","").replace("-","")).matches()) {
            textInputLayoutPhone.setError("Número inválido");
            return false; }
        else {
            textInputLayoutPhone.setError(null);
            return true;
        }
    }

    public static boolean isValidEmailAddressRegex(String email) {
        boolean isEmailIdValid = false;
        if (email != null && email.length() > 0) {
            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(email);
            if (matcher.matches()) {
                isEmailIdValid = true;
            }
        }
        return isEmailIdValid;
    }

    public static boolean isValidName(String name){
        if (Pattern.matches("[a-zA-Záàâãéèêíïóôõöúçñ]+",name.trim())) {
           return true;
        }else{
          return false;
        }
    }


}