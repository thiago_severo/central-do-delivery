package br.com.serveapp.serveapp.fragments.ifood;

import java.util.ArrayList;
import java.util.List;

public class HeaderLineGroup {

    public static List<HeaderLine> headerLines;

    public static void inicialize() {
        headerLines = new ArrayList<>();
    }

    public static void add(HeaderLine headerLine) {
        headerLines.add(headerLine);
    }

    public static void selected(HeaderLine headerLine) {
        try {
            for (HeaderLine temp : headerLines) {
                temp.desselected();
            }
            headerLine.selected();
        } catch (NullPointerException|IndexOutOfBoundsException e) {

        }
    }

    public static void selected(int position) {
        try {
            for (HeaderLine temp : headerLines) {
                temp.desselected();
            }
            if(headerLines.size()>0) {
                headerLines.get(position).selected();
            }
        } catch (Exception e) {

        }
    }
}
