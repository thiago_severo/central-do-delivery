package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentListDemandsEstablishment;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceSelectPedidosEstab extends OnPostResponse {

    FragmentListDemandsEstablishment fragmentListDemandsEstablishment;
    private SwipeRefreshLayout swipeRefreshLayout;

    public ServiceSelectPedidosEstab(FragmentListDemandsEstablishment fragmentListDemandsEstablishment, String idEstabelecimento, SwipeRefreshLayout swipeRefreshLayout) {
        setId(idEstabelecimento);
        this.fragmentListDemandsEstablishment = fragmentListDemandsEstablishment;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_pedidos_do_estab.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}

        try{
            fragmentListDemandsEstablishment.progressBar.setVisibility(View.GONE);
            Toast.makeText(fragmentListDemandsEstablishment.getActivity().getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();

        }catch(NullPointerException e){}

        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(fragmentListDemandsEstablishment.getContext()));
        paramLogin.put("id_estabelecimento", getId());
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONArray result = jsonObject.getJSONArray("result");
            fragmentListDemandsEstablishment.initializeAdapterListItens(result, null);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {
        fragmentListDemandsEstablishment.progressBar.setVisibility(View.GONE);
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                fragmentListDemandsEstablishment.initializeAdapterListItens(result, null);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}
