package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.CashBackActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceCashBack extends OnPostResponse {

    CashBackActivity cashBackActivity;

    public ServiceCashBack(CashBackActivity cashBackActivity) {

        this.cashBackActivity = cashBackActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "cashback.php";

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(cashBackActivity.getApplicationContext(), "Sem internet" , Toast.LENGTH_LONG).show();
        cashBackActivity.mProgress.dismiss();


        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(cashBackActivity));
        return paramLogin;
    }

    /*
        @Override
        public void onCache(JSONObject jsonObject) {
            try {

         //       Toast.makeText(cashBackActivity, "response: "+ jsonObject.toString(), Toast.LENGTH_LONG ).show();
                String saldo = jsonObject.getJSONObject("result").getString("saldo");
         //       Toast.makeText(cashBackActivity, "response: "+ saldo, Toast.LENGTH_LONG ).show();
                JSONArray result = jsonObject.getJSONObject("result").getJSONArray("lista");
                cashBackActivity.initializeList(result, saldo);
                cashBackActivity.mProgress.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    */
    @Override
    public void onResponse(String response) {
        //  cashBackActivity.progressDeleteAddress.setVisibility(View.GONE);


        try {

            JSONObject jsonObject = new JSONObject(response);
            JSONArray result = jsonObject.getJSONObject("result").getJSONArray("lista");
            cashBackActivity.initializeList(result, jsonObject.getJSONObject("result").getDouble("saldo"));
            cashBackActivity.mProgress.dismiss();

            if (cashBackActivity.dialog != null)
                cashBackActivity.dialog.dismiss();

            cashBackActivity.progressBar.setVisibility(View.GONE);
            cashBackActivity.textSaldoCash.setVisibility(View.VISIBLE);
            cashBackActivity.buttonResgatar.setVisibility(View.VISIBLE);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
/*
    @Override
    public void onUpdate(String response) {

        Toast.makeText(cashBackActivity, "response: "+ response, Toast.LENGTH_LONG ).show();

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONObject("result").getJSONArray("lista");
                cashBackActivity.initializeList(result, jsonObject.getJSONObject("result").getString("saldo"));
                cashBackActivity.mProgress.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

*/
}