package br.com.serveapp.serveapp.dao.webService;

import android.content.Intent;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.CreateAccountUserActivity;
import br.com.serveapp.serveapp.LoginActivity;
import br.com.serveapp.serveapp.NavigationDrawerActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceLogin extends OnPostResponse {

    LoginActivity loginActivity;
    public ServiceLogin(LoginActivity loginActivity){
        this.loginActivity = loginActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "select_cliente_where_login.php";
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("login", loginActivity.textEditUsername.getText().toString());
        paramLogin.put("senha", loginActivity.textEditPassword.getText().toString());
        paramLogin.put("token", CreateAccountUserActivity.getToken(loginActivity.getApplicationContext()));
        paramLogin.put("instructionId",  "local");
        return paramLogin;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        loginActivity.mProgress.dismiss();
        Toast.makeText(loginActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {
        if(!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject user = null;

                if (!jsonObject.isNull("result")) {
                    user = jsonObject.getJSONObject("result");
                    Repository.setUser(user);
                    Dao.save(user.toString(), FilesName.user, loginActivity.getApplicationContext());
                    loginActivity.mProgress.dismiss();
                    loginActivity.startActivity(new Intent(loginActivity.getApplicationContext(), NavigationDrawerActivity.class));
                    loginActivity.finish();
                } else {
                    loginActivity.mProgress.dismiss();
                    loginActivity.buildConfirmDialog("Usuário ou senha incorreto", "");
                }
            } catch (JSONException e) {
                e.printStackTrace();
                loginActivity.mProgress.dismiss();
                Toast.makeText(loginActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
            }
    }


}
