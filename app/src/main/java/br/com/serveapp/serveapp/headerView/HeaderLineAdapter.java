package br.com.serveapp.serveapp.headerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;

class HeaderLineAdapter extends RecyclerView.Adapter<HeaderLine> {

    private JSONArray mHeades;
    LinearLayoutManager bodyLayout;
    LinearLayoutManager headerLayout;

    public HeaderLineAdapter(JSONArray mHeades, LinearLayoutManager bodyLayout, LinearLayoutManager headerLayout) {
        this.mHeades = mHeades;
        this.bodyLayout = bodyLayout;
        this.headerLayout = headerLayout;
        HeaderLineGroup.inicialize();
    }

    public void setmHeades(JSONArray mHeades) {
        this.mHeades = mHeades;
    }

    @Override
    public HeaderLine onCreateViewHolder(ViewGroup parent, int viewType) {

        return new HeaderLine(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.header, parent, false), bodyLayout, headerLayout);
    }

    @Override
    public void onBindViewHolder(HeaderLine holder, final int position) {
        try {
            holder.initialize(mHeades.getString(position), position);
            HeaderLineGroup.add(holder);
            if (position == 0) {
                holder.selected();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        mHeades = mHeades;
        return mHeades != null ? mHeades.length() : 0;
    }

}
