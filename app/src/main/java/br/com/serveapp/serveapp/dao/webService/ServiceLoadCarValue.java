package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceLoadCarValue extends OnPostResponse {

    ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity;
    public ServiceLoadCarValue( ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity){
        setId(Repository.idEstabelecimento);
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.listProductsStablishmentDeliveryActivity = listProductsStablishmentDeliveryActivity;
        this.script = "select_produtos_do_carrinho_new.php";
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
       // listProductsStablishmentDeliveryActivity.finish();

    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();

           if(Repository.getIdUsuario(listProductsStablishmentDeliveryActivity.getApplicationContext())==null){
               listProductsStablishmentDeliveryActivity.finish();
           }

           paramLogin.put("id_cliente", Repository.getIdUsuario(listProductsStablishmentDeliveryActivity.getApplicationContext()));
           paramLogin.put("id_estabelecimento",Repository.idEstabelecimento);

        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
            try {
                JSONObject result = jsonObject.getJSONObject("result");
                JSONObject cart = result.getJSONObject("cart");
                if(!cart.isNull("total")){
                    listProductsStablishmentDeliveryActivity.showBtnGoToCar(cart.getJSONObject("total").getString("label"),  cart.getString("qtd"));
                }

               int res =  result.getInt("is_local");
                if(res==1){
                    listProductsStablishmentDeliveryActivity.addTabComandaLocal(result.getString("atendente"));
                }
                else{
                    listProductsStablishmentDeliveryActivity.addTabComandaDelivery();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if(response!=null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");

                JSONObject cart = result.getJSONObject("cart");
                if(!cart.isNull("total")){
                    listProductsStablishmentDeliveryActivity.showBtnGoToCar(cart.getJSONObject("total").getString("label"),  cart.getString("qtd"));
                }

                int res =  result.getInt("is_local");
                if(res==1){
                    listProductsStablishmentDeliveryActivity.addTabComandaLocal(result.getString("atendente"));
                }
                else{
                    listProductsStablishmentDeliveryActivity.addTabComandaDelivery();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}
