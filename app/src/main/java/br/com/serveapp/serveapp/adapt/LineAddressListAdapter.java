package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.CheckGroup;

public class LineAddressListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListnerAddress listner;
    OnItemClickListnerDeleteAddress onItemClickListnerDeleteAddress;
    CheckGroup checkGroup;
    int mPosition = -1;

    String idEndSelected;
    String idComboCity;
    String citySelected;
    Context context;
    public boolean disbleDelete;

    public LineAddressListAdapter(CheckGroup checkGroup, Context context, JSONArray listViewItem, String idEndSelected, boolean disableDelete) {
        this.context = context;
        this.listViewItem = listViewItem;
        this.idEndSelected = idEndSelected;
        this.checkGroup = checkGroup;
        this.disbleDelete = disbleDelete;

    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (listViewItem.getJSONObject(position).isNull("municipios")) {
                return 1;
            } else {
                return 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 1;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder lineSimpleItemHolder = null;

        if (viewType == 0) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_end_combo_city, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolderSelectCity(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_end, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolder(v);
        }

        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {


            if (holder instanceof LineSimpleItemHolderSelectCity) {

                idComboCity = listViewItem.getJSONObject(position).getInt("selected") + "";

                if (!listViewItem.getJSONObject(position).isNull("municipios")) {
                    JSONArray arr = listViewItem.getJSONObject(position).getJSONArray("municipios");
                    List list = new ArrayList();
                    ((LineSimpleItemHolderSelectCity) holder).list = new ArrayList<Integer>();
                    for (int i = 0; i < arr.length(); i++) {
                        list.add(arr.getJSONObject(i).getString("cidade"));
                        ((LineSimpleItemHolderSelectCity) holder).list.add(Integer.parseInt(arr.getJSONObject(i).getString("_id")));
                    }

                    ((LineSimpleItemHolderSelectCity) holder).spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.spinner_item, list);
                    ((LineSimpleItemHolderSelectCity) holder).spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
                    ((LineSimpleItemHolderSelectCity) holder).select_city.setAdapter(((LineSimpleItemHolderSelectCity) holder).spinnerArrayAdapter);
                    ((LineSimpleItemHolderSelectCity) holder).select_city.setSelection(1);

                    Repository.idCitySelected = listViewItem.getJSONObject(position).getString("selected");
                    ((LineSimpleItemHolderSelectCity) holder).setSelected(Integer.parseInt(Repository.idCitySelected));
                    checkGroup.add(((LineSimpleItemHolderSelectCity) holder).radioButton);

                    if (idEndSelected != null) {
                        if (listViewItem.getJSONObject(position).getString("_id").equals(idEndSelected)) {
                            mPosition = position;
                            idEndSelected = null;
                        }
                    }

                    if (position == mPosition) {
                        checkGroup.setChecked(((LineSimpleItemHolderSelectCity) holder).radioButton);
                    }

                    ((LineSimpleItemHolderSelectCity) holder).radioButton.setChecked(position == mPosition);

                }

            } else if (holder instanceof LineSimpleItemHolder) {

                if (listViewItem.getJSONObject(position).getString("_id").equals("0")) {
                    ((LineSimpleItemHolder) holder).end_remove.setVisibility(View.GONE);
                    checkGroup.add(((LineSimpleItemHolder) holder).end_radio);

                }

                ((LineSimpleItemHolder) holder).end_cep.setText(listViewItem.getJSONObject(position).getString("cep"));
                ((LineSimpleItemHolder) holder).end_title.setText(listViewItem.getJSONObject(position).getString("titulo"));
                ((LineSimpleItemHolder) holder).end_frete.setText(listViewItem.getJSONObject(position).getString("frete"));
                ((LineSimpleItemHolder) holder).end_desc.setText(listViewItem.getJSONObject(position).getString("desc"));
                ((LineSimpleItemHolder) holder).end_city.setText(listViewItem.getJSONObject(position).getString("cidade"));
                ((LineSimpleItemHolder) holder).end_radio.setChecked(false);

                checkGroup.add(((LineSimpleItemHolder) holder).end_radio);

                if (idEndSelected != null)
                    if (listViewItem.getJSONObject(position).getString("_id").equals(idEndSelected)) {
                        mPosition = position;
                        idEndSelected = null;
                    }

                if (position == mPosition) {
                    checkGroup.setChecked(((LineSimpleItemHolder) holder).end_radio);
                }


                ((LineSimpleItemHolder) holder).end_radio.setChecked(position == mPosition);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

        public TextView end_title;
        public TextView end_desc;
        public TextView end_city;
        public TextView end_frete;
        public TextView end_cep;
        public RadioButton end_radio;
        public TextView end_remove;

        public LineSimpleItemHolder(View itemView) {
            super(itemView);
            end_cep = itemView.findViewById(R.id.end_cep);
            end_title = itemView.findViewById(R.id.end_title);
            end_frete = itemView.findViewById(R.id.end_frete);

            end_desc = itemView.findViewById(R.id.end_desc);
            end_city = itemView.findViewById(R.id.end_city);
            end_radio = itemView.findViewById(R.id.end_radio);
            end_remove = itemView.findViewById(R.id.end_remove);
            end_remove.setOnClickListener(this);
            end_radio.setOnCheckedChangeListener(this);

            if(disbleDelete){
                end_remove.setVisibility(View.GONE);
            }

        }

        @Override
        public void onClick(View view) {
            if (view.getId() == R.id.end_remove) {
                if (onItemClickListnerDeleteAddress != null) {
                    final int position = getAdapterPosition();
                    try {
                        onItemClickListnerDeleteAddress.onItemClickDelete(listViewItem.getJSONObject(position));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            } else if (view.getId() == R.id.end_radio) {
                try {
                    final int position = getAdapterPosition();
                    mPosition = position;
                    listner.onItemClick(listViewItem.getJSONObject(position));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            try {
                if (b == true) {
                    final int position = getAdapterPosition();
                    mPosition = position;
                    listner.onItemClick(listViewItem.getJSONObject(position));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    class LineSimpleItemHolderSelectCity extends RecyclerView.ViewHolder {

        public Spinner select_city;
        List<Integer> list;
        ArrayAdapter<String> spinnerArrayAdapter;
        RadioButton radioButton;

        public LineSimpleItemHolderSelectCity(View itemView) {
            super(itemView);
            select_city = itemView.findViewById(R.id.spinnerCity);
            radioButton = itemView.findViewById(R.id.end_radio);

            select_city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        JSONObject jsonObject = new JSONObject();
                        String text = select_city.getSelectedItem().toString();
                        citySelected = text;
                        Repository.idCitySelected = list.get(i).toString();

                        if (radioButton.isChecked()) {
                            jsonObject.put("titulo", text);
                            jsonObject.put("id_municipio", Repository.idCitySelected);
                            jsonObject.put("_id", "0");
                            listner.onItemClick(jsonObject);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }

            });

            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    if (b == true) {
                        JSONObject jsonObject = new JSONObject();
                        String text = select_city.getSelectedItem().toString();
                        try {
                            jsonObject.put("titulo", text);
                            jsonObject.put("id_municipio", Repository.idCitySelected);
                            jsonObject.put("_id", "0");
                            listner.onItemClick(jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                }
            });

        }

        public void setSelected(int id) {
            int i = list.indexOf(id);
            select_city.setSelection(i);
            idComboCity = id + "";
        }

    }

    public void setOnClickListenerAddress(OnItemClickListnerAddress listenere) {
        this.listner = listenere;
    }

    public interface OnItemClickListnerAddress {
        void onItemClick(JSONObject item);
    }

    public void setOnClickListenerDeleteAddress(OnItemClickListnerDeleteAddress listenere) {
        this.onItemClickListnerDeleteAddress = listenere;
    }

    public interface OnItemClickListnerDeleteAddress {
        void onItemClickDelete(JSONObject item);
    }

}