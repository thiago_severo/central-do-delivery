package br.com.serveapp.serveapp.headerView;

import java.util.ArrayList;
import java.util.List;

class HeaderLineGroup {

    public static List<HeaderLine> headerLines;

    public static void inicialize(){
        headerLines = new ArrayList<>();
    }

    public static void add(HeaderLine headerLine){
        headerLines.add(headerLine);
    }

    public static void selected(HeaderLine headerLine){
        for (HeaderLine temp :headerLines) {
            temp.desselected();
        }
        headerLine.selected();
    }

    public static void selected(int position) {
        if(headerLines!=null) {
            for (HeaderLine temp : headerLines) {
                temp.desselected();
            }
            try {
                headerLines.get(position).selected();
            } catch (IndexOutOfBoundsException e) {
            }
        }
    }
}
