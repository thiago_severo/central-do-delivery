package br.com.serveapp.serveapp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAddPhone;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.MaskEditUtil;

public class CadastroTelefoneActivityForLoginApi extends AppCompatActivity {

    Button btnProximo;
    Button btnCancelar;
    Request requestServiceAddphone;
    ImageView imgLogo;

    ServiceAddPhone serviceAddPhone;
    TextInputLayout textInputLayoutPhone;
    TextInputEditText text_cd_phone;
    TextInputEditText textDDD;
    TextInputEditText maskedEditText;
    String wantPermission = Manifest.permission.READ_PHONE_STATE;
    ArrayList<String> _mst=new ArrayList<>();
    private static final int PERMISSION_REQUEST_CODE = 1;
    public ProgressDialog mProgress;

    View.OnFocusChangeListener listenerChengeFocus = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if (!hasFocus && v.getId() == R.id.text_cd_bairro) {
                if (validatePhone()) {
                    btnProximo.setEnabled(false);
                }
                ;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_telefone_for_api_login);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);


        btnProximo = findViewById(R.id.btnAvancar);
        textInputLayoutPhone = findViewById(R.id.textInputLayoutPhone);
        maskedEditText = findViewById(R.id.masked_edit_text);
        imgLogo = findViewById(R.id.serveLogo);

/*        BrPhoneNumberFormatter addLineNumberFormatter = new BrPhoneNumberFormatter(new WeakReference<EditText>(maskedEditText));
        maskedEditText.addTextChangedListener(addLineNumberFormatter);
*/

        maskedEditText.addTextChangedListener(MaskEditUtil.mask(maskedEditText, MaskEditUtil.FORMAT_FONE, getApplicationContext()));
        maskedEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                validatePhone();
                textInputLayoutPhone.setError(null);
            }
        });

        mProgress =new ProgressDialog(this);
        String titleId="Adicionando novo numero de telefone...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("por favor, aguarde...");

        /*if (!checkPermission(wantPermission)) {
            requestPermission(wantPermission);
        } else {
            maskedEditText.setMaskedText(getPhone().get(2));
         //   Log.d(TAG, "Phone number: " + getPhone());
         //   _mst = getPhone();
        }*/

        btnCancelar = findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavigationDrawerActivity.refference.finish();
                CadastroTelefoneActivityForLoginApi.this.finish();

            }
        });


       // new ListSetImage(getApplicationContext(), imgLogo).execute("system/central.png");

        requestServiceAddphone = new Request(getApplicationContext() ,new ServiceAddPhone(this));
        btnProximo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (validatePhone() == true) {

                    mProgress.show();
                    Map<String, String> param = new HashMap<>();
                    param.put("phone", maskedEditText.getText().toString());
                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    requestServiceAddphone.request(param);

                }
            }
        });

    }

    private void requestPermission(String permission){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
            Toast.makeText(this, "Phone state permission allows us to get phone number. Please allow it for additional functionality.", Toast.LENGTH_LONG).show();
        }
        ActivityCompat.requestPermissions(this, new String[]{permission},PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onBackPressed() {

       super.onBackPressed();
        NavigationDrawerActivity.refference.finish();
        CadastroTelefoneActivityForLoginApi.this.finish();

    }

    @TargetApi(Build.VERSION_CODES.O)
    private ArrayList<String> getPhone() {

        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), wantPermission) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }

        ArrayList<String> _lst =new ArrayList<>();
        _lst.add(""+String.valueOf(phoneMgr.getCallState()));
        _lst.add(""+phoneMgr.getImei());
        _lst.add(""+phoneMgr.getLine1Number());
        _lst.add(""+phoneMgr.getSimSerialNumber());
        _lst.add(""+phoneMgr.getSimOperatorName());
        _lst.add(""+phoneMgr.getMeid());
        _lst.add(""+String.valueOf(phoneMgr.getSimState()));
        _lst.add(""+phoneMgr.getSimCountryIso());
        return _lst;

    }

    private boolean checkPermission(String permission){
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private boolean validatePhone() {

        String usernameInput = maskedEditText.getText().toString().trim();
        if (maskedEditText.getText().toString().isEmpty()){
            textInputLayoutPhone.setError("O campo não pode ficar vazio");
            return false;
        }else if (maskedEditText.getText().toString().trim().length() < 13) {
            textInputLayoutPhone.setError("número de telefone inválido");
            return false;
        }
        else if (maskedEditText.getText().toString().trim().length() > 14) {
            textInputLayoutPhone.setError("número de telefone muito longo");
            return false;
        }
        else {
            textInputLayoutPhone.setError(null);
            return true;
        }
    }


    public void  buildConfirmDialog(String textMain,  String texto){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(CadastroTelefoneActivityForLoginApi.this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView text = mView.findViewById(R.id.textSucessMessage);
        TextView textViewMain = mView.findViewById(R.id.textMainMsg);

        text.setText(texto);
        textViewMain.setText(textMain);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10, mPendingIntent);
        System.exit(0);
    }

    public void sucessAddPhone(){
        setResult(RESULT_OK);
        finish();
    }


}