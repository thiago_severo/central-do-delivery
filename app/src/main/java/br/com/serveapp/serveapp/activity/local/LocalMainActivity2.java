package br.com.serveapp.serveapp.activity.local;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import br.com.serveapp.serveapp.entidades.Estabelecimento;
import br.com.serveapp.serveapp.entidades.cardapio.GroupItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.entidades.pedido.PedidoLocal;
import br.com.serveapp.serveapp.fragment.local.FragmentListDemandsLocal;
import br.com.serveapp.serveapp.fragment.local.FragmentListProductsLocal;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.enun.StatusPedido;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.MenuItem;

import br.com.serveapp.serveapp.repository.objects.Repository;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentManager;
import androidx.viewpager.widget.ViewPager;

import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static br.com.serveapp.serveapp.repository.objects.Repository.listGroupPedidosToTabLocal;
import static br.com.serveapp.serveapp.repository.objects.Repository.listOfItensPedidosToTabLocal;
import static br.com.serveapp.serveapp.repository.objects.Repository.listOfPedidosToTabLocal;
import static br.com.serveapp.serveapp.repository.objects.Repository.removelAllItensPedidosToTabLocal;

public class LocalMainActivity2 extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TabsPagerAdapter tabePageAdapter;
    public ViewPager viewPager;
    private List<ItemPedido> itemPedidoList = new ArrayList<>();


    TextView textValTotalDemand;
    TextView textTotalItensAdd;
    CardView cardFinishDemand;

    public static final int ITEM_ADD = 5;
    public static final int GET_DEMAND = 6;
    public static final int RESULT_GET_DEMAND_SUCESS = 7;

    public double valTotal=0;
    int qtdItem = 0;

    BottomNavigationView bottomNavigationView;
    FragmentManager fragmentManager = getSupportFragmentManager() ;
 //   private A sfragment =new FragmentHome();
    private BottomNavigationView.OnNavigationItemSelectedListener listener =

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

                    switch (menuItem.getItemId()){
                        case R.id.navigation_left:
                            finish();
                            overridePendingTransition(0,0);
                            break;

                        case R.id.nav_carrinho_local:
                            startActivity(new Intent(getApplicationContext(),ConfirmItensDemandLocal.class));
                            overridePendingTransition(0,0);
                            break;
                    }

                    return  true;
                }
            };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_main2);
        Toolbar toolbar = findViewById(R.id.toolbarMainProductsDelivery);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        tabePageAdapter = new TabsPagerAdapter( getSupportFragmentManager() );
        FragmentListProductsLocal tabCardapio = new FragmentListProductsLocal(this, true);
        FragmentListProductsLocal tabComanda = new FragmentListProductsLocal( this, false);
        FragmentListDemandsLocal tabPedido = new FragmentListDemandsLocal( this);

        //   tabPedidoActivity = new TabPedidoActivity(itemAdapter);

        tabePageAdapter.adicionar(  tabCardapio , "Cardápio");
        tabePageAdapter.adicionar( tabComanda, "Comanda");
        tabePageAdapter.adicionar( tabPedido, "Pedidos");

        viewPager = (ViewPager) findViewById(R.id.pagerScrolling);
        viewPager.setAdapter(tabePageAdapter);

        @SuppressLint("WrongViewCast") TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        textValTotalDemand = findViewById(R.id.textTotalDemandinCar);
        textTotalItensAdd = findViewById(R.id.textQtditensCar);
        cardFinishDemand = findViewById(R.id.card_item_add_to_car);
        cardFinishDemand.setVisibility(View.GONE);
        cardFinishDemand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Repository.itemPedidoToActivityFinalizeDemand.addAll(itemPedidoList);
                startActivityForResult(new Intent(getApplicationContext(), ConfirmItensDemandLocal.class), GET_DEMAND);
                overridePendingTransition(0,0);
            }
        });

        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(listener);
        bottomNavigationView.setSelectedItemId(R.id.nav_home_local);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.local_main_activity2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_tools) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == Activity.RESULT_OK) {
            String result = data.getStringExtra("result");
            //  snackbar.show();
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
        }
        if(resultCode == ITEM_ADD){

            qtdItem+= Repository.itemPedidoList.size();

            itemPedidoList.addAll(Repository.itemPedidoList);
            Repository.removeAll();

            valTotal+=  Double.parseDouble( data.getStringExtra("valTotal"));
            textValTotalDemand.setText("R$ "+valTotal+"");
            textTotalItensAdd.setText("("+qtdItem+")");
            cardFinishDemand.setVisibility(View.VISIBLE);

        };

        if(resultCode == RESULT_GET_DEMAND_SUCESS){

            Repository.pedidoToTabLocal = buildDemand(listOfItensPedidosToTabLocal);
            listGroupPedidosToTabLocal.addAll(listOfItensPedidosToTabLocal);
            removelAllItensPedidosToTabLocal();
            listOfPedidosToTabLocal.add(Repository.pedidoToTabLocal);

            qtdItem =0;
            valTotal = 0;

            itemPedidoList.removeAll(itemPedidoList);
            cardFinishDemand.setVisibility(View.GONE);
            viewPager.setCurrentItem(tabePageAdapter.getCount());

        }

    }

    public Pedido buildDemand(List<GroupItemPedido> groupItemPedidos ){

        Pedido pedido = new PedidoLocal();
        List<ItemPedido> itemPedidos = new ArrayList<>();

        for(GroupItemPedido groupItemPedido : groupItemPedidos){
            for(int i =0; i<groupItemPedido.getCountIntPedido(); i++) {
                itemPedidos.add(groupItemPedido.getItemPedido());
            }
        }

        pedido.setId(new Random().nextInt());
        Estabelecimento estabelecimento2 = new Estabelecimento();
        estabelecimento2.setNome(" Cantina Burgueria ");
        pedido.setEstabelecimento(estabelecimento2);
        pedido.setItens(itemPedidos);
        pedido.setStatus(StatusPedido.RECEBIDO);
        return pedido;
    }

}
