package br.com.serveapp.serveapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import br.com.serveapp.serveapp.adapt.LineAllListMensagemAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListChat;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ListMessagesChatActivity extends AppCompatActivity {

    Request serviceLisChat;
    RecyclerView recyclerChat;
    private SwipeRefreshLayout swipeRefreshLayout;

    TextView bodyChatInactive;
    TextView titleChatInactive;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            finish();
        }

    };

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_msg_activity);

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        try{
            if(Repository.getIdUsuario(this)==null){
                finish();
            }
        }catch (Exception e){
            finish();
        }

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        bodyChatInactive = findViewById(R.id.bodyChatInactive);
        titleChatInactive = findViewById(R.id.titleChatInactive);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);

        recyclerChat = findViewById(R.id.end_list);
        recyclerChat.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        serviceLisChat = new Request(getApplicationContext(), new ServiceListChat(this, swipeRefreshLayout));
        serviceLisChat.request();



        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            serviceLisChat.request();
                        } catch (NullPointerException e) {
                            serviceLisChat = new Request(getApplicationContext(), new ServiceListChat(ListMessagesChatActivity.this, swipeRefreshLayout));
                            serviceLisChat.request();
                        }
                    }
                }
        );

        if(getIntent().hasExtra("serveapp.notifyId")){
            ChatActivity.idPedido = getIntent().getStringExtra("id_pedido");
            startActivityForResult(new Intent(getApplicationContext(), ChatActivity.class), 2);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void initializeAdapterMsgs(JSONArray result) {

        LineAllListMensagemAdapter lineAllListMensagemAdapter = new LineAllListMensagemAdapter(getApplicationContext(), result);
        lineAllListMensagemAdapter.setOnItemClickListner(new LineAllListMensagemAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JSONObject item) {
                try {
                    ChatActivity.idPedido = item.getString("_id");
                    startActivityForResult(new Intent(getApplicationContext(), ChatActivity.class), 2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        recyclerChat.setAdapter(lineAllListMensagemAdapter);

        if(result.length()>0){
            if(titleChatInactive!=null)
            titleChatInactive.setVisibility(View.GONE);
            if(bodyChatInactive!=null)
            bodyChatInactive.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            serviceLisChat.request();
        }

    }

    public void updateListMessages(){
        serviceLisChat.request();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Repository.listMessagesChatActivity = this;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Repository.listMessagesChatActivity == this) {
            Repository.listMessagesChatActivity = null;
        }
    }



}