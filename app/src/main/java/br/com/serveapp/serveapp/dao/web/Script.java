package br.com.serveapp.serveapp.dao.web;

public class Script {
    private static final String url = "https://serveapp.com.br/";
    private static final String folder = "script/";

    //private static final String url = "http://172.20.255.181/";
    //private static final String folder = "serve-admin/public/script/";
    public static final String login = url + folder + "select_cliente_where_login.php";
    public static final String loginApi = url + folder + "login_api.php";
    public static final String recouverPassword = url + folder + "redefinir_senha.php";
    public static final String image = url + folder.replace("script","images");
    public static final String serve_details = url + folder + "serve_details.php";
    public static final String list_estabelecimento = url + folder + "estabelecimento.php";
    public static final String list_chat = url + folder + "select_chat.php";
    public static final String update_address_cart = url + folder + "select_produtos_do_carrinho_new.php";

    public static final String calc_distancia = url + folder + "select_and_calc_distance.php";
    public static final String check_in = url + folder + "check_in.php";
    public static final String select_forma_pagamento = url + folder + "select_forma_pagamento.php";

    public static String getScriptName(String script) {
        script = script.replace("/", "");
        script = script.replace(":", "");
        return script;
    }

    public static String getFolder() {
        return folder;
    }

    public static String getUrl() {
        return url;
    }
}