package br.com.serveapp.serveapp.fragment.local;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.Cliente;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.activity.local.LocalMainActivity2;
import br.com.serveapp.serveapp.adapt.CategorieListAdapter;
import br.com.serveapp.serveapp.adapt.LineProductsForEstabelecimentsAdapter;
import br.com.serveapp.serveapp.adapt.LocalDemandAdapter;

import java.util.List;

import static br.com.serveapp.serveapp.repository.objects.Repository.listOfPedidosToTabLocal;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FragmentListDemandsLocal extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    LocalDemandAdapter adapter;
    LocalMainActivity2 localActivity;
    private static List<Pedido> listItensDemand = null;

    private RecyclerView listDemandOfClient;
    View view;

    private LineProductsForEstabelecimentsAdapter lineAdapter;
    private CategorieListAdapter categorieListAdapter;
    boolean preencher;


    public FragmentListDemandsLocal(Activity activity ) {
        // Required empty public constructor
//        this.localActivity = (LocalMainActivity2) activity;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_fragment_list_products_local, container, false);

            listDemandOfClient = view.findViewById(R.id.recyclerListProductslocal);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
            listDemandOfClient.setLayoutManager(llm);


     //       setAdapter();

        return view;

    }


    public void setAdapter(){
        Cliente cliente = new Cliente();
        cliente.setId(1);

        adapter = new LocalDemandAdapter(getApplicationContext(), listOfPedidosToTabLocal,cliente);
        listDemandOfClient.setAdapter(adapter);
    }

}
