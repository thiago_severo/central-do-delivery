package br.com.serveapp.serveapp.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.adapt.AdapterHorizontalListCategoryItens;
import br.com.serveapp.serveapp.adapt.LineDemandGeral;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAvaliarItemPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceAvaliarPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceCancelPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceSelectPedidosEstab;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.repository.objects.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

public class FragmentListDemandsEstablishment extends Fragment implements AvaliacaoInterface, AvaliacaoItemInterface {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;
    Request getRequestListDemands;
    Request requestCancelDemand;
    ProgressBar progressBarSendFeedBack;
    ProgressBar progressBarItemPedido;
    Request serviceAvaliarItem;
    public ProgressBar progressBar;
    private SwipeRefreshLayout swipeRefreshLayout;

    public static long time;
    View view;
    public static int cdCategory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    TextView titleEmptyDemands;
    TextView bodyListDemandsEmpty;
    private static RecyclerView recyclerItensDemand;
    public static JSONArray listRecyclerHorizontalCategories;
    RecyclerView horizontalRecyclerCategories;
    AdapterHorizontalListCategoryItens adapterHorizontalListCategoryItens;
    ArrayList<ItemPedido> itemPedidos;
    public LineDemandGeral lineItemDemandEstabelishment;
    Activity activity;
    AlertDialog dialog = null;
    Request requestAvaliarItem;
    Request requestAvaliarPedido;
    public static FragmentListDemandsEstablishment refference;

    public FragmentListDemandsEstablishment(Activity activity) {
        this.activity = activity;
    }

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_favorits, container, false);

        recyclerItensDemand = view.findViewById(R.id.listFavorits);
        swipeRefreshLayout = view.findViewById(R.id.swiperefresh);
        titleEmptyDemands = view.findViewById(R.id.titleEmptyDemands);
        bodyListDemandsEmpty = view.findViewById(R.id.bodyListDemandsEmpty);
        progressBar = view.findViewById(R.id.progressBar6);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try{
                            getRequestListDemands.request();
                        }catch (NullPointerException e){
                            if (activity instanceof ListProductsStablishmentDeliveryActivity)
                                getRequestListDemands = new Request(getActivity().getApplicationContext(), new ServiceSelectPedidosEstab(FragmentListDemandsEstablishment.this, Repository.idEstabelecimento, swipeRefreshLayout));
                            else {
                                getRequestListDemands = new Request(getActivity().getApplicationContext(), new ServiceSelectPedidosEstab(FragmentListDemandsEstablishment.this, "0", swipeRefreshLayout));
                            }
                        }
                    }
                }
        );

        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recyclerItensDemand.setLayoutManager(llm);
        // ----
        requestAvaliarPedido = new Request(getApplicationContext(), new ServiceAvaliarPedido(this, activity));
        requestAvaliarItem = new Request(getApplicationContext(), new ServiceAvaliarItemPedido(this, activity));
        requestCancelDemand = new Request(getActivity().getApplicationContext(), new ServiceCancelPedido(this));

        if (activity instanceof ListProductsStablishmentDeliveryActivity)
            getRequestListDemands = new Request(getActivity().getApplicationContext(), new ServiceSelectPedidosEstab(this, Repository.idEstabelecimento, swipeRefreshLayout));
        else {
            getRequestListDemands = new Request(getActivity().getApplicationContext(), new ServiceSelectPedidosEstab(this, "0", swipeRefreshLayout));
        }

        progressBar.setVisibility(View.VISIBLE);
        getRequestListDemands.request();

        return view;
    }

   public void dimissProgressBarSendFeedBack(){
    progressBarSendFeedBack.setVisibility(View.GONE);
   }

    public void updateList() {
        if(getRequestListDemands!=null){
            progressBar.setVisibility(View.VISIBLE);
            getRequestListDemands.request();
        }
    }

    public void initializeAdapterListCategories() {
        try {
            adapterHorizontalListCategoryItens = new AdapterHorizontalListCategoryItens(getPositionCategoty(cdCategory), getApplicationContext(), listRecyclerHorizontalCategories);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        horizontalRecyclerCategories.setAdapter(adapterHorizontalListCategoryItens);
        horizontalRecyclerCategories.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        int position = 0;
        super.onActivityResult(requestCode, resultCode, data);

        try {
            int pos = data.getIntExtra("position", 0);
            lineItemDemandEstabelishment.listItensDemand.getJSONObject(pos).put("status", "entregue");
            lineItemDemandEstabelishment.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private int getPositionCategoty(int cdCategory) throws JSONException {
        int pos = 0;
        for (int i = 0; i < listRecyclerHorizontalCategories.length(); i++) {
            String val = listRecyclerHorizontalCategories.getJSONObject(i).getString("_id");

            if (cdCategory == Integer.parseInt(val)) {
                pos = i;
                break;
            }

        }
        return pos;
    }


    public void dimissPopUpAvaliacao() {
        dialog.dismiss();
    }


    public void initializeAdapterListItens(JSONArray result, String position) {

        if(result.length()>0){
            bodyListDemandsEmpty.setVisibility(View.GONE);
            titleEmptyDemands.setVisibility(View.GONE);
        }

        time = Calendar.getInstance().getTimeInMillis();
        lineItemDemandEstabelishment = new LineDemandGeral(getActivity(), getApplicationContext(), result);
        lineItemDemandEstabelishment.setOnItemClickCancelListener(new LineDemandGeral.onItemClickCancel() {
            @Override
            public void cancel(JSONObject jsonObject) {
                createCancelDemand(jsonObject);
            }
        });

        lineItemDemandEstabelishment.setOnClickListenerViewDetalhes(new LineDemandGeral.OnItemClickListenerViewDetalhes() {
            @Override
            public void onItemClickListenerViewDetalhes(JSONObject itemCart, int position) {
                try {
                    DetailDemand.idPedido = itemCart.getString("ped_id");
                    DetailDemand.detailObject = itemCart;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivityForResult(new Intent(getApplicationContext(), DetailDemand.class), 1);
            }
        });

        lineItemDemandEstabelishment.setOnItemClickAvaliate(new LineDemandGeral.OnItemClickAvaliate() {
            @Override
            public void openAvaliate(final JSONObject jsonObject) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        createAvaliationDemand(jsonObject);
                    }
                });
            }
        });

        lineItemDemandEstabelishment.setShowPopUpAvaliationListener(new LineDemandGeral.ShowPopUpAvaliationListener() {
            @Override
            public void showPopUpAvaliation(final JSONObject jsonObject, final String position) {

                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);

                        final RatingBar mRatingBar;
                        TextView mRatingScale;
                        TextView text_type_evaluate;
                        final EditText mFeedback;
                        Button mSendFeedback;
                        Button mCancelFeedFeedback;
                        TextView text_type_evaluate_estabelecimento;

                        View mView = getLayoutInflater().inflate(R.layout.activity_evaluate_item, null);
                        text_type_evaluate_estabelecimento = mView.findViewById(R.id.text_type_evaluate_estabelecimento);
                        mRatingBar = (RatingBar) mView.findViewById(R.id.ratting_avaliation);
                        mRatingScale = (TextView) mView.findViewById(R.id.tvRatingScale);
                        mFeedback = (EditText) mView.findViewById(R.id.descAvaliacao);
                        mSendFeedback = (Button) mView.findViewById(R.id.btnSubmit);
                        mCancelFeedFeedback = mView.findViewById(R.id.btnCancelFeedFeedback);
                        progressBarItemPedido = mView.findViewById(R.id.progressBarItemPedido);
                        progressBarItemPedido.setVisibility(View.GONE);

                        mBuilder.setView(mView);
                        dialog = mBuilder.create();
                        dialog.show();

                        try {
                            text_type_evaluate_estabelecimento.setText(jsonObject.getString("nome"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        setRattingAction(mRatingBar, mRatingScale);
                        mSendFeedback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Map<String, String> param = new HashMap<>();
                                try {
                                    param.put("id_estabelecimento", Repository.idEstabelecimento);
                                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                                    param.put("function_method", "avaliar_produto");
                                    param.put("id_produtoSelecionado", jsonObject.getString("id"));
                                    param.put("nota", mRatingBar.getRating() + "");
                                    param.put("descricao", mFeedback.getText().toString());
                                    progressBarItemPedido.setVisibility(View.VISIBLE);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                ServiceAvaliarItemPedido.position = position;
                                requestAvaliarItem.request(param);
                            }
                        });

                        mCancelFeedFeedback.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.dismiss();
                            }
                        });

                    }
                });


            }
        });

        if (position != null) {
            recyclerItensDemand.scrollToPosition(Integer.parseInt(position));
        }
        recyclerItensDemand.setAdapter(lineItemDemandEstabelishment);

        recyclerItensDemand.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (activity instanceof ListProductsStablishmentDeliveryActivity) {
                    CardView fab = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.cardFab);

                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (activity instanceof ListProductsStablishmentDeliveryActivity) {
                    CardView fab = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.cardFab);
                    ConstraintLayout fab1 = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.constransCallAtendente);
                    ConstraintLayout fab2 = ListProductsStablishmentDeliveryActivity.refference.findViewById(R.id.constraintLayoutLeft);

                    if(dy>0){
                        fab.setVisibility(View.GONE);
                        fab1.setVisibility(View.GONE);
                        fab2.setVisibility(View.GONE);
                    }
                    if(dy<0){ fab.setVisibility(View.VISIBLE);}
                }
            }
        });
    }

    public void dimissProgressAvaliacaoItem(){
         progressBarItemPedido.setVisibility(View.GONE);
    }

    public void showMsg(final String titulo, final String mensagem) {

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
                View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);

                TextView textView = mView.findViewById(R.id.textSucessMessage);
                TextView textViewSucess = mView.findViewById(R.id.textSucessMessage);

                textView.setText(titulo);
                textViewSucess.setText(mensagem);

                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();

                Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
                btnOkMessage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });

    }

    public void setRattingAction(RatingBar mRatingBar, final TextView mRatingScale) {

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                mRatingScale.setText(String.valueOf(v));

                switch ((int) ratingBar.getRating()) {
                    case 1:
                        mRatingScale.setText("Muito Ruim");
                        break;
                    case 2:
                        mRatingScale.setText("Precisa Melhorar");
                        break;
                    case 3:
                        mRatingScale.setText("Bom");
                        break;
                    case 4:
                        mRatingScale.setText("Muito Bom");
                        break;
                    case 5:
                        mRatingScale.setText("Excelente");
                        break;
                    default:
                        mRatingScale.setText("");

                }

            }

        });

    }

    public void createCancelDemand(final JSONObject param) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);
                View mView = getLayoutInflater().inflate(R.layout.confirm_cancel, null);

                Button btnNo = mView.findViewById(R.id.btnRecuseCancel);
                Button btnYes = mView.findViewById(R.id.btnConfirmCancel);

                mBuilder.setView(mView);
                dialog = mBuilder.create();
                dialog.show();

                btnNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                btnYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        requestCancelDemand = new Request(getActivity().getApplicationContext(), new ServiceCancelPedido(FragmentListDemandsEstablishment.this));

                        Map<String, String> paramLogin = new HashMap<>();
                        try {
                            paramLogin.put("function_method", "cancel");
                            paramLogin.put("id_pedido", param.getString("ped_id"));
                            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
                            paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        requestCancelDemand.request(paramLogin);
                        dialog.dismiss();
                    }});


                    }}) ;

    }


                public void createAvaliationDemand(final JSONObject jsonObject) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(activity);

                final RatingBar mRatingBar;
                final RatingBar ratting_avaliation_atendimento;
                TextView mRatingScale;
                final EditText mFeedback;
                final EditText mFeedbackAtendimento;

                Button mSendFeedback;
                Button mCancelFeedFeedback = null;
                final Switch switchPrazo;
                final Switch switchEsperado;

               View mView = getLayoutInflater().inflate(R.layout.activity_evaluate_demand, null);
                progressBarSendFeedBack = mView.findViewById(R.id.progressBarSendFeedBack);
                TextView text_type_evaluate_estabelecimento = mView.findViewById(R.id.text_type_evaluate_estabelecimento);;
                TextView text_type_evaluate_atendimento = mView.findViewById(R.id.text_type_evaluate_atendimento);
                TextView tvRatingScaleAtendimento = mView.findViewById(R.id.tvRatingScaleAtendimento);

                mRatingBar = (RatingBar) mView.findViewById(R.id.ratting_avaliation);
                mRatingScale = (TextView) mView.findViewById(R.id.tvRatingScale);

                mFeedback = (EditText) mView.findViewById(R.id.descAvaliacao);
                ratting_avaliation_atendimento = (RatingBar) mView.findViewById(R.id.ratting_avaliation_atendimento);
                mFeedbackAtendimento = (EditText) mView.findViewById(R.id.descAvaliacaoAtendimento);

                progressBarSendFeedBack.setVisibility(View.GONE);

                mSendFeedback = (Button) mView.findViewById(R.id.btnSubmit);
                mCancelFeedFeedback = mView.findViewById(R.id.btnCancelFeedFeedback);
                switchPrazo = mView.findViewById(R.id.switchPrazo);
                switchEsperado = mView.findViewById(R.id.switchEsperado);
                ;
                switchPrazo.setChecked(true);
                switchEsperado.setChecked(true);

                mBuilder.setView(mView);
                dialog = mBuilder.create();
                dialog.show();


                try {
                    if (jsonObject.getString("type").equals("delivery")) {
                        text_type_evaluate_estabelecimento.setText("PEDIDO");
                        text_type_evaluate_atendimento.setText("ENTREGA");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setRattingAction(mRatingBar, mRatingScale);
                setRattingAction(ratting_avaliation_atendimento, tvRatingScaleAtendimento);
                mSendFeedback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Map<String, String> param = new HashMap<>();
                        try {
                            param.put("id_pedido", jsonObject.getString("ped_id"));
                            param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                            param.put("nota_pedido", mRatingBar.getRating() + "");
                            param.put("desc_pedido", mFeedback.getText().toString());
                            param.put("received_as_described", switchEsperado.isChecked() + "");
                            param.put("received_within", switchPrazo.isChecked() + "");
                            param.put("nota_atendimento", ratting_avaliation_atendimento.getRating() + "");
                            param.put("desc_atendimento", mFeedbackAtendimento.getText().toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        progressBarSendFeedBack.setVisibility(View.VISIBLE);
                        requestAvaliarPedido.request(param);
                    }
                });

                mCancelFeedFeedback.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refference = this;
    }


}