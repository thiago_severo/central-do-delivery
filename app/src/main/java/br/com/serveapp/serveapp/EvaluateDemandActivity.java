package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class EvaluateDemandActivity extends AppCompatActivity {
    RatingBar mRatingBar;
    TextView mRatingScale;
    TextView text_type_evaluate;
    EditText mFeedback;
    Button mSendFeedback;
    Button mCancelFeedFeedback;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evaluate_demand);
        getSupportActionBar().hide();


        mRatingBar = (RatingBar) findViewById(R.id.ratingBarLikedListProduct3);
        mRatingScale = (TextView) findViewById(R.id.tvRatingScale);
        mFeedback = (EditText) findViewById(R.id.descAvaliacao);
        mSendFeedback = (Button) findViewById(R.id.btnSubmit);
        mCancelFeedFeedback = findViewById(R.id.btnCancelFeedFeedback);
        text_type_evaluate = findViewById(R.id.text_type_evaluate_estabelecimento);


        String typeDemand="";
        if(getIntent().hasExtra("typeDemand")) {
            typeDemand = getIntent().getStringExtra("typeDemand");
        }

        if(typeDemand.equals("delivery"))
            text_type_evaluate.setText("ATENDIMENTO/ENTREGA");
        else if(typeDemand.equals("local"))
            text_type_evaluate.setText("ESTABELECIMENTO/PEDIDO");

        mCancelFeedFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                mRatingScale.setText(String.valueOf(v));

                switch ((int) ratingBar.getRating()) {

                    case 1:
                        mRatingScale.setText("Very bad");
                        break;
                    case 2:
                        mRatingScale.setText("Need some improvement");
                        break;
                    case 3:
                        mRatingScale.setText("Good");
                        break;
                    case 4:
                        mRatingScale.setText("Great");
                        break;
                    case 5:
                        mRatingScale.setText("Awesome. I love it");
                        break;
                    default:
                        mRatingScale.setText("");

                }

            }

        });





        mSendFeedback.setOnClickListener(new View.OnClickListener() {

            @Override

            public void onClick(View view) {

                if (mFeedback.getText().toString().isEmpty()) {
                    Toast.makeText(EvaluateDemandActivity.this, "Please fill in feedback text box", Toast.LENGTH_LONG).show();
                } else {
             //       mRatingBar.setRating(0);
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(EvaluateDemandActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    Button mBtnSucessFeedBack = (Button) mView.findViewById(R.id.btnConfirmDeleteCardCredit);
                    mBtnSucessFeedBack.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.cancel();
                            finish();
                        }
                    });

                    //Toast.makeText(EvaluateDemandActivity.this, "Thank you for sharing your feedback", Toast.LENGTH_SHORT).show();
                }

            }

        });


        DisplayMetrics dm = getResources().getDisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.9), (int)(height*.75));

    }
}
