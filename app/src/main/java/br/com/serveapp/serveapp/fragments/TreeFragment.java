package br.com.serveapp.serveapp.fragments;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import br.com.serveapp.serveapp.MainActivity;
import br.com.serveapp.serveapp.R;

public class TreeFragment extends Fragment {


    public FragmentTransaction fragmentTransaction;
    Button button;

    TextView textView;


        @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment3, container, false);
          //  button =  view.findViewById(R.id.cancelFragment);

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    remove();
                }
            });
                    view.setBackgroundColor(Color.RED);

        return view;

    }


    public void remove(){
        MainActivity mainActivity = (MainActivity) getActivity();
        fragmentTransaction = mainActivity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.exit_to_left, R.anim.exit_to_left, R.anim.exit_to_left, R.anim.entrer_from_right);
        mainActivity.fragmentListHome.remove( mainActivity.fragmentListHome.get(mainActivity.fragmentListHome.size()-1));
       // fragmentTransaction.replace(R.id.fragment_container_favorits, mainActivity.fragmentListHome.get(mainActivity.fragmentListHome.size()-1)).commit();


    }




    }



