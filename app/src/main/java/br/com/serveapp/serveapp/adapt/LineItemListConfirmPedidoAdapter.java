package br.com.serveapp.serveapp.adapt;

import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.synnapps.carouselview.CarouselView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceRemoveItemCart;
import br.com.serveapp.serveapp.dao.webService.ServiceUpdateItensCart;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class LineItemListConfirmPedidoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    LayoutInflater inflater;
    public JSONArray listItensCarrinho;
    Context context;
    Request requestUpdateItensCart;
    Request requestDeleteItemCart;
    TextView textViewTotalValueCart;
    ProgressBar progressBar;
    OnItemClickListener listener;
    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;

    public LineItemListConfirmPedidoAdapter(ConfirmItensPedidoActiviy confirmItensPedidoActiviy, Context context, JSONArray jsonArray, TextView textTotalValueCart, ProgressBar progressBar) {
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.textViewTotalValueCart = textTotalValueCart;
        this.context = context;
        this.listItensCarrinho = jsonArray;
        this.progressBar = progressBar;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_product, parent, false);
        return new ItemCart(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {

            JSONObject itemCar = listItensCarrinho.getJSONObject(position);
            ItemCart line = ((ItemCart) holder);
            line.valueItem.setText(itemCar.getString("valor"));
            line.nomeItem.setText(itemCar.getString("nome"));
            line.qtdItens = itemCar.getInt("qtd");
            line.textCountItensPedido.setText(line.qtdItens + "");
            if(!itemCar.isNull("itens"))
                line.textViewDesc.setText(itemCar.getString("itens"));
            if(itemCar.isNull("desconto") || itemCar.getString("desconto").equals("null")){
                line.imageCupomDesconto.setVisibility(View.GONE);
            }else{
                line.imageCupomDesconto.setVisibility(View.VISIBLE);
                line.textCupomDescont2.setText(itemCar.getString("desconto"));
            }
            //((ItemCart) holder).textDataAddToCart.setText(itemCar.getString("data"));
            line.textDataAddToCart.setText("");
            line.textHourAddToCart.setText(itemCar.getString("hora"));
            line.obj = itemCar;

            if (itemCar.getInt("brinde") == 0) {
                line.isBrinde.setVisibility(View.GONE);
            } else {
                line.isBrinde.setVisibility(View.VISIBLE);
            }

            if (!itemCar.isNull("foto")) {
                new ListSetImage(context, line.imageItemCardapio).execute(itemCar.getString("foto"), line.progressImage);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listItensCarrinho.length();
    }

    public void removeItem(int position) {
        listItensCarrinho.remove(position);
        notifyDataSetChanged();
    }

    class ItemCart extends RecyclerView.ViewHolder {

        public JSONObject obj;
        public TextView nomeItem;
        public TextView valueItem;
        public ImageView imageItemCardapio;
        public ImageView btn_down_detail;
        public ImageView btn_accrease_item_demand;
        public ImageView btn_decrease_item;
        public TextView textCountItensPedido;
        public TextView textCupomDescont2;

        public TextView textViewDesc;

        public int qtdItens = 1;
        public TextView textHourAddToCart;
        public TextView textDataAddToCart;
        public ProgressBar progressImage;
        public ImageView imageCupomDesconto;
        ImageView optionsText;
        private ImageView isBrinde;

        public ItemCart(View itemView) {
            super(itemView);

            isBrinde = itemView.findViewById(R.id.imageIsBrinde);
            optionsText = itemView.findViewById(R.id.optionsText);
            textDataAddToCart = (TextView) itemView.findViewById(R.id.tempPreparo);
            textHourAddToCart = (TextView) itemView.findViewById(R.id.textMedida);
            valueItem = (TextView) itemView.findViewById(R.id.txtValorItemMenu);
            textCountItensPedido = (TextView) itemView.findViewById(R.id.textQtdItemDemandFinalize);
            nomeItem = (TextView) itemView.findViewById(R.id.nomeItemCardapio);
            imageItemCardapio = (ImageView) itemView.findViewById(R.id.imageItemProducts);
            progressImage = itemView.findViewById(R.id.progressImage);
            textViewDesc = itemView.findViewById(R.id.textViewDesc);
            textCupomDescont2 = itemView.findViewById(R.id.textCupomDescont2);
            imageCupomDesconto = itemView.findViewById(R.id.imageCupomDescont3);



            //recyclerDetailItemCar.setVisibility(GONE);
            //cardContentLine = itemView.findViewById(R.id.cardExpandableItemPedido);

            btn_down_detail = itemView.findViewById(R.id.btn_down_detail);
            btn_accrease_item_demand = itemView.findViewById(R.id.btn_accrease_item_demand);
            btn_decrease_item = itemView.findViewById(R.id.btn_decrease_item);


            ///////////////////////
            itemView.findViewById(R.id.imageEstabeleciment).setVisibility(View.GONE);
            //////////////////////

            optionsText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final int position = getAdapterPosition();
                    PopupMenu popup = new PopupMenu(context, optionsText);
                    popup.inflate(R.menu.menu_options_line_cart);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.navigation_delete:
                                    try {
                                        deleteItem(listItensCarrinho.getJSONObject(position).getString("_id"), position);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    return true;
                                case R.id.navigation_edit:
                                    if (listener != null) {
                                        try {
                                            listener.onItemClickListener(listItensCarrinho.getJSONObject(position), position);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    return true;
                                default:
                                    return false;
                            }
                        }
                    });
                    popup.show();
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        int position = getAdapterPosition();
                        listener.onItemClickListener(listItensCarrinho.getJSONObject(position), position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });


            btn_accrease_item_demand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    try {
                        progressBar.setVisibility(View.VISIBLE);
                        qtdItens++;
                        requestUpdateItensCart = new Request(context, new ServiceUpdateItensCart(confirmItensPedidoActiviy, context, valueItem, textViewTotalValueCart, textCountItensPedido, obj, progressBar));
                        JSONObject itemCart = listItensCarrinho.getJSONObject(position);
                        requestUpdateItensCart.request(getParamLogin(itemCart, "+1"));
                    } catch (Exception e) {

                    }
                }
            });

            btn_decrease_item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    try {
                        if (qtdItens >= 2) {
                            qtdItens--;
                            progressBar.setVisibility(View.VISIBLE);
                            requestUpdateItensCart = new Request(context, new ServiceUpdateItensCart(confirmItensPedidoActiviy, context, valueItem, textViewTotalValueCart, textCountItensPedido, obj, progressBar
                            ));
                            JSONObject itemCart = listItensCarrinho.getJSONObject(position);
                            requestUpdateItensCart.request(getParamLogin(itemCart, "-1"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            add_expand_image();

        }


        private void downUpCard(final View view) {
            /*if (view.getVisibility() == View.GONE) {
                ObjectAnimator.ofFloat(btn_down_detail, "rotation", 180, 360).start();
                view.setVisibility(View.VISIBLE);
                view.animate()
                        .alpha(1f)
                        .setDuration(500)
                        .setListener(null);

            } else if (view.getVisibility() == View.VISIBLE) {
                ObjectAnimator.ofFloat(btn_down_detail, "rotation", 0, 180).start();
                view.animate()
                        .alpha(0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                view.setVisibility(View.GONE);
                            }
                        });
            }
             */
        }

        private void add_expand_image(){

            imageItemCardapio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(confirmItensPedidoActiviy);
                    View mView = (confirmItensPedidoActiviy).getLayoutInflater().inflate(R.layout.detail_product, null);
                    final CarouselView carouselView = (CarouselView) mView.findViewById(R.id.carouselView);

                    final TextView textViewAddressEvent = mView.findViewById(R.id.textViewAddressEvent);
                    final ImageButton imageButtonCloseEvent = mView.findViewById(R.id.imageButtonCloseEvent);
                    final Button buttonOkEvent = mView.findViewById(R.id.buttonOkEvent);

                    try{
                        JSONArray banners = new JSONArray(obj.getString("fotos"));
                        carouselView.setPageCount(banners.length());
                        CarouselAdapter adapter = new CarouselAdapter(confirmItensPedidoActiviy, banners);
                        carouselView.setViewListener(adapter);
                    }catch (Exception e){

                    }

                /*try {
                    new ListSetImage(fragment.getContext(), imgEvent).execute(item.getString("foto"));
                    //textViewAddressEvent.setText(item.getString("endereco"));

                } catch (JSONException e) {
                    e.printStackTrace();
                }*/

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    imageButtonCloseEvent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    buttonOkEvent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });
                }
            });

        }

    }


    public void deleteItem(String id_product, int position) {

        requestDeleteItemCart = new Request(context, new ServiceRemoveItemCart(confirmItensPedidoActiviy, context, textViewTotalValueCart, position, this));
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(confirmItensPedidoActiviy));
        paramLogin.put("id_produto_selecionado", id_product);
        paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
        paramLogin.put("function_method", "remove_product");
        requestDeleteItemCart.request(paramLogin);

    }


    Map<String, String> getParamLogin(JSONObject jsonObject, String qtdItens) {
        final Map<String, String> paramLogin = new HashMap<>();
        try {
            paramLogin.put("id_produto_selecionado", jsonObject.getString("_id"));
            paramLogin.put("tipo_atendimento", Repository.getTipoAtendimento());
            paramLogin.put("id_entrega", Repository.getIdEntrega(context));
            paramLogin.put("id_cliente", Repository.getIdUsuario(context));
            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
            paramLogin.put("increment", qtdItens + "");
            paramLogin.put("function_method", "update_qtd_product");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return paramLogin;
    }

    public interface OnItemClickListener {
        public void onItemClickListener(JSONObject itemCart, int position);
    }

    public void setOnClickListener(OnItemClickListener onClickListener) {
        this.listener = onClickListener;
    }


}
