package br.com.serveapp.serveapp.drawer;

import android.graphics.Bitmap;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import androidx.annotation.RequiresApi;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

import br.com.serveapp.serveapp.dao.web.Script;

public class SetImageWeb extends AsyncTask<String, Void, Bitmap> {
    ProgressDialog mProgressDialog;
    Context context;
    public SetImageWeb(Context context){
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        // Create a progressdialog
       /* mProgressDialog = new ProgressDialog(main);
        // Set progressdialog title
        mProgressDialog.setTitle("Download Image Tutorial");
        // Set progressdialog message
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        // Show progressdialog
        mProgressDialog.show(); */
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected Bitmap doInBackground(String... URL) {
        Bitmap bitmap = null;
        File file = null;
        String imageName = URL[0];
        String nameNameFormat = imageName.replace("/","_");
        // verificar se existe já no local
        try {
            InputStream is = context.openFileInput(nameNameFormat);
            bitmap = BitmapFactory.decodeStream(is);

            loadLocal();
        } catch (FileNotFoundException e) {
            loadWeb();
            String imageURL = Script.image+imageName;
            try {
                // Download Image from URL
                InputStream input = new java.net.URL(imageURL).openStream();
                System.out.println("ok");
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
             //   saveImage(context, bitmap, imageName );
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

        //String imageURL = URL[0];
        //String imageURL = "https://cdn3.iconfinder.com/data/icons/google-suits-1/32/1_google_search_logo_engine_service_suits-512.png";
        //String imageURL ="http://192.168.0.103/image/serve.png";
        //String imageURL = "https://images.unsplash.com/photo-1527224538127-2104bb71c51b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60";

        //String imageURL = "https://pbs.twimg.com/media/D_Iv-22WsAApeR2?format=jpg&name=small";


        return bitmap;
    }

    public void saveImage(Context context, Bitmap b, String imageName) {
        FileOutputStream foStream;
        try {
         /*   foStream = context.openFileOutput(imageName, Context.MODE_PRIVATE);
            b.compress(Bitmap.CompressFormat.PNG, 100, foStream);
            foStream.close();*/
        } catch (Exception e) {
            Log.d("saveImage", "Exception 2, Something went wrong!");
            e.printStackTrace();
        }
    }

    public void loadLocal(){

    }

    public void loadWeb(){

    }

    @Override
    protected void onPostExecute(Bitmap result) {
        //image.setImageBitmap(result);
        //ProgressBar.setVisibility(View.GONE);
    }

}
