package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.CreateAddressPopUpActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class ServiceAddressRegister extends OnPostResponse {

    CreateAddressAccountActivity createAddressAccountActivity;
    CreateAddressPopUpActivity createAddressPopUpActivity;

    public ServiceAddressRegister(CreateAddressPopUpActivity createAddressPopUpActivity, CreateAddressAccountActivity createAddressAccountActivity) {

        this.createAddressPopUpActivity = createAddressPopUpActivity;
        this.createAddressAccountActivity = createAddressAccountActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_ends_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        if(createAddressAccountActivity!=null) {
            Toast.makeText(createAddressAccountActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
            createAddressAccountActivity.mProgressCreateAddress.dismiss();
            createAddressAccountActivity.btn_cadastrar.setEnabled(true);

        }
        if(createAddressPopUpActivity!=null) {
            Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
            createAddressPopUpActivity.mProgressCreateAddress.dismiss();
            createAddressPopUpActivity.btn_cadastrar.setEnabled(true);
        }

    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();

        paramLogin.put("id_cliente", Repository.getIdUsuario(createAddressAccountActivity));
        paramLogin.put("titulo", createAddressAccountActivity.txtCdTitleAddress.getText().toString());
        paramLogin.put("cep", createAddressAccountActivity.textInputCep.getText().toString());
        paramLogin.put("logradouro", createAddressAccountActivity.text_cd_rua.getText().toString());
        paramLogin.put("num", createAddressAccountActivity.text_cd_numero.getText().toString());
        paramLogin.put("complemento", createAddressAccountActivity.text_cd_complemento.getText().toString());
        paramLogin.put("bairro", createAddressAccountActivity.text_cd_bairro.getText().toString());
        paramLogin.put("cidade", createAddressAccountActivity.text_cd_cidade.getText().toString());
        paramLogin.put("estado", createAddressAccountActivity.spinner.getSelectedItem().toString());

        paramLogin.put("function_method", "add");

        return paramLogin;
    }

    @Override
    public void onResponse(String response) {

        if (!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);

                if (!jsonObject.isNull("result")) {
                    JSONObject user = Repository.getUser(createAddressAccountActivity);

                    if (createAddressAccountActivity != null) {

                        if(user.isNull("id_endereco")){
                            user.put("id_endereco", "0");
                            user.put("phone", null);
                            Dao.save(user.toString(), FilesName.user, createAddressAccountActivity.getApplicationContext());
                        }

                        createAddressAccountActivity.startInitialActivities();
                        createAddressAccountActivity.mProgressCreateAddress.dismiss();
                        createAddressAccountActivity.finish();

                    } else {

                        if(user.isNull("id_endereco")){

                            user.put("id_endereco", "0");
                            Dao.save(user.toString(), FilesName.user, createAddressPopUpActivity.getApplicationContext());

                        }

                        createAddressPopUpActivity.mProgressCreateAddress.dismiss();
                        createAddressPopUpActivity.loadFinish();

                    }

                } else {

                    if (createAddressAccountActivity != null) {
                        Toast.makeText(createAddressAccountActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                        createAddressAccountActivity.mProgressCreateAddress.dismiss();
                    }
                    if(createAddressPopUpActivity !=null ){
                        Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                        createAddressPopUpActivity.mProgressCreateAddress.dismiss();
                    }

                }
            } catch (JSONException e) {

                if (createAddressAccountActivity != null) {
                    Toast.makeText(createAddressAccountActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                    createAddressAccountActivity.mProgressCreateAddress.dismiss();
                }
                if(createAddressPopUpActivity !=null ){
                    Toast.makeText(createAddressPopUpActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                    createAddressPopUpActivity.mProgressCreateAddress.dismiss();
                }

            }
    }

}