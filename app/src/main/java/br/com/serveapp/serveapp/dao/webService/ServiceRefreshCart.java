package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceRefreshCart extends OnPostResponse {

    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;

    public ServiceRefreshCart(String id, ConfirmItensPedidoActiviy confirmItensPedidoActiviy) {
        setId(Repository.idEstabelecimento);
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        Toast.makeText(confirmItensPedidoActiviy.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        confirmItensPedidoActiviy.finish();
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        try {
            paramLogin.put("id_cliente", Repository.getIdUsuario(confirmItensPedidoActiviy));
            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
            paramLogin.put("id_end_cliente", Repository.getUser(confirmItensPedidoActiviy).getString("id_endereco"));
            if (Repository.atendimentoLocal) {
                paramLogin.put("tipo_atendimento", "local");
            } else {
                paramLogin.put("tipo_atendimento", "delivery");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        confirmItensPedidoActiviy.reload(jsonObject);
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null")) {
            try {
                confirmItensPedidoActiviy.reload(new JSONObject(response));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
