package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.SplashScreenEstabeleciment;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class AdapterHorizontalPromotions extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public ArrayList<String> categoriaList = new ArrayList<>();
    public JSONArray categorias;
    public JSONObject config;
    Context context;
    private ListenerClickCategories listenerClickCategories;

    public void setListenerClickCategories(ListenerClickCategories listenerClickCategories) {
        this.listenerClickCategories = listenerClickCategories;
    }

    private AdapterHorizontalPromotions.onItemClickListener listener;
    int mPosition;

    final static int TypePercent = 1;
    final static int TypeItem = 2;

    public AdapterHorizontalPromotions(Context context, JSONObject promotion) {
        try {
            this.categorias = promotion.getJSONArray("list");
            this.config = promotion.getJSONObject("config");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder lineSimpleItemHolder = null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_promotion_estab, parent, false);
        lineSimpleItemHolder = new LineSimplePromotion(v);

        return lineSimpleItemHolder;
    }

    private int getColor(String color){
        try {
            return Color.parseColor(config.getString(color));
        } catch (JSONException e) {
            return 0;
        }
    }

    private void config(LineSimplePromotion line){
        line.card.setBackgroundColor(getColor("background"));

        line.name.setTextColor(getColor("text"));
        line.line.setBackgroundColor(getColor("text"));
        line.newPriging.setTextColor(getColor("text"));
        line.oldPriging.setTextColor(getColor("text"));

        line.estab_name.setTextColor(getColor("estab_text"));
        line.estab_name.setBackgroundColor(getColor("estab_back"));

        //line.estab_name.setBackgroundColor(Color.parseColor("#E89300"));

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        try {
            JSONObject cat = categorias.getJSONObject(position);
            ((LineSimplePromotion) holder).id = cat.getString("_id");
            ((LineSimplePromotion) holder).estab_id = cat.getString("estab_id");
            ((LineSimplePromotion) holder).estab_foto = cat.getString("estab_foto");

            ((LineSimplePromotion) holder).name.setText(cat.getString("nome"));
            ((LineSimplePromotion) holder).estab_name.setText(cat.getString("estab_name"));
            ((LineSimplePromotion) holder).newPriging.setText(cat.getString("new_valor"));
            ((LineSimplePromotion) holder).oldPriging.setText(cat.getString("old_valor"));
            ImageView img = ((LineSimplePromotion) holder).imagem;
            new ListSetImage(holder.itemView.getContext(), img).execute(cat.getString("foto"));
            ((LineSimplePromotion) holder).textCupomDescont.setText(cat.getString("desconto"));
            config(((LineSimplePromotion) holder));
            if (!cat.isNull("desconto")){
                ((LineSimplePromotion) holder).textCupomDescont.setText(cat.getString("desconto"));
                holder.itemView.findViewById(R.id.textCupomDescont).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.textOffCupomDesconto).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.imageCupomDescont).setVisibility(View.VISIBLE);

                holder.itemView.findViewById(R.id.view3).setVisibility(View.VISIBLE);
                holder.itemView.findViewById(R.id.oldPriging).setVisibility(View.VISIBLE);

            }
            else{
                holder.itemView.findViewById(R.id.textCupomDescont).setVisibility(View.INVISIBLE);
                holder.itemView.findViewById(R.id.textOffCupomDesconto).setVisibility(View.INVISIBLE);
                holder.itemView.findViewById(R.id.imageCupomDescont).setVisibility(View.INVISIBLE);

                holder.itemView.findViewById(R.id.view3).setVisibility(View.INVISIBLE);
                holder.itemView.findViewById(R.id.oldPriging).setVisibility(View.INVISIBLE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return categorias.length();
    }

    class LineSimplePromotion extends RecyclerView.ViewHolder {

        public String id;
        public String estab_id;
        public String estab_foto;
        public TextView name;
        public TextView newPriging;
        public TextView oldPriging;
        public TextView estab_name;
        public TextView textCupomDescont;
        public ConstraintLayout card;
        public ImageView imagem;
        public View line;

        public LineSimplePromotion(final View itemView) {
            super(itemView);

            imagem = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            newPriging = itemView.findViewById(R.id.newPriging);
            oldPriging = itemView.findViewById(R.id.oldPriging);
            estab_name = itemView.findViewById(R.id.estab_name);
            textCupomDescont = itemView.findViewById(R.id.textCupomDescont);
            card = itemView.findViewById(R.id.card);
            line = itemView.findViewById(R.id.view3);

            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Intent intent = new Intent(itemView.getContext(), SplashScreenEstabeleciment.class);

                            Repository.idEstabelecimento = estab_id;
                            Repository.atendimentoLocal = false;


                            intent.putExtra("id_produto", id);
                            intent.putExtra("nome", estab_name.getText().toString());
                            intent.putExtra("slogan", "As melhores promoções");
                            intent.putExtra("foto", estab_foto);

                            itemView.getContext().startActivity(intent);


                        }
                    }
            );

        }
    }

    public interface onItemClickListener {
        void onItemClick(JSONObject brinde, boolean isCupom);
    }

    public void setListener(AdapterHorizontalPromotions.onItemClickListener listener) {
        this.listener = listener;
    }

    public interface ListenerClickCategories {
        public void clickCategorie(String categorie);
    }


}

