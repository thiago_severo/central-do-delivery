package br.com.serveapp.serveapp.entidades.pedido;

import java.util.Date;
import java.util.List;

import br.com.serveapp.serveapp.entidades.Cliente;
import br.com.serveapp.serveapp.entidades.Estabelecimento;
import br.com.serveapp.serveapp.enun.StatusPedido;

public class Pedido {


    private long id;
    private List<ItemPedido> itens;
    private Date data;
    private Estabelecimento estabelecimento;
    private StatusPedido status;
    private float desconto;
    private Cliente cliente;

    public List<ItemPedido> getItens() {
        return itens;
    }
    public void setItens(List<ItemPedido> itens) {
        this.itens = itens;
    }
    public Date getData() {
        return data;
    }
    public void setData(Date data) {
        this.data = data;
    }
    public Estabelecimento getEstabelecimento() {
        return estabelecimento;
    }
    public void setEstabelecimento(Estabelecimento estabelecimento) {
        this.estabelecimento = estabelecimento;
    }
    public StatusPedido getStatus() {
        return status;
    }
    public void setStatus(StatusPedido status) {
        this.status = status;
    }
    public float getDesconto() {
        return desconto;
    }
    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }
    public Cliente getCliente() {
        return cliente;
    }
    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

}
