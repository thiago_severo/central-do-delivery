package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineAllListMensagemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    Context context;
    OnItemClickListner onItemClickListner;

    public LineAllListMensagemAdapter(Context context, JSONArray listViewItem) {
        this.listViewItem = listViewItem;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (listViewItem.getJSONObject(position).getString("sentby").equals("c")) {
                return 0;
            } else {
                return 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_all_chat, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolderChat(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
                new ListSetImage(this.context, ((LineSimpleItemHolderChat) holder).foto).execute(listViewItem.getJSONObject(position).getString("foto"));
                ((LineSimpleItemHolderChat) holder).nomeFavEstabelecimento.setText(listViewItem.getJSONObject(position).getString("nome"));
                ((LineSimpleItemHolderChat) holder).texto.setText(listViewItem.getJSONObject(position).getString("texto"));
                ((LineSimpleItemHolderChat) holder).hora.setText(listViewItem.getJSONObject(position).getString("hora"));
                ((LineSimpleItemHolderChat) holder).data.setText(listViewItem.getJSONObject(position).getString("data"));
                ((LineSimpleItemHolderChat) holder).textAlert.setText(listViewItem.getJSONObject(position).getString("alert"));
                if(listViewItem.getJSONObject(position).getString("alert").equals("0")) {
                    ((LineSimpleItemHolderChat) holder).alert.setVisibility(View.GONE);
                    ((LineSimpleItemHolderChat) holder).texto.setTypeface(((LineSimpleItemHolderChat) holder).texto.getTypeface(), Typeface.NORMAL);
                }else{
                ((LineSimpleItemHolderChat) holder).alert.setVisibility(View.VISIBLE);
                ((LineSimpleItemHolderChat) holder).texto.setTypeface(((LineSimpleItemHolderChat) holder).texto.getTypeface(), Typeface.BOLD);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public interface OnItemClickListner {
        void onItemClick(JSONObject item);
    }

    public void setOnItemClickListner( OnItemClickListner onItemClickListner ){
        this.onItemClickListner = onItemClickListner;
    }

    class LineSimpleItemHolderChat extends RecyclerView.ViewHolder {

        public TextView nomeFavEstabelecimento;
        public TextView texto;
        public TextView hora;
        public TextView data;
        public TextView textAlert;
        public ImageView foto;
        public CardView cardView;
        public View alert;

        public LineSimpleItemHolderChat(View itemView) {
            super(itemView);

            foto = itemView.findViewById(R.id.imageLogoEstabeleciment);
            nomeFavEstabelecimento = itemView.findViewById(R.id.nomeFavEstabelecimento);
            texto = itemView.findViewById(R.id.texto);
            data =  itemView.findViewById(R.id.data);
            hora =  itemView.findViewById(R.id.hora);
            textAlert = itemView.findViewById(R.id.textAlert);
            cardView = itemView.findViewById(R.id.cardContentImageEst);
            alert = itemView.findViewById(R.id.imageView37);
            cardView.setRadius(15);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                               if(onItemClickListner!=null){
                                   try {
                                       onItemClickListner.onItemClick(listViewItem.getJSONObject(getAdapterPosition()));
                                   } catch (JSONException e) {
                                       e.printStackTrace();
                                   }
                               }
                }
            });

        }
    }

}