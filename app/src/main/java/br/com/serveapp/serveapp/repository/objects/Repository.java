package br.com.serveapp.serveapp.repository.objects;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import br.com.serveapp.serveapp.ChatActivity;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.ListMessagesChatActivity;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.entidades.cardapio.GroupItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;

public class Repository {


    private static JSONObject user = null;
    public static String idTipoPagamentoSelecionado = null;
    public static String tipoPagamentoSelecionado = "dinheiro";
    public static String idEstabelecimento = "0";
    public static boolean atendimentoLocal = false;
    public static ListProductsStablishmentDeliveryActivity activityVisible;
    public static DetailDemand activityDetailDemandVisible;

    public static ConfirmItensPedidoActiviy activityCupom;
    public static ChatActivity activityChatVisible;
    public static ListMessagesChatActivity listMessagesChatActivity;

    public static int qtdCart = -1;
    public static String token = null;
    public static Favorite fav = new Favorite();
    public static Set<Integer> itens;
    public static final int finalizeCreateDemand = 99;

    public static List<ItemPedido> itemPedidoList = new ArrayList<>();
    public static List<ItemPedido> itemPedidoToActivityFinalizeDemand = new ArrayList<>();
    public static List<GroupItemPedido> listOfItensPedidosToTabLocal = new ArrayList<>();
    public static List<Pedido> listOfPedidosToTabLocal = new ArrayList<>();

    public static List<GroupItemPedido> listGroupPedidosToTabLocal = new ArrayList<>();
    public static List<GroupItemPedido> listGroupPedidosToHistoricDelivery = new ArrayList<>();
    public static List<Pedido> listOfPedidosHistoric = new ArrayList<>();
    public static JSONArray listHorarios = new JSONArray();
    public static JSONArray listCards = new JSONArray();

    public static Pedido pedidoToTabLocal = null;
    public static Pedido pedidoToHistoricDelivery = new Pedido();
    public static String idCitySelected = null;

    public static String idLastDetailDemand = null;

    public static int getQtdCart() {
        return qtdCart;
    }

    public static String getTipoAtendimento() {
        return atendimentoLocal ? "local" : "delivery";
    }

    public static String urlIconCard = null;


    public static String getIdUsuario(Context context) {
        try {
            return getUser(context).getString("_id");
        } catch (JSONException|NullPointerException e) {
            return "0";
        }
    }


    public static JSONObject getUser(Context context){
        if(Repository.user == null){
            try {
                String returnUser = Dao.read(FilesName.user, context);
                Repository.user = new JSONObject(returnUser);
            } catch (JSONException e) {
            }
        }

        return Repository.user;
    }

    public static String getIdEntrega(Context context) {
        try {
            return getUser(context).getString("id_endereco");
        } catch (JSONException|NullPointerException e) {
            return "0";
        }
    }


    public static void removeAll() {
        itemPedidoList.removeAll(itemPedidoList);
    }

    public static void removeAllItensToFinalizaeDemand() {
        itemPedidoToActivityFinalizeDemand.removeAll(itemPedidoToActivityFinalizeDemand);
    }

    public static void removelAllItensPedidosToTabLocal() {
        listOfItensPedidosToTabLocal.removeAll(listOfItensPedidosToTabLocal);
    }

    public static void removeAllGroupPedidosToHistoricDelivery() {
        listGroupPedidosToHistoricDelivery.removeAll(listGroupPedidosToHistoricDelivery);
    }


    public static void setUser(JSONObject user) {
        Repository.user = user;
    }
}
