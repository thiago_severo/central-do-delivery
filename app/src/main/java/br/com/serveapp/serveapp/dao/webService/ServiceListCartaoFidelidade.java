package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.FidelidadeCardActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListCartaoFidelidade extends OnPostResponse {

    FidelidadeCardActivity fidelidadeCardActivity;

    public ServiceListCartaoFidelidade(FidelidadeCardActivity fidelidadeCardActivity) {
        this.fidelidadeCardActivity = fidelidadeCardActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_cartoes_fidelidade_do_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(fidelidadeCardActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(fidelidadeCardActivity));
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {

            try {
                JSONArray result = jsonObject.getJSONArray("result");
                fidelidadeCardActivity.initializeAdapter(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                fidelidadeCardActivity.initializeAdapter(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}