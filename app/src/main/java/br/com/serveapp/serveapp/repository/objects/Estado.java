package br.com.serveapp.serveapp.repository.objects;

import java.util.HashMap;
import java.util.Map;

public class Estado {

    private static Map<String, String> uf;
    public static String[] estados = new String[]{
            "AC",
            "AL",
            "AP",
            "AM",
            "BA",
            "CE",
            "DF",
            "ES",
            "GO",
            "MA",
            "MT",
            "MS",
            "MG",
            "PA",
            "PB",
            "PR",
            "PE",
            "PI",
            "RJ",
            "RN",
            "RS",
            "RO",
            "RR",
            "SC",
            "SP",
            "SE",
            "TO"
    };

    public static String getUfShortName(String ufLongName){
        return getUf().get(ufLongName);
    }

    public static Map<String, String> getUf(){
        if(uf==null) {
            uf = new HashMap<>();
            uf.put("Acre", "AC");
            uf.put("Alagoas", "AL");
            uf.put("Amapá", "AP");
            uf.put("Amazonas", "AM");
            uf.put("Bahia", "BA");
            uf.put("Ceará", "CE");
            uf.put("Distrito Federal", "DF");
            uf.put("Espírito Santo", "ES");
            uf.put("Goiás", "GO");
            uf.put("Maranhão", "MA");
            uf.put("Mato Grosso", "MT");
            uf.put("Mato Grosso do Sul", "MS");
            uf.put("Minas Gerais", "MG");
            uf.put("Pará", "PA");
            uf.put("Paraíba", "PB");
            uf.put("Paraná", "PR");
            uf.put("Pernambuco", "PE");
            uf.put("Piauí", "PI");
            uf.put("Rio de Janeiro", "RJ");
            uf.put("Rio Grande do Norte", "RN");
            uf.put("Rio Grande do Sul", "RS");
            uf.put("Rondônia", "RO");
            uf.put("Roraima", "RR");
            uf.put("Santa Catarina", "SC");
            uf.put("São Paulo", "SP");
            uf.put("Sergipe", "SE");
            uf.put("Tocantins", "TO");
        }
        return uf;
    }

}
