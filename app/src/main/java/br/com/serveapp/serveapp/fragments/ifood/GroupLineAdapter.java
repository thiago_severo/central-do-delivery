package br.com.serveapp.serveapp.fragments.ifood;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import br.com.serveapp.serveapp.R;

public class GroupLineAdapter extends RecyclerView.Adapter<GroupLine> {

    private JSONArray mHeades;
    LinearLayoutManager layoutManager;
    Fragment fragment;
    Activity activity;

    public GroupLineAdapter(JSONArray mHeades, LinearLayoutManager layoutManager, Fragment fragment, Activity activity ) {
        this.mHeades = mHeades;
        this.fragment = fragment;
        this.layoutManager = layoutManager;
        this.activity = activity;
    }

    public GroupLineAdapter(JSONArray mHeades, LinearLayoutManager layoutManager, Activity activity) {
        this.mHeades = mHeades;
        this.fragment = fragment;
        this.layoutManager = layoutManager;
        this.activity = activity;
    }

    @Override
    public GroupLine onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.group_products_establishment, parent, false);
        return new GroupLine(view);
    }

    @Override
    public void onBindViewHolder(GroupLine holder, final int position) {
        try {
            holder.initialize(mHeades.getJSONObject(position), fragment, activity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.setLayoutManager(layoutManager);
    }

    @Override
    public int getItemCount() {
        return mHeades != null ? mHeades.length() : 0;
    }

}
