package br.com.serveapp.serveapp.adapt;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.synnapps.carouselview.ViewListener;

import org.json.JSONArray;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class CarouselAdapter implements ViewListener {


    Context context;
    JSONArray images;
    public CarouselAdapter(Context context, JSONArray images){
        this.context = context;
        this.images = images;
    }

    public void setImages(JSONArray images) {
        this.images = images;
    }

    @Override
    public View setViewForPosition(int position) {
        try{
            View view = ((Activity)context).getLayoutInflater().inflate(R.layout.banner, null);

            ImageView customView = view.findViewById(R.id.imageEvent);
            //ImageView customView = new ImageView(context);
            new ListSetImage(context, customView).execute(images.getString(position));

            return view;
        }catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
