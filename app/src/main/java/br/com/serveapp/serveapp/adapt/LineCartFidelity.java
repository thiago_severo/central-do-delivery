package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.R;
import com.nex3z.flowlayout.FlowLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineCartFidelity extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListnerAddress listner;
    Context context;
    public LineCartFidelity(JSONArray jsonArray, Context context){
        this.context = context;
        listViewItem = jsonArray;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
           View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_cart_fidelidade, parent, false);
             lineSimpleItemHolder = new LineSimpleItemHolder(v);
            return lineSimpleItemHolder;
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            new ListSetImage(context, ((LineSimpleItemHolder)holder).iconEstFidel ).execute(listViewItem.getJSONObject(position).getString("est_foto"));
            ((LineSimpleItemHolder)holder).desc_cart_fidelidade.setText(listViewItem.getJSONObject(position).getString("descricao"));
            ((LineSimpleItemHolder)holder).nameEstab.setText(listViewItem.getJSONObject(position).getString("est_nome"));
            ((LineSimpleItemHolder)holder).textValidadeFidelidade.setText(listViewItem.getJSONObject(position).getString("validade"));
            int pontos = listViewItem.getJSONObject(position).getInt("pontos");
            int total = listViewItem.getJSONObject(position).getInt("total");
            buildLinePoints( ((LineSimpleItemHolder)holder).layoutCountPontos, pontos, total);
        } catch (JSONException e) {
            e.printStackTrace();
       }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        public ImageView img_estabelecimentos;
        public TextView desc_cart_fidelidade;
        public TextView textDescFidelity;
        public TextView   textValidadeFidelidade;
        public TextView   nameEstab;
        public TextView   textNomeValidade;
        public ImageView iconEstFidel;

        FlowLayout layoutCountPontos;
        public LineSimpleItemHolder(View itemView) {
            super(itemView);
            img_estabelecimentos = itemView.findViewById(R.id.iconEstFidel);
            desc_cart_fidelidade = itemView.findViewById(R.id.textDescFidelity);

            layoutCountPontos = itemView.findViewById(R.id.layoutCountPontos);
            textDescFidelity = itemView.findViewById(R.id.textDescFidelity);
            textValidadeFidelidade = itemView.findViewById(R.id.textValidadeFidelidade);
            iconEstFidel = itemView.findViewById(R.id.iconEstFidel);
            nameEstab  = itemView.findViewById(R.id.nameEstab);
            textValidadeFidelidade = itemView.findViewById(R.id.textValidadeFidelidade);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    try {
                        final int position = getAdapterPosition();
                        listner.onItemClick(listViewItem.getJSONObject(position));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        }



    }


    public void buildLinePoints(FlowLayout linearLayout, int pontos, int maxPontos){

        int i=0;
        while (i<pontos){
            Button btn = new Button(context);
            btn.setLayoutParams(new LinearLayout.LayoutParams(80, LinearLayout.LayoutParams.WRAP_CONTENT));
            i++;
            btn.setBackground(context.getResources().getDrawable(R.drawable.borda_arredondada_dark));
            btn.setTextColor(context.getResources().getColor(R.color.background));
            btn.setText("0"+i);
            linearLayout.addView(btn);

        }

        while (i<maxPontos){
            Button btn = new Button(context);
            btn.setLayoutParams(new LinearLayout.LayoutParams(80, LinearLayout.LayoutParams.WRAP_CONTENT));
            i++;
            btn.setBackground(context.getResources().getDrawable(R.drawable.borda_arredondada2));
            btn.setTextColor(context.getResources().getColor(R.color.background));
            btn.setText("0"+i);


            linearLayout.addView(btn);

        }


    }


    public interface OnItemClickListnerAddress{
        void onItemClick(JSONObject item);
    }

}