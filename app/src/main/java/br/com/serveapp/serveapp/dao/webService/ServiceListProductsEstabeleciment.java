package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.fragments.FragmentListProductsEstablishment;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceListProductsEstabeleciment extends OnPostResponse {

    FragmentListProductsEstablishment fragmentListProductsEstablishment;
    private SwipeRefreshLayout swipeRefreshLayout;

    public ServiceListProductsEstabeleciment(FragmentListProductsEstablishment fragmentListProductsEstablishment, SwipeRefreshLayout swipeRefreshLayout) {
        this.setId(Repository.idEstabelecimento);
        this.fragmentListProductsEstablishment = fragmentListProductsEstablishment;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_produtos_da_categoria_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        super.onErrorResponse(error);
        try{
            swipeRefreshLayout.setRefreshing(false);
            fragmentListProductsEstablishment.progressBar.setVisibility(View.GONE);
        }catch (NullPointerException e){}
        Toast.makeText(fragmentListProductsEstablishment.getContext(), "Sem internet", Toast.LENGTH_LONG).show();

    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        // paramLogin.put("id_categoria", getId());
        paramLogin.put("id_estabelecimento", getId());
        paramLogin.put("id_cliente", Repository.getIdUsuario(fragmentListProductsEstablishment.getContext()));
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {

        try {

            JSONArray result = jsonObject.getJSONArray("result");
            int isLocal = jsonObject.getInt("is_local");

            if(result == null || result.length()<=0){
                fragmentListProductsEstablishment.bodyListProductsEmpty.setVisibility(View.VISIBLE);
                fragmentListProductsEstablishment.titleListProcutsEmprty.setVisibility(View.VISIBLE);
            }

            fragmentListProductsEstablishment.progressBar.setVisibility(View.GONE);
            fragmentListProductsEstablishment.setListEmpty(jsonObject);
            fragmentListProductsEstablishment.ifood.setAdapter(result, isLocal);
            //fragmentListProductsEstablishment.initializeAdapterListProducts(result);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResponse(String response) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
    }

    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                if(result == null || result.length()<=0){
                    fragmentListProductsEstablishment.bodyListProductsEmpty.setVisibility(View.VISIBLE);
                    fragmentListProductsEstablishment.titleListProcutsEmprty.setVisibility(View.VISIBLE);
                }
                int isLocal = jsonObject.getInt("is_local");
                fragmentListProductsEstablishment.progressBar.setVisibility(View.GONE);
                fragmentListProductsEstablishment.setListEmpty(jsonObject);
                fragmentListProductsEstablishment.ifood.setAdapter(result, isLocal);
                //fragmentListProductsEstablishment.initializeAdapterListProducts(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
