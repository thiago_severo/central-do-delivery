package br.com.serveapp.serveapp.entidades.cardapio;

import br.com.serveapp.serveapp.entidades.Avaliacao;
import br.com.serveapp.serveapp.entidades.SubItem;

import java.io.Serializable;
import java.util.List;

public class ItemCardapio implements Serializable {

    private long id;
    private String logo;
    private String nome, descricao;
    private int tempoPreparo;
    private float valor;
    private float desconto;
    private Avaliacao avaliacao;
    private List<SubItem> itens;
    public int categoria;


    public List<SubItem> getItens() {
        return itens;
    }
    public void setItens(List<SubItem> itens) {
        this.itens = itens;
    }


    public String getLogo() {
        return logo;
    }
    public void setLogo(String logo) {
        this.logo = logo;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    public int getTempoPreparo() {
        return tempoPreparo;
    }
    public void setTempoPreparo(int tempoPreparo) {
        this.tempoPreparo = tempoPreparo;
    }
    public float getValor() {
        return valor;
    }
    public void setValor(float valor) {
        this.valor = valor;
    }
    public float getDesconto() {
        return desconto;
    }
    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }
    public Avaliacao getAvaliacao() {
        return avaliacao;
    }
    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

}
