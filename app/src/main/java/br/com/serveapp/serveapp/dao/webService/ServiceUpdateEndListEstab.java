package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.NavigationDrawerActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceUpdateEndListEstab extends OnPostResponse {

    NavigationDrawerActivity navigationDrawerActivity;
    public ServiceUpdateEndListEstab(NavigationDrawerActivity navigationDrawerActivity){
        setId(Repository.idEstabelecimento);
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.navigationDrawerActivity = navigationDrawerActivity;
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(navigationDrawerActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        final Map<String, String> paramLogin = new HashMap<>();
        try {
            paramLogin.put("id_cliente",Repository.getIdUsuario(navigationDrawerActivity));
            paramLogin.put("id_end_cliente", Repository.getUser(navigationDrawerActivity).getString("id_endereco"));
            paramLogin.put("function_method", "set_end");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return paramLogin;
    }


    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if(!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONObject("result").getJSONArray("list");
                JSONArray categorias = jsonObject.getJSONObject("result").getJSONArray("categorias");
                JSONObject promotions = jsonObject.getJSONObject("result").getJSONObject("destaque");

                String id = jsonObject.getJSONObject("result").getJSONObject("end").getString("_id");
                String titulo = jsonObject.getJSONObject("result").getJSONObject("end").getString("titulo");
                Repository.fav.reset(result);
                navigationDrawerActivity.initializeAdapter(result, categorias, promotions);
                navigationDrawerActivity.updateAddress(titulo, id);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
