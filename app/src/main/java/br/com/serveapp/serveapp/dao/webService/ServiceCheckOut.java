package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentReadQrCode;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCheckOut extends OnPostResponse {

    ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity;

    FragmentReadQrCode fragmentReadQrCode;
    public ServiceCheckOut(ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity) {
        this.listProductsStablishmentDeliveryActivity = listProductsStablishmentDeliveryActivity;
        this.fragmentReadQrCode = fragmentReadQrCode;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "check_in.php";
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        listProductsStablishmentDeliveryActivity.mProgress.dismiss();
    }

    @Override
    public void onResponse(String response) {

        if (!response.equals("null")) {

            try {

                JSONObject jsonObject = new JSONObject(response);

                if(!jsonObject.isNull("result")){
                    listProductsStablishmentDeliveryActivity.mProgress.dismiss();
                    listProductsStablishmentDeliveryActivity.addTabComandaDelivery();
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(listProductsStablishmentDeliveryActivity, response, Toast.LENGTH_LONG).show();
                listProductsStablishmentDeliveryActivity.mProgress.dismiss();

            }
        }


    }

}