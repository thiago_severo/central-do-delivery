package br.com.serveapp.serveapp.adapt;


import android.content.Context;
import android.graphics.Paint;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.fragments.FragmentComanda;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.dao.web.Request;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineComanda extends RecyclerView.Adapter<RecyclerView.ViewHolder>  implements View.OnCreateContextMenuListener {

    public onItemClickCancel onItemClickCancelListener;
    LayoutInflater inflater;
    public JSONArray listItensDemand;
    Context context;
    public boolean cancel = true;
    Request requestPedirOutro;
    static int header = 0;
    PedirOutroListener pedirOutroListener;

  public LineComanda(FragmentComanda fragmentComanda, Context context, JSONArray listItensDemand) {
        this.listItensDemand = listItensDemand;
        this.context = context;
     }


    @Override
    public int getItemViewType(int position) {
      if(position==0){
          return  header;
      }
      else {
          return  position;
      }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder viewHolder =null;
        if(viewType==0){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_header_comanda, parent, false);
            viewHolder = new ItemHeader(view);

        }else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_comanda, parent, false);
            viewHolder = new ItemDemand(view);
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
      try {
          JSONObject jsonObject = listItensDemand.getJSONObject(position);

          if (holder instanceof ItemHeader) {

              ((ItemHeader) holder).total.setText(jsonObject.getString("total"));
              ((ItemHeader) holder).totalPedidos.setText(jsonObject.getString("total_pedidos"));
              ((ItemHeader) holder).couvert.setText(jsonObject.getString("couvert"));
              ((ItemHeader) holder).percent.setText(jsonObject.getJSONObject("taxa_servico").getString("percent"));
              ((ItemHeader) holder).valor.setText(jsonObject.getJSONObject("taxa_servico").getString("valor"));
              ((ItemHeader) holder).textData.setText(jsonObject.getString("data"));

          } else {
              ((ItemDemand) holder).valorIndividualSemDesconto.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
              ((ItemDemand) holder).nameUser.setText(jsonObject.getString("nome"));
              ((ItemDemand) holder).valorIndividualSemDesconto.setText(jsonObject.getString("sem_desconto"));
              ((ItemDemand) holder).valorIndividual.setText(jsonObject.getString("total"));
              LineItemComanda lineItemComanda = new LineItemComanda(context, jsonObject.getJSONArray("produtos"));
              lineItemComanda.setLickOutroListener(new LineItemComanda.onClickPedirOutro() {
                  @Override
                  public void clickPedirOutro(JSONObject jsonObject) {
                      pedirOutroListener.clickPedirOutro(jsonObject);
                  }
              });
              ((ItemDemand) holder).recyclerViewItensComanda.setAdapter(lineItemComanda);
          }
          } catch(JSONException e){
              e.printStackTrace();
          }

    }

    @Override
    public int getItemCount() {
        return listItensDemand.length();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.setHeaderTitle("Select The Action");
        menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
        menu.add(0, v.getId(), 0, "SMS");

    }


    class ItemHeader extends RecyclerView.ViewHolder {

        public TextView total;
        public TextView totalPedidos;
        public TextView couvert;
        public TextView valor;
        public TextView percent;
        public TextView textData;



        public ItemHeader(View itemView) {
            super(itemView);

            textData = (TextView) itemView.findViewById(R.id.textData);
            total = (TextView) itemView.findViewById(R.id.total);
            totalPedidos = (TextView) itemView.findViewById(R.id.totalPedidos);
            couvert = (TextView) itemView.findViewById(R.id.couvert);
            valor = (TextView) itemView.findViewById(R.id.valor);
            percent = (TextView) itemView.findViewById(R.id.percent);

        }

    }

    class ItemDemand extends RecyclerView.ViewHolder {

        public TextView nameUser;
        public TextView valorIndividualSemDesconto;
        public TextView valorIndividual;
        public RecyclerView recyclerViewItensComanda;


        public ItemDemand(View itemView) {
            super(itemView);
            nameUser = (TextView) itemView.findViewById(R.id.nameUser);;
            valorIndividualSemDesconto = (TextView) itemView.findViewById(R.id.valorIndividualSemDesconto);
            valorIndividual = (TextView) itemView.findViewById(R.id.valorIndividual);
           recyclerViewItensComanda = (RecyclerView) itemView.findViewById(R.id.recyclerViewItensComanda);
           LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            recyclerViewItensComanda.setLayoutManager(linearLayoutManager);
            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add("delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            //do what u want
                            return true;
                        }
                    });
                }
            });

        }

    }
    public interface PedirOutroListener{
      public void clickPedirOutro(JSONObject jsonObject);
    }

    public void setPedirOutroListener(PedirOutroListener pedirOutroListener){
        this.pedirOutroListener = pedirOutroListener;
    }

   public interface onItemClickCancel {
        public void cancel(JSONObject jsonObject);
    }

    public void setOnItemClickCancelListener(onItemClickCancel onItemClickCancelListener){
        this.onItemClickCancelListener = onItemClickCancelListener;
    }

    }