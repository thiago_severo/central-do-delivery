package br.com.serveapp.serveapp.activity.local;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.bignerdranch.expandablerecyclerview.Model.ParentObject;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.entidades.cardapio.GroupItemPedido;
import br.com.serveapp.serveapp.entidades.cardapio.Ingrediente;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.repository.objects.Repository;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static br.com.serveapp.serveapp.repository.objects.Repository.listOfItensPedidosToTabLocal;

public class ConfirmItensDemandLocal extends AppCompatActivity {


    RecyclerView recyclerViewListPedidosConfirm;
    Button btnConfirmarPedido;
    Button btnCancelPedido;

    Pedido demand;
    List<ItemPedido> itemPedidoListOrder = new ArrayList<>();
    List<GroupItemPedido> ListGroupItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_itens_demand_local);

        Toolbar toolbarConfirmDemand = findViewById(R.id.toolbarConfimDemandLocal);
        setSupportActionBar(toolbarConfirmDemand);

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbarConfirmDemand.setNavigationIcon(R.drawable.ic_serve_back);
        toolbarConfirmDemand.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        String[] plants = new String[]{
                "Mesa 1",
                "Bolean birch",
                "Canoe birch",
                "Cherry birch",
                "European weeping birch"
        };


        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item,plants
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        Spinner spinner = findViewById(R.id.spinnerSelectDemandTable);
        spinner.setAdapter(spinnerArrayAdapter);

        recyclerViewListPedidosConfirm = findViewById(R.id.recyclerListLocalToConfirm);;
        recyclerViewListPedidosConfirm.setLayoutManager(new LinearLayoutManager(this));

        itemPedidoListOrder.addAll(Repository.itemPedidoToActivityFinalizeDemand);
        Repository.removeAllItensToFinalizaeDemand();
        demand = new Pedido();
        demand.setItens(itemPedidoListOrder);
        demand.setId(0);
        demand.setData(Calendar.getInstance().getTime());



        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_local);
        //   bottomNavigationView.setOnNavigationItemSelectedListener(listener);
        bottomNavigationView.setSelectedItemId(R.id.nav_carrinho_local);

      /*  btnConfirmarPedido = findViewById(R.id.btnConfirmarPedidoLocal);
        btnConfirmarPedido.setText("PEDIR R$ "+getTextBtnConfirm());
        btnConfirmarPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setResult(LocalMainActivity2.RESULT_GET_DEMAND_SUCESS, null);
                finish();
            }
        });

        btnCancelPedido = findViewById(R.id.btnCancelarPedidoLocal);
        btnCancelPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if(itemPedidoListOrder.size()<1){
            btnConfirmarPedido.setVisibility(View.GONE);
            btnCancelPedido.setVisibility(View.GONE);
            Snackbar snackbar = Snackbar.make(findViewById(R.id.headerConfirmDemandLocal), "Seu carrinho está vazio!", Snackbar.LENGTH_LONG);
            snackbar.show();
        }

*/

    }


    private List<ParentObject> initData() {

        List< GroupItemPedido> _titleParents =orgItensByGroup();
        List<GroupItemPedido> titles = _titleParents;

        listOfItensPedidosToTabLocal.addAll(titles);

        List<ParentObject> parentObject = new ArrayList<>();
        for(GroupItemPedido title:titles)
        {
            List<Object> childList = new ArrayList<>();
            Ingrediente ingrediente = new Ingrediente();
            ingrediente.setNome("chipotle");
            childList.add(ingrediente);
            childList.add(ingrediente);
            childList.add(ingrediente);
            title.setChildObjectList(childList);
            parentObject.add(title);
        }

        return parentObject;

    }




    List<GroupItemPedido> orgItensByGroup(){

        ListGroupItem = new ArrayList<GroupItemPedido>();
        List<ItemPedido> temp = new ArrayList<>();
        List<ItemPedido> tempToRemove = new ArrayList<>();

        temp.addAll( itemPedidoListOrder);

        while (temp.size()>0) {

            int countItemGroup = 0;
            ItemPedido itemPedido = temp.get(0);
            GroupItemPedido groupItemPedido = new GroupItemPedido();
            //temp.remove(temp.get(0).getItem());

            for (int i = 0; i < temp.size(); i++) {
                if(temp.get(i).getId()==itemPedido.getId()){
                    //    temp.remove(i);
                    tempToRemove.add(temp.get(i));
                    countItemGroup++;
                }
            }

            temp.removeAll(tempToRemove);
            tempToRemove.removeAll(tempToRemove);

            groupItemPedido.setItemPedido(itemPedido);
            groupItemPedido.setCountIntPedido(countItemGroup);
            groupItemPedido.setValorToal(itemPedido.getValor());

            ListGroupItem.add(groupItemPedido);

        }

        return ListGroupItem;
    }


    private String getTextBtnConfirm(){
        float valAdded = 0;
        for (GroupItemPedido groupItemPedido : ListGroupItem )
        {
            valAdded += groupItemPedido.getValorToal()* groupItemPedido.getCountIntPedido();
        }
        return valAdded+"";
    }

}
