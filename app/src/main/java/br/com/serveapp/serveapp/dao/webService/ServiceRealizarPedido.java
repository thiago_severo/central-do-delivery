package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.ConfirmDataDemandActivity;
import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

public class ServiceRealizarPedido extends OnPostResponse {

    ConfirmItensPedidoActiviy confirmItensPedidoActiviy;
    ConfirmDataDemandActivity confirmDataDemandActivity;
    public ServiceRealizarPedido(ConfirmItensPedidoActiviy confirmItensPedidoActiviy, ConfirmDataDemandActivity confirmDataDemandActivity){
        this.confirmItensPedidoActiviy = confirmItensPedidoActiviy;
        this.confirmDataDemandActivity = confirmDataDemandActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "realizar_pedido.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        System.out.println("ok");

        if(confirmItensPedidoActiviy!=null) {
            Toast.makeText(confirmItensPedidoActiviy, "erro de conexão", Toast.LENGTH_SHORT).show();
            confirmItensPedidoActiviy.confirmPedido.setEnabled(true);
            confirmItensPedidoActiviy.mProgress.dismiss();
        }

        if(confirmDataDemandActivity!=null){
            System.out.println("errorrrr "+ error.getMessage());
            Toast.makeText(confirmDataDemandActivity, "erro de conexãoooo "+ error.getMessage(),   Toast.LENGTH_LONG).show();
            confirmDataDemandActivity.confirmPedido.setEnabled(true);
            confirmDataDemandActivity.mProgress.dismiss();
        }

    }

    @Override
    public void onResponse(String response) {
        if(!response.equals("null"))

            System.out.println("errorrrr "+ response);

            if(confirmItensPedidoActiviy!=null) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getJSONObject("result").getJSONObject("msg").getString("cod").equals("1")) {
                        confirmItensPedidoActiviy.stabelecimentoClosed(jsonObject.getJSONObject("result").getJSONObject("msg").getString("msg"));
                        confirmItensPedidoActiviy.confirmPedido.setEnabled(true);
                    }
                    else if(jsonObject.getJSONObject("result").getJSONObject("msg").getString("cod").equals("2")){
                    //    confirmItensPedidoActiviy.receiveSeucessResponsePedido(jsonObject.getJSONObject("result").getString("id_pedido"));
                        Toast.makeText(confirmItensPedidoActiviy.getApplicationContext(), " Erro ao realizar a transação. Verifique os dados do seu cartão!" , Toast.LENGTH_SHORT).show();
                        confirmItensPedidoActiviy.confirmPedido.setEnabled(true);
                    }
                    else {
                        confirmItensPedidoActiviy.receiveSeucessResponsePedido(jsonObject.getJSONObject("result").getString("id_pedido"));
                        confirmItensPedidoActiviy.confirmPedido.setEnabled(true);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(confirmItensPedidoActiviy, "erro de conexão", Toast.LENGTH_SHORT).show();
                    confirmItensPedidoActiviy.mProgress.dismiss();
                }

            }

        if(confirmDataDemandActivity!=null) {

            try {

                System.out.println("responseeeeee: "+ response);

                JSONObject jsonObject = new JSONObject(response);

                if (jsonObject.getJSONObject("result").getJSONObject("msg").getString("cod").equals("1")) {
                    confirmDataDemandActivity.stabelecimentoClosed(jsonObject.getJSONObject("result").getJSONObject("msg").getString("msg"));
                    confirmDataDemandActivity.confirmPedido.setEnabled(true);
                } else {

                   // Toast.makeText(confirmDataDemandActivity.getApplicationContext(), "response: "+ response, Toast.LENGTH_LONG).show();

                    confirmDataDemandActivity.receiveSeucessResponsePedido(jsonObject.getJSONObject("result").getString("id_pedido"));
                    confirmDataDemandActivity.confirmPedido.setEnabled(true);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(confirmDataDemandActivity, "erro de conexão", Toast.LENGTH_SHORT).show();
                confirmDataDemandActivity.mProgress.dismiss();
            }

        }

    }

}