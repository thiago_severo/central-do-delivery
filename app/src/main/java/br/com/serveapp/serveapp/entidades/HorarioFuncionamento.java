package br.com.serveapp.serveapp.entidades;

import br.com.serveapp.serveapp.enun.DiaSemana;

public class HorarioFuncionamento {

    private long id;
    private DiaSemana dia;
    private String abertura;
    private String fechamento;

    public DiaSemana getDia() {
        return dia;
    }
    public void setDia(DiaSemana dia) {
        this.dia = dia;
    }
    public String getAbertura() {
        return abertura;
    }
    public void setAbertura(String abertura) {
        this.abertura = abertura;
    }
    public String getFechamento() {
        return fechamento;
    }
    public void setFechamento(String fechamento) {
        this.fechamento = fechamento;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
}
