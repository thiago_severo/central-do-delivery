package br.com.serveapp.serveapp.dao.webService;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.ConfirmItensPedidoActiviy;
import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceUpdateItemCart extends OnPostResponse {

    DetalheItemPopUpActivity detalheItemPopUpActivity;
    private int positionItem;

    public ServiceUpdateItemCart(DetalheItemPopUpActivity detalheItemPopUpActivity, int positionItem) {
        setId(Repository.idEstabelecimento);
        this.positionItem = positionItem;
        this.detalheItemPopUpActivity = detalheItemPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        detalheItemPopUpActivity.receiveErroResponse();
    }

    @Override
    public void onResponse(String response) {
        JSONObject obj = null;
        if (response != null && !response.equals("null")) {
            try {
                obj = new JSONObject(response);
                ConfirmItensPedidoActiviy.reference.reload(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        detalheItemPopUpActivity.receiveSucessResponseUpdateItem(obj);
    }


}