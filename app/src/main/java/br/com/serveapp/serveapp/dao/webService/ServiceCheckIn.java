package br.com.serveapp.serveapp.dao.webService;

import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentReadQrCode;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.SplashScreenEstabeleciment;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCheckIn extends OnPostResponse {

    ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity;

    FragmentReadQrCode fragmentReadQrCode;
    public ServiceCheckIn(FragmentReadQrCode fragmentReadQrCode,  ListProductsStablishmentDeliveryActivity listProductsStablishmentDeliveryActivity) {
        this.listProductsStablishmentDeliveryActivity = listProductsStablishmentDeliveryActivity;
        this.fragmentReadQrCode = fragmentReadQrCode;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "check_in.php";
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(listProductsStablishmentDeliveryActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        listProductsStablishmentDeliveryActivity.mProgress.dismiss();
    }

    @Override
    public void onResponse(String response) {

        if (!response.equals("null")) {

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");

                if( Repository.idEstabelecimento.equals(result.getString("id_estabelecimento"))){

                    listProductsStablishmentDeliveryActivity.addTabComandaLocal(null);
                    listProductsStablishmentDeliveryActivity.mProgress.dismiss();
                    if(fragmentReadQrCode.dialog!=null)
                    fragmentReadQrCode.dialog.dismiss();
                    listProductsStablishmentDeliveryActivity.floatingActionButtonCallQr.setVisibility(View.VISIBLE);
                }else{

                    Repository.idEstabelecimento = result.getString("id_estabelecimento");
                    Repository.atendimentoLocal = true;
                    Intent intentOpenServiceLocal = new Intent(listProductsStablishmentDeliveryActivity, SplashScreenEstabeleciment.class);
                    intentOpenServiceLocal.putExtra("foto", result.getString("foto"));
                    intentOpenServiceLocal.putExtra("slogan", result.getString("slogan"));
                    intentOpenServiceLocal.putExtra("nome", result.getString("nome"));
                    intentOpenServiceLocal.putExtra("tipoAtendimento", "local");

                    listProductsStablishmentDeliveryActivity.startActivity(intentOpenServiceLocal);
                    listProductsStablishmentDeliveryActivity.mProgress.dismiss();
                    listProductsStablishmentDeliveryActivity.finish();

                }

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(listProductsStablishmentDeliveryActivity, response, Toast.LENGTH_LONG).show();
                listProductsStablishmentDeliveryActivity.mProgress.dismiss();
                fragmentReadQrCode.scannerView.startCamera();
                fragmentReadQrCode.scannerView.resumeCameraPreview(fragmentReadQrCode);
            }
        }


    }

}