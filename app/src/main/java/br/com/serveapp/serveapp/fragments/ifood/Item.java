package br.com.serveapp.serveapp.fragments.ifood;

public class Item {

    private String desc;
    int categoria;

    public Item(String desc, int categoria){
        this.desc = desc;
        this.categoria = categoria;
    }

    public int getCategoria() {
        return categoria;
    }

    public String getDesc() {
        return desc;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
