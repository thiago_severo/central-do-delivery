package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import br.com.serveapp.serveapp.adapt.AdapterHorizontalListBindes;
import br.com.serveapp.serveapp.adapt.LineItemListConfirmPedidoAdapter;
import br.com.serveapp.serveapp.adapt.LinePaymentFormListAdapterView;
import br.com.serveapp.serveapp.adapt.LineRemoveCupom;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceCupom;
import br.com.serveapp.serveapp.dao.webService.ServiceListIntensCart;
import br.com.serveapp.serveapp.dao.webService.ServiceLoadDealhesPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceRealizarPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceRefreshCart;
import br.com.serveapp.serveapp.dao.webService.ServiceUpdateEndCart;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.repository.objects.Repository;

import static br.com.serveapp.serveapp.repository.objects.Repository.finalizeCreateDemand;


public class ConfirmItensPedidoActiviy extends BaseEstabelecimentActivity {

    RecyclerView recyclerViewListPedidosConfirm;
    RecyclerView recyclerViewCuponsToRemove;
    List<ItemPedido> itemPedidoListOrder = new ArrayList<>();
    private LineItemListConfirmPedidoAdapter lineItemListConfirmPedidoAdapter;
    Request requestServiceUpdateEndCart;
    Request requestListItensToCart;
    TextView textTimeDelivery;
    Request requestRelizarPedido;

    TextView textValFrete;
    TextView textEnderecoDelivery;
    CardView cardLocalEntregaPedido;
    CardView cardChangeLocalEntregaPedido;
    TextView textNameToolbarEstab;
    RecyclerView recyclerViewBrindes;
    TextView textTotalToPay;
    TextView textTempoEntrega;
    JSONObject result;

    public static CardView confirmPedido;
    TextView textBtnProsseguir;
    private Button btAddCupom;
    CardView cardConfirmDataUser;
    Button btnChangePayment;
    CardView cardPayment;
    public ProgressBar progressIncrement;
    String idEndSelected;
    String descEndSelected;
    static String valueCart;
    static String qtd;
    public static Set<Integer> itensCartToUpdate;
    public static JSONArray listItensViewUpadate;
    private static AdapterHorizontalListBindes adapterHorizontalListBindes;
    public static Double valor = 0.0;
    Request requestServiceCupom;
    TextView textBeforeDiscoutn;
    public static ConfirmItensPedidoActiviy reference;
    private Paint p = new Paint();
    boolean entrega_local;
    boolean needMoney = false;
    public ProgressDialog mProgress;
    public ProgressDialog mProgressApplyCode;
    public TextView textPagamentoSelecionado;
    public ImageView icon_pagamento;
    public TextView formaPagamento;
    public TextView textView18;
    // CardView cardTitleFormaPagamento;
    CardView cardTaxaEntrega;
    FloatingActionButton btnQrCode;
    boolean sucessDemand = false;
    int isOpen = 0;

    private String descEnd;
    private String idEndereco;
    private int isLocal;
    private JSONObject alreadyAddress = new JSONObject();

    boolean isMultipleCard;
    boolean isPaymentOnline;
    String cvv;
    String bandeira;
    String token;
    String idCardCredit;
    String nameHolder;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            finish();
        }

    };

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        try {
            if (Repository.getIdUsuario(getApplicationContext()) == null || Repository.idEstabelecimento == null) {
                finish();
            }
        } catch (Exception e) {
            finish();
        }

        setContentView(R.layout.activity_navigation_drawer_carrinho);
        Toolbar toolbarConfirmDemand = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbarConfirmDemand);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        mProgress = new ProgressDialog(this);
        String titleId = "Aguarde";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Estamos enviando o seu pedido...");
        LinePaymentFormListAdapterView.id = 0;

        mProgressApplyCode = new ProgressDialog(this);
        String titleIdApply = "Verificando...";
        mProgressApplyCode.setTitle(titleIdApply);
        mProgressApplyCode.setMessage("buscando o cupom inserido...");

        btnQrCode = findViewById(R.id.btnQrCode);
        btnQrCode.setVisibility(View.GONE);
        progressIncrement = findViewById(R.id.progressIncrement);
        progressIncrement.setVisibility(View.GONE);

        //cardTitleFormaPagamento = findViewById(R.id.cardTitleFormaPagamento);

        textView18 = findViewById(R.id.textView18);
        cardTaxaEntrega = findViewById(R.id.cardTaxaEntrega);
        icon_pagamento = findViewById(R.id.icon_pagamento);
        formaPagamento = findViewById(R.id.textView18);
        textPagamentoSelecionado = findViewById(R.id.textPagamentoSelecionado);
        confirmPedido = findViewById(R.id.confirmPedido);
        textBtnProsseguir = findViewById(R.id.textBtnProsseguir);
        textNameToolbarEstab = findViewById(R.id.textNameToolbarEstab);
        textTempoEntrega = findViewById(R.id.textTimeDelivery);
        btAddCupom = findViewById(R.id.buttonUseCupom);

        if (getIntent().hasExtra("nome")) {
            textNameToolbarEstab.setText(getIntent().getStringExtra("nome").toUpperCase());
        }
        textBeforeDiscoutn = findViewById(R.id.textBeforeDiscoutn);
        textBeforeDiscoutn.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        textBeforeDiscoutn.setVisibility(View.GONE);

        recyclerViewCuponsToRemove = findViewById(R.id.recyclerCuponsRemover);
        recyclerViewCuponsToRemove.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        btnChangePayment = findViewById(R.id.btnChangePayment);
        cardPayment = findViewById(R.id.cardPaymant);
        textTotalToPay = findViewById(R.id.textTotalToPay);
        textTimeDelivery = findViewById(R.id.textTimeDelivery);

        requestServiceCupom = new Request(getApplicationContext(), new ServiceCupom(this));
        requestServiceUpdateEndCart = new Request(getApplicationContext(), new ServiceUpdateEndCart(this));

        btnChangePayment.setVisibility(View.GONE);
        icon_pagamento.setVisibility(View.GONE);
        textPagamentoSelecionado.setVisibility(View.GONE);

            requestRelizarPedido = new Request(getApplicationContext(), new ServiceRealizarPedido(this, null));
            confirmPedido.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                boolean troco = !entrega_local && LinePaymentFormListAdapterView.id == 0;

                if (troco == true && !idEndereco.equals("0")) {
                    //showNeedMoney();

                    Intent intentConfirmDemand = new Intent(getApplicationContext(), ConfirmDataDemandActivity.class);
                    intentConfirmDemand.putExtra("isLocal", isLocal);
                    intentConfirmDemand.putExtra("valueToPay", valueCart);
                    intentConfirmDemand.putExtra("endereco", descEnd);
                    intentConfirmDemand.putExtra("formaPagamento", valueCart + " [" + textPagamentoSelecionado.getText().toString() + "]");
                    intentConfirmDemand.putExtra("idEndereco", idEndSelected);
                    intentConfirmDemand.putExtra("isLocal", isLocal);

                    intentConfirmDemand.putExtra("isPaymentOnline", isPaymentOnline);
                    intentConfirmDemand.putExtra("isMultipleCard",isMultipleCard);
                    intentConfirmDemand.putExtra("bandeira", bandeira);
                    intentConfirmDemand.putExtra("cvv", cvv);
                    intentConfirmDemand.putExtra("idCardCredit", idCardCredit);
                    intentConfirmDemand.putExtra("token", token);
                    intentConfirmDemand.putExtra("nameHolder", nameHolder);

                    //Toast.makeText(getApplicationContext(), "bandeiraaa "+ bandeira, Toast.LENGTH_LONG).show();

                    try {
                        JSONObject end = result.getJSONObject("end");
                        intentConfirmDemand.putExtra("revise_dados", end.getString("revise_dados"));
                        intentConfirmDemand.putExtra("cliente", end.getString("cliente"));
                        intentConfirmDemand.putExtra("reference", end.getString("reference"));
                    } catch (JSONException e) {
                        //Toast.makeText(ConfirmItensPedidoActiviy.this, "ERRO", Toast.LENGTH_LONG).show();
                    }

                    startActivityForResult(intentConfirmDemand, 11);
                        overridePendingTransition(R.anim.entrer_from_right,
                                R.anim.exit_to_left);

                } else {

                    Map<String, String> paramUpdate = new HashMap<>();
                    paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);
                    paramUpdate.put("forma_pagamento", LinePaymentFormListAdapterView.idSaved + "");
                    paramUpdate.put("valor_total", valueCart);

                    if (idEndereco.equals("0") && isLocal == 1) {

                        requestRelizarPedido.request(paramUpdate);
                        mProgress.show();
                        confirmPedido.setEnabled(false);

                    } else {

                        //Toast.makeText(getApplicationContext(), "cvvvvv "+ cvv, Toast.LENGTH_LONG).show();
                        // confirmDemand(paramUpdate);
                        Intent intentConfirmDemand = new Intent(getApplicationContext(), ConfirmDataDemandActivity.class);
                        intentConfirmDemand.putExtra("isLocal", isLocal);
                        intentConfirmDemand.putExtra("valueToPay", valueCart);
                        intentConfirmDemand.putExtra("endereco", descEnd);
                        intentConfirmDemand.putExtra("formaPagamento", valueCart + " [" + textPagamentoSelecionado.getText().toString() + "]");
                        intentConfirmDemand.putExtra("idEndereco", idEndSelected);
                        intentConfirmDemand.putExtra("isLocal", isLocal);

                        intentConfirmDemand.putExtra("isPaymentOnline", isPaymentOnline);
                        intentConfirmDemand.putExtra("bandeira", bandeira);
                        intentConfirmDemand.putExtra("cvv", cvv);
                        intentConfirmDemand.putExtra("token", token);
                        intentConfirmDemand.putExtra("isMultipleCard",isMultipleCard);
                        intentConfirmDemand.putExtra("idCardCredit", idCardCredit);
                        intentConfirmDemand.putExtra("valorCompra", valor);
                        intentConfirmDemand.putExtra("nameHolder", nameHolder);

                        try {
                            JSONObject end = result.getJSONObject("end");
                            intentConfirmDemand.putExtra("revise_dados", end.getString("revise_dados"));
                            intentConfirmDemand.putExtra("cliente", end.getString("cliente"));
                            intentConfirmDemand.putExtra("reference", end.getString("reference"));
                        } catch (JSONException e) {
                            //Toast.makeText(ConfirmItensPedidoActiviy.this, "ERRO", Toast.LENGTH_LONG).show();
                        }
                        startActivityForResult(intentConfirmDemand, 11);
                        overridePendingTransition(R.anim.entrer_from_right,
                                R.anim.exit_to_left);

                    }

                }
            }
        });

        toolbarConfirmDemand.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent updateCartValue = new Intent();
                updateCartValue.putExtra("cartValue", valueCart);
                updateCartValue.putExtra("qtd", qtd);
                updateCartValue.putExtra("valor", valor);
                if (Repository.atendimentoLocal == true) {
                    updateCartValue.putExtra("isLocal", 1);
                }
                setResult(ListProductsStablishmentDeliveryActivity.RESULT_UPDATE_CAR_VALUE, updateCartValue);
                onBackPressed();
            }
        });

        textEnderecoDelivery = findViewById(R.id.textShowAddressDelivery);
        cardChangeLocalEntregaPedido = findViewById(R.id.cardChangeLocalEntregaPedido);
        // cardLocalEntregaPedido = findViewById(R.id.cardLocalEntregaPedido);
        cardChangeLocalEntregaPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent openAdddress = new Intent(getApplicationContext(), ChangeAddressPopUpActivity.class);
                openAdddress.putExtra("id_endereco", idEndSelected);
                openAdddress.putExtra("text_endereco", descEndSelected);
                openAdddress.putExtra("updateCart", true);
                openAdddress.putExtra("openByCart", "ok");
                startActivityForResult(openAdddress, 0);
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);

            }
        });

        textValFrete = findViewById(R.id.textTaxaEntregaPedido);
        recyclerViewListPedidosConfirm = findViewById(R.id.recyclerListToConfirm);
        recyclerViewListPedidosConfirm.setLayoutManager(new LinearLayoutManager(this));

        recyclerViewBrindes = findViewById(R.id.recyclerViewBrindes);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(RecyclerView.HORIZONTAL);
        recyclerViewBrindes.setLayoutManager(llm);

        MyNavigationNew.setButtonsAction(this);

        cardPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnChangePayment.getVisibility() == View.VISIBLE) {
                    Intent intent = new Intent(getApplicationContext(), FormaPagamentoActivity.class);
                    startActivityForResult(intent, 66);
                }
            }
        });

        btnChangePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), FormaPagamentoActivity.class);
                startActivityForResult(intent, 66);

            }
        });

        btAddCupom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aplicarCodigo();
            }
        });

        View bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setVisibility(View.GONE);

        if (Repository.idEstabelecimento != null) {

            if (Repository.idEstabelecimento.equals("0")) {

                btnQrCode.setVisibility(View.GONE);
                bottomNavigationView.setVisibility(View.VISIBLE);

                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                DrawerLayout drawer = findViewById(R.id.drawer_layout);
                NavigationView navigationView = findViewById(R.id.nav_view);
                ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                        this, drawer, toolbarConfirmDemand, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
                drawer.addDrawerListener(toggle);
                toggle.syncState();
                navigationView.setNavigationItemSelectedListener(new MyNavigationMenu(this, navigationView));

            } else {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    getSupportActionBar().setDisplayShowHomeEnabled(true);
                }
            }

            requestListItensToCart = new Request(getApplicationContext(), new ServiceRefreshCart(Repository.getIdUsuario(getApplicationContext()), this));
            requestListItensToCart.request();

        } else {
            this.finish();
        }

        if (getIntent().hasExtra("requestCode")) {
            Repository.removeAllItensToFinalizaeDemand();
        }

        itemPedidoListOrder.addAll(Repository.itemPedidoToActivityFinalizeDemand);

    }

    @Override
    public void onResume() {
        super.onResume();
        reference = this;
        Repository.activityCupom = this;

    }

    @Override
    protected void onPause() {
        super.onPause();
        Repository.activityCupom = null;
    }

    public void reload(JSONObject jsonObject) {

        try {

            result = jsonObject.getJSONObject("result");

            if (result.getJSONObject("msg").getInt("cod") != 0) {
                Toast.makeText(getApplicationContext(), result.getJSONObject("msg").getString("text"), Toast.LENGTH_LONG).show();
            }

            JSONArray itens = result.getJSONArray("itens");
            JSONArray cupons = result.getJSONArray("cupons_aplicados");
            entrega_local = result.getInt("entrega_local") == 1;
            isOpen = result.getInt("open");


            if(isOpen==1) {
                confirmPedido.setEnabled(true);
                textBtnProsseguir.setText("Prosseguir");
            }else{
                confirmPedido.setEnabled(false);
                textBtnProsseguir.setText("Estabelecimento Fechado");
            }

            updateTempo(result.getJSONObject("cart").getString("tempo"));
            initializeAdapterListItensCart(itens, result.getString("est_name"), result.getString("id_estabelecimento"));
            updateValTotal(result.getJSONObject("cart").getJSONObject("total").getString("label"));
            updateAddress(result.getJSONObject("end").getString("titulo"), result.getJSONObject("end").getString("id"));

            descEnd = result.getJSONObject("end").getString("desc");
            if(descEnd==null || descEnd.equals("null")){
            descEnd = result.getJSONObject("end").getString("titulo");
            }

            isLocal = result.getInt("is_local");
            int res = result.getInt("is_local");
            updateFrete(result.getString("frete"), res, result.getJSONObject("end").getString("id"));
            valor = result.getJSONObject("cart").getJSONObject("total").getDouble("valor");
            JSONObject bonus = result.getJSONObject("cart").getJSONObject("bonus");
            initializeBrindesAdapter(bonus.getJSONArray("brindes"), bonus.getJSONArray("cuponns"));
            updateValueBoforeDiscount(jsonObject.getJSONObject("result").getJSONObject("cart").getJSONObject("sem_desconto").getString("label"));
            updateCartValue(jsonObject.getJSONObject("result").getJSONObject("cart").getJSONObject("total").getString("label"), jsonObject.getJSONObject("result").getJSONObject("cart").getString("qtd"));
            initCuponsAplicados(cupons);
            initializePayment(result);

            mProgressApplyCode.dismiss();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void reloadWithoutPayment() {

    }

    public void initCuponsAplicados(JSONArray cuponsAplicados) {

        LineRemoveCupom lineRemoveCupom = null;
        lineRemoveCupom = new LineRemoveCupom(cuponsAplicados);
        recyclerViewCuponsToRemove.setAdapter(lineRemoveCupom);
        lineRemoveCupom.setOnItemClickListner(new LineRemoveCupom.OnItemClickListner() {
            @Override
            public void onItemClick(JSONObject item) {
                try {
                    progressIncrement.setVisibility(View.VISIBLE);
                    Map<String, String> param = new HashMap<>();
                    param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    param.put("id_estabelecimento", Repository.idEstabelecimento);
                    param.put("id_cupom", item.getString("id"));
                    param.put("function_method", "remove_cupom");
                    requestServiceCupom.request(param);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        textView18.setVisibility(View.VISIBLE);
        isPaymentOnline = false;

        if (resultCode == 11) {

            cvv = data.getStringExtra("cvv");
            bandeira = data.getStringExtra("bandeira");

            token = data.getStringExtra("token");
            nameHolder = data.getStringExtra("nameHolder");
            idCardCredit = data.getStringExtra("idCardCredit");

            textPagamentoSelecionado.setText("Pagamento pelo app com: " + " " + bandeira.toUpperCase() + " **** " + data.getStringExtra("card"));
            textView18.setVisibility(View.GONE);

            new ListSetImage(getApplicationContext(), icon_pagamento).execute("card/" + data.getStringExtra("bandeira").replace(" ", "") + ".png");
            LinePaymentFormListAdapterView.iconSelected = "card/" + data.getStringExtra("bandeira").replace(" ", "") + ".png";

            isPaymentOnline = true;

        }

        if (requestCode == 11) {

            if (resultCode == finalizeCreateDemand) {
                finish();
                setResult(RESULT_FIRST_USER);
            } else {
                progressIncrement.setVisibility(View.VISIBLE);
                descEndSelected = data.getStringExtra("titulo");
                idEndSelected = data.getStringExtra("_id");
                if (data.hasExtra("cep")) {
                    textEnderecoDelivery.setText(descEndSelected + " - " + data.getStringExtra("cep"));
                } else {
                    textEnderecoDelivery.setText(descEndSelected);
                }

                Map<String, String> paramUpdate = new HashMap<>();
                paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                paramUpdate.put("id_endereco", idEndSelected);
                paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);
                paramUpdate.put("function_method", "set_end");

                requestServiceUpdateEndCart.request(paramUpdate);
            }

            /*  requestListItensToCart = new Request(getApplicationContext(), new ServiceRefreshCart(Repository.getIdUsuario(getApplicationContext()), this));
            requestListItensToCart.request();*/

        } else if (requestCode == 88 && resultCode == Activity.RESULT_OK) {

            int position = data.getIntExtra("position", 0);
            progressIncrement.setVisibility(View.VISIBLE);

            try {

                updateValTotal(data.getStringExtra("newValueCart"));
                lineItemListConfirmPedidoAdapter.listItensCarrinho.getJSONObject(position).put("detalhe", data.getStringExtra("detalhe"));
                lineItemListConfirmPedidoAdapter.listItensCarrinho.getJSONObject(position).put("qtd", data.getIntExtra("qtdProduto", 0));
                lineItemListConfirmPedidoAdapter.listItensCarrinho.getJSONObject(position).put("itens", listItensViewUpadate);
                lineItemListConfirmPedidoAdapter.listItensCarrinho.getJSONObject(position).put("selected", new JSONArray(itensCartToUpdate));
                lineItemListConfirmPedidoAdapter.listItensCarrinho.getJSONObject(position).put("valor", data.getStringExtra("newValueItem"));
                lineItemListConfirmPedidoAdapter.notifyDataSetChanged();
                listItensViewUpadate = new JSONArray();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (resultCode == Activity.RESULT_OK) {

            try {

                progressIncrement.setVisibility(View.VISIBLE);
                JSONObject jsonObject = new JSONObject(data.getStringExtra("endereco"));
                alreadyAddress = jsonObject;
                descEndSelected = jsonObject.getString("titulo");
                idEndSelected = jsonObject.getString("_id");
                textEnderecoDelivery.setText(descEndSelected + " - " + jsonObject.getString("cep"));

                Map<String, String> paramUpdate = new HashMap<>();
                paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                paramUpdate.put("id_endereco", idEndSelected);
                paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);
                paramUpdate.put("function_method", "set_end");

                requestServiceUpdateEndCart.request(paramUpdate);
                requestListItensToCart = new Request(getApplicationContext(), new ServiceRefreshCart(Repository.getIdUsuario(getApplicationContext()), this));
                requestListItensToCart.request();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } else if (resultCode == 66) {

            if (Repository.tipoPagamentoSelecionado != null && !Repository.tipoPagamentoSelecionado.equals("dinheiro")) {
                textPagamentoSelecionado.setText(Repository.tipoPagamentoSelecionado);
                new ListSetImage(getApplicationContext(), icon_pagamento).execute(data.getStringExtra("icon"));
                LinePaymentFormListAdapterView.iconSelected = data.getStringExtra("icon");
            } else {
                LinePaymentFormListAdapterView.iconSelected = null;
                textPagamentoSelecionado.setText("dinheiro");
                icon_pagamento = findViewById(R.id.icon_pagamento);
                icon_pagamento.setImageBitmap(null);
                new ListSetImage(getApplicationContext(), icon_pagamento).execute("card/money_icon.png");
                icon_pagamento.setImageResource(R.drawable.ic_serve_money);
            }

        }


    }

    public void update() {
        Map<String, String> param = new HashMap<>();
        param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
        param.put("id_estabelecimento", Repository.idEstabelecimento);
        try {
            requestServiceCupom.request(param);
        } catch (NullPointerException e) {
            requestServiceCupom = new Request(getApplicationContext(), new ServiceCupom(this));
            requestServiceCupom.request(param);
        }

    }

    public void initializePayment(JSONObject result) {
        try {

            LinePaymentFormListAdapterView.idSaved = result.getJSONObject("payment").getInt("id");
            LinePaymentFormListAdapterView.id = result.getJSONObject("payment").getInt("id");

            Repository.tipoPagamentoSelecionado = result.getJSONObject("payment").getString("name");
            textPagamentoSelecionado.setText(result.getJSONObject("payment").getString("name"));

            int centralPay = result.getJSONObject("payment").getInt("central_pay");

            if(centralPay == 1){

                isPaymentOnline = true;
                FormaPagamentoActivity.is_payment_online = true;
                FormaPagamentoActivity.select_online = true;

                token = result.getJSONObject("payment").getString("token");
                cvv = result.getJSONObject("payment").getString("cvv");
                bandeira = result.getJSONObject("payment").getString("bandeira");
                nameHolder = result.getJSONObject("payment").getString("cliente");

            }

            if (!result.getJSONObject("payment").isNull("icon")) {
                LinePaymentFormListAdapterView.iconSelected = result.getJSONObject("payment").getString("icon");
                new ListSetImage(getApplicationContext(), icon_pagamento).execute(result.getJSONObject("payment").getString("icon"));
            } else {

                icon_pagamento.setImageBitmap(null);
                new ListSetImage(getApplicationContext(), icon_pagamento).execute("card/money_icon.png");
                icon_pagamento.setImageResource(R.drawable.ic_serve_money);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void initializeAdapterListItensCart(JSONArray result, String est_name, String id_estabelecimento) {

        Repository.idEstabelecimento = id_estabelecimento;
        textNameToolbarEstab.setText(est_name.toUpperCase());
        lineItemListConfirmPedidoAdapter = new LineItemListConfirmPedidoAdapter(this, this, result, textTotalToPay, progressIncrement);
        lineItemListConfirmPedidoAdapter.setOnClickListener(new LineItemListConfirmPedidoAdapter.OnItemClickListener() {
            @Override
            public void onItemClickListener(JSONObject itemCart, int position) {
                try {

                    Repository.itens = new HashSet<>();
                    JSONArray itemSelected = itemCart.getJSONArray("selected");
                    for (int i = 0; i < itemSelected.length(); ) {
                        Repository.itens.add(itemSelected.getInt(i));
                        i++;
                    }

                    Intent intent = new Intent(getApplicationContext(), DetalheItemPopUpActivity.class);
                    intent.putExtra("op", "update");

                    try {
                        intent.putExtra("foto", itemCart.getString("foto"));
                        intent.putExtra("value", itemCart.getDouble("value") / itemCart.getInt("qtd"));
                        intent.putExtra("valor", itemCart.getString("valor"));
                        intent.putExtra("valorBase", itemCart.getDouble("valorBase"));
                        intent.putExtra("nome", itemCart.getString("nome"));
                        intent.putExtra("descricao", itemCart.getString("descricao"));
                        intent.putExtra("id_produto", itemCart.getString("id_produto"));
                        intent.putExtra("id_produto_selecionado", itemCart.getString("_id"));
                        intent.putExtra("qtd", itemCart.getInt("qtd"));
                        intent.putExtra("position", position);
                        intent.putExtra("detalhe", itemCart.getString("detalhe"));
                        intent.putExtra("avaliacao", itemCart.getString("avaliacao"));
                        intent.putExtra("tempoPreparo", itemCart.getString("tempoPreparo"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //startActivityForResult(intent, 88);
                    new DetalheItemPopUpActivity(ConfirmItensPedidoActiviy.this, intent).show();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        recyclerViewListPedidosConfirm.setAdapter(lineItemListConfirmPedidoAdapter);
        enableSwipe();

    }

    public void updateValTotal(String valTotal) {
        valueCart = valTotal;
        textTotalToPay.setText(valTotal);
    }

    public void updateTempo(String tempo) {
        if (tempo == null) {
            textTempoEntrega.setVisibility(View.GONE);
        } else {
            textTempoEntrega.setText(tempo);
            textTempoEntrega.setVisibility(View.VISIBLE);
        }

    }

    public static void updateCartValue(String valTotal, String quantidade) {
        valueCart = valTotal;
        qtd = quantidade;
        if (Integer.parseInt(quantidade) < 1) {
            confirmPedido.setVisibility(View.GONE);
        }

        Repository.qtdCart = Integer.parseInt(qtd);

    }

    private void setVisibleFrete(int v) {
        cardTaxaEntrega.setVisibility(v);
    }

    private void setVisibleFormaPagamento(int v) {
        btnChangePayment.setVisibility(v);
        icon_pagamento.setVisibility(v);
        formaPagamento.setVisibility(v);
        textPagamentoSelecionado.setVisibility(v);
    }

    public void updateFrete(String valFrete, int isLocal, String id_endereco) {
        Repository.atendimentoLocal = (isLocal == 1);
        idEndereco = id_endereco;
        if (!id_endereco.equals("0")) {
            setVisibleFormaPagamento(View.VISIBLE);
            setVisibleFrete(View.VISIBLE);
        } else if (isLocal == 0) {
            setVisibleFormaPagamento(View.VISIBLE);
            setVisibleFrete(View.GONE);
        } else {
            setVisibleFormaPagamento(View.GONE);
            setVisibleFrete(View.GONE);
        }
        textValFrete.setText(valFrete);
    }

    public void updateModoCallDelivery(int modo) {
        if (modo == 1) {
            btnQrCode.setImageResource(R.drawable.ic_serve_finger);
        } else {
            btnQrCode.setImageResource(R.drawable.qr_code_icon);
        }
    }

    public void setVisibleDelivery() {
        Repository.atendimentoLocal = false;
        btnChangePayment.setVisibility(View.VISIBLE);
        // cardTitleFormaPagamento.setVisibility(View.VISIBLE);
        icon_pagamento.setVisibility(View.VISIBLE);
        textPagamentoSelecionado.setVisibility(View.VISIBLE);
    }

    public void updateAddress(String textAddress, String idSelected) {
        this.idEndSelected = idSelected;
        this.descEndSelected = textAddress;
        textEnderecoDelivery.setText(descEndSelected);
    }

    public void initializeBrindesAdapter(JSONArray brindes, JSONArray cupons) {
        adapterHorizontalListBindes = new AdapterHorizontalListBindes(getApplicationContext(), brindes, cupons);
        adapterHorizontalListBindes.setListener(new AdapterHorizontalListBindes.onItemClickListener() {
            @Override
            public void onItemClick(JSONObject brinde, boolean isCupom) {
                Map<String, String> paramUpdate = new HashMap<>();
                try {
                    if (isCupom) {
                        paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                        paramUpdate.put("id_estabelecimento", Repository.idEstabelecimento);
                        paramUpdate.put("id_cupom", brinde.getString("_id"));
                        paramUpdate.put("function_method", "add_cupom");
                        requestServiceCupom.request(paramUpdate);
                        progressIncrement.setVisibility(View.VISIBLE);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), DetalheItemPopUpActivity.class);
                        try {

                            intent.putExtra("qtdMaxBrindes", brinde.getInt("qtd"));
                            intent.putExtra("avaliacao", brinde.getString("avaliacao"));
                            intent.putExtra("foto", brinde.getString("foto"));
                            intent.putExtra("value", 0.00);
                            intent.putExtra("valor", "R$ 0,00");
                            intent.putExtra("nome", brinde.getString("nome"));
                            intent.putExtra("descricao", "descricao");
                            intent.putExtra("id_produto", brinde.getString("id_produto"));
                            intent.putExtra("is_brinde", true);
                            intent.putExtra("id_prog_fidelidade", brinde.getString("id_pf"));
                            intent.putExtra("tempoPreparo", brinde.getString("tempoPreparo"));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //startActivityForResult(intent, 77);
                        new DetalheItemPopUpActivity(ConfirmItensPedidoActiviy.this, intent).show();


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        recyclerViewBrindes.setAdapter(adapterHorizontalListBindes);
    }

    public static void updateBrindesAdapter(JSONArray brindes, JSONArray cupons, Double valor) {
        adapterHorizontalListBindes.setList(brindes, cupons);
        adapterHorizontalListBindes.notifyDataSetChanged();
    }

    public void updateValueBoforeDiscount(String label) {
        textBeforeDiscoutn.setVisibility(View.VISIBLE);
        textBeforeDiscoutn.setText(label);
    }

    private void enableSwipe() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.LEFT) {
                    try {
                        progressIncrement.setVisibility(View.VISIBLE);
                        final JSONObject deletedModel = lineItemListConfirmPedidoAdapter.listItensCarrinho.getJSONObject(position);
                        lineItemListConfirmPedidoAdapter.deleteItem(deletedModel.getString("_id"), position);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    //      removeItem(position);
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                Bitmap icon;
                if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                    View itemView = viewHolder.itemView;
                    float height = (float) itemView.getBottom() - (float) itemView.getTop();
                    float width = height / 3;

                    if (dX > 0) {
                        p.setColor(getResources().getColor(R.color.background));
                        RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.remove);
                        RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    } else {
                        p.setColor(getResources().getColor(R.color.background));
                        RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                        c.drawRect(background, p);
                        icon = BitmapFactory.decodeResource(getResources(), R.drawable.remove);
                        RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                        c.drawBitmap(icon, null, icon_dest, p);
                    }
                }
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerViewListPedidosConfirm);
    }


    public void receiveSeucessResponsePedido(String idPedido) {

        DetailDemand.idPedido = idPedido;

        Request requestDetailPedido = new Request(getApplicationContext(), new ServiceLoadDealhesPedido(this, idPedido));
        requestDetailPedido.request();

    }

    public void stabelecimentoClosed(String textMain) {

        mProgress.dismiss();
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ConfirmItensPedidoActiviy.this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView text = mView.findViewById(R.id.textSucessMessage);
        TextView textViewMain = mView.findViewById(R.id.textMainMsg);

        text.setVisibility(View.GONE);
        textViewMain.setText(textMain);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        confirmPedido.setEnabled(true);
    }

    public void refreshCart() {
        requestListItensToCart = new Request(getApplicationContext(), new ServiceListIntensCart(Repository.getIdUsuario(getApplicationContext()), this));
        requestListItensToCart.request();
    }

    public void aplicarCodigo() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);

        View mView = getLayoutInflater().inflate(R.layout.aplicar_cupom, null);
        final EditText editTextCodePromo = mView.findViewById(R.id.editTextCodePromo);

        Button btnAplicar = mView.findViewById(R.id.btnAplicar);
        ImageButton btnExit = mView.findViewById(R.id.imageButtonCloseEvent);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        mProgress.dismiss();
        dialog.show();

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnAplicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String, String> param = new HashMap<>();

                param.put("cod_promocional", editTextCodePromo.getText().toString());
                param.put("function_method", "cod_promocional");
                param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                param.put("id_estabelecimento", Repository.idEstabelecimento);

                dialog.dismiss();
                mProgressApplyCode.show();
                requestListItensToCart.request(param);

            }
        });

    }

    public void confirmDemand(final Map<String, String> param) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);

        View mView = getLayoutInflater().inflate(R.layout.confirm_demand, null);
        final TextView textEnderecoEntrega = mView.findViewById(R.id.textEndereco);
        final TextView paymentFormConfirm = mView.findViewById(R.id.paymentFormConfirm);

        textEnderecoEntrega.setText(descEnd);
        paymentFormConfirm.setText(valueCart + " [" + textPagamentoSelecionado.getText().toString() + "]");

        Button btnCancelMoney = mView.findViewById(R.id.btnRecuseCancel);
        Button btnFinishMoney = mView.findViewById(R.id.btnConfirmCancel);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        mProgress.dismiss();
        dialog.show();

        btnCancelMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnFinishMoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestRelizarPedido.request(param);
                mProgress.show();
                confirmPedido.setEnabled(false);
            }

        });

    }

    @Override
    public void onBackPressed() {

        Intent updateCartValue = new Intent();
        updateCartValue.putExtra("cartValue", valueCart);
        updateCartValue.putExtra("qtd", qtd);
        updateCartValue.putExtra("valor", valor);
        if (Repository.atendimentoLocal == true) {
            updateCartValue.putExtra("isLocal", 1);
        }

        setResult(ListProductsStablishmentDeliveryActivity.RESULT_UPDATE_CAR_VALUE, updateCartValue);

        super.onBackPressed();
        overridePendingTransition(R.anim.entrer_from_leftt,
                R.anim.exit_to_right);
    }

    public void initializeTimeDemand(String string) {
        textTimeDelivery.setText("(" + string + ")");
    }

}