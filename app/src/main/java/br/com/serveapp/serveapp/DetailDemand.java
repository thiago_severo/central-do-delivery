package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;

import br.com.serveapp.serveapp.adapt.LineItemDemandEstabelishment;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAvaliarItemPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceAvaliarPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceCancelDetailPedido;
import br.com.serveapp.serveapp.dao.webService.ServiceConfirmRecebimento;
import br.com.serveapp.serveapp.dao.webService.ServiceDealhesPedido;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.fragments.AvaliacaoInterface;
import br.com.serveapp.serveapp.fragments.AvaliacaoItemInterface;
import br.com.serveapp.serveapp.fragments.FragmentListDemandsEstablishment;
import br.com.serveapp.serveapp.repository.objects.Repository;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.transferwise.sequencelayout.SequenceLayout;
import com.transferwise.sequencelayout.SequenceStep;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DetailDemand extends AppCompatActivity implements AvaliacaoInterface, AvaliacaoItemInterface {

    long kay;

    public boolean noRefresh;

    private LinearLayout linearStateContent;

    Request requestDetailPedido;
    TextView ped_id;
    TextView processamento_hora;
    TextView codigo;
    TextView processamento_data;
    public ProgressBar progressBar;

    TextView textCancel;
    ConstraintLayout constCancel;
    TextView previsao_entrega;

    TextView estab_telefone;
    TextView frete;
    TextView desc_entrega;
    CardView cardValDelivery;
    Button buttonAvaliarPed;
    RecyclerView produtos;
    Request requestCancelDemand;
    Request requesConfirmRecebimento;
    ProgressBar progressBarSendFeedBack;

    ProgressBar progressDemand;
    int pStatus;
    public long time;
    public Date dateResponse;
    Thread thread;

    ImageView imageShowMsgs;
    ImageView imageViewCallPhone;
    Button buttonCancelPedido;
    TextView produtos_value;
    TextView textViewEntrega;
    TextView desconto_value;
    TextView total_value;
    TextView labelFormaPagamento;
    TextView formaPag_name;
    TextView textViewTotal;
    TextView textTaxaEntrega;
    TextView textDiscountPedido;
    ImageView imageView;
    TextView textValueEntrega;
    Request requestAvaliarItem;
    Request requestAvaliarPedido;
    public AlertDialog dialog = null;
    ProgressBar progressBarItemPedido;
    public static JSONObject detailObject;

    public static String idPedido;
    Button buttonConfirmRecebimento;
    public boolean confirmRecebimento = false;
    SequenceStep step1, step2, step3, step4, step5;
    public SequenceLayout sequenceLayout;

    public ProgressDialog mProgressCancel;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            doRestart();
        }

    };

    boolean isConfirmRecebimento;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().hasExtra("id_pedido")) {
            idPedido = getIntent().getStringExtra("id_pedido");
        }

        if(idPedido==null){
            finish();
        }

        Repository.idLastDetailDemand = idPedido;

        mProgressCancel = new ProgressDialog(this);
        String titleId = "Aguarde";
        mProgressCancel.setTitle(titleId);
        mProgressCancel.setMessage("Estamos cancelando o seu pedido...");

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);
        setContentView(R.layout.activity_detail_demand);
        Toolbar toolbar = findViewById(R.id.toolbar1);
        progressBar = findViewById(R.id.progressBar7);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        cardValDelivery = findViewById(R.id.cardValDelivery);
        imageShowMsgs = findViewById(R.id.imageShowMsgs);
        buttonConfirmRecebimento = findViewById(R.id.buttonConfirmRecebimento);
        buttonAvaliarPed = findViewById(R.id.buttonAvaliarPed);

        textTaxaEntrega = findViewById(R.id.textTaxaEntrega);

        linearStateContent = findViewById(R.id.linearStateContent);

        progressDemand = findViewById(R.id.progressDemand2);

        buttonCancelPedido = findViewById(R.id.buttonCancelPedido);
        imageView = findViewById(R.id.imageViewFormPag);
        produtos = findViewById(R.id.produtos);
        ped_id = findViewById(R.id.nome_estabelecimento_chat);
        processamento_hora = findViewById(R.id.processamento_hora);
        codigo = findViewById(R.id.codigo);
        processamento_data = findViewById(R.id.processamento_data);
        previsao_entrega = findViewById(R.id.previsao_entrega);
        constCancel = findViewById(R.id.constCancel);

        constCancel.setVisibility(View.GONE);
        textCancel = findViewById(R.id.textCancel);

        estab_telefone = findViewById(R.id.estab_telefone);
        labelFormaPagamento = findViewById(R.id.labelFormaPagamento);

        frete = findViewById(R.id.end_frete);
        desc_entrega = findViewById(R.id.desc_entrega);
        textDiscountPedido = findViewById(R.id.textDiscountPedido);

        requesConfirmRecebimento = new Request(getApplicationContext(), new ServiceConfirmRecebimento(this));
        requestCancelDemand = new Request(getApplicationContext(), new ServiceCancelDetailPedido(this));
        requestAvaliarPedido = new Request(getApplicationContext(), new ServiceAvaliarPedido(this, this));
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        produtos.setLayoutManager(llm);
        produtos_value = findViewById(R.id.produtos_value);
        textViewEntrega = findViewById(R.id.textValueEntrega);
        desconto_value = findViewById(R.id.desconto_value);
        total_value = findViewById(R.id.total_value);
        formaPag_name = findViewById(R.id.formaPag_name);
        textViewTotal = findViewById(R.id.textViewTotal);
        imageViewCallPhone = findViewById(R.id.imageViewCallPhone);

        imageViewCallPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", estab_telefone.getText().toString(), null));
                startActivity(intent);
            }
        });

        buttonConfirmRecebimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> param = new HashMap<>();
                param.put("id_pedido", idPedido);
                requesConfirmRecebimento.request(param);
            }
        });

        imageShowMsgs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ChatActivity.idPedido = idPedido;
                startActivity(new Intent(getApplicationContext(), ChatActivity.class));
            }
        });

        buttonAvaliarPed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //requestAvaliarPedido.request();
                createAvaliationDemand(false);
            }
        });

        buttonCancelPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogCancel();
            }
        });

            if(idPedido==null){
            this.finish();
        }

        requestAvaliarItem = new Request(getApplicationContext(), new ServiceAvaliarItemPedido(this, this));
        requestDetailPedido = new Request(getApplicationContext(), new ServiceDealhesPedido(this, idPedido));
        progressBar.setVisibility(View.VISIBLE);

        requestDetailPedido.request();

    }

    public void updateDetailPedido() {
        progressBar.setVisibility(View.VISIBLE);
        requestDetailPedido.request();
    }

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    public void initialView() {
        getApplicationContext().startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));
    }


    public void showDialogCancel() {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);

        View mView = getLayoutInflater().inflate(R.layout.dialog_confirm_cancel, null);
        Button btnCancel = mView.findViewById(R.id.btnCancelCancel);
        Button btnConfirmCancel = mView.findViewById(R.id.btnConfirmCancel);

        mBuilder.setView(mView);
        dialog = mBuilder.create();
        dialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnConfirmCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Map<String, String> paramLogin = new HashMap<>();
                paramLogin.put("function_method", "cancel");
                paramLogin.put("id_pedido", idPedido);
                paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
                paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                requestCancelDemand.request(paramLogin);

                mProgressCancel.show();

            }
        });

        dialog.show();
    }


    void setStautsDelivery(String tipo_pedido, final String status, String processamento_hora, String preparo_hora, String envio_hora, String entrega_hora) {

        sequenceLayout = new SequenceLayout(this);

        ViewGroup.LayoutParams llp = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        sequenceLayout.setLayoutParams(llp);
        sequenceLayout.setProgressForegroundColor(Color.RED);
        linearStateContent.removeAllViews();
        linearStateContent.addView(sequenceLayout);

        if (status.equalsIgnoreCase("1")) {
            buttonCancelPedido.setVisibility(View.VISIBLE);
        } else {
            buttonCancelPedido.setVisibility(View.GONE);
        }

        step1.setActive(false);
        step2.setActive(false);
        step3.setActive(false);
        step4.setActive(false);

        step1.setSubtitleTextAppearance(R.style.StepStyleSubTitle);
        step1.setAnchorTextAppearance(R.style.StepStyleSubTitle);

        step2.setSubtitleTextAppearance(R.style.StepStyleSubTitle);
        step2.setAnchorTextAppearance(R.style.StepStyleSubTitle);

        step3.setSubtitleTextAppearance(R.style.StepStyleSubTitle);
        step3.setAnchorTextAppearance(R.style.StepStyleSubTitle);

        step4.setSubtitleTextAppearance(R.style.StepStyleSubTitle);
        step4.setAnchorTextAppearance(R.style.StepStyleSubTitle);


        switch (status) {
            case "1": {
                step1.setTitleTextAppearance(R.style.StepStyleTitle);
                step2.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step3.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step4.setTitleTextAppearance(R.style.StepStyleSubTitle);

                step1.setSubtitle("O pedido está sob análise, aguarde...");
                step1.setActive(true);

                buttonCancelPedido.setVisibility(View.VISIBLE);
                buttonConfirmRecebimento.setVisibility(View.GONE);
                break;
            }
            case "2": {
                step1.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step2.setTitleTextAppearance(R.style.StepStyleTitle);
                step3.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step4.setTitleTextAppearance(R.style.StepStyleSubTitle);

                buttonCancelPedido.setVisibility(View.GONE);
                buttonConfirmRecebimento.setVisibility(View.VISIBLE);
                step2.setSubtitle("O pedido está sendo preparado, aguarde...");
                step2.setActive(true);
                break;
            }
            case "3": {
                step1.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step2.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step3.setTitleTextAppearance(R.style.StepStyleTitle);
                step4.setTitleTextAppearance(R.style.StepStyleSubTitle);

                step3.setSubtitle("O pedido saiu para entrega, pode chegar no seu endereço a quaquer momento...");
                step3.setActive(true);
                buttonCancelPedido.setVisibility(View.GONE);
                buttonConfirmRecebimento.setVisibility(View.VISIBLE);

                break;
            }
            case "4": {
                step1.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step2.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step3.setTitleTextAppearance(R.style.StepStyleSubTitle);
                step4.setTitleTextAppearance(R.style.StepStyleTitle);
                step4.setSubtitle("Seu pedido foi entregue, obrigado pela preferência!");
                step4.setActive(true);

                buttonCancelPedido.setVisibility(View.GONE);
                buttonConfirmRecebimento.setVisibility(View.GONE);

                break;
            }
            case "5":{
                buttonCancelPedido.setVisibility(View.GONE);
                buttonConfirmRecebimento.setVisibility(View.GONE);
            }

        }

        sequenceLayout.addView(step1);
        sequenceLayout.addView(step2);
        if (tipo_pedido.equals("delivery")) {
            sequenceLayout.addView(step3);
        }
        sequenceLayout.addView(step4);


        sequenceLayout.setVisibility(View.GONE);
        sequenceLayout.invalidate();
        sequenceLayout.refreshDrawableState();
        sequenceLayout.invalidate();
        sequenceLayout.setVisibility(View.VISIBLE);

    }


    private void setStautsStep(boolean step1, boolean step2, boolean step3, boolean step4) {

        this.step1.setActive(step1);
        this.step2.setActive(step2);
        this.step3.setActive(step3);
        this.step4.setActive(step4);

        if (step1 == false) {
            //th
        }

    }

    public void initializeAdapter(JSONObject objectDetalhe) {
        try {

            if(dialog!=null) {
                dialog.dismiss();
                mProgressCancel.dismiss();
            }

            detailObject = objectDetalhe;
            if (objectDetalhe.getString("ped_status").equals("4")) {
                buttonConfirmRecebimento.setVisibility(View.GONE);
                buttonCancelPedido.setVisibility(View.GONE);
            }

            ped_id.setText("PEDIDO " + objectDetalhe.getString("ped_id"));
            processamento_hora.setText(objectDetalhe.getString("processamento_hora").replace("-", ""));
            codigo.setText(objectDetalhe.getString("codigo"));
            processamento_data.setText(objectDetalhe.getString("processamento_data"));
            previsao_entrega.setText(objectDetalhe.getString("previsao_entrega"));
            if (objectDetalhe.getInt("ped_status") == 5) {
                // PEDIDO CANCELADO
                buttonConfirmRecebimento.setVisibility(View.GONE);
                buttonCancelPedido.setVisibility(View.GONE);
                constCancel.setVisibility(View.VISIBLE);
                previsao_entrega.setVisibility(View.GONE);
                textCancel.setText("*Motivo: " + objectDetalhe.getString("desc_cancel"));
            } else {
                constCancel.setVisibility(View.GONE);
                previsao_entrega.setVisibility(View.VISIBLE);
            }

            estab_telefone.setText(objectDetalhe.getString("estab_telefone"));

            if (objectDetalhe.getString("type").equals("delivery")) {

                frete.setText(objectDetalhe.getString("frete"));
                desc_entrega.setText(objectDetalhe.getString("desc_entrega"));

            } else {

                cardValDelivery.setVisibility(View.GONE);
                desc_entrega.setVisibility(View.GONE);
                textViewEntrega.setVisibility(View.GONE);
                formaPag_name.setVisibility(View.GONE);
                textTaxaEntrega.setVisibility(View.GONE);
                labelFormaPagamento.setVisibility(View.GONE);

            }

            desconto_value.setText(objectDetalhe.getString("desconto_value"));
            total_value.setText(objectDetalhe.getString("total_value"));
            textViewTotal.setText(objectDetalhe.getString("total_value"));
            formaPag_name.setText(objectDetalhe.getString("formaPag_name"));
            produtos_value.setText(objectDetalhe.getString("produtos_value"));
            textViewEntrega.setText(objectDetalhe.getString("frete"));
            textDiscountPedido.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            textDiscountPedido.setText(objectDetalhe.getString("sem_desconto"));

            if (!objectDetalhe.isNull("formaPag_icon"))
                new ListSetImage(getApplicationContext(), imageView).execute(objectDetalhe.getString("formaPag_icon"));

            final LineItemDemandEstabelishment lineItemDemandEstabelishment = new LineItemDemandEstabelishment(getApplicationContext(), objectDetalhe.getJSONArray("produtos"));
            produtos.setAdapter(lineItemDemandEstabelishment);

            lineItemDemandEstabelishment.setOnClickAvaliationListenerItem(new LineItemDemandEstabelishment.onClickAvaliationListenerItem() {
                @Override
                public void onClickAvaliationListenerItem(final JSONObject jsonObject, final int position) {

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailDemand.this);

                    final RatingBar mRatingBar;
                    TextView mRatingScale;
                    final EditText mFeedback;
                    Button mSendFeedback;
                    Button mCancelFeedFeedback;
                    TextView text_type_evaluate_estabelecimento;

                    View mView = getLayoutInflater().inflate(R.layout.activity_evaluate_item, null);

                    progressBarItemPedido = mView.findViewById(R.id.progressBarItemPedido);
                    text_type_evaluate_estabelecimento = mView.findViewById(R.id.text_type_evaluate_estabelecimento);
                    mRatingBar = (RatingBar) mView.findViewById(R.id.ratting_avaliation);
                    mRatingScale = (TextView) mView.findViewById(R.id.tvRatingScale);
                    mFeedback = (EditText) mView.findViewById(R.id.descAvaliacao);
                    mSendFeedback = (Button) mView.findViewById(R.id.btnSubmit);
                    mCancelFeedFeedback = mView.findViewById(R.id.btnCancelFeedFeedback);
                    progressBarItemPedido.setVisibility(View.GONE);


                    mBuilder.setView(mView);
                    dialog = mBuilder.create();
                    dialog.show();

                    try {
                        text_type_evaluate_estabelecimento.setText(jsonObject.getString("nome"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    setRattingAction(mRatingBar, mRatingScale);
                    mSendFeedback.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Map<String, String> param = new HashMap<>();
                            try {
                                param.put("id_estabelecimento", Repository.idEstabelecimento);
                                param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                                param.put("function_method", "avaliar_produto");
                                param.put("id_produtoSelecionado", jsonObject.getString("id"));
                                param.put("nota", mRatingBar.getRating() + "");
                                param.put("descricao", mFeedback.getText().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                lineItemDemandEstabelishment.listItensCarrinho.getJSONObject(position).put("avaliacao", mRatingBar.getRating());
                                lineItemDemandEstabelishment.notifyDataSetChanged();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            confirmRecebimento = false;
                            progressBarItemPedido.setVisibility(View.VISIBLE);
                            requestAvaliarItem.request(param);
                        }
                    });

                    mCancelFeedFeedback.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                }
            });

            if (!objectDetalhe.isNull("progress")) {

                pStatus = objectDetalhe.getJSONObject("progress").getInt("percent");
                time = objectDetalhe.getJSONObject("progress").getInt("time_up_percent");

                String strDate = objectDetalhe.getJSONObject("progress").getString("time");
                try {
                    dateResponse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(strDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                startProgressTime();
            }


            step1 = new SequenceStep(getApplicationContext());
            step2 = new SequenceStep(getApplicationContext());
            step3 = new SequenceStep(getApplicationContext());
            step4 = new SequenceStep(getApplicationContext());

            step1.setSubtitleTextAppearance(10);
            step1.setTitle("EM ANÁLISE");
            step2.setTitle("PREPARANDO");
            step3.setTitle("SAIU PARA ENTREGA");
            step4.setTitle("ENTREGUE");


            step1.setAnchor(objectDetalhe.getString("processamento_hora").replace("-", ""));
            step2.setAnchor(objectDetalhe.getString("preparo_hora").replace("-", ""));
            step3.setAnchor(objectDetalhe.getString("envio_hora").replace("-", ""));
            step4.setAnchor(objectDetalhe.getString("entrega_hora").replace("-", ""));

            setStautsDelivery(objectDetalhe.getString("type"), objectDetalhe.getString("ped_status"), objectDetalhe.getString("processamento_hora"), objectDetalhe.getString("preparo_hora"), objectDetalhe.getString("envio_hora"), objectDetalhe.getString("entrega_hora"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

            if(confirmRecebimento == true){
                createAvaliationDemand(true);
            }

    }

    public void setRattingAction(RatingBar mRatingBar, final TextView mRatingScale) {

        mRatingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                mRatingScale.setText(String.valueOf(v));

                switch ((int) ratingBar.getRating()) {
                    case 1:
                        mRatingScale.setText("Muito Ruim");
                        break;
                    case 2:
                        mRatingScale.setText("Precisa Melhorar");
                        break;
                    case 3:
                        mRatingScale.setText("Bom");
                        break;
                    case 4:
                        mRatingScale.setText("Muito Bom");
                        break;
                    case 5:
                        mRatingScale.setText("Excelente");
                        break;
                    default:
                        mRatingScale.setText("");

                }

            }

        });

    }

    public void updateStatus(){

        requestDetailPedido.request();
        confirmRecebimento = true;

    }


    public void createAvaliationDemand(boolean confirmRecebimento) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailDemand.this);
        final RatingBar mRatingBar;
        final RatingBar ratting_avaliation_atendimento;
        TextView mRatingScale;
        final EditText mFeedback;
        final EditText mFeedbackAtendimento;
        Button mSendFeedback;
        Button mCancelFeedFeedback = null;
        final Switch switchPrazo;
        final Switch switchEsperado;

        View mView = getLayoutInflater().inflate(R.layout.activity_evaluate_demand, null);
        TextView text_type_evaluate_estabelecimento = mView.findViewById(R.id.text_type_evaluate_estabelecimento);
        TextView text_type_evaluate_atendimento = mView.findViewById(R.id.text_type_evaluate_atendimento);
        TextView tvRatingScaleAtendimento = mView.findViewById(R.id.tvRatingScaleAtendimento);
        progressBarSendFeedBack = mView.findViewById(R.id.progressBarSendFeedBack);
        progressBarSendFeedBack.setVisibility(View.GONE);

        mRatingBar = (RatingBar) mView.findViewById(R.id.ratting_avaliation);
        mRatingScale = (TextView) mView.findViewById(R.id.tvRatingScale);

        mFeedback = (EditText) mView.findViewById(R.id.descAvaliacao);
        ratting_avaliation_atendimento = (RatingBar) mView.findViewById(R.id.ratting_avaliation_atendimento);
        mFeedbackAtendimento = (EditText) mView.findViewById(R.id.descAvaliacaoAtendimento);

        mSendFeedback = (Button) mView.findViewById(R.id.btnSubmit);
        mCancelFeedFeedback = mView.findViewById(R.id.btnCancelFeedFeedback);
        switchPrazo = mView.findViewById(R.id.switchPrazo);
        switchEsperado = mView.findViewById(R.id.switchEsperado);
        switchPrazo.setChecked(true);
        switchEsperado.setChecked(true);

        mBuilder.setView(mView);
        dialog = mBuilder.create();
        dialog.show();

        try {
            if (detailObject.getString("type").equals("delivery")) {
                text_type_evaluate_estabelecimento.setText("PEDIDO");
                text_type_evaluate_atendimento.setText("ENTREGA");
            }
        } catch (
                JSONException e) {
            e.printStackTrace();
        }

        setRattingAction(mRatingBar, mRatingScale);
        setRattingAction(ratting_avaliation_atendimento, tvRatingScaleAtendimento);
        mSendFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Map<String, String> param = new HashMap<>();
                param.put("id_pedido", idPedido);
                param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                param.put("nota_pedido", mRatingBar.getRating() + "");
                param.put("desc_pedido", mFeedback.getText().toString());
                param.put("received_as_described", switchEsperado.isChecked() + "");
                param.put("received_within", switchPrazo.isChecked() + "");
                param.put("nota_atendimento", ratting_avaliation_atendimento.getRating() + "");
                param.put("desc_atendimento", mFeedbackAtendimento.getText().toString());
                requestAvaliarPedido.request(param);
                progressBarSendFeedBack.setVisibility(View.VISIBLE);
                dialog.dismiss();

            }
        });

        mCancelFeedFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                if (getIntent().hasExtra("demandFinalized")) {
                    setResultado();
                }
            }
        });

        confirmRecebimento = false;

    }

    @Override
    public void showMsg(String titulo, String mensagem) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(DetailDemand.this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView textView = mView.findViewById(R.id.textSucessMessage);
        TextView textViewSucess = mView.findViewById(R.id.textSucessMessage);

        textView.setText(titulo);
        textViewSucess.setText(mensagem);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

    }

    @Override
    public void initializeAdapterListItens(JSONArray result, String position) {
        FragmentListDemandsEstablishment.refference.initializeAdapterListItens(result, position);
    }

    @Override
    public void dimissPopUpAvaliacao() {
        dialog.dismiss();
        if (confirmRecebimento == true) {
            setResultado();
        }
    }

    @Override
    public void dimissProgressAvaliacaoItem() {
        progressBarItemPedido.setVisibility(View.GONE);
    }

    @Override
    public void dimissProgressBarSendFeedBack() {
        dialog.dismiss();
        progressBarSendFeedBack.setVisibility(View.GONE);
    }

    public void setResultado() {

        setResult(65);
     //   finish();
        requestDetailPedido.request();

    }

    public void startProgressTime() {
        progressDemand.setProgress(pStatus);
        if (thread != null) thread.interrupt();
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                runn();
            }
        });

        thread.start();

    }


    private void runn() {
        long now = Calendar.getInstance().getTimeInMillis();
        long tempDecorrido = now - dateResponse.getTime();
        if (pStatus < 100) {
            pStatus += ((int) tempDecorrido / time);
        }
        kay = Calendar.getInstance().getTimeInMillis();
        long temp = kay;
        int progress = pStatus;
        while (progress < 100 && kay == temp) {
            final int finalProgress = progress;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressDemand.setProgress(finalProgress);
                }
            });
            try {
                Thread.sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            progress++;
        }
        if (kay == temp) {

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Drawable progressDrawable = progressDemand.getProgressDrawable().mutate();
                    progressDrawable.setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
                    progressDemand.setProgressDrawable(progressDrawable);
                }
            });

            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        Repository.activityDetailDemandVisible = this;
    }

    @Override
    public void onPause() {
        if (Repository.activityDetailDemandVisible == this) {
            Repository.activityDetailDemandVisible = null;
        }
        super.onPause();
    }

    @Override
    public void onBackPressed() {

        /*if(getIntent().hasExtra("id_pedido")) {

            Intent intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);

        }*/

        setResult(RESULT_FIRST_USER);

        super.onBackPressed();

    }

}