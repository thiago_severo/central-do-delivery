package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentComanda;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListComanda extends OnPostResponse {

    FragmentComanda fragmentComanda;
    private SwipeRefreshLayout swipeRefreshLayout;

    public ServiceListComanda(FragmentComanda fragmentComanda, SwipeRefreshLayout swipeRefreshLayout) {
        this.setId(Repository.idEstabelecimento);
        this.fragmentComanda = fragmentComanda;
        this.swipeRefreshLayout = swipeRefreshLayout;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_pedidos_comanda.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}

        try{
            Toast.makeText(fragmentComanda.getActivity().getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        }catch (NullPointerException e){}

        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_estabelecimento", getId());
        paramLogin.put("id_cliente", Repository.getIdUsuario(fragmentComanda.getContext()));
        return paramLogin;
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONObject result = jsonObject.getJSONObject("result");
            fragmentComanda.initializeAdapterListProducts(result.getJSONArray("itens"));
            fragmentComanda.initializeHeader(result.getString("total"), result.getString("couvert"), result.getJSONObject("taxa_servico").getString("valor"), result.getJSONObject("taxa_servico").getString("percent"), result.getString("mesa"));
        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response) {
        try{
            swipeRefreshLayout.setRefreshing(false);
        }catch (NullPointerException e){}
    }

    @Override
    public void onUpdate(String response) {
        if (!response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                result.getString("data");
                fragmentComanda.initializeAdapterListProducts(result.getJSONArray("itens"));
                fragmentComanda.initializeHeader(result.getString("total"), result.getString("couvert"), result.getJSONObject("taxa_servico").getString("valor"), result.getJSONObject("taxa_servico").getString("percent"), result.getString("mesa"));
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }
}
