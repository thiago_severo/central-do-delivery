package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.FragmentListDemandsEstablishment;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceCancelPedido extends OnPostResponse {

        FragmentListDemandsEstablishment fragmentListDemandsEstablishment;
        ListProductsStablishmentDeliveryActivity activity;

        public ServiceCancelPedido( FragmentListDemandsEstablishment fragmentListDemandsEstablishment) {
        this.activity = activity;
        this.fragmentListDemandsEstablishment = fragmentListDemandsEstablishment;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "select_pedidos_do_estab.php";
        }

        @Override
        public void onErrorResponse(VolleyError error) {
        Toast.makeText(fragmentListDemandsEstablishment.getActivity().getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onResponse (String response){

        }

        @Override
        public void onUpdate (String response){
            if (response != null && !response.equals("null"))
                try {

                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getJSONObject("msg").getInt("cod")!=0){
                        ListProductsStablishmentDeliveryActivity.refference.showMsg(jsonObject.getJSONObject("msg").getString("text"),"OPS:(");
                    }

                    if(fragmentListDemandsEstablishment!=null) {
                        JSONArray result = jsonObject.getJSONArray("result");
                        fragmentListDemandsEstablishment.initializeAdapterListItens(result, null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }
