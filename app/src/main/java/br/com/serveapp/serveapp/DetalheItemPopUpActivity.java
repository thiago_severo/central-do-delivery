package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.synnapps.carouselview.CarouselView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.CarouselAdapter;
import br.com.serveapp.serveapp.adapt.DetalheItemAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAddToCar;
import br.com.serveapp.serveapp.dao.webService.ServiceBrinde;
import br.com.serveapp.serveapp.dao.webService.ServiceItemDetail;
import br.com.serveapp.serveapp.dao.webService.ServiceUpdateItemCart;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.entidades.SubItem;
import br.com.serveapp.serveapp.entidades.cardapio.ItemCardapio;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.ItenPedido;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class DetalheItemPopUpActivity extends AlertDialog {

    List<SubItem> subItemList;
    RecyclerView listItensProductCardapio;
    Button btnCloseItem;
    ImageView btn_decrease_item;
    ImageView btn_add_item;
    TextView textQtdItemDemand;
    ItenPedido itenPedido;
    JSONArray listGoupItens;
    Pedido pedido;
    ArrayList<ItemPedido> itemPedidoList = new ArrayList<>();

    private double valAgregadoItem = 0;
    ItemCardapio itemCardapio;

    public int qtdItemPedido = 1;
    public int qtdItemPedidoMax = 1;
    public int qtdMaxBrinde = 4;

    public Button btnAdicionarItemCardapio;
    public JSONObject produto;

    Request requestDetailItem;
    DetalheItemAdapter detalheItemAdapter;
    TextView textNameItemPopUp;
    TextView textDescricaoProduto;
    TextView txtCategoria;

    public float valor_total;

    Intent intent;
    Activity activity;
    //ImageView blurImageView;
    //
    // ImageView imageCircle;
    RatingBar ratingAvaliacaoProduto;

    ProgressBar mProgress;
    Thread thread;
    int pStatus = 0;
    Handler handler = new Handler();
    Request requestServicerAddToCar;
    Request requestServiceAddBrinde;
    private static final float BITMAP_SCALE = 0.04f;
    private static final float BLUR_RADIUS = 25.0f;
    ProgressBar progressButon;
    boolean enableUpdate;
    Request requestServicerUpdateItemCart;

    CarouselView carouselView;
    ImageView imageProduto;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            //getActivity().finish();
            dismiss();
        }

    };

    public DetalheItemPopUpActivity(Activity context, Intent intent) {
        super(context);
        this.activity = context;
        this.intent = intent;
    }


    public Context getApplicationContext() {
        return activity;
    }

    public Activity getActivity(){
        return activity;
    }

    public Intent getIntentt(){
        return intent;
    }

    @SuppressLint("WrongViewCast")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (Repository.getIdUsuario(getActivity()) == null || !getIntentt().hasExtra("id_produto")) {
                //getActivity().finish();
                dismiss();
            }
        } catch (Exception e) {
            //getActivity().finish();
            dismiss();
        }

        setContentView(R.layout.activity_detalhe_item_cardapio_pop_up);

        if (!getIntentt().hasExtra("op")) {
            Repository.itens = new HashSet<>();
        }

        progressButon = findViewById(R.id.progressIncrement);
        progressButon.setVisibility(View.GONE);

        carouselView =  findViewById(R.id.carouselView);
        imageProduto =  findViewById(R.id.imageProduto);
        txtCategoria = findViewById(R.id.txtCategoria);

        listItensProductCardapio = findViewById(R.id.listItensProducts);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity().getApplicationContext());
        listItensProductCardapio.setLayoutManager(llm);

        DisplayMetrics dm = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .92), (int) (height * .9));
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE|WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        //getWindow().setLayout((int) (width), (int) (height));

        btnCloseItem = findViewById(R.id.btnCloseItem);
        btnCloseItem.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent returnIntent = new Intent();
                        getActivity().setResult(Activity.RESULT_CANCELED, returnIntent);
                        btnAdicionarItemCardapio.setEnabled(false);
                        //getActivity().finish();
                        dismiss();
                    }
                }
        );

        //blurImageView = findViewById(R.id.imageBlurTopPopUp);
        //imageCircle = findViewById(R.id.circleImage);
        ratingAvaliacaoProduto = findViewById(R.id.ratingAvaliacaoProduto);

        requestServicerAddToCar = new Request(getActivity().getApplicationContext(), new ServiceAddToCar(this));
        requestServiceAddBrinde = new Request(getActivity().getApplicationContext(), new ServiceBrinde(this));

        btnAdicionarItemCardapio = findViewById(R.id.btnAdicionarItemCardapio);
        btnAdicionarItemCardapio.setEnabled(false);
        btnAdicionarItemCardapio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Map<String, String> paramLogin = new HashMap<>();
                buildParams(paramLogin);
            }

        });

        textDescricaoProduto = findViewById(R.id.textDescricaoProduto);
        textNameItemPopUp = findViewById(R.id.account_mail);
        textDescricaoProduto.setText(getIntentt().getStringExtra("descricao"));
        textNameItemPopUp.setText(getIntentt().getStringExtra("nome"));
        ratingAvaliacaoProduto.setRating(Float.parseFloat(getIntentt().getStringExtra("avaliacao")));

        /*try{
            JSONArray banners = new JSONArray(getIntent().getStringExtra("fotos"));
            carouselView.setPageCount(banners.length());
            CarouselAdapter adapter = new CarouselAdapter(this, banners);
            carouselView.setViewListener(adapter);
        }catch (Exception e){

        }*/


        textQtdItemDemand = findViewById(R.id.textQtdItemDemandFinalize);
        btn_decrease_item = findViewById(R.id.btn_decrease_item);
        btn_decrease_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (qtdItemPedido >= 2) {
                    qtdItemPedido--;
                    updateValueButton(itenPedido.valorTotal);
                    textQtdItemDemand.setText(qtdItemPedido + "");
                }
            }
        });

        if (getIntentt().hasExtra("qtdMaxBrindes")) {
            qtdMaxBrinde = getIntentt().getIntExtra("qtdMaxBrindes", 1);
        }

        btn_add_item = findViewById(R.id.btn_accrease_item_demand);
        btn_add_item.setEnabled(false);
        btn_add_item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (getIntentt().hasExtra("qtdMaxBrindes")) {
                    qtdItemPedidoMax = qtdMaxBrinde;
                }

                if (qtdItemPedido < qtdItemPedidoMax) {
                    qtdItemPedido++;
                    updateValueButton(itenPedido.valorTotal);
                    textQtdItemDemand.setText(qtdItemPedido + "");

                }
            }
        });

        if (getIntentt().hasExtra("op")) {
            enableUpdate = true;
            int position = getIntentt().getIntExtra("position", 0);
            requestServicerUpdateItemCart = new Request(getActivity().getApplicationContext(), new ServiceUpdateItemCart(this, position));
            qtdItemPedido = getIntentt().getIntExtra("qtd", 1);
            btnAdicionarItemCardapio.setText(("" +
                    "ATUALIZAR R$ " + setMoneyFormat(getIntentt().getDoubleExtra("valorBase", 0) * qtdItemPedido)).replace(".", ","));
            textQtdItemDemand.setText(qtdItemPedido + "");
        } else {
            btnAdicionarItemCardapio.setText(("ADICIONAR R$ " + setMoneyFormat(getIntentt().getDoubleExtra("value", 0))).replace(".", ","));
        }

        mProgress = findViewById(R.id.progressBarSplash);
        mProgress.setVisibility(View.VISIBLE);

        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus <= 100) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            mProgress.setProgress(pStatus);
                            if (pStatus == 99) {
                                pStatus = 0;
                            }
                        }
                    });
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    pStatus++;
                }
            }
        });

        thread.start();
        requestDetailItem = new Request(getActivity().getApplicationContext(), new ServiceItemDetail(this, getIntentt().getStringExtra("id_produto")));
        requestDetailItem.request();

    }

    public void disableProgress() {
        mProgress.setVisibility(View.GONE);
        thread.interrupt();
    }


    private boolean isValid(JSONArray listGroupItens) {
        JSONObject group;
        try {
            for (int i = 0; i < listGroupItens.length(); i++) {
                group = listGroupItens.getJSONObject(i);
                int min = group.getInt("qtdMin");
                int max = group.getInt("qtdMax");
                int count = 0;
                JSONArray itens = group.getJSONArray("itens");
                for (int j = 0; j < itens.length(); j++) {
                    if (itens.getJSONObject(j).getInt("def") == 1) {
                        //if(itens.getJSONObject(j).getInt("def") == 1 && itens.getJSONObject(j).getInt("disponivel") == 1){
                        count++;
                    }
                }
                if ((min != 0 && count < min) || (max != 0 && count > max)) {
                    int a = 1;
                    return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        int a = 1;
        return true;
    }

    public void initializeAdapter(JSONArray listGroupItens, int qtdMax, JSONObject produto) {
        this.produto = produto;
        JSONArray banners = new JSONArray();
        try{
            banners = produto.getJSONArray("fotos");
            CarouselAdapter adapter = new CarouselAdapter(getActivity(), banners);
            carouselView.setViewListener(adapter);
            carouselView.setPageCount(banners.length());
            //carouselView.setVisibility(View.VISIBLE);
        }catch (Exception e){
            e.printStackTrace();
            CarouselAdapter adapter = new CarouselAdapter(getActivity(), new JSONArray());
            carouselView.setViewListener(adapter);
            adapter.setImages(banners);
            //carouselView.setVisibility(View.INVISIBLE);
        }

        try {

            textDescricaoProduto.setText(produto.getString("descricao"));

            //txtCategoria.setText("ROMERYTO");
            //txtCategoria.setText(produto.getString("categoria"));
            if(!produto.isNull("categoria"))
                txtCategoria.setText(produto.getString("categoria"));
            else
                txtCategoria.setText("");

            if(produto.getString("descricao").trim().length()>0){
                textDescricaoProduto.setVisibility(View.VISIBLE);
            }else{
                textDescricaoProduto.setVisibility(View.GONE);
            }
            textNameItemPopUp.setText(produto.getString("nome"));
            ratingAvaliacaoProduto.setRating(Float.parseFloat(produto.getString("avaliacao")));
            new ListSetImage(getActivity().getApplicationContext(), imageProduto).execute(produto.getString("foto"));
            //new ListSetImage3(getApplicationContext(), blurImageView).execute(produto.getString("foto"));

            if (!btnAdicionarItemCardapio.getText().toString().contains("ATUALIZAR")) {
                btnAdicionarItemCardapio.setText(("ADICIONAR R$ " + setMoneyFormat(produto.getDouble("value"))).replace(".", ","));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        btn_add_item.setEnabled(true);
        qtdItemPedidoMax = qtdMax;

        disableProgress();
        if (!isValid(listGroupItens)) {
            disableBtnAddItem();
        }
        itenPedido = new ItenPedido();
        if (getIntentt().hasExtra("op")) {
            itenPedido.valObservacao = getIntentt().getStringExtra("detalhe");
            itenPedido.valorTotal = getIntentt().getDoubleExtra("valorBase", 0);
            detalheItemAdapter = new DetalheItemAdapter("updateLine", itenPedido, getActivity().getApplicationContext(), listGroupItens, this);
        } else {
            try {
                itenPedido.valorTotal = produto.getDouble("value");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            detalheItemAdapter = new DetalheItemAdapter(null, itenPedido, getActivity().getApplicationContext(), listGroupItens, this);
        }
        listItensProductCardapio.setAdapter(detalheItemAdapter);
    }

    public void enableBtnAddItem() {
        btnAdicionarItemCardapio.setBackground(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.drawable.borda_arredondada_light));
        btnAdicionarItemCardapio.setEnabled(true);
        btnAdicionarItemCardapio.setClickable(true);
    }

    public void disableBtnAddItem() {
        btnAdicionarItemCardapio.setClickable(false);
        btnAdicionarItemCardapio.setEnabled(false);
        btnAdicionarItemCardapio.setBackground(ContextCompat.getDrawable(getActivity().getApplicationContext(), R.color.com_facebook_button_background_color_disabled));
    }

    public void updateValueButton(double valor) {
        if (getIntentt().hasExtra("op")) {
            btnAdicionarItemCardapio.setText(("ATUALIZAR R$ " + setMoneyFormat(valor * qtdItemPedido)).replace(".", ","));
        } else {
            btnAdicionarItemCardapio.setText(("ADICIONAR R$ " + setMoneyFormat(valor * qtdItemPedido)).replace(".", ","));
        }
    }

    public String setMoneyFormat(double val) {
        DecimalFormat moneyFormat = new DecimalFormat("0.00");
        return moneyFormat.format(val);
    }

    public void receiveSucessResponse(String label, String quantidade) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("qtd", quantidade);
        returnIntent.putExtra("label", label);
        getActivity().setResult(Activity.RESULT_OK, returnIntent);
        enableBtnAddItem();
        if(activity instanceof  ListProductsStablishmentDeliveryActivity)
            ((ListProductsStablishmentDeliveryActivity)activity).receiveSucessResponse(label, quantidade);
        progressButon.setVisibility(View.GONE);
        //getActivity().finish();
        dismiss();
    }

    public void receiveSucessResponseFromBrindes() {
        enableBtnAddItem();
        progressButon.setVisibility(View.GONE);
        //getActivity().finish();
        dismiss();
    }

    public void receiveSucessResponseUpdateItem(JSONObject obj) {
        enableBtnAddItem();
        progressButon.setVisibility(View.GONE);
        //getActivity().finish();
        dismiss();
    }

    public void receiveErroResponse() {
        updateValueButton(itenPedido.valorTotal);
        enableBtnAddItem();
        progressButon.setVisibility(View.GONE);
    }

    private void buildParams(Map<String, String> paramLogin) {

        if (enableUpdate) {
            paramLogin.put("detalhe", itenPedido.valObservacao);
            paramLogin.put("id_cliente", Repository.getIdUsuario(getActivity().getApplicationContext()));
            paramLogin.put("id_produto_selecionado", getIntentt().getStringExtra("id_produto_selecionado"));
            paramLogin.put("valor", itenPedido.valorTotal + "");
            paramLogin.put("id_produto", getIntentt().getStringExtra("id_produto"));
            paramLogin.put("itens", new JSONArray(Repository.itens).toString());
            paramLogin.put("qtd", qtdItemPedido + "");
            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
            paramLogin.put("function_method", "update_product");

            btnAdicionarItemCardapio.setText("");
            progressButon.setVisibility(View.VISIBLE);

            if (btnAdicionarItemCardapio.isEnabled() && btnAdicionarItemCardapio.isClickable())
                requestServicerUpdateItemCart.request(paramLogin);
            disableBtnAddItem();

        } else {
            paramLogin.put("detalhe", itenPedido.valObservacao);
            paramLogin.put("id_cliente", Repository.getIdUsuario(getActivity().getApplicationContext()));
            paramLogin.put("id_produto", getIntentt().getStringExtra("id_produto"));
            paramLogin.put("valor", itenPedido.valorTotal + "");
            paramLogin.put("itens", new JSONArray(Repository.itens).toString());
            paramLogin.put("qtd", qtdItemPedido + "");
            paramLogin.put("function_method", "add_product");
            paramLogin.put("id_estabelecimento", Repository.idEstabelecimento);
            btnAdicionarItemCardapio.setText("");
            progressButon.setVisibility(View.VISIBLE);

            if (getIntentt().hasExtra("is_brinde")) {
                paramLogin.put("", "");
                paramLogin.put("function_method", "add_brinde");
                paramLogin.put("id_prog_fidelidade", getIntentt().getStringExtra("id_prog_fidelidade"));
                if (btnAdicionarItemCardapio.isEnabled() && btnAdicionarItemCardapio.isClickable())
                    requestServiceAddBrinde.request(paramLogin);
            } else {

                if (btnAdicionarItemCardapio.isEnabled() && btnAdicionarItemCardapio.isClickable())
                    requestServicerAddToCar.request(paramLogin);
            }

            disableBtnAddItem();

        }

    }

}