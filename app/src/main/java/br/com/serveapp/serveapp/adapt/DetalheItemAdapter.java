package br.com.serveapp.serveapp.adapt;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.SubItem;
import br.com.serveapp.serveapp.entidades.cardapio.Ingrediente;
import br.com.serveapp.serveapp.entidades.pedido.GroupItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.ItenPedido;
import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.CheckGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DetalheItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<SubItem> subItemList;
    List<Ingrediente> ingredienteListRadio = new ArrayList<>();
    private final int VIEW_TYPE_CHECK_INCREMENT = 2;
    DetalheItemAdapter.onItemClickCheckListner listener;
    ItemPedido itemPedidoReturn;
    List<Ingrediente> ingredienteLisParsableRadio = new ArrayList<>();

    List<SubItem> listSubItemRadioButton = new ArrayList<>();
    public List<Ingrediente> listIngredientRadioSelected = new ArrayList<>();

    private Context context;
    JSONArray listGroupItens = new JSONArray();
    public static final int VIEW_TYPE_RADIO = 0;
    public static final int VIEW_TYPE_OBS = 2;
    public static final int VIEW_TYPE_CHECK_BOX = 1;
    public ItenPedido itenPedido;
    DetalheItemPopUpActivity detalheItemPopUpActivity;
    String option;
    JSONArray listItensPreSelecteds = new JSONArray();
    JSONArray listTotalItensSelecteds = new JSONArray();


    public DetalheItemAdapter(String op, ItenPedido itenPedido, Context context, JSONArray listGroupItens, DetalheItemPopUpActivity detalheItemPopUpActivity) {
        this.option = op;
        this.itenPedido = itenPedido;
        listGroupItens.put("obs");
        this.listGroupItens = listGroupItens;
        this.detalheItemPopUpActivity = detalheItemPopUpActivity;
        this.context = context;
        initializePedido(listGroupItens);
    }

    int positionG = 0;
    boolean btnEnable = true;
    ArrayList<Boolean> checkEnableds;


    @Override
    public int getItemViewType(int position) {
        int returnType = 0;
        try {
            if (listGroupItens.get(position).toString() == "obs") {
                returnType = VIEW_TYPE_OBS;
            } else if ((listGroupItens.getJSONObject(position).getInt("qtdMax")) == 1 && (listGroupItens.getJSONObject(position).getInt("qtdMin")) == 1) {
                returnType = VIEW_TYPE_RADIO;
            } else {
                returnType = VIEW_TYPE_CHECK_BOX;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnType;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;
        if (viewType == VIEW_TYPE_OBS) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_note_item, parent, false);
            LineObservation lineFavoritHolder = new LineObservation(v);
            viewHolder = lineFavoritHolder;
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_group_item_product, parent, false);
            LineSimpleItemHolder lineFavoritHolder = new LineSimpleItemHolder(v);
            viewHolder = lineFavoritHolder;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        if (holder instanceof LineObservation) {
            if (itenPedido.valObservacao != null)
                ((LineObservation) holder).detailObservation.setText(itenPedido.valObservacao);
        } else
            try {
                JSONObject group = listGroupItens.getJSONObject(position);
                ((LineSimpleItemHolder) (holder)).id = listGroupItens.getJSONObject(position).getInt("id_categoria");
                ((LineSimpleItemHolder) (holder)).txtLabel.setText(listGroupItens.getJSONObject(position).getString("label"));
                ((LineSimpleItemHolder) (holder)).txt_nome_item.setText(listGroupItens.getJSONObject(position).getString("nome"));
                ((LineSimpleItemHolder) (holder)).checkGroup = new CheckGroup(listGroupItens.getJSONObject(position).getInt("qtdMin"), listGroupItens.getJSONObject(position).getInt("qtdMax"));
                final LineDetailViewItemAdapter lineDetailViewItemAdapter = new LineDetailViewItemAdapter(option,
                        ((LineSimpleItemHolder) (holder)).checkGroup, itenPedido.itemGroupPedidos.get(position), group.getInt("qtdMax"), group.getInt("qtdMin"), context, listGroupItens.getJSONObject(position).getJSONArray("itens"), listItensPreSelecteds, listTotalItensSelecteds);

                if (itenPedido.itemGroupPedidos.get(position).valorInicial <= 0) {
                    ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.GONE);
                } else {
                    ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.VISIBLE);
                }

                lineDetailViewItemAdapter.setListener(
                        new LineDetailViewItemAdapter.onItemClickListener() {

                            @Override
                            public void onItemClick(JSONObject item) {
                                try {

                                    if (itenPedido.itemGroupPedidos.get(position).itemSelectedRadio != null) {
                                        Repository.itens.remove(itenPedido.itemGroupPedidos.get(position).itemSelectedRadio.getInt("id_item"));
                                    }

                                    Repository.itens.add(item.getInt("id_item"));
                                    itenPedido.valorTotal -= itenPedido.itemGroupPedidos.get(position).valorRadio;
                                    itenPedido.itemGroupPedidos.get(position).valorRadio = item.getDouble("value");
                                    itenPedido.itemGroupPedidos.get(position).itemSelectedRadio = item;
                                    itenPedido.itemGroupPedidos.get(position).valorInicial = item.getDouble("value");
                                    itenPedido.valorTotal += itenPedido.itemGroupPedidos.get(position).valorRadio;
                                    detalheItemPopUpActivity.updateValueButton(itenPedido.valorTotal);
                                    ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setText(("R$ " + itenPedido.itemGroupPedidos.get(position).valorInicial + "0").replace(".", ","));

                                    if (enabledButton() == true) {
                                        detalheItemPopUpActivity.enableBtnAddItem();
                                    }

                                    if (itenPedido.itemGroupPedidos.get(position).valorInicial <= 0) {
                                        ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.GONE);
                                    } else {
                                        ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.VISIBLE);
                                    }
                                    ((LineSimpleItemHolder) (holder)).textSelectedRadio.setText(item.getString("nome"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                );

                lineDetailViewItemAdapter.setListenerCheck(new LineDetailViewItemAdapter.onItemClickListenerCheck() {
                    @Override
                    public void onItemClickCheck(JSONObject item, boolean check, int count) {
                        JSONArray tempRemove = ((LineSimpleItemHolder) (holder)).itensCheck;

                        if (check == true) { //add
                            ((LineSimpleItemHolder) (holder)).itensCheck.put(item);
                            try {
                                itenPedido.itemGroupPedidos.get(position).valorInicial += item.getDouble("value");
                                itenPedido.valorTotal += item.getDouble("value");
                                detalheItemPopUpActivity.updateValueButton(itenPedido.valorTotal);
                                if (itenPedido.itemGroupPedidos.get(position).valorInicial <= 0) {
                                    ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.GONE);
                                } else {
                                    ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {   //remove
                            int indexRemove = 0;
                            for (int i = 0; i < tempRemove.length(); i++) {
                                try {
                                    if (tempRemove.getJSONObject(i).getInt("id_item") == item.getInt("id_item")) {
                                        indexRemove = i;
                                        break;
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            ((LineSimpleItemHolder) (holder)).itensCheck.remove(indexRemove);

                            try {
                                itenPedido.valorTotal -= item.getDouble("value");
                                itenPedido.itemGroupPedidos.get(position).valorInicial -= item.getDouble("value");
                                detalheItemPopUpActivity.updateValueButton(itenPedido.valorTotal);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        itenPedido.itemGroupPedidos.get(position).itemSelectedsCheck = ((LineSimpleItemHolder) (holder)).itensCheck;
                        if (enabledButton() == true) {
                            detalheItemPopUpActivity.enableBtnAddItem();
                        }

                        ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setText(("+ R$ " + itenPedido.itemGroupPedidos.get(position).valorInicial + "0").replace(".", ","));
                        if (itenPedido.itemGroupPedidos.get(position).valorInicial <= 0) {
                            ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.GONE);
                        } else {
                            ((LineSimpleItemHolder) (holder)).textTotalAdicionais.setVisibility(View.VISIBLE);
                        }
                        ((LineSimpleItemHolder) (holder)).textSelectedRadio.setText("");
                        JSONArray temp = ((LineSimpleItemHolder) (holder)).itensCheck;
                        for (int i = 0; i < temp.length(); i++) {

                            try {
                                if(((LineSimpleItemHolder) (holder)).textSelectedRadio.getText().toString().length()<=0){
                                    ((LineSimpleItemHolder) (holder)).textSelectedRadio.setText(  " " + temp.getJSONObject(i).getString("nome"));
                                }else{
                                    ((LineSimpleItemHolder) (holder)).textSelectedRadio.setText(((LineSimpleItemHolder) (holder)).textSelectedRadio.getText().toString() + ", " + temp.getJSONObject(i).getString("nome"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                });

                ((LineSimpleItemHolder) (holder)).recycleListItens.setAdapter(lineDetailViewItemAdapter);
            } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    @Override
    public int getItemCount() {
        return listGroupItens.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

        private RecyclerView recycleListItens;
        private CardView cv;
        private TextView txt_nome_item;
        private TextView txtLabel;
        private ImageView imageDownItem;
        private TextView qtdMax;
        private TextView qtdMin;
        private int id;
        private CardView cardDownItem;
        private CardView cardBody;
        private CardView cardDetailItemDemandFinalize;
        private TextView textSelectedRadio;
        private TextView textTotalAdicionais;

        private JSONArray itensCheck = new JSONArray();
        private TextView textErro;
        private CheckGroup checkGroup;
        private RadioGroup radioGroup;


        public LineSimpleItemHolder(View itemView) {
            super(itemView);
            textSelectedRadio = itemView.findViewById(R.id.textSelectedRadio);
            cardBody = itemView.findViewById(R.id.GIcardBody);
            imageDownItem = itemView.findViewById(R.id.imageDownItem);
            txtLabel = itemView.findViewById(R.id.txtLabel);
            cardDownItem = itemView.findViewById(R.id.cardDownItem);
            txt_nome_item = itemView.findViewById(R.id.textNomeGroupItem);
            recycleListItens = itemView.findViewById(R.id.recyclerListItem);
            textTotalAdicionais = itemView.findViewById(R.id.textTotalAdicionais);

            LinearLayoutManager llm = new LinearLayoutManager(context);
            recycleListItens.setLayoutManager(llm);
            cardDownItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    downUpCard(view);
                }
            });

        }

        private void downUpCard(View view) {
            if (cardBody.getVisibility() == View.GONE) {
                ObjectAnimator.ofFloat(cardDownItem, "rotation", 180, 360).start();
                cardBody.setVisibility(View.VISIBLE);
                cardBody.animate()
                        .alpha(1f)
                        .setDuration(500)
                        .setListener(null);
            } else if (cardBody.getVisibility() == View.VISIBLE) {
                ObjectAnimator.ofFloat(cardDownItem, "rotation", 0, 180).start();
                cardBody.animate()
                        .alpha(0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                cardBody.setVisibility(View.GONE);
                            }
                        });
            }
        }


    }


    class LineObservation extends RecyclerView.ViewHolder {

        EditText detailObservation;

        public LineObservation(View itemView) {
            super(itemView);

            detailObservation = itemView.findViewById(R.id.detailObservation);
            detailObservation.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    itenPedido.valObservacao = detailObservation.getText().toString();
                }
            });
        }

    }

    public interface onItemClickCheckListner {
        void onItemClick(int itemCardapio);
    }


    public boolean containsCheckAndRadio() {

        boolean check = false;
        boolean radio = false;

        for (int i = 0; i < itenPedido.itemGroupPedidos.size(); i++) {
            if (itenPedido.itemGroupPedidos.get(i).typeComponent.equals("check")) {
                check = true;
                break;
            }
        }

        for (int i = 0; i < itenPedido.itemGroupPedidos.size(); i++) {
            if (itenPedido.itemGroupPedidos.get(i).typeComponent.equals("radio")) {
                radio = true;
                break;
            }
        }

        return radio && check;
    }

    public boolean containsOnlyCheck() {

        boolean check = false;

        for (int i = 0; i < itenPedido.itemGroupPedidos.size(); i++) {
            if (itenPedido.itemGroupPedidos.get(i).typeComponent.equals("check")) {
                check = true;
                break;
            }
        }

        return check;
    }


    public boolean containsOnlyRadio() {

        boolean radio = false;

        for (int i = 0; i < itenPedido.itemGroupPedidos.size(); i++) {
            if (itenPedido.itemGroupPedidos.get(i).typeComponent.equals("radio")) {
                radio = true;
                break;
            }
        }

        return radio;
    }

    public boolean enabledButton() {

        boolean btnEnabledCheck = true;
      //  boolean btnEnabledRadio = true;

        for (int i = 0; i < itenPedido.itemGroupPedidos.size(); i++)
            if (itenPedido.itemGroupPedidos.get(i).typeComponent.equals("check")) {

                if (itenPedido.itemGroupPedidos.get(i).itemSelectedsCheck == null) {
                    itenPedido.itemGroupPedidos.get(i).itemSelectedsCheck = new JSONArray();
                }

                if (itenPedido.itemGroupPedidos.get(i).itemSelectedsCheck != null) {
                    if ((itenPedido.itemGroupPedidos.get(i).qtdMin == 0 || itenPedido.itemGroupPedidos.get(i).itemSelectedsCheck.length() >= itenPedido.itemGroupPedidos.get(i).qtdMin)
                            && (itenPedido.itemGroupPedidos.get(i).qtdMax == 0 || itenPedido.itemGroupPedidos.get(i).itemSelectedsCheck.length() <= itenPedido.itemGroupPedidos.get(i).qtdMax)
                    ) {
                       // btnEnabledCheck = true;
                    } else {
                        btnEnabledCheck = false;
                        break;
                    }

                } else {
                    btnEnabledCheck = false;
                    break;
                }
            }else{

                if (itenPedido.itemGroupPedidos.get(i).itemSelectedRadio == null) {
                    btnEnabledCheck = false;
                    break;
                }
            }


        /*if (containsCheckAndRadio()) {
            return btnEnabledCheck && btnEnabledRadio;
        } else if (containsOnlyCheck()) {
            return btnEnabledCheck;
        } else if (containsOnlyRadio()) {
            return btnEnabledRadio;
        } else {
            return true;
        }*/

        return btnEnabledCheck;

    }


    void initializePedido(JSONArray listGroupItens) {
        for (int i = 0; i < listGroupItens.length(); i++) {
            try {

                if (listGroupItens.getJSONObject(i).getInt("qtdMax") == 1 && (listGroupItens.getJSONObject(i).getInt("qtdMin")) == 1) {
                    GroupItemPedido groupItemPedido = new GroupItemPedido();
                    groupItemPedido.typeComponent = "radio";
                    groupItemPedido.qtdMax = listGroupItens.getJSONObject(i).getInt("qtdMax");
                    groupItemPedido.qtdMin = listGroupItens.getJSONObject(i).getInt("qtdMin");
                    groupItemPedido.idGroupItem = listGroupItens.getJSONObject(i).getString("id_categoria");
                    groupItemPedido.enabled = false;
                    groupItemPedido.valorInicial = 0;
                    itenPedido.itemGroupPedidos.add(groupItemPedido);
                } else {
                    GroupItemPedido groupItemPedido = new GroupItemPedido();
                    groupItemPedido.typeComponent = "check";
                    groupItemPedido.qtdMax = listGroupItens.getJSONObject(i).getInt("qtdMax");
                    groupItemPedido.qtdMin = listGroupItens.getJSONObject(i).getInt("qtdMin");
                    groupItemPedido.idGroupItem = listGroupItens.getJSONObject(i).getString("id_categoria");
                    groupItemPedido.enabled = false;
                    groupItemPedido.valorInicial = 0;
                    itenPedido.itemGroupPedidos.add(groupItemPedido);
                }

            } catch (JSONException e) {

            }

        }

        if (enabledButton()) {
            detalheItemPopUpActivity.enableBtnAddItem();
        }

    }


}