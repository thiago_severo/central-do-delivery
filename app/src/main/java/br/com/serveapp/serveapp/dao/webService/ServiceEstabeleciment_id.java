package br.com.serveapp.serveapp.dao.webService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceEstabeleciment_id extends OnPostResponse {

    LineFavoritEstabelecimentAdapter.LineFavoritHolder line;
    String id_end_cliente;

    public ServiceEstabeleciment_id(String id, String id_end_cliente, LineFavoritEstabelecimentAdapter.LineFavoritHolder line) {
        setId(id);
        this.line = line;
        this.id_end_cliente = id_end_cliente;
        this.setAutoReconnect(true);
        this.setSaveCache(true);
        this.script = "estabelecimento_id.php";
    }

    @Override
    public Map<String, String> getParam() {
        Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_cliente", Repository.getIdUsuario(line.itemView.getContext()));
        paramLogin.put("id_estabelecimento", getId());
        paramLogin.put("id_end_cliente", id_end_cliente);
        return paramLogin;
    }

    @Override
    public void onResponse(String response) {

    }


    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            line.set(jsonObject.getJSONObject("result"));
        } catch (JSONException e) {
        //    e.printStackTrace();
        }
    }


    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                line.set(jsonObject.getJSONObject("result"));
            } catch (JSONException e) {
             //   e.printStackTrace();
            }
        }
    }

}
