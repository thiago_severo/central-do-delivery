package br.com.serveapp.serveapp.drawer;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.SplashScreenHomeApp;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.repository.objects.Repository;

import org.json.JSONException;
import org.json.JSONObject;

public class SplashSetImageCast extends SetImageWebBipMap {

    ImageView imageView;
    ProgressBar progressBar;
    Context context;
    SplashScreenHomeApp splashScreenHomeApp;

    public SplashSetImageCast(SplashScreenHomeApp splashScreenHomeApp, Context context, ImageView imageView, ProgressBar progressBar) {
        super(context);
        this.context = context;
        this.imageView = imageView;
        this.progressBar = progressBar;
        this.splashScreenHomeApp = splashScreenHomeApp;
    }

    public void loadWeb(){

        new Handler(Looper.getMainLooper()).post(new Runnable(){
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    protected void onPostExecute(final Bitmap result) {

        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
                imageView.setImageBitmap(result);
            }
        });
        final String returnUser = Dao.read(FilesName.user, context);

        try {
            JSONObject jsonObject = new JSONObject(returnUser);
            Repository.setUser(jsonObject);
        } catch (JSONException e) {
        }



    }

}