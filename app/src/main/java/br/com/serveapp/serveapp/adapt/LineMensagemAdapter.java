package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineMensagemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    Context context;
    public LineMensagemAdapter(Context context, JSONArray listViewItem) {
        this.listViewItem = listViewItem;
        this.context = context;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (listViewItem.getJSONObject(position).getString("sentby").equals("c")) {
                return 0;
            } else {
                return 1;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return super.getItemViewType(position);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;

        if (viewType == 0) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_line_mensagem, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolderClient(v);
        } else if (viewType == 1) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.estab_line_mensagem, parent, false);
            lineSimpleItemHolder = new LineSimpleItemHolderEstab(v);
        }

        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            if(holder instanceof LineSimpleItemHolderClient) {
                new ListSetImage(this.context, ((LineSimpleItemHolderClient) holder).imageViewConfirmMsg).execute(listViewItem.getJSONObject(position).getString("status"));
                ((LineSimpleItemHolderClient) holder).text_mensagem.setText(listViewItem.getJSONObject(position).getString("texto"));
                ((LineSimpleItemHolderClient) holder).hora_mensagem.setText(listViewItem.getJSONObject(position).getString("data"));
            }else if(holder instanceof LineSimpleItemHolderEstab){
                ((LineSimpleItemHolderEstab) holder).text_mensagem.setText(listViewItem.getJSONObject(position).getString("texto"));
                ((LineSimpleItemHolderEstab) holder).hora_mensagem.setText(listViewItem.getJSONObject(position).getString("data"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public interface OnItemClickListner {
        void onItemClick(JSONObject item);
    }

    class LineSimpleItemHolderClient extends RecyclerView.ViewHolder {

        public TextView text_mensagem;
        public TextView hora_mensagem;
        public ImageView imageViewConfirmMsg;

        public LineSimpleItemHolderClient(View itemView) {
            super(itemView);
            imageViewConfirmMsg = itemView.findViewById(R.id.imageViewConfirmMsg);
            text_mensagem = itemView.findViewById(R.id.textMensagem);
            hora_mensagem = itemView.findViewById(R.id.horaMensagem);

        }
    }

    class LineSimpleItemHolderEstab extends RecyclerView.ViewHolder {

        public TextView text_mensagem;
        public TextView hora_mensagem;
        public LineSimpleItemHolderEstab(View itemView) {
            super(itemView);
            text_mensagem = itemView.findViewById(R.id.textMensagem);
            hora_mensagem = itemView.findViewById(R.id.horaMensagem);

        }
    }

}