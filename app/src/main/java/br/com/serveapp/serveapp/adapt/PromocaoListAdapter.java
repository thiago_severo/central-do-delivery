package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class PromocaoListAdapter extends RecyclerView.Adapter<PromocaoListAdapter.LineSimpleItemHolder>{

    List<Categoria> categoriaList;
    JSONArray destaquesList;
    private PromocaoListAdapter.OnItemClickListner listener;
    private Context context;
    public PromocaoListAdapter(Context context, JSONArray destaquesList){
        this.destaquesList = destaquesList;
        this.context = context;
    }

    @NonNull
    @Override
    public LineSimpleItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_item_promotion, parent, false);
        LineSimpleItemHolder lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LineSimpleItemHolder holder, int position) {

        try {
            holder.txt_item.setText(destaquesList.getJSONObject(position).getString("nome"));
            holder.txt_value_promo.setText(destaquesList.getJSONObject(position).getString("valor"));
            holder.txt_perc_descont.setText(destaquesList.getJSONObject(position).getString("desconto"));
            new ListSetImage(context, holder.imageItemPromo).execute(destaquesList.getJSONObject(position).getString("foto"));

            if(destaquesList.getJSONObject(position).isNull("desconto") ||
                    destaquesList.getJSONObject(position).getString("desconto").equals("null")){
                    holder.imageCupomDesconto.setVisibility(View.GONE);
           }
            else {
                holder.imageCupomDesconto.setVisibility(View.VISIBLE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
     /*   Resources res = context.getResources();
        String mDrawableName = (categoriaList.get(position).getLogo());
        int resID = res.getIdentifier(mDrawableName , "drawable", context.getPackageName());
        Drawable drawable = res.getDrawable(resID );
        holder.imageCateg.setImageDrawable(drawable );
*/
    }

    @Override
    public int getItemCount() {
        return destaquesList.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder{

        private CardView cv;
        public TextView txt_item;
        public TextView txt_value_promo;
        public TextView txt_perc_descont;
        public ImageView imageItemPromo;
        public ImageView imageCupomDesconto;

        public LineSimpleItemHolder(View itemView){
            super(itemView);

            txt_item = itemView.findViewById(R.id.textNamDestaqueHome);
            txt_value_promo = itemView.findViewById(R.id.textPriceDestaque);
            txt_perc_descont = itemView.findViewById(R.id.textCupomDescont);
            imageCupomDesconto = itemView.findViewById(R.id.imageCupomDescont);
            imageItemPromo = itemView.findViewById(R.id.image);

   //         imageCateg = itemView.findViewById(R.id.imagemCategorieItemListHome);
    /*        itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener!=null && position!=RecyclerView.NO_POSITION)
                        listener.onItemClick(categoriaList.get(position));
                }
            });
*/
        }
    }

    public interface OnItemClickListner{
        void onItemClick(Categoria categoria);
    }

    public void setOnItemClickListener(PromocaoListAdapter.OnItemClickListner listener){
        this.listener = listener;
    }

}