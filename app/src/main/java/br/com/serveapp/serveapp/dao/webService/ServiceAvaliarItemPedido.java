package br.com.serveapp.serveapp.dao.webService;

import android.app.Activity;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.fragments.AvaliacaoItemInterface;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceAvaliarItemPedido extends OnPostResponse {

    AvaliacaoItemInterface fragmentListDemandsEstablishment;
    Activity activity;
    public static String position;

    public ServiceAvaliarItemPedido(AvaliacaoItemInterface fragmentListDemandsEstablishment, Activity activity) {
        this.activity = activity;
        this.fragmentListDemandsEstablishment = fragmentListDemandsEstablishment;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "select_pedidos_do_estab.php";

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(activity, "erro de conexão", Toast.LENGTH_LONG).show();
    }

        @Override
        public void onResponse (String response){
            fragmentListDemandsEstablishment.dimissPopUpAvaliacao();
            fragmentListDemandsEstablishment.dimissProgressAvaliacaoItem();
        }

        @Override
        public void onUpdate (String response){
            if (response != null && !response.equals("null"))
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray result = jsonObject.getJSONArray("result");
                    fragmentListDemandsEstablishment.initializeAdapterListItens(result, position);
                    fragmentListDemandsEstablishment.dimissProgressAvaliacaoItem();
                    position = null;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }

    }