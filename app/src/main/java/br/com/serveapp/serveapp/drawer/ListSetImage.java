package br.com.serveapp.serveapp.drawer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import br.com.serveapp.serveapp.dao.web.Script;

public class ListSetImage{

    ImageView imageView;
    Context context;
    public static int TIME_SHORT = 500;
    public static int TIME_LONG = 1000;
    public static int TIME_NONE = 0;

    private boolean local = false;
    private boolean key = false;
    private int time;
    public ListSetImage( Context context, ImageView imageView) {
        this(context,imageView,TIME_LONG);
    }

    public ListSetImage( Context context, ImageView imageView, int time) {
        this.context = context;
        this.imageView = imageView;
        this.time = time;
    }


    public void execute(String imageName){
        String imageURL = Script.image+imageName;
        Picasso.get().load(imageURL).fit()
                .centerInside()
                .into(imageView);
    }

    public void execute(String imageName, final View progress){
        String imageURL = Script.image+imageName;
        //progress.setVisibility(View.GONE);
        Picasso.get().load(imageURL).fit()
                .centerInside()
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        (imageView).setAlpha(0f);
                        (imageView).animate().setDuration(time).alpha(1f).start();
                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError(Exception e) {
                        //e.printStackTrace();
                        //progress.setVisibility(View.GONE);
                        //Toast.makeText(progress.getContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    }
                });
    }

    protected void onPostExecute(final Bitmap result) {

        imageView.setImageBitmap(result);

    }

    private static final float BITMAP_SCALE = 0.04f;
    private static final float BLUR_RADIUS = 25.0f;

    public Bitmap blur(Bitmap image) {
        if (null == image) return null;
        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(context);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);
        //Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    @SuppressLint("NewApi")
    public static Bitmap blur(Context context, Bitmap image) {
        int width = Math.round(image.getWidth() * BITMAP_SCALE);
        int height = Math.round(image.getHeight() * BITMAP_SCALE);

        Bitmap inputBitmap = Bitmap.createScaledBitmap(image, width, height,
                false);
        Bitmap outputBitmap = Bitmap.createBitmap(inputBitmap);

        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(rs,
                Element.U8_4(rs));
        Allocation tmpIn = Allocation.createFromBitmap(rs, inputBitmap);
        Allocation tmpOut = Allocation.createFromBitmap(rs, outputBitmap);
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);

        return outputBitmap;
    }

}