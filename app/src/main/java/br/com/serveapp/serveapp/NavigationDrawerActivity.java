package br.com.serveapp.serveapp;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.AdapterHorizontalCategories;
import br.com.serveapp.serveapp.adapt.AdapterHorizontalPromotions;
import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.web.Script;
import br.com.serveapp.serveapp.dao.webService.ServiceEstabeleciment;
import br.com.serveapp.serveapp.drawer.ListSetImage2;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.repository.objects.Variables;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static android.graphics.Color.WHITE;

public class NavigationDrawerActivity extends RequestActivity
        implements ZXingScannerView.ResultHandler {

    private final static int RequestAddPhone = 11;
    private final static int requestForAddressNull = 9;

    boolean flash;
    boolean breakAutoScroll;
    int position = 0;
    private JSONArray listEstabeleciment;
    private JSONArray jsonFilter;
    private RecyclerView listRecycleCategories;
    private RecyclerView listRecyclePromotions;

    public ProgressDialog mProgress;

    private Request requestEstabeleciment;
    String idEndSelected;
    String descEndSelected;
    String idMunicipio;
    private String filter;

    ProgressBar progressBar;
    BottomNavigationView bottomNavigationView;
    private static RecyclerView listFilterEstabeleciments;
    private static LineFavoritEstabelecimentAdapter lineAdapter;
    TextView textEnderecoDelivery;
    public static NavigationDrawerActivity refference;

    CoordinatorLayout mCoordinatorLayour;
    AppBarLayout mAppBarLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    CardView cardChangeLocalEntregaPedido;

    private LinearLayoutManager linearLayoutManager2;
    public TextView textTitleEmptyEstab;
    public TextView textBodyEmptyEstab;
    private ImageView serveLogo;
    private ImageView floatingActionButtonCallQr;
    private CardView fab;

    public void initializeCategorias(JSONArray listCategorias, JSONObject promotions) {

        final AdapterHorizontalCategories adapterHorizontalCategories = new AdapterHorizontalCategories(getApplicationContext(), listCategorias);
        adapterHorizontalCategories.setListenerClickCategories(new AdapterHorizontalCategories.ListenerClickCategories() {
            @Override
            public void clickCategorie(String categorie) {
                filterCategorias(categorie);
            }
        });

        final AdapterHorizontalPromotions adapterHorizontalPromotions = new AdapterHorizontalPromotions(getApplicationContext(), promotions);


        listRecycleCategories.setAdapter(adapterHorizontalCategories);
        listRecyclePromotions.setAdapter(adapterHorizontalPromotions);
        listRecyclePromotions.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                //position = newState;
                position = linearLayoutManager2.findFirstVisibleItemPosition();
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                breakAutoScroll = dx!=0;
            }
        });

        final int tempoDeEspera = 4000;
        new Thread(new Runnable() {
            @Override
            public void run() {
                SystemClock.sleep(tempoDeEspera);
                while(true){
                    if(linearLayoutManager2!=null && !breakAutoScroll){
                        //int position = linearLayoutManager2.findFirstVisibleItemPosition()+1;
                        position++;
                        int size = adapterHorizontalPromotions.getItemCount();
                        if(position>=size){ position = 0; }
                        //listRecyclePromotions.position(position);
                        listRecyclePromotions.smoothScrollToPosition(position);
                       // listRecyclePromotions.findViewHolderForLayoutPosition(position);
                        // linearLayoutManager2.position (position);
                        //listRecyclePromotions.findViewHolderForLayoutPosition(1);
                        //linearLayoutManager2.scrollToPositionWithOffset(linearLayoutManager2.findFirstVisibleItemPosition(),1);
                    }
                    breakAutoScroll = false;
                    SystemClock.sleep(tempoDeEspera);
                }

            }
        }).start();



       // listEstabeleciment.getJSONObject(i).getString("tipo_estabelecimento")

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);

        listRecycleCategories = findViewById(R.id.listRecycleCategories);
        listRecyclePromotions = findViewById(R.id.listRecyclePromotions);
        textBodyEmptyEstab = findViewById(R.id.textBodyEmptyEstab);
        textTitleEmptyEstab = findViewById(R.id.textTitleEmptyEstab);
        swipeRefreshLayout = findViewById(R.id.swiperefreshEstabeleciments);
        cardChangeLocalEntregaPedido = findViewById(R.id.cardChangeLocalEntregaPedido);
        mCoordinatorLayour = (CoordinatorLayout) findViewById(R.id.coordinatorLayout3);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.appBarLayout);
        textEnderecoDelivery = findViewById(R.id.textShowAddressDelivery);
        progressBar = findViewById(R.id.progressBar3);
        serveLogo = findViewById(R.id.serveLogo);

        floatingActionButtonCallQr = findViewById(R.id.floatingActionButtonCallQr);

        fab = NavigationDrawerActivity.this.findViewById(R.id.cardFab);

        floatingActionButtonCallQr.setImageResource(R.drawable.qrcode);
        textBodyEmptyEstab.setVisibility(View.GONE);
        textTitleEmptyEstab.setVisibility(View.GONE);

        floatingActionButtonCallQr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), ExpandableScanner.class));
            }
        });

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(RecyclerView.HORIZONTAL);

        listRecycleCategories.setLayoutManager(linearLayoutManager);

        linearLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager2.setOrientation(RecyclerView.HORIZONTAL);

        listRecyclePromotions.setLayoutManager(linearLayoutManager2);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        update();
                    }
                }
        );

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        String token = task.getResult().getToken();
                        String msg = getString(R.string.fcm_token, token);
                        Log.d("MYFIREBASE", msg);
                    }
                });

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        requestEstabeleciment = new Request(getApplicationContext(), new ServiceEstabeleciment(progressBar, this, swipeRefreshLayout));
        listFilterEstabeleciments = findViewById(R.id.recyclerFilterEstabeleciments);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        listFilterEstabeleciments.setLayoutManager(llm);

        listFilterEstabeleciments.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(dy>0){
                    fab.setVisibility(View.GONE);
                    floatingActionButtonCallQr.setVisibility(View.GONE);
                }
                if(dy<0){
                    fab.setVisibility(View.VISIBLE);
                    floatingActionButtonCallQr.setVisibility(View.VISIBLE);
                }

            }
        });

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        List list = new ArrayList<BarcodeFormat>();
        list.add(BarcodeFormat.QR_CODE);

        toolbar.getNavigationIcon().setColorFilter(WHITE, PorterDuff.Mode.MULTIPLY);

        MyNavigationNew.setButtonsAction(this);
        final SearchView searchView = findViewById(R.id.searchEstabeleciements);
        SearchManager searchManager = (SearchManager) this.getSystemService(Context.SEARCH_SERVICE);
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                filterProducts(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                filterProducts(newText);
                return true;
            }
        });


      //  new ListSetImage2(getApplicationContext(), serveLogo).execute("system/central_toolbar.png");


        mProgress = new ProgressDialog(this);
        String titleId = "Entrando...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("verificando conexão com a internet...");

        cardChangeLocalEntregaPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                cardChangeLocalEntregaPedido.setEnabled(false);
                Intent openAdddress = new Intent(getApplicationContext(), ChangeAddressPopUpActivity.class);
                openAdddress.putExtra("id_endereco", idEndSelected);
                openAdddress.putExtra("text_endereco", descEndSelected);
                startActivityForResult(openAdddress, 0);
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);

            }
        });

        final Map<String, String> paramLogin = new HashMap<>();
        try {
            paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
            paramLogin.put("id_end_cliente", Repository.getUser(getApplicationContext()).getString("id_endereco"));
            paramLogin.put("function_method", "list");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        refference = this;

        verifyAddress();
        verifiyPhone();

        progressBar.setVisibility(View.VISIBLE);
        requestEstabeleciment.request(paramLogin);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new MyNavigationMenu(this, navigationView));

       /* if(Repository.qtdCart>0){
            MyNavigationMenu.a(bottomNavigationView, this);
        }*/

    }

    public void setQtdCart() {
        TextView badge = findViewById(R.id.badge);

        if(Repository.getQtdCart()>0) {
            badge.setText(Repository.getQtdCart()+"");
            badge.setVisibility(View.VISIBLE);
        }else{
            badge.setText("");
            badge.setVisibility(View.GONE);
        }
    }

    public void verifiyPhone() {

        try {

            JSONObject user = Repository.getUser(this);
            if (user.isNull("telefone") || user.getString("telefone").length() < 13) {
                Intent addPhone = new Intent(getApplicationContext(), CadastroTelefoneActivityForLoginApi.class);
                startActivityForResult(addPhone, RequestAddPhone);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void verifyAddress() {

        try {

            JSONObject user = Repository.getUser(this);

            if (user.isNull("id_endereco") && user.isNull("id_municipio")) {

                Intent addAddress = new Intent(getApplicationContext(), SearchAddressActivity.class);
                addAddress.putExtra("createUser", "y");
                addAddress.putExtra("requestForAddressNull", "y");
                startActivityForResult(addAddress, requestForAddressNull);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void updateAddress(String textAddress, String idSelected) {
        this.idEndSelected = idSelected;
        this.descEndSelected = textAddress;
        textEnderecoDelivery.setText(descEndSelected);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        refference = this;
        setQtdCart();


        /*        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {

        } else {
            ActivityCompat.requestPermissions(NavigationDrawerActivity.this, new String[]{Manifest.permission.CAMERA}, 1024);
        }
        int i = 0;

*/

        /*if(Repository.qtdCart>0){
            MyNavigationMenu.a(bottomNavigationView, this);
        }*/

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RequestAddPhone) {
            if (resultCode == RESULT_OK) {
                verifyAddress();
            }
        } else if (requestCode == requestForAddressNull) {

            final Map<String, String> paramUpdate = new HashMap<>();

            try {

                paramUpdate.put("id_municipio", "4");
                paramUpdate.put("id_endereco", getIntent().getStringExtra("id_endereco"));
                paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                paramUpdate.put("function_method", "set_end");

            } catch (Exception e) {
                e.printStackTrace();
            }

            progressBar.setVisibility(View.VISIBLE);
            requestEstabeleciment.request(paramUpdate);

        } else {
            if (resultCode == Variables.EXIT_APPLICATION) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                finish();
            }
            if (resultCode == Activity.RESULT_OK) {

                try {

                    cardChangeLocalEntregaPedido.setEnabled(true);
                    JSONObject jsonObject = new JSONObject(data.getStringExtra("endereco"));
                    descEndSelected = jsonObject.getString("titulo");
                    idEndSelected = jsonObject.getString("_id");
                    if (!jsonObject.isNull("id_municipio")) {
                        idMunicipio = jsonObject.getString("id_municipio");
                    }
                    textEnderecoDelivery.setText(descEndSelected);
                    Map<String, String> paramUpdate = new HashMap<>();
                    if (idMunicipio != null)

                        paramUpdate.put("id_municipio", idMunicipio);
                    paramUpdate.put("id_endereco", idEndSelected);
                    paramUpdate.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                    paramUpdate.put("function_method", "set_end");
                    progressBar.setVisibility(View.VISIBLE);
                    requestEstabeleciment.request(paramUpdate);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }

    }

    private void update() {
        Map<String, String> param = new HashMap<>();

        try {
            param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
            param.put("id_end_cliente", Repository.getUser(getApplicationContext()).getString("id_endereco"));
            param.put("function_method", "list");

            try {
                requestEstabeleciment.request(param);
            } catch (NullPointerException e2) {
                requestEstabeleciment = new Request(getApplicationContext(), new ServiceEstabeleciment(progressBar, this, swipeRefreshLayout));
                requestEstabeleciment.request(param);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 100ms
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 500);*/
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(getApplicationContext(), "Verifique sua conexão com a internet", Toast.LENGTH_LONG).show();
        mProgress.dismiss();
    }


    @Override
    public void onResponseServe(String response) {
        if (!response.equals("null")) {
            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                Repository.idEstabelecimento = result.getString("id_estabelecimento");
                Repository.atendimentoLocal = true;
                Intent intentOpenServiceLocal = new Intent(getApplicationContext(), SplashScreenEstabeleciment.class);
                intentOpenServiceLocal.putExtra("foto", result.getString("foto"));
                intentOpenServiceLocal.putExtra("slogan", result.getString("slogan"));
                intentOpenServiceLocal.putExtra("nome", result.getString("nome"));

                intentOpenServiceLocal.putExtra("tipoAtendimento", "local");
                startActivity(intentOpenServiceLocal);
                mProgress.dismiss();

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

            }
        }

    }

    @Override
    public void onCache(String response) {

    }

    private void filterProducts(String filter) {
        this.filter = filter;
        try {
            jsonFilter = new JSONArray();
            for (int i = 0; i < listEstabeleciment.length(); i++) {
                JSONObject tempEstab = listEstabeleciment.getJSONObject(i);
                if (filter(filter, tempEstab)) {
                    jsonFilter.put(tempEstab);
                }
            }
            updateList(jsonFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void filterCategorias(String filter) {
        this.filter = filter;
        try {
            jsonFilter = new JSONArray();
            for (int i = 0; i < listEstabeleciment.length(); i++) {
                JSONObject tempEstab = listEstabeleciment.getJSONObject(i);
                String id = listEstabeleciment.getJSONObject(i).getString("cat");
                if (filter.equals(id) || filter.equals("0")) {
                    jsonFilter.put(tempEstab);
                }
            }
            updateList(jsonFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void initializeAdapter(JSONArray listEstabeleciment, JSONArray listCategorias, JSONObject listPromotions) {
        this.listEstabeleciment = listEstabeleciment;
        updateList(listEstabeleciment);
        initializeCategorias(listCategorias, listPromotions);
    }

    public void updateList(JSONArray listEstabeleciment) {

        if (listEstabeleciment.length() > 0) {
            textBodyEmptyEstab.setVisibility(View.GONE);
            textTitleEmptyEstab.setVisibility(View.GONE);
        } else {
            textBodyEmptyEstab.setVisibility(View.VISIBLE);
            textTitleEmptyEstab.setVisibility(View.VISIBLE);
        }

        lineAdapter = new LineFavoritEstabelecimentAdapter(getApplicationContext(), listEstabeleciment);
        listFilterEstabeleciments.setAdapter(lineAdapter);

    }

    @Override
    public void handleResult(Result scanResult) {
        mProgress.show();
        String id_cliente = Repository.getIdUsuario(getApplicationContext());
        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("function_method", "check-in");
        paramLogin.put("id_mesa", scanResult.getText().toString());
        paramLogin.put("instructionId", "check-in");
        paramLogin.put("id_cliente", id_cliente);
        request(Script.check_in, paramLogin);

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }


    private void setAppBarOffset(int offsetPx) {
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) mAppBarLayout.getLayoutParams();
        AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
        behavior.onNestedPreScroll(mCoordinatorLayour, mAppBarLayout, null, 0, offsetPx, new int[]{0, 0});
    }

    private boolean filter(String filter, JSONObject estabelecimento) {
        double value = 0;
        try {
            filter = filter.toUpperCase();
            String[] f = filter.split(" ");
            /*String temp = (estabelecimento.getString("nome") + " "
                    + estabelecimento.getString("tipo_estabelecimento") + " "
                    + estabelecimento.getString("tipo_atendimento") + " "
                    + estabelecimento.getString("descricao")).toUpperCase();*/
            String temp = estabelecimento.getString("nome").toUpperCase();
            for (String s : f) {
                if (!temp.contains(s))
                    return false;
                else {
                    temp = temp.replaceFirst(s, "");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return true;
    }


}