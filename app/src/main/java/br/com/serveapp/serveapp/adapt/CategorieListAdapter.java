package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.cardapio.Categoria;
import br.com.serveapp.serveapp.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategorieListAdapter extends RecyclerView.Adapter<CategorieListAdapter.LineSimpleItemHolder>{


    List<Categoria> categoriaList;
    private CategorieListAdapter.OnItemClickListner listener;
    private Context context;
    public CategorieListAdapter(Context context, List<Categoria> categorias){
        categoriaList = categorias;
        this.context = context;
    }


    @NonNull
    @Override
    public LineSimpleItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_categorie_item, parent, false);
        LineSimpleItemHolder lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LineSimpleItemHolder holder, int position) {
            holder.txt_nome_categ.setText(categoriaList.get(position).getNome());

        Resources res = context.getResources();
        String mDrawableName = (categoriaList.get(position).getLogo());
        int resID = res.getIdentifier(mDrawableName , "drawable", context.getPackageName());
        Drawable drawable = res.getDrawable(resID );
        holder.imageCateg.setImageDrawable(drawable );


    }

    @Override
    public int getItemCount() {
        return categoriaList.size();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder{

        private CardView cv;
        public TextView txt_nome_categ;
        public CircleImageView imageCateg;

        public LineSimpleItemHolder(View itemView){
            super(itemView);
            txt_nome_categ = itemView.findViewById(R.id.textNamDestaqueHome);
            imageCateg = itemView.findViewById(R.id.image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener!=null && position!=RecyclerView.NO_POSITION)
                        listener.onItemClick(categoriaList.get(position));
                }
            });

        }
    }


    public interface OnItemClickListner{
        void onItemClick(Categoria categoria);
    }

    public void setOnItemClickListener(CategorieListAdapter.OnItemClickListner listener){
        this.listener = listener;
    }

}
