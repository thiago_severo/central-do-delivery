package br.com.serveapp.serveapp.fragments.ifood;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.serveapp.serveapp.R;

public class GroupLine extends RecyclerView.ViewHolder {

    public TextView title;
    public RecyclerView groupList;
    JSONObject item;
    LinearLayoutManager layoutManager;
    Fragment fragment;
    Activity activity;

    public void setLayoutManager(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    public GroupLine(View itemView) {
        super(itemView);
        title = itemView.findViewById(R.id.groupTitle);
        groupList = itemView.findViewById(R.id.groupList);
    }

    public void initialize(final JSONObject item, Fragment fragment, Activity activity){
        this.item = item;
        this.activity = activity;
        this.fragment = fragment;
        try {
            title.setText(item.getString("nome")+"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setupRecycler();
    }

    private void setupRecycler() {

        // Configurando o gerenciador de layout para ser uma lista.
        final LinearLayoutManager layoutManager = new LinearLayoutManager(groupList.getContext());

        groupList.setLayoutManager(layoutManager);
        final List<Item> mUsers = new ArrayList<>();
        try {
            if(item.getString("orientation").equalsIgnoreCase("horizontal")) {
                String s = item.getString("orientation");
                layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                groupList.setAdapter(new LineHorizontalItemAdapter(item.getJSONArray("itens"), layoutManager, fragment, activity));
            }else{
                groupList.setAdapter(new LineVerticalProductAdapter(item.getJSONArray("itens"), layoutManager, fragment, activity));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
