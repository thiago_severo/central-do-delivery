package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;

public class AdapterGridCategoryItens extends RecyclerView.Adapter<AdapterGridCategoryItens.LineSimpleItemHolder> {

    List<Categoria> categoriaList;
    JSONArray categoriaListObjects;

    private AdapterGridCategoryItens.onItemClickListener listener;
    private Context context;

    public AdapterGridCategoryItens(Context context,  JSONArray categoriaListObjects){
        this.context = context;
        this.categoriaListObjects = categoriaListObjects;
    }

    @NonNull
    @Override
    public LineSimpleItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_grid_item_category_home, parent, false);
        AdapterGridCategoryItens.LineSimpleItemHolder lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull LineSimpleItemHolder holder, int position) {
        try {
            holder.txt_nome_categ.setText(categoriaListObjects.getJSONObject(position).getString("nome") +  Integer.parseInt(categoriaListObjects.getJSONObject(position).getString("_id")));
            holder.id = Integer.parseInt(categoriaListObjects.getJSONObject(position).getString("_id"));
            new ListSetImage(context, holder.imageCateg).execute(categoriaListObjects.getJSONObject(position).getString("foto"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return categoriaListObjects.length();
    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {

            private CardView cv;
            public TextView txt_nome_categ;
            public ImageView imageCateg;
            public int id;

            public LineSimpleItemHolder(View itemView) {
                super(itemView);
                txt_nome_categ = itemView.findViewById(R.id.textNamDestaqueHome);
                txt_nome_categ.getBackground().setAlpha(250);
                imageCateg = itemView.findViewById(R.id.image);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = getAdapterPosition();
                        if (listener != null && position != RecyclerView.NO_POSITION) {
                            try {
                                listener.onItemClick(categoriaListObjects.getJSONObject(position));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });

            }
        }

   public interface onItemClickListener{
       void onItemClick(JSONObject categoria);
   }

    public void setListener(AdapterGridCategoryItens.onItemClickListener listener) {
        this.listener = listener;
    }
}

