package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.SplashScreenEstabeleciment;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceCalDistance;
import br.com.serveapp.serveapp.dao.webService.ServiceEstabeleciment_id;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.entidades.Estabelecimento;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class LineFavoritEstabelecimentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> // implements Filterable
{

    private RecyclerView favoritEstabelecimentList;
    private List<Estabelecimento> listEstabeleciment;
    private JSONArray filtredListEstabeleciment;
    private JSONArray exampleListFull;
    private Request requestCalDistancia;
    private JSONArray jsonArrayEstabeleciment;
    private OnItemClickListner listener;
    private Context context;
    private static List<Estabelecimento> estabelecimentoListST;
    private RecyclerView.ViewHolder linetHolder = null;
    private static final int VIEW_TYPE_DELIVERY = 3;
    private static final int VIEW_TYPE_DELIVERY_LOCAL = 1;
    private static final int LOCAL = 2;
    private Request request;
    private Map<String, String> param;
    boolean isFavorito;
    private HashMap<String, Long> timerMap;
    private static HashMap<String, JSONObject> objMap;

    public LineFavoritEstabelecimentAdapter(Context context, JSONArray estabelecimentoList) {
        jsonArrayEstabeleciment = estabelecimentoList;
        filtredListEstabeleciment = jsonArrayEstabeleciment;
        exampleListFull = jsonArrayEstabeleciment;
        this.context = context;
    }

    public static HashMap<String, JSONObject> getObjMap() {
        if(objMap == null)
            objMap = new HashMap<>();
        return objMap;
    }

    public static void addObjMap(JSONObject obj) {
        try {
            String id = obj.getString("_id");
            LineFavoritEstabelecimentAdapter.getObjMap().put(id, obj);
        } catch (JSONException e) {}
    }

    public JSONObject getObj(String id) {
        if(isRefreshTimer(id)){
            return null;
        }else{
            return getObjMap().get(id);
        }
    }

    private void setTimerMap(String id){
        Long timer = Calendar.getInstance().getTimeInMillis();
        getTimerMap().put(id,timer);
    }

    private boolean isRefreshTimer(String id){
        Long timer = getTimerMap().get(id);
        Long now = Calendar.getInstance().getTimeInMillis();
        Long interval = 10000L;
        if(timer== null || now > (timer+interval)) {
            setTimerMap(id);
            return true;
        }
        else
            return false;
    }

    private HashMap<String, Long> getTimerMap(){
        if(timerMap == null){
            timerMap = new HashMap<>();
        }
        return timerMap;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_estabeleciment, parent, false);
        return new LineFavoritHolder(v);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        try {
            JSONObject obj = jsonArrayEstabeleciment.getJSONObject(position);
//            String nome = obj.getString("nome");
//            String avaliacao = obj.getString("avaliacao");
            ((LineFavoritHolder) (holder)).txt_nome_estabelecimento.setText(obj.getString("nome").toUpperCase());
            request(holder, obj.getString("_id"));
        } catch (Exception e) {
           // e.printStackTrace();
        }
    }

    private void request(RecyclerView.ViewHolder holder, String id){
        JSONObject obj = getObj(id);
        if(obj == null){
            ServiceEstabeleciment_id service = new ServiceEstabeleciment_id(id, "0", ((LineFavoritHolder) holder));
            Request r = new Request(holder.itemView.getContext(), service);
            r.request();
        }else{
            ((LineFavoritHolder) holder).set(obj);
        }

    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return jsonArrayEstabeleciment.length();
    }

    public class LineFavoritHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView central_pay;
        public ImageView aceita_cartao;
        public CardView cv;
        public TextView txt_nome_estabelecimento;
        public TextView txt_desc_estabelecimento;
        public TextView txt_distancia;
        public TextView textValueFrete;
        public TextView textTime;
        public TextView textHorarioDelivery;
        public ImageView imageCateg;
        public ImageView imgPgtoFrete;
        public TextView textViewTypeAtendiment;
        public TextView textViewTypeAtendimentLocal;
        public View labelFlagActiveOnlyDeliveryOrLocal;
        public View labelFlagActiveOnlyLocal;
        public View viewNovo;
        public TextView textNovo;
        CardView cardView;
        public ImageView imageLogoEstabeleciment;

        JSONObject obj;

        public ConstraintLayout conteinerDeliveryIsOpen;
        public ConstraintLayout conteinerLocalIsOpen;
        public ConstraintLayout conteinerStatusLocalDelivery;
        public RatingBar ratingBarLikedListProduct3;

        public ProgressBar progressImage;

        public LineFavoritHolder(@NonNull View itemView) {
            super(itemView);

            cv = itemView.findViewById(R.id.cardEstabelecimentLine);

            central_pay = itemView.findViewById(R.id.central_pay);
            aceita_cartao = itemView.findViewById(R.id.aceita_cartao);
            textViewTypeAtendiment = itemView.findViewById(R.id.textViewTypeAtendiment);
            labelFlagActiveOnlyDeliveryOrLocal = itemView.findViewById(R.id.labelFlagActiveOnlyDeliveryOrLocal);
            labelFlagActiveOnlyLocal = itemView.findViewById(R.id.FlagLocal);
            txt_nome_estabelecimento = itemView.findViewById(R.id.nomeFavEstabelecimento);
            txt_desc_estabelecimento = itemView.findViewById(R.id.descFavEstabelecimento);
            txt_distancia = itemView.findViewById(R.id.localDistance);
            textTime = itemView.findViewById(R.id.tempoEntregaTodos);
            textValueFrete = itemView.findViewById(R.id.textValueFrete);
            imgPgtoFrete = itemView.findViewById(R.id.imgPgtoFrete);
            ratingBarLikedListProduct3 = itemView.findViewById(R.id.ratingBarLikedListProduct3);

            textHorarioDelivery = itemView.findViewById(R.id.textHorarioDelivery);
            textViewTypeAtendiment = itemView.findViewById(R.id.textViewTypeAtendiment);
            imageCateg = itemView.findViewById(R.id.imageLogoEstabeleciment);
            imageLogoEstabeleciment = itemView.findViewById(R.id.imageLogoEstabeleciment);
            viewNovo = itemView.findViewById(R.id.viewNovo);
            textNovo = itemView.findViewById(R.id.textNovo);
            cardView = itemView.findViewById(R.id.cardContentImageEst);
            progressImage = itemView.findViewById(R.id.progressImage);
            progressImage.setVisibility(View.VISIBLE);
            cardView.setRadius(15);

            textNovo.setVisibility(View.GONE);
            viewNovo.setVisibility(View.GONE);

            itemView.setOnClickListener(this);
            //conteinerDeliveryIsOpen = itemView.findViewById(R.id.conteinerDeliveryIsOpen);
            //conteinerLocalIsOpen = itemView.findViewById(R.id.conteinerLocalIsOpen);
            //conteinerStatusLocalDelivery = itemView.findViewById(R.id.conteinerStatusLocalDelivery);

            /*itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int position = getAdapterPosition();
                            if (listener != null && position != RecyclerView.NO_POSITION) {
                                try {
                                    if(jsonArrayEstabeleciment.getJSONObject(position).getInt("em_breve") == 1){
                                        Toast.makeText(context, "Aguarde! O estabelecimento estará disponível em breve.", Toast.LENGTH_LONG).show();
                                    }else{
                                        listener.onItemClick(jsonArrayEstabeleciment.getJSONObject(position));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
            );*/


        }

        @Override
        public void onClick(View view) {

            Intent intent = new Intent(itemView.getContext(), SplashScreenEstabeleciment.class);
            int em_breve = 0;

            try {

                em_breve = obj.getInt("em_breve");
                String temp = obj.getString("_id");
                Repository.idEstabelecimento = temp;
                Repository.atendimentoLocal = false;

                intent.putExtra("nome", obj.getString("nome"));
                intent.putExtra("slogan", obj.getString("slogan"));
                intent.putExtra("foto", obj.getString("foto"));


            } catch (Exception e) {
                e.printStackTrace();
            }

            if (em_breve == 1) {
                Toast.makeText(context, "Aguarde! O estabelecimento estará disponível em breve.", Toast.LENGTH_LONG).show();
            } else {
                itemView.getContext().startActivity(intent);
                //itemView.getContext().overridePendingTransition(0, 0);
            }


        }

        public void set(JSONObject obj) {
            try {
                this.obj = obj;
                addObjMap(obj);
                //new ListSetImage(context, imageLogoEstabeleciment).execute(obj.getString("foto"));
                new ListSetImage(context, imageLogoEstabeleciment).execute(obj.getString("foto"), progressImage);

                //JSONObject obj = jsonArrayEstabeleciment.getJSONObject(position);
                String nome = obj.getString("nome");
                txt_nome_estabelecimento.setText(obj.getString("nome").toUpperCase());
                txt_desc_estabelecimento.setText(obj.getString("descricao"));
                textTime.setText(obj.getString("temp_min") + "-" + obj.getString("temp_max"));

                if (obj.getString("frete") != null && !obj.getString("frete").equals("null")) {
                    textValueFrete.setText(obj.getString("frete"));
                } else {
                    textValueFrete.setVisibility(View.GONE);
                    imgPgtoFrete.setVisibility(View.GONE);
                }

                if (obj.getString("aceita_cartao").equals("0")) {
                    aceita_cartao.setVisibility(View.GONE);
                }

                if(!obj.has("central_pay")  || obj.get("central_pay").equals("0")){
                    central_pay.setVisibility(View.GONE);
                }

                if (obj.getString("novo").equals("1")) {
                    viewNovo.setVisibility(View.VISIBLE);
                    textNovo.setVisibility(View.VISIBLE);
                }

                if (obj.getString("em_breve").equals("1")) {
                    viewNovo.setVisibility(View.VISIBLE);
                    textNovo.setVisibility(View.VISIBLE);
                    textNovo.setText("EM BREVE");
                    cv.setAlpha(0.4f);
                    cv.setEnabled(false);
                }

                if (obj.isNull("distancia")) {
                    //if (temp_distancia == null || temp_distancia.equals("null")) {
                    param = new HashMap<>();
                    param.put("id_cliente", Repository.getIdUsuario(context));
                    param.put("id_estabelecimento", obj.getString("_id"));
                    //param.put("id_end_estabelecimento", obj.getString("id_endereco"));
                    new Request(context, new ServiceCalDistance(this)).request(param);//
                } else {
                    txt_distancia.setText(obj.getString("distancia"));
                }

                if (obj.getJSONObject("delivery").getInt("aberto") == 1) {
                    //textViewTypeAtendiment.setTextColor(context.getResources().getColor(android.R.color.holo_green_light));
                    textViewTypeAtendiment.setTextColor(Color.rgb(24,112,0));
                    textViewTypeAtendiment.setText("ABERTO");
                } else {
                    textViewTypeAtendiment.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                    textViewTypeAtendiment.setText("FECHADO");
                }

                textHorarioDelivery.setText(obj.getJSONObject("delivery").getString("label"));

                if (!obj.isNull("avaliacao")) {
                    ratingBarLikedListProduct3.setRating(Float.valueOf(obj.getString("avaliacao")));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public interface OnItemClickListner {
        void onItemClick(JSONObject userModel);
    }

    public void setOnItemClickListener(OnItemClickListner listener) {
        this.listener = listener;
    }

    public void setListEstabeleciments(JSONArray listEstabelecimetn) {
        jsonArrayEstabeleciment = listEstabelecimetn;
    }

}
