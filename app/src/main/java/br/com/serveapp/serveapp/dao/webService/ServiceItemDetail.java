package br.com.serveapp.serveapp.dao.webService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceItemDetail extends OnPostResponse {

    DetalheItemPopUpActivity detalheItemPopUpActivity;
    private String idProduto;

    public ServiceItemDetail(DetalheItemPopUpActivity detalheItemPopUpActivity, String idProduto) {
        setId(idProduto);
        this.detalheItemPopUpActivity = detalheItemPopUpActivity;
        this.idProduto = idProduto;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_detalhes_do_produto.php";
    }

    @Override
    public Map<String, String> getParam() {

        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_produto", idProduto);
        paramLogin.put("id_cliente", Repository.getIdUsuario(detalheItemPopUpActivity.getApplicationContext()));

        return paramLogin;
    }


    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            int qtdMax = jsonObject.getInt("qtd_max");
            JSONArray result = jsonObject.getJSONArray("result");
            JSONObject produto = jsonObject.getJSONObject("produto");
            Repository.fav.reset(result);
            detalheItemPopUpActivity.initializeAdapter(result, qtdMax, produto);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject produto = jsonObject.getJSONObject("produto");
                int qtdMax = jsonObject.getInt("qtd_max");
                JSONArray result = jsonObject.getJSONArray("result");
                detalheItemPopUpActivity.initializeAdapter(result, qtdMax, produto);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
