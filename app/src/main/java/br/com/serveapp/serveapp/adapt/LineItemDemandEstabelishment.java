package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.EvaluateItemPopUPActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;
import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class LineItemDemandEstabelishment extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    LayoutInflater inflater;
    public JSONArray listItensCarrinho;
    TextView textOptions;


    onClickAvaliationListenerItem onClickAvaliationListenerItem;
    OnClickAvaliationListener onClickAvaliationListener;
    private final int VIEW_TYPE_DEMAND_NOT_FINISH = 1;
    private final int VIEW_TYPE_DEMAND_FINISH = 3;
    private final int VIEW_TYPE_DEMAND_FINISH_AVALIETED = 2;
    private  Context context;

    public LineItemDemandEstabelishment( Context context, JSONArray jsonArray) {
        this.textOptions = textOptions;
        this.context = context;
        this.listItensCarrinho = jsonArray;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
        RecyclerView.ViewHolder viewHolder =null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_item_demand_solicited, parent, false);

        viewHolder = new ItemCart(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        try {

            final JSONObject itemCar = listItensCarrinho.getJSONObject(position);

            if(!itemCar.isNull("estab_foto") && !itemCar.getString("estab_foto").equals("null")) {
                new ListSetImage(context, ((ItemCart) holder).iconEstabHist).execute(itemCar.getString("estab_foto"));
            }

            if(onClickAvaliationListener!=null) {
                ((ItemCart) holder).buttonaAvaliarItemPedido.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickAvaliationListener.onClickAvaliationListener(itemCar);
                    }

                });
            }else{
                ((ItemCart) holder).buttonaAvaliarItemPedido.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickAvaliationListenerItem.onClickAvaliationListenerItem(itemCar, position);
                    }

                });
            }

                if(itemCar.isNull("avaliacao") || itemCar.getString("avaliacao").equals("null")) {
                ((ItemCart) holder).ratting_avaliation.setVisibility(GONE);
                }else{
                    ((ItemCart) holder).ratting_avaliation.setRating(itemCar.getInt("avaliacao"));
                    ((ItemCart) holder).buttonaAvaliarItemPedido.setVisibility(GONE);
                    ((ItemCart) holder).ratting_avaliation.setVisibility(VISIBLE);
                }


                ((ItemCart) holder).nomeItem.setText(itemCar.getString("nome"));
                ((ItemCart) holder).textParseValue.setText(itemCar.getString("qtd"));
                new ListSetImage(context,((ItemCart) holder)._imageItemCardapio).execute(itemCar.getString("foto"));

                ((ItemCart) holder).valueItem.setText(itemCar.getString("valor"));
                ((ItemCart) holder).textStatusDemand.setText(itemCar.getString("status"));
                JSONArray itens = itemCar.getJSONArray("itens");
                if(itens.length()<=0){
                    ((ItemCart) holder).btn_down_detail.setVisibility(GONE);
                }
            ((ItemCart) holder).recyclerDetailItemCar.setAdapter(new LineDetailItemCartAdapter(itemCar.getJSONArray("itens")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return listItensCarrinho.length();
    }


    class ItemCart extends RecyclerView.ViewHolder {

        public TextView nomeItem;
        public TextView valueItem;
        public ImageView _imageItemCardapio;
        public ImageView btn_down_detail;

        public CardView cardContentLine;
        public RecyclerView recyclerView;
        public int qtdItens = 1;
        public TextView textHourAddToCart;
        public TextView textDataAddToCart;
        public RecyclerView recyclerDetailItemCar;


        public TextView textStatusDemand;
        public TextView textParseValue;
        public TextView numPedido;
        public ProgressBar progressDemand;
        public RatingBar ratting_avaliation;
        Thread thread;
        int pStatus;
        int time;
        Handler handler = new Handler();
        public Button buttonaAvaliarItemPedido;
        ImageView iconEstabHist;

        public ItemCart(View itemView) {
            super(itemView);

            buttonaAvaliarItemPedido = itemView.findViewById(R.id.buttonaAvaliarItemPedido);
            textStatusDemand = (TextView) itemView.findViewById(R.id.statusItemPedido);
            textParseValue = (TextView) itemView.findViewById(R.id.textParseValue);
            numPedido = (TextView) itemView.findViewById(R.id.numPedido);
            progressDemand = (ProgressBar) itemView.findViewById(R.id.progressDemand);
            ratting_avaliation = (RatingBar) itemView.findViewById(R.id.ratting_avaliation);
            iconEstabHist  = itemView.findViewById(R.id.iconEstabHist);
            recyclerDetailItemCar = (RecyclerView) itemView.findViewById(R.id.recyclerDetailItemCar);
            LinearLayoutManager llm = new LinearLayoutManager(context);
            recyclerDetailItemCar.setLayoutManager(llm);
            textDataAddToCart = (TextView) itemView.findViewById(R.id.textDataDemand);
            textHourAddToCart = (TextView) itemView.findViewById(R.id.textHourAddToCart);
            valueItem = (TextView) itemView.findViewById(R.id.textValueItemDemandFinalize);
            nomeItem = (TextView) itemView.findViewById(R.id.itemPedidoDetalheText);
            _imageItemCardapio = (ImageView) itemView.findViewById(R.id.foto);

            //recyclerDetailItemCar.setVisibility(GONE);
            cardContentLine = itemView.findViewById(R.id.cardExpandableItemPedido);
            btn_down_detail = itemView.findViewById(R.id.btn_down_detail);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    downUpCard(recyclerDetailItemCar);
                }
            });

        }

        private void downUpCard(final View view) {
           /* if (view.getVisibility() == View.GONE) {
                ObjectAnimator.ofFloat(btn_down_detail, "rotation", 0, 180).start();
                view.setVisibility(View.VISIBLE);
                view.animate()
                        .alpha(1f)
                        .setDuration(500)
                        .setListener(null);

            } else if (view.getVisibility() == View.VISIBLE) {
                ObjectAnimator.ofFloat(btn_down_detail, "rotation", 180, 360).start();
                view.animate()
                        .alpha(0f)
                        .setDuration(500)
                        .setListener(new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                view.setVisibility(View.GONE);
                            }
                        });

            }*/

        }


        class ItemCartNotEv extends RecyclerView.ViewHolder {

            public TextView nomeItem;
            public TextView valueItem;
            public ImageView _imageItemCardapio;
            public ImageView btn_down_detail;
            public Button btnAvaliacao;
            public CardView cardContentLine;
            public RecyclerView recyclerView;
            public int qtdItens = 1;
            public TextView textHourAddToCart;
            public TextView textDataAddToCart;
            public RecyclerView recyclerDetailItemCar;
            public CardView cardExpandableItemPedido;
            public TextView textStatusDemand;
            public TextView textParseValue;
            public TextView numPedido;
            public ProgressBar progressDemand;
            public RatingBar ratting_avaliation;
            public ItemCartNotEv(View itemView) {
                super(itemView);

                textStatusDemand = (TextView) itemView.findViewById(R.id.statusItemPedido);
                textParseValue = (TextView) itemView.findViewById(R.id.textParseValue);
                numPedido = (TextView) itemView.findViewById(R.id.numPedido);
                progressDemand = (ProgressBar) itemView.findViewById(R.id.progressDemand);
                ratting_avaliation = (RatingBar) itemView.findViewById(R.id.ratting_avaliation);
                btnAvaliacao = (Button) itemView.findViewById(R.id.buttonAvaliacao);
                recyclerDetailItemCar = (RecyclerView) itemView.findViewById(R.id.recyclerDetailItemCar);
                LinearLayoutManager llm = new LinearLayoutManager(context);
                recyclerDetailItemCar.setLayoutManager(llm);
                textDataAddToCart = (TextView) itemView.findViewById(R.id.textDataDemand);
                textHourAddToCart = (TextView) itemView.findViewById(R.id.textHourAddToCart);
                recyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerDetailItemCar);
                valueItem = (TextView) itemView.findViewById(R.id.textValueItemDemandFinalize);
                nomeItem = (TextView) itemView.findViewById(R.id.itemPedidoDetalheText);
                _imageItemCardapio = (ImageView) itemView.findViewById(R.id.foto);
                cardExpandableItemPedido = (CardView) itemView.findViewById(R.id.cardExpandableItemPedido);
                //recyclerDetailItemCar.setVisibility(GONE);
                cardContentLine = itemView.findViewById(R.id.cardExpandableItemPedido);
                btn_down_detail = itemView.findViewById(R.id.btn_down_detail);

                cardExpandableItemPedido.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (view.getVisibility() == GONE) {
                            downUpCard(recyclerDetailItemCar);
                        }


              /*      else if(view.getVisibility()== View.VISIBLE){
                        ObjectAnimator.ofFloat(btn_down_detail, "rotation", 0, 180).start();
                        view.animate()
                                .alpha(0f)
                                .setDuration(500)
                                .setListener(new AnimatorListenerAdapter() {
                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        view.setVisibility(View.GONE);
                                    }
                                });
                    }*/

                    }
                });

                btnAvaliacao.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, EvaluateItemPopUPActivity.class);
                        intent.putExtra("position", getAdapterPosition());
                        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                        //       context.startActivityForResult(intent,88);
                    }
                });

            }

            private void downUpCard(final View view) {
                //    btn_down_detail.setRotation(90);
               /* view.setVisibility(View.VISIBLE);
                view.animate()
                        .alpha(1f)
                        .setDuration(500)
                        .setListener(null);*/
            }

        }

    }

    public void setOnClickAvaliationListener(OnClickAvaliationListener onClickAvaliationListener){
        this.onClickAvaliationListener = onClickAvaliationListener;
    }

    public interface OnClickAvaliationListener{
        public void onClickAvaliationListener(JSONObject jsonObject);
    }

    public interface onClickAvaliationListenerItem{
        public void onClickAvaliationListenerItem(JSONObject jsonObject, int position);
    }

    public void setOnClickAvaliationListenerItem(onClickAvaliationListenerItem onClickAvaliationListenerItem){
        this.onClickAvaliationListenerItem = onClickAvaliationListenerItem;
    }


}