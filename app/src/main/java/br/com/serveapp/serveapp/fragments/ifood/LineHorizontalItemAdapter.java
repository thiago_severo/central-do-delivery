package br.com.serveapp.serveapp.fragments.ifood;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;

import br.com.serveapp.serveapp.R;

public class LineHorizontalItemAdapter extends RecyclerView.Adapter<LineHorizontalItem> {

    private JSONArray itens;
    LinearLayoutManager layoutManager;
    Fragment fragment;
    Activity activity;

    public LineHorizontalItemAdapter(JSONArray itens, LinearLayoutManager layoutManager, Fragment fragment, Activity activity) {
        this.itens = itens;
        this.fragment = fragment;
        this.activity = activity;
        this.layoutManager = layoutManager;
    }

    @Override
    public LineHorizontalItem onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.line_promotion_estab, parent, false);
        return new LineHorizontalItem(view);
    }

    @Override
    public void onBindViewHolder(LineHorizontalItem holder, final int position) {
        try {
            holder.initialize(itens.getJSONObject(position));
            holder.setFragment(fragment);
            holder.setActivity(activity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        holder.setLayoutManager(layoutManager);
    }

    @Override
    public int getItemCount() {
        return itens != null ? itens.length() : 0;
    }

}
