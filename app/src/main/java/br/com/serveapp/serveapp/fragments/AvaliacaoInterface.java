package br.com.serveapp.serveapp.fragments;

public interface AvaliacaoInterface {

    public void showMsg(final String titulo, final String mensagem);
    public void dimissPopUpAvaliacao();

    void dimissProgressBarSendFeedBack();
}
