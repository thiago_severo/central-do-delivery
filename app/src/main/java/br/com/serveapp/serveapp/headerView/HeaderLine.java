package br.com.serveapp.serveapp.headerView;

import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;

class HeaderLine extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView moreButton;
    String header;
    View selectHeader;
    int categoria;
    LinearLayoutManager bodyLayout;
    LinearLayoutManager headerLayout;

    public HeaderLine(View itemView, LinearLayoutManager bodyLayout, LinearLayoutManager headerLayout) {
        super(itemView);
        this.bodyLayout = bodyLayout;
        this.headerLayout = headerLayout;
        moreButton = itemView.findViewById(R.id.titleHeader);
        selectHeader = itemView.findViewById(R.id.selectHeader);
        selectHeader.setVisibility(View.INVISIBLE);
    }

    public void selected(){
        selectHeader.setVisibility(View.VISIBLE);
    }

    public void desselected(){
        selectHeader.setVisibility(View.INVISIBLE);
    }

    public void initialize(final String header, int categoria){
        this.header = header;
        this.categoria =categoria;
        moreButton.setText(header);
        moreButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        HeaderLineGroup.selected(this);
        bodyLayout.scrollToPositionWithOffset(categoria, 0);
    }
}
