package br.com.serveapp.serveapp.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.ScrollingActivity;
import br.com.serveapp.serveapp.adapt.AdapterGridCategoryItens;
import br.com.serveapp.serveapp.adapt.LineProductsForEstabelecimentsAdapter;
import br.com.serveapp.serveapp.adapt.LocalDemandAdapter;
import br.com.serveapp.serveapp.adapt.PromocaoListAdapter;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.entidades.Cliente;
import br.com.serveapp.serveapp.entidades.cardapio.Categoria;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

import static br.com.serveapp.serveapp.repository.objects.Repository.listOfPedidosToTabLocal;
import static com.facebook.FacebookSdk.getApplicationContext;


public class FragmentListHorarios extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    int tipoEstab;

    private Request requestListCategoryEstabeleciment;
    private Request requestListDestaquesEstabeleciment;
    ConstraintLayout constraintLayout;
    LocalDemandAdapter adapter;
    private RecyclerView listDemandOfClient;
    View view;

    private LineProductsForEstabelecimentsAdapter lineAdapter;
    boolean preencher;

    ScrollingActivity scrollingActivity;
    TextView header_line_hora_delivery;
    TextView header_line_hora_local;
    JSONArray listHorarios;


    public FragmentListHorarios(ScrollingActivity activity, JSONArray getListHorarios, int tipoEstab) {
        this.tipoEstab = tipoEstab;
        listHorarios = getListHorarios;
        scrollingActivity = activity;
        //  getListHorarios(getListHorarios);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }


    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
    }

    RecyclerView recycleGridCategoryItensEstabeleciment;
    RecyclerView recycleHorizontalListItensEstabeleciment;
    public AdapterGridCategoryItens categorieListAdapter;
    PromocaoListAdapter promocaoListAdapter;
    List<Categoria> categoriaList;
    TableLayout ll;

    @SuppressLint("ResourceType")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_list_horarios, container, false);
        ll = (TableLayout) view.findViewById(R.id.tableBodyLayoutLocalDelivery);

        header_line_hora_delivery = view.findViewById(R.id.header_line_hora_delivery);
        header_line_hora_local = view.findViewById(R.id.header_line_hora_local);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getListHorarios(listHorarios);

    }

    public void setAdapter() {
        Cliente cliente = new Cliente();
        cliente.setId(1);

        adapter = new LocalDemandAdapter(getApplicationContext(), listOfPedidosToTabLocal, cliente);
        listDemandOfClient.setAdapter(adapter);

    }

    public void initializeAdapterDestaques(JSONArray categoriaListObjects) {
        promocaoListAdapter = new PromocaoListAdapter(getApplicationContext(), categoriaListObjects);
        recycleHorizontalListItensEstabeleciment.setAdapter(promocaoListAdapter);
        recycleHorizontalListItensEstabeleciment.getAdapter().notifyDataSetChanged();

    }

    public void initializeAdapterCategories(final JSONArray categoriaListObjects) {
        categorieListAdapter = new AdapterGridCategoryItens(getApplicationContext(), categoriaListObjects);
        recycleGridCategoryItensEstabeleciment.setAdapter(categorieListAdapter);
        recycleGridCategoryItensEstabeleciment.getAdapter().notifyDataSetChanged();

    }

    public void getListHorarios(JSONArray horarios) {

        horarios.length();

        for (int i = 0; i < horarios.length(); i++) {
            try {

                final TableRow tableRow = (TableRow) getLayoutInflater().inflate(R.layout.table_row_delivery_local, null);

                TextView tvLoc;
                TextView tvDel;
                TextView tvDia;

                tvLoc = (TextView) tableRow.findViewById(R.id.line_hora_local);
                tvDel = (TextView) tableRow.findViewById(R.id.line_hora_delivery);

                if (tipoEstab == 2)
                    if (!horarios.getJSONObject(i).isNull("delivery")) {

                        tvDia = (TextView) tableRow.findViewById(R.id.title_table_dia);
                        tvDia.setText(horarios.getJSONObject(i).getString("dia"));
                        ll.addView(tableRow);

                        for (int j = 0; j < horarios.getJSONObject(i).getJSONArray("delivery").length(); j++) {
                            tvDel.setText(horarios.getJSONObject(i).getJSONArray("delivery").getJSONObject(j).getString("inicio") + "-" +
                                    horarios.getJSONObject(i).getJSONArray("delivery").getJSONObject(j).getString("fim"));

                        }
                    }

                if (tipoEstab == 1)
                    if (!horarios.getJSONObject(i).isNull("local")) {

                        tvDia = (TextView) tableRow.findViewById(R.id.title_table_dia);
                        tvDia.setText(horarios.getJSONObject(i).getString("dia"));
                        ll.addView(tableRow);

                        for (int j = 0; j < horarios.getJSONObject(i).getJSONArray("local").length(); j++) {
                            tvLoc.setText(horarios.getJSONObject(i).getJSONArray("local").getJSONObject(j).getString("inicio") + "-" +
                                    horarios.getJSONObject(i).getJSONArray("local").getJSONObject(j).getString("fim"));

                        }
                    }

                if (tipoEstab == 3) {

                    tvDia = (TextView) tableRow.findViewById(R.id.title_table_dia);
                    tvDia.setText(horarios.getJSONObject(i).getString("dia"));
                    ll.addView(tableRow);

                    if (!horarios.getJSONObject(i).isNull("local")) {

                        for (int j = 0; j < horarios.getJSONObject(i).getJSONArray("local").length(); j++) {
                            tvLoc.setText(horarios.getJSONObject(i).getJSONArray("local").getJSONObject(j).getString("inicio") + "-" +
                                    horarios.getJSONObject(i).getJSONArray("local").getJSONObject(j).getString("fim"));

                        }
                    }

                    for (int j = 0; j < horarios.getJSONObject(i).getJSONArray("delivery").length(); j++) {
                        tvDel.setText(horarios.getJSONObject(i).getJSONArray("delivery").getJSONObject(j).getString("inicio") + "-" +
                                horarios.getJSONObject(i).getJSONArray("delivery").getJSONObject(j).getString("fim"));

                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
