package br.com.serveapp.serveapp;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MyNavigation implements BottomNavigationView.OnNavigationItemSelectedListener {

    private Activity context;
    public MyNavigation(Activity context){
        this.context = context;
    }

    private void startActivity(Class<?> activityClass){
        if(context.getClass() != activityClass){
            context.startActivity(new Intent(context.getApplicationContext(), activityClass));
            context.overridePendingTransition(0, 0);
            context.finish();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.navigation_fav_fav:
                startActivity(FavoritsActivity.class);

                break;
            case R.id.nav_fav_home:
                startActivity(NavigationDrawerActivity.class);

                break;
            case R.id.nav_fav_carrinho:
                Repository.idEstabelecimento = "0";
                startActivity(ConfirmItensPedidoActiviy.class);

                break;
            case R.id.nav_atracoes:
                startActivity(AtracoesActivity.class);

                break;
            case R.id.nav_fav_produtos:
                startActivity(ListAllProducts.class);

                break;

        }
        return true;
    }

}
