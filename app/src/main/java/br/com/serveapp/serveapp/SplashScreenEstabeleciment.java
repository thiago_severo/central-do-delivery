package br.com.serveapp.serveapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import br.com.serveapp.serveapp.drawer.ListSetImage;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SplashScreenEstabeleciment extends AppCompatActivity {

    private ImageView imgLogoEstabelecimento;
    private TextView textSloganEstabelecimento;
    private TextView textNameEstabeleciment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen_estabeleciment);

        textSloganEstabelecimento = findViewById(R.id.textSloganEstabelecimento);
        imgLogoEstabelecimento = findViewById(R.id.ImgSeloClient);
        textNameEstabeleciment = findViewById(R.id.textNameEstabeleciment);

        textNameEstabeleciment.setText(getIntent().getStringExtra("nome"));
        textSloganEstabelecimento.setText(getIntent().getStringExtra("slogan"));
        String foto = getIntent().getStringExtra("foto");


        new ListSetImage(getApplicationContext(), imgLogoEstabelecimento,0).
                execute(getIntent().getStringExtra("foto"));

        openEstab();
    }

    private void openEstab(){
        final Intent intent = new Intent(getApplicationContext(), ListProductsStablishmentDeliveryActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);

        intent.putExtra("nome", getIntent().getStringExtra("nome"));
        intent.putExtra("slogan", getIntent().getStringExtra("slogan"));
        intent.putExtra("logo", getIntent().getStringExtra("foto"));

        if (getIntent().hasExtra("id_produto")) {
            intent.putExtra("id_produto", getIntent().getStringExtra("id_produto"));

        }
        /*
        intent.putExtra("distancia", getIntent().getStringExtra("distancia"));
        intent.putExtra("rating", getIntent().getIntExtra("rating", 3));
        intent.putExtra("frete", '0');
        intent.putExtra("fav", getIntent().getBooleanExtra("fav", false));

        intent.putExtra("status", getIntent().getStringExtra("status"));
        intent.putExtra("tempoEntrega", getIntent().getStringExtra("tempoEntrega") );
        intent.putExtra("taxaEntrega", getIntent().getStringExtra("taxaEntrega"));
        */

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getApplicationContext().startActivity(intent);
                finish();
            }
        }, 500);
    }

}
