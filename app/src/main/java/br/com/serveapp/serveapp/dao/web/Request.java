package br.com.serveapp.serveapp.dao.web;

import android.content.Context;
import android.os.Handler;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import br.com.serveapp.serveapp.dao.Dao;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class Request implements Response.Listener<String>, Response.ErrorListener {

    private StringRequest stringRequest;
    private RequestQueue requestQueue;
    private OnPostResponse onPostResponse;
    private Context context;
    private String cache;
    private int numReconnect;
    private boolean enable;

    public OnPostResponse getOnPostResponse() {
        return onPostResponse;
    }

    public Request(Context context, OnPostResponse onPostResponse) {
        this.context = context;
        this.enable = true;
        requestQueue = Volley.newRequestQueue(context);
        this.onPostResponse = onPostResponse;
        this.numReconnect = 0;
    }

    public void request() {
        request(onPostResponse.getParam());
    }

    private synchronized boolean isEnable() {
        return enable;
    }

    private synchronized void setEnable(boolean enable) {
        this.enable = enable;
    }

    public void request(final Map<String, String> params) {
        params.put("api_key", "tpeVo2SZCALxTjlrYimU13psgHVUNc7YB");
        if (isEnable()) {
            setEnable(false);
            // verificar antes o arquivo local
            readCache();

            stringRequest = new StringRequest(com.android.volley.Request.Method.POST, onPostResponse.getScript(), this, this) {
                protected Map<String, String> getParams() {
                    return params;
                }
            };


            stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
    }

    private void readCache() {
        cache = Dao.readCache(onPostResponse.getFileCacheName(), context);
        if (cache != null){
            try {
                JSONObject jsonObject = new JSONObject(cache);
                onPostResponse.onCache(jsonObject);
            } catch (JSONException e) {
                Dao.delete(onPostResponse.getFileCacheName(), context);
            }
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        if (onPostResponse.isAutoReconnect()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onPostResponse.onStateConnection(StateConnection.RECONNECTED);
                    stringRequest = new StringRequest(com.android.volley.Request.Method.POST, onPostResponse.getScript(), Request.this, Request.this) {
                        protected Map<String, String> getParams() {
                            return onPostResponse.getParam();
                        }
                    };
                    requestQueue.add(stringRequest);
                }
            }, 1000);

        } else {
            onPostResponse.onStateConnection(StateConnection.OFFLINE);
            onPostResponse.onErrorResponse(error);
        }
        setEnable(true);
    }

    @Override
    public void onResponse(String response) {
        setEnable(true);
        response = response.replace("\n", "").replace("\r", "");
        onPostResponse.onStateConnection(StateConnection.ONLINE);
        if (onPostResponse.isSaveCache()) {
            Dao.saveCache(response, onPostResponse.getFileCacheName(), context);
        }
        onPostResponse.onResponse(response);
        if (response != null && (cache == null || !cache.equals(response))) {
            onPostResponse.onUpdate(response);
        }
        setEnable(true);
    }

}
