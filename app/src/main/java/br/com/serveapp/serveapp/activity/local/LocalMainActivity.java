package br.com.serveapp.serveapp.activity.local;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.os.Bundle;

import br.com.serveapp.serveapp.fragment.local.FragmentListProductsLocal;
import br.com.serveapp.serveapp.R;
import com.google.android.material.tabs.TabLayout;

public class LocalMainActivity extends AppCompatActivity {

    public ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_main);

        getSupportActionBar().hide();

        initItens();

      //  ItemAdapter itemAdapter = new ItemAdapter(itemList,getApplicationContext(),false);
        TabsPagerAdapter adapter = new TabsPagerAdapter( getSupportFragmentManager() );
        FragmentListProductsLocal tabCardapio = new FragmentListProductsLocal(this, false);
        FragmentListProductsLocal tabConfirmacao = new FragmentListProductsLocal( this,false);
     //   tabPedidoActivity = new TabPedidoActivity(itemAdapter);

        adapter.adicionar(  tabCardapio , "Cardápio");
        adapter.adicionar( tabConfirmacao, "Comanda");

        viewPager = (ViewPager) findViewById(R.id.pagerScrolling);
        viewPager.setAdapter(adapter);

        @SuppressLint("WrongViewCast") TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);


    }



    public void initItens() {

//        itemList = new ArrayList<>();
//        itemList.add(new Item("Pitú Cola 300 ml", "50% de álcool", 2.3));
//        itemList.add(new Item("Cerveja Iataipava ", "puro malte", 5));
//        itemList.add(new Item("Guaraná Hiran", "1L", 3));
//        itemList.add(new Item("Cerveja Petra 300 ml", "50% de álcool", 2.3));
//        itemList.add(new Item("Cerveja Iataipava ", "puro malte", 5));
//        itemList.add(new Item("Guaraná Hiran", "1L", 3));

    }


}
