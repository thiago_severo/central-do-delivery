package br.com.serveapp.serveapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import br.com.serveapp.serveapp.fragments.FragmentHome;
import br.com.serveapp.serveapp.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class MainActivity extends AppCompatActivity implements ZXingScannerView.ResultHandler, NavigationView.OnNavigationItemSelectedListener {

    TextView textView;
    Button buttonLogout;

    FragmentHome fDash = new FragmentHome();
    FragmentHome fNot = new FragmentHome();
    FragmentHome fHome = new FragmentHome();
    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
     FragmentManager fragmentManager = getSupportFragmentManager() ;

   // Fragment fragment = new FragmentHome();
   // Fragment sfragment =new FragmentHome();

    ZXingScannerView scannerView;


    private DrawerLayout dl;
    private ActionBarDrawerToggle t;
    private NavigationView nv;

    public   List<Fragment> fragmentListHome = new ArrayList<>();
 public   List<FragmentHome> fragmentListDashboard = new ArrayList<>();
 public   List<FragmentHome> fragmentListNot = new ArrayList<>();


    private BottomNavigationView.OnNavigationItemSelectedListener listener =

            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

            //     FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();


                    switch (menuItem.getItemId()){

                        case R.id.nav_fav_home:
                        //    fragmentListHome.get(fragmentListHome.size()-1).setFragmentTransaction(fragmentTransaction);
                        //    fragmentListHome.get(fragmentListHome.size()-1).setFragmentContainer(R.id.fragment_container);


                        //    sfragment = fragmentListHome.get(fragmentListHome.size()-1);
                       //     sfragment = new FragmentHome();
                            Toast.makeText(getApplicationContext(),"size:"+fragmentListHome.size(),Toast.LENGTH_LONG).show();
                            break;

                        case R.id.navigation_fav_fav:
                          //  fragmentListDashboard.get(fragmentListDashboard.size()-1).setFragmentTransaction(fragmentTransaction);
                          //  fragmentListDashboard.get(fragmentListDashboard.size()-1).setFragmentContainer(R.id.fragment_container);
                         //   sfragment = fragmentListDashboard.get(fragmentListDashboard.size()-1);
                         //   sfragment = new FragmentFavorit();
                            startActivity(new Intent(getApplicationContext(), FavoritsActivity.class));
                         //   startActivity();
                            break;

                        case R.id.nav_fav_carrinho:
                           // fragmentListNot.get(fragmentListNot.size()-1).setFragmentTransaction(fragmentTransaction);
                           // fragmentListNot.get(fragmentListNot.size()-1).setFragmentContainer(R.id.fragment_container);
                          //  sfragment = fragmentListNot.get(fragmentListNot.size()-1);
                      //      sfragment = new FragmentHome();
                            break;

                    }

                //    fragmentManager.beginTransaction().replace(R.id.fragment_container_main_home, sfragment).commit();

                    return  true;
                }
            };



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_navigation);
        //teste commit commit
        getSupportActionBar().hide();

      //  fragmentListHome.add(fHome);
        fragmentListDashboard.add(fDash);
        fragmentListNot.add(fNot);

      //  fragmentManager.beginTransaction().replace(R.id.fragment_container_main_home, sfragment).commit();

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(listener);

        textView = findViewById(R.id.homeText);
        buttonLogout = findViewById(R.id.bt_logout);

        scannerView = findViewById(R.id.z_xing_scannerr);




   /*     if(intent.hasExtra("json")) {

            try {

                JSONObject json=new JSONObject( intent.getExtras().getString("json"));
            textView.setText( json.getString("email")+ ": "+ json.getString("last_name"));


        } catch (JSONException e) {

                e.printStackTrace();

            }
            }

*/


 /*  buttonLogout.setOnClickListener(
        new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                disconnectFromFacebook();
                finish();
            }
        }
);*/
       // setContentView(layout);

        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

        builder.setTitle("Opção");
        builder.setMessage("Deseja cadastrar um endereço?");



        builder.setPositiveButton("SIM", new DialogInterface.OnClickListener() {


            public void onClick(DialogInterface dialog, int which) {

                // Do nothing, but close the dialog
                Intent intent = new Intent(getApplicationContext(), CreateAddressAccountActivity.class);
               // dialog.dismiss();
                startActivity(intent);
            }
        });

        builder.setNegativeButton("NÃO", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                // Do nothing
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();


    }

    @Override

    public boolean onOptionsItemSelected(MenuItem item) {

        if(t.onOptionsItemSelected(item))
            return true;
        return super.onOptionsItemSelected(item);

    }



    public void disconnectFromFacebook() {

        if (AccessToken.getCurrentAccessToken() == null) {
            return; // already logged out
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, new GraphRequest
                .Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {

                LoginManager.getInstance().logOut();

            }
        }).executeAsync();




    }



    @Override
    protected void onPause(){
        super.onPause();
        scannerView.stopCamera();

    }


    @Override
    protected void onResume(){
        super.onResume();
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            scannerView.setResultHandler(this); /* Set handler for ZXingScannerView */
            scannerView.startCamera(); /* Start camera */
        } else {
            ActivityCompat.requestPermissions(MainActivity.this, new
                    String[]{Manifest.permission.CAMERA}, 1024);
        }


    }


    @Override
    public void handleResult(Result result) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        return false;
    }
}