package br.com.serveapp.serveapp.adapt;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.FormaPagamentoActivity;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;

public class LineSelectCard extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listViewItem;
    OnItemClickListner onItemClickListner;
    Context context;

    public LineSelectCard(JSONArray listViewItem, Context context) {
        this.listViewItem = listViewItem;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder lineSimpleItemHolder = null;
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_card_credit, parent, false);
        lineSimpleItemHolder = new LineSimpleItemHolder(v);
        return lineSimpleItemHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        try {

            //        Toast.makeText(context, "bandeira "+ listViewItem.getJSONObject(position).toString() , Toast.LENGTH_LONG).show();

            String bandeira = listViewItem.getJSONObject(position).getString("bandeira");
            new ListSetImage(context, ((LineSimpleItemHolder) holder).imageCardCredit).execute("card/" + bandeira.toLowerCase().replace(" ", "") + ".png");
//            new ListSetImage(context, ((LineSimpleItemHolder)holder).imageCardCredit).execute( "card/amex.png");

            String number = listViewItem.getJSONObject(position).getString("number");
            //     ((LineSimpleItemHolder) holder).imageCardCredit.setImageBitmap(null);
            String num = number.replace(" ", "");
            //           System.out.println("num: " + num);
            //     ((LineSimpleItemHolder) holder).numCard.setText(listViewItem.getJSONObject(position).getString("card"));
            //     ((LineSimpleItemHolder) holder).nameCard.setText(listViewItem.getJSONObject(position).getString("type"));
            ((LineSimpleItemHolder)holder).card.put("number", "**** "+ num);
            ((LineSimpleItemHolder) holder).numCard.setText("**** "+ num);
      /*      ((LineSimpleItemHolder)holder).card.put("bandeira", bandeira.toUpperCase());
            ((LineSimpleItemHolder)holder).card.put("numeroCartao", number.trim().replace(" ", ""));
      //    ((LineSimpleItemHolder)holder).card.put("numeroCartao", number.trim().replace(" ", ""));
*/          ((LineSimpleItemHolder)holder).card.put("cvv", listViewItem.getJSONObject(position).getString("cvv"));
            ((LineSimpleItemHolder)holder).card.put("isMultiple", true);
            ((LineSimpleItemHolder)holder).card.put("bandeira", bandeira.toLowerCase());
            ((LineSimpleItemHolder)holder).card.put("card", listViewItem.getJSONObject(position).getString("number"));
            ((LineSimpleItemHolder)holder).card.put("_id", listViewItem.getJSONObject(position).getString("_id"));
            ((LineSimpleItemHolder)holder).card.put("token", listViewItem.getJSONObject(position).getString("token"));
            ((LineSimpleItemHolder)holder).card.put("nameHolder", listViewItem.getJSONObject(position).getString("cliente_name"));

            if (listViewItem.getJSONObject(position).getInt("_id") == LinePaymentFormListAdapterView.idSaved && FormaPagamentoActivity.select_online ==true) {
                LinePaymentFormListAdapterView.setSelected((((LineSimpleItemHolder)holder)).border, listViewItem.getJSONObject(position).getInt("_id")) ;
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return listViewItem.length();
    }

    public interface OnItemClickListner {

        void onItemClick(boolean selectOnline, JSONObject card, View selected);
        void onItemClickDelete(boolean selectOnline, JSONObject card);

    }

    class LineSimpleItemHolder extends RecyclerView.ViewHolder {


        public TextView textDelete;
        public TextView numCard;
        public ImageView imageCardCredit;
        //public TextView nameCard;
        public TextView options;
        public View itemView;
        public JSONObject card;
        public View border;

        public LineSimpleItemHolder(View itemView) {
            super(itemView);


            //nameCard = itemView.findViewById(R.id.nameCard);
            imageCardCredit = itemView.findViewById(R.id.imageCardCredit);
            numCard = itemView.findViewById(R.id.numCard);
            options = itemView.findViewById(R.id.textDelete);
            textDelete = itemView.findViewById(R.id.textDelete);
            border = itemView.findViewById(R.id.border);

            this.itemView = itemView;
            //
            card = new JSONObject();

            // numCard.setText("card");
            //imageCardCredit.setImageBitmap(null);

            textDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListner.onItemClickDelete(true, card);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClickListner.onItemClick(true, card, border);
                    //Toast.makeText(context, "cardd: "+ card.toString(), Toast.LENGTH_LONG).show();
                }
            });

        }


    }


    public void setOnItemClickListner(OnItemClickListner onItemClickListner) {
        this.onItemClickListner = onItemClickListner;
    }

}