package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import br.com.serveapp.serveapp.util.MaskEditUtil;
import br.com.serveapp.serveapp.util.ValidationUtilField;
import br.com.serveapp.serveapp.R;

import com.github.pinball83.maskededittext.MaskedEditText;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;

public class CadastroTelefoneActivity extends AppCompatActivity {

    Button btnProximo;
    Button btnCancelar;

    TextInputLayout textInputLayoutPhone;
    TextInputEditText text_cd_phone;
    TextInputEditText textDDD;
    TextInputEditText maskedEditText;
    String wantPermission = Manifest.permission.READ_PHONE_STATE;
    ArrayList<String> _mst=new ArrayList<>();
    private static final int PERMISSION_REQUEST_CODE = 1;


    View.OnFocusChangeListener listenerChengeFocus = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {

            if (!hasFocus && v.getId() == R.id.text_cd_bairro) {
                if (validatePhone()) {
                    btnProximo.setEnabled(false);
                }
                ;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_telefone);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        btnProximo = findViewById(R.id.btnAvancar);
        textInputLayoutPhone = findViewById(R.id.textInputLayoutPhone);
        maskedEditText = findViewById(R.id.text_cd_phone);
        maskedEditText.addTextChangedListener(MaskEditUtil.mask(maskedEditText, MaskEditUtil.FORMAT_FONE, getApplicationContext()));
        maskedEditText.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable editable) {
                validatePhone();
                textInputLayoutPhone.setError(null);
            }
        });

        //  maskedEditText.setMaskedText(getMyPhoneNumber().replace('+', ' ').trim());

     /*   if (!checkPermission(wantPermission)) {
            requestPermission(wantPermission);
        } else {
            maskedEditText.setMaskedText(getPhone().get(2));
          //  Log.d(TAG, "Phone number: " + getPhone());
            _mst = getPhone();

            for (String op: _mst) {
                Log.i("Device Information", String.valueOf(op));
            }
        }*/

        btnCancelar = findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnProximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validatePhone() == true) {

                    Intent intent_cadastro = new Intent(getApplicationContext(), CreateAccountUserActivity.class);
                    intent_cadastro.putExtra("num_telefone", maskedEditText.getText().toString());
                    startActivity(intent_cadastro);
                    btnProximo.setEnabled(false);
                    overridePendingTransition(R.anim.entrer_from_right,
                            R.anim.exit_to_left);

                }
            }
        });

    }

    @Override
    protected void onResume(){
        super.onResume();
        btnProximo.setEnabled(true);
    }

    private void requestPermission(String permission){
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)){
            Toast.makeText(this, "Phone state permission allows us to get phone number. Please allow it for additional functionality.", Toast.LENGTH_LONG).show();
        }
        ActivityCompat.requestPermissions(this, new String[]{permission},PERMISSION_REQUEST_CODE);
    }

    @TargetApi(Build.VERSION_CODES.O)
    private ArrayList<String> getPhone() {

        TelephonyManager phoneMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), wantPermission) != PackageManager.PERMISSION_GRANTED) {
            return null;
        }
        ArrayList<String> _lst =new ArrayList<>();
        _lst.add(""+String.valueOf(phoneMgr.getCallState()));
        _lst.add(""+phoneMgr.getImei());
        _lst.add(""+phoneMgr.getLine1Number());
        _lst.add(""+phoneMgr.getSimSerialNumber());
        _lst.add(""+phoneMgr.getSimOperatorName());
        _lst.add(""+phoneMgr.getMeid());
        _lst.add(""+String.valueOf(phoneMgr.getSimState()));
        _lst.add(""+phoneMgr.getSimCountryIso());
        return _lst;

    }

    private boolean checkPermission(String permission){
        if (Build.VERSION.SDK_INT >= 23) {
            int result = ContextCompat.checkSelfPermission(this, permission);
            if (result == PackageManager.PERMISSION_GRANTED){
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }

    private boolean validatePhone() {

        String usernameInput = maskedEditText.getText().toString().trim();
        if (maskedEditText.getText().toString().isEmpty()){
            textInputLayoutPhone.setError("O campo não pode ficar vazio");
            return false;

        }else if (maskedEditText.getText().toString().trim().length() < 14) {

            textInputLayoutPhone.setError("número de telefone inválido");
            return false;

        }
        else if (maskedEditText.getText().toString().trim().length() > 18) {
            textInputLayoutPhone.setError("número de telefone muito longo");
            return false;
        }

        /*else if (usernameInput.length()<8 || !ValidationUtilField.TELEPHONE_PATTERN.matcher(usernameInput.replace("+","").replace("-","")).matches()) {
            textInputLayoutPhone.setError("Número inválido");
            return false;
        }*/
        else {
            textInputLayoutPhone.setError(null);
            return true;
        }
    }

}