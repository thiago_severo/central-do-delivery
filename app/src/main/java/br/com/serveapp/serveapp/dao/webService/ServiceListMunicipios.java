package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.CreateAddressCityAccount;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceListMunicipios extends OnPostResponse {

    CreateAddressCityAccount createAddressAccountActivity;

    public ServiceListMunicipios(CreateAddressCityAccount createAddressAccountActivity) {
        this.createAddressAccountActivity = createAddressAccountActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_municipios.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(createAddressAccountActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        createAddressAccountActivity.btn_cd_confirm_city.setEnabled(false);
        super.onErrorResponse(error);
    }

    @Override
    public Map<String, String> getParam() {
        return new HashMap<>();
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {
            JSONArray result = jsonObject.getJSONArray("result");
            createAddressAccountActivity.initializeMun(result);
            createAddressAccountActivity.btn_cd_confirm_city.setEnabled(true);
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(createAddressAccountActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
            createAddressAccountActivity.btn_cd_confirm_city.setEnabled(false);

        }
    }

    @Override
    public void onResponse(String response) {
    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                createAddressAccountActivity.initializeMun(result);
                createAddressAccountActivity.btn_cd_confirm_city.setEnabled(true);
            } catch (JSONException e) {
                e.printStackTrace();

                Toast.makeText(createAddressAccountActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                createAddressAccountActivity.btn_cd_confirm_city.setEnabled(false);

            }
        }
    }
}