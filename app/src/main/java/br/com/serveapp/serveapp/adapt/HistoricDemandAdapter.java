package br.com.serveapp.serveapp.adapt;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.Cliente;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.enun.StatusPedido;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HistoricDemandAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Cliente cliente;
    JSONArray jsonArray;
    HistoricDemandAdapter.OnItemClickListner listener;
    Context context;
    private final int VIEW_TYPE_DEMAND_NOT_FINISH = 1;
    private final int VIEW_TYPE_DEMAND_FINISH_NOT_LIKED = 2;
    private final int VIEW_TYPE_DEMAND_FINISH_ALREADY_LIKED = 3;

    public HistoricDemandAdapter(Context context, JSONArray jsonArray) {
        this.jsonArray = jsonArray;
        this.context = context;
        this.cliente = cliente;
    }

    @Override
    public int getItemViewType(int position) {
        int returno = 0;
        try {
            if ((jsonArray.get(position) != null) && (jsonArray.getJSONObject(position).getString("status").equals(StatusPedido.ENTREGUE.toString().toLowerCase()))) {
                returno = VIEW_TYPE_DEMAND_FINISH_ALREADY_LIKED;
            } else if ((jsonArray.get(position) != null) && (jsonArray.getJSONObject(position).getString("status").equals(StatusPedido.PREPARANDO.toString().toLowerCase()))) {
                returno = VIEW_TYPE_DEMAND_NOT_FINISH;
            } else {
                return 0;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return returno;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        if (viewType == VIEW_TYPE_DEMAND_NOT_FINISH) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_promotion_home_estabeleciment, parent, false);
            HistoricDemandAdapter.LineHistoricDemand lineHistoricDemand = new HistoricDemandAdapter.LineHistoricDemand(view);
            lineHistoricDemand.progressBar.setProgress(50);
            lineHistoricDemand.ratingBar.setVisibility(View.GONE);
            viewHolder = lineHistoricDemand;
        } else if (viewType == VIEW_TYPE_DEMAND_FINISH_NOT_LIKED) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_promotion_home_estabeleciment, parent, false);
            HistoricDemandAdapter.LineHistoricDemand lineHistoricDemand = new HistoricDemandAdapter.LineHistoricDemand(view);
            lineHistoricDemand.progressBar.setProgress(100);
            lineHistoricDemand.ratingBar.setVisibility(View.GONE);
            viewHolder = lineHistoricDemand;
        } else if (viewType == VIEW_TYPE_DEMAND_FINISH_ALREADY_LIKED) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_historic_demand_liked, parent, false);
            HistoricDemandAdapter.LineHistoricDemandLiked lineHistoricDemandLiked = new HistoricDemandAdapter.LineHistoricDemandLiked(view);
            lineHistoricDemandLiked.progressBar.setProgress(100);
            viewHolder = lineHistoricDemandLiked;
        }

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        return jsonArray.length();
    }

    void collapseExpandTextView(final CardView cardView) {
        if (cardView.getVisibility() == View.GONE) {
            cardView.setVisibility(View.VISIBLE);
            AlphaAnimation animation1 = new AlphaAnimation(0.2f, 1.0f);
            animation1.setDuration(500);
            cardView.setAlpha(1f);
            cardView.startAnimation(animation1);

        } else {

            cardView.setAlpha(0.5f);
            cardView.animate()
                    .alpha(0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            cardView.setVisibility(View.GONE);
                        }
                    });

        }


    }


    public interface OnItemClickListner {
        void onItemClick(JSONObject pedido);
    }

    public void setOnItemClickListener(HistoricDemandAdapter.OnItemClickListner listener) {
        this.listener = listener;
    }


    public class LineHistoricDemandLiked extends RecyclerView.ViewHolder {

        RatingBar ratingBarEvaluate;
        TextView textNameEstabeleciment;
        TextView textStatus;
        CardView cardDetailPedido;
        CardView cardPedido;
        LinearLayout linearLayoutDetailDemand;
        ProgressBar progressBar;

        public LineHistoricDemandLiked(@NonNull View itemView) {
            super(itemView);

            textStatus = itemView.findViewById(R.id.textDemandStatus);
            ratingBarEvaluate = itemView.findViewById(R.id.ratting_avaliation);
            textNameEstabeleciment = itemView.findViewById(R.id.textNameEstabeleciment);
            cardDetailPedido = itemView.findViewById(R.id.cardDetailDemand);
            cardPedido = itemView.findViewById(R.id.cardRolation);
            linearLayoutDetailDemand = itemView.findViewById(R.id.linearLayoutCardDemand);
            progressBar = itemView.findViewById(R.id.progressBarDemandLiked);

            ratingBarEvaluate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        try {
                            listener.onItemClick(jsonArray.getJSONObject(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    }


    public class LineHistoricDemand extends RecyclerView.ViewHolder {

        Button btnAvaliar;
        TextView textNameEstabeleciment;
        TextView textStatus;
        TextView textDateDemand;
        TextView textTotalDemand;
        CardView cardDetailPedido;
        CardView cardPedido;
        ProgressBar progressBar;
        LinearLayout linearLayoutDetailDemand;
        RatingBar ratingBar;
        public LineHistoricDemand(@NonNull View itemView) {
            super(itemView);
            textStatus = itemView.findViewById(R.id.textDemandStatus);
            textDateDemand = itemView.findViewById(R.id.textDateDemand);
            textTotalDemand = itemView.findViewById(R.id.textTotalValueDemand);
            ratingBar = itemView.findViewById(R.id.ratting_avaliation);
            btnAvaliar = itemView.findViewById(R.id.btnAvaliateDemand);
            textNameEstabeleciment = itemView.findViewById(R.id.textNameEstabeleciment);
            cardDetailPedido = itemView.findViewById(R.id.cardDetailDemand);
            cardPedido  = itemView.findViewById(R.id.cardRolation);
            linearLayoutDetailDemand = itemView.findViewById(R.id.linearLayoutCardDemand);
            progressBar = itemView.findViewById(R.id.progressBarDemandLiked);
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.GREEN));

            btnAvaliar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if(listener!=null && position!=RecyclerView.NO_POSITION) {
                        try {
                            btnAvaliar.setVisibility(View.GONE);
                            ratingBar.setVisibility(View.VISIBLE);
                            listener.onItemClick(jsonArray.getJSONObject(position));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof HistoricDemandAdapter.LineHistoricDemand) {
            try {
                ((HistoricDemandAdapter.LineHistoricDemand) holder).textNameEstabeleciment.setText(jsonArray.getJSONObject(position).getString("nomeEstabeleciment"));
                ((HistoricDemandAdapter.LineHistoricDemand) holder).textStatus.setText(jsonArray.getJSONObject(position).getString("status"));
            } catch (JSONException e) { e.printStackTrace(); }

            ((HistoricDemandAdapter.LineHistoricDemand) holder).cardPedido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    collapseExpandTextView(((LocalDemandAdapter.LineHistoricDemand) holder).cardDetailPedido);
                }
            });

        } else if (holder instanceof HistoricDemandAdapter.LineHistoricDemandLiked) {

            try {
                ((HistoricDemandAdapter.LineHistoricDemandLiked) holder).textNameEstabeleciment.setText(jsonArray.getJSONObject(position).getString("nomeEstabeleciment"));
                ((LineHistoricDemandLiked) holder).textStatus.setText(jsonArray.getJSONObject(position).getString("status"));
                ((LineHistoricDemandLiked) holder).progressBar.setProgress(100);

            } catch (JSONException e) {
                e.printStackTrace();
            }

            ((LineHistoricDemandLiked) holder).cardPedido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    collapseExpandTextView(((LineHistoricDemandLiked) holder).cardDetailPedido);
                }
            });
        }
    }
}