package br.com.serveapp.serveapp.entidades;

import br.com.serveapp.serveapp.entidades.cardapio.Ingrediente;
import br.com.serveapp.serveapp.enun.TipoSubItemCardapio;

import java.io.Serializable;
import java.util.List;

public class SubItem implements Serializable {

    private long id;
    private int qtdMin, qtdMax;
    private String nome;
    private List<Ingrediente> ingredientes;
    private TipoSubItemCardapio tipoSubItemCardapio;
    private boolean obrigatório;

    public void setTipoSubItemCardapio(TipoSubItemCardapio tipoSubItemCardapio) {
        this.tipoSubItemCardapio = tipoSubItemCardapio;
    }

    public TipoSubItemCardapio getTipoSubItemCardapio() {
        return tipoSubItemCardapio;
    }

    public int getQtdMin() {
        return qtdMin;
    }
    public void setQtdMin(int qtdMin) {
        this.qtdMin = qtdMin;
    }
    public int getQtdMax() {
        return qtdMax;
    }
    public void setQtdMax(int qtdMax) {
        this.qtdMax = qtdMax;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public List<Ingrediente> getIngredientes() {
        return ingredientes;
    }
    public void setIngredientes(List<Ingrediente> ingredientes) {
        this.ingredientes = ingredientes;
    }
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

}
