package br.com.serveapp.serveapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.entidades.Estabelecimento;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class FavoritsActivity extends AppCompatActivity {

    private List<Estabelecimento> estabelecimentoList;
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private LineFavoritEstabelecimentAdapter lineFavoritEstabelecimentAdapter;
    private RecyclerView recycler_list_favorit;

    BottomNavigationView bottomNavigationView;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
           doRestart();
        }

    };

    public void doRestart() {
        startActivity(new Intent(this , NavigationDrawerActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            if( Repository.getUser(getApplicationContext())==null ){
               doRestart();
            }
        }catch (Exception e){
            finish();
        }

        setContentView(R.layout.activity_navigation_drawer_favoritos);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(0,0);
            }
        });

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

        lineFavoritEstabelecimentAdapter = new LineFavoritEstabelecimentAdapter(getApplicationContext(), Repository.fav.list());
        lineFavoritEstabelecimentAdapter.setOnItemClickListener(new LineFavoritEstabelecimentAdapter.OnItemClickListner() {
            @Override
            public void onItemClick(JSONObject estabelecimento) {
                Intent intent = new Intent(getApplicationContext(), SplashScreenEstabeleciment.class);
                try {
                    String temp = estabelecimento.getString("_id");
                    Repository.idEstabelecimento = temp;
                    Repository.atendimentoLocal = false;
                    intent.putExtra("nome",  estabelecimento.getString("nome"));
                    intent.putExtra("slogan",  estabelecimento.getString("slogan"));
                    intent.putExtra("foto", estabelecimento.getString("foto"));
                    intent.putExtra("distancia", estabelecimento.getString("distancia"));
                    intent.putExtra("fav", estabelecimento.getString("fav").equals("1"));
                    intent.putExtra("rating", 3);
                    intent.putExtra("tipoAtendimento", "delivery");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
                overridePendingTransition(0,0);
            }
        });

        recycler_list_favorit = findViewById(R.id.recycler_list_favorit);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        recycler_list_favorit.setLayoutManager(llm);
        recycler_list_favorit.setAdapter(lineFavoritEstabelecimentAdapter);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        MyNavigationNew.setButtonsAction(this);
        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new MyNavigationMenu(this, navigationView));


    }


    private List<Estabelecimento> initializeDataFavorits() {
        estabelecimentoList = new ArrayList<>();
        Estabelecimento estabelecimento = new Estabelecimento();
        estabelecimento.setDesc("O melhor hamburguer da região. O verdadeiro sabor do hambuerger");
        estabelecimento.setNome("CANTINA BURGUERIA");
        estabelecimento.setLogo("ic_burgeria");
        estabelecimentoList.add(estabelecimento);

        Estabelecimento estabelecimento2 = new Estabelecimento();
        estabelecimento2.setDesc("O melhor hamburguer da região. O verdadeiro sabor do hambuerger");
        estabelecimento2.setNome("CREPERIA XAXADO");
        estabelecimento2.setLogo("creperia");
        estabelecimentoList.add(estabelecimento2);

        return estabelecimentoList;
    }


    @Override
    protected void onResume() {
        super.onResume();
        lineFavoritEstabelecimentAdapter.setListEstabeleciments( Repository.fav.list());
        lineFavoritEstabelecimentAdapter.notifyDataSetChanged();
        if(bottomNavigationView!=null)
            bottomNavigationView.setSelectedItemId(R.id.navigation_fav_fav);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0,0);
    }



}
