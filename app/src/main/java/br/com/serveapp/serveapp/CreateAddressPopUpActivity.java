package br.com.serveapp.serveapp;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAddressRegister;
import br.com.serveapp.serveapp.dao.webService.ServiceEditAddress;
import br.com.serveapp.serveapp.dao.webService.ServiceGetAddressByCep;
import br.com.serveapp.serveapp.repository.objects.Estado;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.MaskEditUtil;
import br.com.serveapp.serveapp.util.ValidationUtilField;


public class CreateAddressPopUpActivity extends AppCompatActivity implements View.OnFocusChangeListener {

    private LocationManager locationManager;
    private Button btn;
    CardView btn_your_location;
    public Button btn_cadastrar;

    public TextInputEditText textInputEditText;
    public TextInputEditText textInputCep;
    public TextInputEditText text_cd_bairro;
    public TextInputEditText text_cd_rua;
    public TextInputEditText text_cd_cidade;
    public TextInputEditText text_cd_numero;
    public TextInputEditText text_cd_complemento;
    public TextInputEditText txtCdTitleAddress;
    public TextInputEditText descAvaliacao;

    public String idEndereco;
    public Spinner spinner;

    public TextInputLayout textInputLayoutCep;
    public TextInputLayout textInputLayoutRua;
    public TextInputLayout textInputLayoutNumero;
    public TextInputLayout textInputLayoutBairro;
    public TextInputLayout textInputLayoutCidade;

    private String lastCEP;

    public ProgressDialog mProgress;
    public ProgressDialog mProgressActiveGps;
    public ProgressDialog mProgressCreateAddress;

    private static final int REQUEST_CHECK_SETTINGS = 613;
    private LocationRequest mLocationRequest;
    private FusedLocationProviderClient fusedLocationClient;
    private Request requestCep;
    private Request requestCreateAddress;
    private Request requestEditAddress;

    ArrayAdapter<String> spinnerArrayAdapter;

    private BottomSheetBehavior mBottomSheetBehavior1, mBottomSheetBehavior2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address_account_pop_up);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);
            }
        });

        spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, Estado.estados
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);

        spinner = findViewById(R.id.spinnerCdUf);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setSelection(Estado.estados.length - 1);

        mProgress = new ProgressDialog(this);
        String titleId = "Recebendo dados da localização...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Aguarde...");

        mProgressActiveGps = new ProgressDialog(this);
        String titleActiveGps = "Ativando o GPS...";
        mProgressActiveGps.setTitle(titleActiveGps);
        mProgressActiveGps.setMessage("Aguarde...");

        mProgressCreateAddress = new ProgressDialog(this);
        String titleCreateAddress = "Criando um novo endereço...";
        mProgressCreateAddress.setTitle(titleCreateAddress);
        mProgressCreateAddress.setMessage("Aguarde...");

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        final AlertDialog.Builder builder = new AlertDialog.Builder(CreateAddressPopUpActivity.this);

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));
            }
        });

        txtCdTitleAddress = findViewById(R.id.cardNumber);
        text_cd_rua = findViewById(R.id.txtValidateCard);
        textInputCep = findViewById(R.id.txtCdCpfUser);
        text_cd_bairro = findViewById(R.id.text_cd_bairro);
        text_cd_cidade = findViewById(R.id.spinnerCidade);
        text_cd_numero = findViewById(R.id.cd_dgt_verificador);
        text_cd_complemento = findViewById(R.id.text_nome_titular);
        btn_cadastrar = findViewById(R.id.btn_cd_create_account);
        descAvaliacao = findViewById(R.id.descAvaliacao);

        imm.hideSoftInputFromWindow(txtCdTitleAddress.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(text_cd_rua.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(textInputCep.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(text_cd_bairro.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(text_cd_cidade.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(text_cd_numero.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(text_cd_complemento.getWindowToken(), 0);

        textInputLayoutCep = findViewById(R.id.textInputLayoutCpfCnpjTitular);
        textInputLayoutRua = findViewById(R.id.textInputLayoutValidateCard);
        textInputLayoutNumero = findViewById(R.id.textInputLayoutCdVerificador);
        textInputLayoutBairro = findViewById(R.id.textInputLayoutBairro);
        textInputLayoutCidade = findViewById(R.id.textInputLayoutCidade);

        requestCep = new Request(getApplicationContext(), new ServiceGetAddressByCep(this, null));

        textInputCep.addTextChangedListener(MaskEditUtil.mask(textInputCep, MaskEditUtil.FORMAT_CEP, getApplicationContext()));
        textInputCep.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutCep.setError(null);
                MaskEditUtil.mask(textInputCep, MaskEditUtil.FORMAT_CEP, getApplicationContext());
            }
        });

        textInputCep.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (!focus) {
                    String temp = textInputCep.getText().toString().replace("-", "");
                    if (!temp.equals(lastCEP) && temp.length() == 8) {
                        lastCEP = temp;
                        mProgress.show();
                        requestCep.request();
                    }
                }
            }
        });

        text_cd_complemento.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }
            @Override
            public void afterTextChanged(Editable editable) {
                text_cd_complemento.setError(null);
            }
        });

        text_cd_cidade.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                text_cd_cidade.setError(null);
            }
        });

        text_cd_rua.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                text_cd_rua.setError(null);
            }
        });

        text_cd_bairro.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutBairro.setError(null);
            }
        });

        text_cd_numero.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutNumero.setError(null);
            }
        });

        if (!getIntent().hasExtra("editAddress")) {

            if (getIntent().getStringExtra("bairro") != null && !getIntent().getStringExtra("bairro").equals("null")) {
                text_cd_bairro.setText(getIntent().getStringExtra("bairro"));
            }

            if (getIntent().getStringExtra("cidade") != null && !getIntent().getStringExtra("cidade").equals("null") && !getIntent().getStringExtra("cidade").equals("")) {
                text_cd_cidade.setText(getIntent().getStringExtra("cidade"));
                text_cd_cidade.setEnabled(false);
            } else {
                Toast.makeText(getApplicationContext(), "busque por rua, bairro ou cep", Toast.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);
                finish();
            }

            if (getIntent().getStringExtra("rua") != null && !getIntent().getStringExtra("rua").equals("null")) {
                text_cd_rua.setText(getIntent().getStringExtra("rua"));
            }

            if (getIntent().getStringExtra("cep") != null && !getIntent().getStringExtra("cep").equals("null")) {
                textInputCep.setText(getIntent().getStringExtra("cep"));
            }

            if (getIntent().getStringExtra("uf") != null && !getIntent().getStringExtra("uf").equals("null") && !getIntent().getStringExtra("uf").equals("")) {
                setSelectSpiner(getIntent().getStringExtra("uf"));
                spinner.setEnabled(false);
            } else {
                setResult(RESULT_CANCELED);
                finish();
            }

            final Map<String, String> paramLogin = new HashMap<>();
            paramLogin.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
            requestCreateAddress = new Request(getApplicationContext(), new ServiceAddressRegister(this, null));

            btn_cadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (getIsValid()) {

                        btn_cadastrar.setEnabled(false);

                        paramLogin.put("cep", textInputCep.getText().toString());
                        paramLogin.put("titulo", txtCdTitleAddress.getText().toString());
                        paramLogin.put("logradouro", text_cd_rua.getText().toString());
                        paramLogin.put("ponto_ref", descAvaliacao.getText().toString());
                        paramLogin.put("num", text_cd_numero.getText().toString());
                        paramLogin.put("complemento", text_cd_complemento.getText().toString());
                        paramLogin.put("bairro", text_cd_bairro.getText().toString());
                        paramLogin.put("cidade", text_cd_cidade.getText().toString());
                        paramLogin.put("estado", spinner.getSelectedItem().toString());
                        paramLogin.put("function_method", "add");


                        mProgressCreateAddress.show();
                        requestCreateAddress.request(paramLogin);
                    }
                }
            });

        } else {

            if (getIntent().getStringExtra("cidade") != null && !getIntent().getStringExtra("cidade").equals("null") && !getIntent().getStringExtra("cidade").equals("")) {
                text_cd_cidade.setText(getIntent().getStringExtra("cidade"));
                text_cd_cidade.setEnabled(false);
            } else {
                Toast.makeText(getApplicationContext(), "busque por rua, bairro ou cep", Toast.LENGTH_LONG).show();
                setResult(RESULT_CANCELED);
                finish();
            }

            txtCdTitleAddress.setText(getIntent().getStringExtra("titulo"));

            if (getIntent().getStringExtra("ponto_ref") != null && !getIntent().getStringExtra("ponto_ref").equals("null")) {
                descAvaliacao.setText(getIntent().getStringExtra("ponto_ref"));
            }

            if (getIntent().getStringExtra("complemento") != null && !getIntent().getStringExtra("complemento").equals("null")) {
                text_cd_complemento.setText(getIntent().getStringExtra("complemento"));
            }

            if (getIntent().getStringExtra("numero") != null && !getIntent().getStringExtra("numero").equals("null")) {
                text_cd_numero.setText(getIntent().getStringExtra("numero"));
            }

            if (getIntent().getStringExtra("bairro") != null && !getIntent().getStringExtra("bairro").equals("null")) {
                text_cd_bairro.setText(getIntent().getStringExtra("bairro"));
            }

            if (getIntent().getStringExtra("rua") != null && !getIntent().getStringExtra("rua").equals("null")) {
                text_cd_rua.setText(getIntent().getStringExtra("rua"));
            }

            if (getIntent().getStringExtra("cep") != null && !getIntent().getStringExtra("cep").equals("null")) {
                textInputCep.setText(getIntent().getStringExtra("cep"));
            }

            if (getIntent().getStringExtra("uf") != null && !getIntent().getStringExtra("uf").equals("null") && !getIntent().getStringExtra("uf").equals("")) {

                String uf = getIntent().getStringExtra("uf");
                setSelectSpiner(getIntent().getStringExtra("uf"));
                spinner.setEnabled(false);

            } else {
                setResult(RESULT_CANCELED);
                finish();
            }

            idEndereco = getIntent().getStringExtra("id_endereco");

            btn_cadastrar.setText("ATUALIZAR");
            requestEditAddress = new Request(getApplicationContext(), new ServiceEditAddress(this));
            btn_cadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (getIsValid()) {

                        String titleCreateAddress = "Atualizando o endereço...";
                        mProgressCreateAddress.setTitle(titleCreateAddress);
                        mProgressCreateAddress.setMessage("Aguarde...");

                        mProgressCreateAddress.show();
                        requestEditAddress.request();
                    }
                }
            });

        }

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        btn_your_location = findViewById(R.id.button_your_loc);
        btn_your_location.setVisibility(View.GONE);
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        createLocationRequest();

    }

    public int getPositionOfListUf(String uf) {
        int pos = 0;
        for (int i = 0; i < Estado.estados.length; i++) {
            if (Estado.estados[i].equals(uf)) {
                pos = i;
                break;
            }
        }
        return pos;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.entrer_from_right,
                R.anim.exit_to_left);

    }

    public void setSelectSpiner(String uf) {

        int position = 0;

        if (uf.length() <= 2) {
            position = spinnerArrayAdapter.getPosition(uf);
        } else {
            position = spinnerArrayAdapter.getPosition(Estado.getUfShortName(uf));
        }

        spinner.setSelection(position);
    }

    ///////////////// GPS //////////////
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void onActiviteGPS() {

        mProgressActiveGps.show();

        final FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        final LocationRequest mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(100);
        mLocationRequest.setFastestInterval(50);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        final LocationCallback mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    if (location != null) {//TODO: UI updates.
                    }
                }
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }

       /* fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(final Location location) {
                if (location != null) {
                    btn_your_location.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            onActiviteGPS();
                            useLocation(location);
                        }
                    });
                } else {
                    onActiviteGPS();
                }
            }
        });*/

        mProgressActiveGps.dismiss();

        btn_your_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    Activity#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for Activity#requestPermissions for more details.
                        return;
                    }
                }

                fusedLocationClient.requestLocationUpdates(mLocationRequest, mLocationCallback, null);
                fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        //    onActiviteGPS();
                        useLocation(location);
                    }
                });


            }
        });

       /* fusedLocationClient.getLastLocation().addOnFailureListener(
                new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgressActiveGps.dismiss();
                    }
                }
        );*/

    }

    private void useLocation(Location location) {
        mProgressActiveGps.dismiss();
        mProgress.show();
        Geocoder gcd = new Geocoder(this, Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null) {
            String uf = "PE";
            String addressLine = addresses.get(0).getAddressLine(0);
            if (addressLine != null) {
                String[] arrayAddressLine = addressLine.split("-");
                uf = arrayAddressLine[2].split(",")[0];
            }
            lastCEP = addresses.get(0).getPostalCode().replace("-", "");
            text_cd_rua.setText(addresses.get(0).getThoroughfare());
            textInputCep.setText(addresses.get(0).getPostalCode());
            text_cd_cidade.setText(addresses.get(0).getSubAdminArea());
            text_cd_cidade.setEnabled(false);
            text_cd_bairro.setText(addresses.get(0).getSubLocality());
            text_cd_numero.setText(addresses.get(0).getFeatureName());
            text_cd_complemento.setText(" ");
            setSelectSpiner(uf.trim());
            spinner.setEnabled(false);
        }

        mProgress.dismiss();

    }

    private void onRecuseActiviteGPS() {
        //Toast.makeText(this, "GPS OFF :/", Toast.LENGTH_SHORT).show();
    }

    private void userLocation() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        SettingsClient client = LocationServices.getSettingsClient(this);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        // VERIFICA SE O GPS JÁ ESTÁ ATIVADO
        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                onActiviteGPS();
            }
        });

        // CASO O GPS NÃO ESTEJA ATIVADO
        task.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (e instanceof ResolvableApiException) {
                    try {
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        resolvable.startResolutionForResult(CreateAddressPopUpActivity.this, REQUEST_CHECK_SETTINGS);
                    } catch (IntentSender.SendIntentException ignored) {
                    }
                }
            }
        });
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    onActiviteGPS();
                    break;
                case Activity.RESULT_CANCELED:
                    onRecuseActiviteGPS();
                    break;
            }
        }
    }

    private boolean textValidateCep() {
        String usernameInput = textInputCep.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputLayoutCep.setError(null);
            return true;
        } else if (ValidationUtilField.validateCep(textInputCep, textInputLayoutCep) == false) {
            textInputLayoutCep.setError("CEP inválido");
            return false;
        } else {
            textInputLayoutCep.setError(null);
            return true;
        }
    }

    private boolean textValidateReferencia() {
        String str = descAvaliacao.getText().toString().trim();
        if (str.length()<10) {
            descAvaliacao.setError("Descrição curta");
            return false;
        } else {
            descAvaliacao.setError(null);
            return true;
        }
    }


    private boolean textValidateRua() {
        String usernameInput = text_cd_rua.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputLayoutRua.setError("O campo não pode ficar vazio");
            return false;
        } else {
            textInputLayoutRua.setError(null);
            return true;
        }
    }

    public boolean validateNumero() {
        String usernameInput = text_cd_numero.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputLayoutNumero.setError("Número ?");
            return false;
        } else {
            textInputLayoutNumero.setError(null);
            return true;
        }
    }

    public boolean validateBairro() {
        String usernameInput = text_cd_bairro.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputLayoutBairro.setError("O campo não pode ficar vazio");
            return false;
        } else {
            textInputLayoutBairro.setError(null);
            return true;
        }
    }

    public boolean validateCidade() {
        String usernameInput = text_cd_cidade.getText().toString().trim();
        if (usernameInput.isEmpty()) {
            textInputLayoutCidade.setError("O campo não pode ficar vazio");
            return false;
        } else {
            textInputLayoutCidade.setError(null);
            return true;
        }
    }


    public void startInitialActivities() {
        Intent intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
        startActivity(intent);
    }

    public void loadFinish() {


        Intent intent = new Intent();
        try {
            intent.putExtra("id_endereco", Repository.getUser(getApplicationContext()).getString("id_endereco"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    public boolean getIsValid() {

        //   Toast.makeText(getApplicationContext(), "idEndereco "+idEndereco , Toast.LENGTH_LONG).show();

        boolean isValidRua = textValidateRua();
        boolean isValidBairro = validateBairro();
        boolean isValidCep = textValidateCep();
        boolean isValidNumero = validateNumero();
        boolean isValidCidade = validateCidade();
        boolean isValidReferencia = textValidateReferencia();

        return isValidRua && isValidBairro && isValidCep && isValidNumero && isValidCidade && isValidReferencia;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus && v.getId() == R.id.txtCdCpfUser) {
            //  textValidateCep();
        } else if (!hasFocus && v.getId() == R.id.txtValidateCard) {
            textValidateRua();
        } else if (!hasFocus && v.getId() == R.id.cd_dgt_verificador) {
            //  validateNumero();
        } else if (!hasFocus && v.getId() == R.id.text_cd_bairro) {
            //  validateBairro();
        } else if (!hasFocus && v.getId() == R.id.spinnerCidade) {
            //  validateCidade();
        }
    }
}
