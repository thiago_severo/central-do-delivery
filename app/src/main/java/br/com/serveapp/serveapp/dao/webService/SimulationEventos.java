package br.com.serveapp.serveapp.dao.webService;

public class SimulationEventos {

    public static String result = "{" +
            "\"result\": [" +
            "{" +
            "\"data\": \"hoje\"," +
            "\"eventos\": [" +
            "{" +
            "\"foto\":\"cat_beer.jpg\"," +

            "\"atracoes\":[\"zezo\", \"lilla\"]," +

            "\"endereco\":\"Rua da praça\"," +
            "\"fotoOrganizadora\":\"francisca.jpg\"," +
            "\"localTitle\":\"Praça publica\"," +
            "\"titulo\":\"Festa do multirão\"," +
            "\"descricao\":\"Melhor festa do nordeste\"," +
            "\"data\":\"2019-09-08\"," +
            "\"hora_inicio\":\"22:00:00\"," +
            "\"hora_fim\":\"03:00:00\"," +
            "\"valor\":\"R$ 10,00\"," +
            "\"nome\":\"nova biz\"," +
            "\"tipo_cobranca\":1" +
            "}," +
            "{" +
            "\"foto\":\"cat_beer.jpg\"," +

            "\"atracoes\":[\"zezo\", \"lilla\"]," +

            "\"endereco\":\"Rua da praça\"," +
            "\"fotoOrganizadora\":\"francisca.jpg\"," +
            "\"localTitle\":\"Praça publica\"," +
            "\"titulo\":\"Festa do alto do bom jesus\"," +
            "\"descricao\":\"Melhor festa do nordeste\"," +
            "\"data\":\"2019-09-08\"," +
            "\"hora_inicio\":\"22:00:00\"," +
            "\"hora_fim\":\"03:00:00\"," +
            "\"valor\":\"R$ 10,00\"," +
            "\"nome\":\"nova biz\"," +
            "\"tipo_cobranca\":1" +
            "}" +
            "]" +
            "" +
            "}," +
            "{" +
            "\"data\": \"08/09\"," +
            "\"eventos\": [" +
            "{" +
            "\"foto\":\"cat_beer.jpg\"," +
            "\"atracoes\":[\"zezo\", \"lilla\"]," +

            "\"endereco\":\"Rua da praça\"," +
            "\"fotoOrganizadora\":\"\"," +
            "\"localTitle\":\"Praça publica\"," +
            "\"titulo\":\"Festa de Setembro\"," +
            "\"descricao\":\"Melhor festa do nordeste\"," +
            "\"data\":\"2019-09-08\"," +
            "\"hora_inicio\":\"22:00:00\"," +
            "\"hora_fim\":\"03:00:00\"," +
            "\"valor\":\"R$ 10,00\"," +
            "\"nome\":\"nova biz\"," +
            "\"tipo_cobranca\":1" +
            "}," +
            "{" +
            "\"foto\":\"cat_beer.jpg\"," +
            "\"atracoes\":[\"zezo\", \"lilla\"]," +
            "\"endereco\":\"Rua da praça\"," +
            "\"fotoOrganizadora\":\"\"," +
            "\"localTitle\":\"Praça publica\"," +
            "\"titulo\":\"Aniversário\"," +
            "\"descricao\":\"Melhor festa do nordeste\"," +
            "\"data\":\"2019-09-08\"," +
            "\"hora_inicio\":\"22:00:00\"," +
            "\"hora_fim\":\"03:00:00\"," +
            "\"valor\":\"R$ 10,00\"," +
            "\"nome\":\"nova biz\"," +
            "\"tipo_cobranca\":1" +
            "}" +
            "]" +
            "" +
            "}" +
            "]" +
            "}";
}
