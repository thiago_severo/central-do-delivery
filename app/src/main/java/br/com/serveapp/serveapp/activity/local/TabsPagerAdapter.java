package br.com.serveapp.serveapp.activity.local;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    public List<Fragment> fragments = new ArrayList<>();
    private List<String> titulos = new ArrayList<>();

    public FragmentManager fragmentManager;
    public TabsPagerAdapter(FragmentManager fm) {
        super(fm);
        fragmentManager = fm;

    }

    public void adicionar(Fragment fragment, String tituloAba){
        this.fragments.add(fragment);
        this.titulos.add(tituloAba);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0){
           fragments.get(0);
        }
        return this.fragments.get(position);
    }



    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

        @Override
    public int getCount() {
        return this.fragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        return this.titulos.get(position);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}