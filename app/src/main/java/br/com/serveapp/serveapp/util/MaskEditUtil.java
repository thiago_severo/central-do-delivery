package br.com.serveapp.serveapp.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.util.InputMismatchException;
import java.util.regex.Pattern;

import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.Repository;

public abstract class MaskEditUtil {

    public static final String FORMAT_CPF = "###.###.###-##";
    public static final String FORMAT_FONE = "(##)#####-####";
    public static final String FORMAT_DDD = "(##)";
    public static final String CODE_SMS = "######";
    public static final String FORMAT_CEP = "#####-###";

    public static final String FORMAT_VALID_DATE = "##/##";
    public static final String FORMAT_VALID_NAME = "[a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ]* [a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](.*)";

    public static final String CODE_CARD = "###";
    public static final String DATE_CARD = "###";

    public static final String CODE_CARD4 = "####";

    public static final String FORMAT_DATE = "##/##/####";
    public static final String FORMAT_HOUR = "##:##";
    public static final String CARD_PAYMENT = "##### #### #### ####";

    public static validCardData validCardData;

    private static final String CPFMask = "###.###.###-##";
    private static final String CNPJMask = "##.###.###/####-##";

    static Pattern nomeSobreNome = Pattern.compile("[a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ]* [a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](.*)");
    static Pattern master = Pattern.compile("^5[1-5][0-9]{14}"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern visa = Pattern.compile("^4[0-9]{12}(?:[0-9]{3})?$"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern dinners = Pattern.compile("^3(?:0[0-5]|[68][0-9])[0-9]{11}$"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern amex = Pattern.compile("^3[47][0-9]{13}"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern jcb = Pattern.compile("^(?:2131|1800|35\\d{3})\\d{11}"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern elo = Pattern.compile("^(?:401178|401179|431274|438935|451416|457393|457631|457632|504175|627780|636297|636368|\n" +
            "    655000|655001|651652|651653|651654|650485|650486|650487|650488|506699|5067[0-6][0-9]|\n" +
            "    50677[0-8]|509\\d{3})\\d{10}$"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern discover = Pattern.compile("^6(?:011|5[0-9]{2})[0-9]{12}"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern hipercard = Pattern.compile("^(606282\\d{10}(\\d{3})?)"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr
    static Pattern hipercard2 = Pattern.compile("^(3841\\d{15})"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr


    public static String formatCard(String cardNumber) {
        if (cardNumber == null) return null;
        char delimiter = ' ';
        return cardNumber.replaceAll(".{4}(?!$)", "$0" + delimiter);
    }

    public static String formatDate(String cardNumber) {
        if (cardNumber == null) return null;
        char delimiter = '/';
        return cardNumber.replaceAll(".{2}(?!$)", "$0" + delimiter);
    }


    public static TextWatcher maskCard(final EditText ediTxt, final Context context, final ImageView imgCard, final Button btnClean, final TextInputLayout textInputLayout) {

        return new TextWatcher() {

            boolean isUpdating;
            String old = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                textInputLayout.setError(null);

                if(btnClean!=null)
                if(ediTxt.getText().length()>0){
                    btnClean.setVisibility(View.VISIBLE);
                }

                String str = "";

                if (s.length() >= 25) {
                    str = MaskEditUtil.unmask(s.toString().substring(0, 24));
                } else {
                    str = MaskEditUtil.unmask(s.toString());
                }

                String mascara = "";

                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }

                mascara = formatCard(str);
                isUpdating = true;
                ediTxt.setText(mascara);
                ediTxt.setSelection(mascara.length());

                String card1 = getBandeira(mascara.replace(" ", ""));

                if (card1 != null && !card1.equals("null")) {
                 //   imgCard.setImageBitmap(null);

               //     Repository.urlIconCard = "card/" + getBandeira(mascara.replace(" ", "")) + ".png";
                    new ListSetImage(context, imgCard).execute("card/" + getBandeira(mascara.replace(" ", "")) + ".png");

                    System.out.println("bandeira: " + "card/" + getBandeira(mascara.replace(" ", "")) + ".png");
                    validCardData.validCard(true, getBandeira(mascara.replace(" ", "")));
                } else {
                    imgCard.setImageBitmap(null);
                    new ListSetImage(context, imgCard).execute("card/" + "cardIcon" + ".jpg");
                    validCardData.validCard(false, null);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        };
    }

    public static String getBandeira(String numberCard) {

        String retorno = null;

        if (master.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "master";
        } else if (elo.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "elo";
        } else if (visa.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "visa";
        } else if (amex.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "amex";
        } else if (dinners.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "diners";
        } else if (hipercard.matcher(numberCard.replace(" ", "")).matches()) {///////
            retorno = "hipercard";
        } else if (hipercard2.matcher(numberCard.replace(" ", "")).matches()) {///////
            retorno = "hipercard";
        } else if (discover.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "discover";
        } else if (jcb.matcher(numberCard.replace(" ", "")).matches()) {
            retorno = "jcb";
        }

        return retorno;

    }


    public static TextWatcher mask(final EditText ediTxt, final String mask, final Context context) {

        return new TextWatcher() {

            boolean isUpdating;
            String old = "";

            @Override
            public void afterTextChanged(final Editable s) {

            }

            @Override
            public void beforeTextChanged(final CharSequence s, final int start, final int count, final int after) {
            }

            @Override
            public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {

                final String str = MaskEditUtil.unmask(s.toString());
                String mascara = "";

                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }

                int i = 0;

                for (final char m : mask.toCharArray()) {

                    if (m != '#' && str.length() > old.length()) {

                        mascara += m;
                        continue;

                    }
                    try {

                        mascara += str.charAt(i);
                    } catch (final Exception e) {
                        break;
                    }

                    i++;

                }

                isUpdating = true;
                ediTxt.setText(mascara);
                ediTxt.setSelection(mascara.length());

            }
        };
    }


    public static TextWatcher maskDate(final EditText ediTxt) {

        return new TextWatcher() {
            boolean isUpdating;
            String old = "";

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {

                String str = "";

                if (s.length() >= 5) {
                    str = MaskEditUtil.unmask(s.toString().substring(0, 5));
                } else {
                    str = MaskEditUtil.unmask(s.toString());
                }

                String mascara = "";

                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }

                mascara = formatDate(str);
                isUpdating = true;
                ediTxt.setText(mascara);
                ediTxt.setSelection(mascara.length());

            }

            @Override
            public void afterTextChanged(Editable editable) {
            }

        };
    }


    public static String unmask(final String s) {
        return s.replaceAll("[.]", "").replaceAll("[-]", "").replaceAll("[/]", "").replaceAll("[(]", "").replaceAll("[ ]", "").replaceAll("[:]", "").replaceAll("[)]", "");
    }


    public interface validCardData {
        public void validCard(boolean isValid, String bandeira);
    }

    public static void setValidCardData(validCardData validCardDataInterface) {
        validCardData = validCardDataInterface;
    }


    private static final String maskCNPJ = "##.###.###/####-##";
    private static final String maskCPF = "###.###.###-##";

    public static TextWatcher insert2(final EditText editText, final String maskParam, final Button btnClean, final TextInputLayout textInputLayout ) {
        return new TextWatcher() {

            boolean isUpdating;
            String old = "";

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                textInputLayout.setError(null);

                if(btnClean!=null)
                    if(editText.getText().length()>0){
                        btnClean.setVisibility(View.VISIBLE);
                    }

                String str = unmask(s.toString());
                String mask;
                String defaultMask = getDefaultMask(str);

                mask = maskParam;

                String mascara = "";
                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }
                int i = 0;
                for (char m : mask.toCharArray()) {
                    if ((m != '#' && str.length() > old.length()) || (m != '#' && str.length() < old.length() && str.length() != i)) {
                        mascara += m;
                        continue;
                    }

                    try {
                        mascara += str.charAt(i);
                    } catch (Exception e) {
                        break;
                    }
                    i++;
                }
                isUpdating = true;
                editText.setText(mascara);
                editText.setSelection(mascara.length());
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        };
    }

    public static TextWatcher insert(final EditText editText, final Button btnClean) {
        return new TextWatcher() {
            boolean isUpdating;
            String old = "";

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(editText.getText().length()>0) {
                    btnClean.setVisibility(View.VISIBLE);
                }

                String str = unmask(s.toString());
                String mask;
                String defaultMask = getDefaultMask(str);
                switch (str.length()) {
                    case 11:
                        mask = maskCPF;
                        break;
                    case 14:
                        mask = maskCNPJ;
                        break;

                    default:
                        mask = defaultMask;
                        break;
                }

                String mascara = "";
                if (isUpdating) {
                    old = str;
                    isUpdating = false;
                    return;
                }
                int i = 0;
                for (char m : mask.toCharArray()) {
                    if ((m != '#' && str.length() > old.length()) || (m != '#' && str.length() < old.length() && str.length() != i)) {
                        mascara += m;
                        continue;
                    }

                    try {
                        mascara += str.charAt(i);
                    } catch (Exception e) {
                        break;
                    }
                    i++;
                }
                isUpdating = true;
                editText.setText(mascara);
                editText.setSelection(mascara.length());
            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        };
    }

    private static String getDefaultMask(String str) {
        String defaultMask = maskCPF;
        if (str.length() > 11) {
            defaultMask = maskCNPJ;
        }
        return defaultMask;
    }


    public static boolean isCNPJ(String CNPJ) {

        CNPJ = removeCaracteresEspeciais(CNPJ);

        // considera-se erro CNPJ's formados por uma sequencia de numeros iguais
        if (CNPJ.equals("00000000000000") || CNPJ.equals("11111111111111") || CNPJ.equals("22222222222222") || CNPJ.equals("33333333333333") || CNPJ.equals("44444444444444") || CNPJ.equals("55555555555555") || CNPJ.equals("66666666666666") || CNPJ.equals("77777777777777") || CNPJ.equals("88888888888888") || CNPJ.equals("99999999999999") || (CNPJ.length() != 14))
            return (false);

        char dig13, dig14;
        int sm, i, r, num, peso;

        // "try" - protege o código para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 2;
            for (i = 11; i >= 0; i--) {
                // converte o i-ésimo caractere do CNPJ em um número:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posição de '0' na tabela ASCII)
                num = (int) (CNPJ.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10)
                    peso = 2;
            }

            r = sm % 11;
            if ((r == 0) || (r == 1))
                dig13 = '0';
            else
                dig13 = (char) ((11 - r) + 48);

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 2;
            for (i = 12; i >= 0; i--) {
                num = (int) (CNPJ.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso + 1;
                if (peso == 10)
                    peso = 2;
            }

            r = sm % 11;
            if ((r == 0) || (r == 1))
                dig14 = '0';
            else
                dig14 = (char) ((11 - r) + 48);

            // Verifica se os dígitos calculados conferem com os dígitos informados.
            if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13)))
                return (true);
            else
                return (false);
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    private boolean validateName(String name) {

        boolean res = false;
        Pattern amex = Pattern.compile("[a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ]* [a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](.*)"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr

        if(amex.matcher(name).matches()){
            res = true;
        }

        return res;
    }

    public static boolean isCPF(String CPF) {

        CPF = removeCaracteresEspeciais(CPF);

        // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (CPF.equals("00000000000") || CPF.equals("11111111111") || CPF.equals("22222222222") || CPF.equals("33333333333") || CPF.equals("44444444444") || CPF.equals("55555555555") || CPF.equals("66666666666") || CPF.equals("77777777777") || CPF.equals("88888888888") || CPF.equals("99999999999") || (CPF.length() != 11))
            return (false);

        char dig10, dig11;
        int sm, i, r, num, peso;

        // "try" - protege o codigo para eventuais erros de conversao de tipo (int)
        try {
            // Calculo do 1o. Digito Verificador
            sm = 0;
            peso = 10;
            for (i = 0; i < 9; i++) {
                // converte o i-esimo caractere do CPF em um numero:
                // por exemplo, transforma o caractere '0' no inteiro 0
                // (48 eh a posicao de '0' na tabela ASCII)
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else
                dig10 = (char) (r + 48); // converte no respectivo caractere numerico

            // Calculo do 2o. Digito Verificador
            sm = 0;
            peso = 11;
            for (i = 0; i < 10; i++) {
                num = (int) (CPF.charAt(i) - 48);
                sm = sm + (num * peso);
                peso = peso - 1;
            }

            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig11 = '0';
            else
                dig11 = (char) (r + 48);

            // Verifica se os digitos calculados conferem com os digitos informados.
            if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
                return (true);
            else
                return (false);
        } catch (InputMismatchException erro) {
            return (false);
        }
    }

    private static String removeCaracteresEspeciais(String doc) {
        if (doc.contains(".")) {
            doc = doc.replace(".", "");
        }
        if (doc.contains("-")) {
            doc = doc.replace("-", "");
        }
        if (doc.contains("/")) {
            doc = doc.replace("/", "");
        }
        return doc;
    }

}
