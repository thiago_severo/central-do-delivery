package br.com.serveapp.serveapp.fragments;

import org.json.JSONArray;

public interface AvaliacaoItemInterface {

    public void initializeAdapterListItens(JSONArray result, String position);

    public void dimissPopUpAvaliacao();

    public void dimissProgressAvaliacaoItem();

}