package br.com.serveapp.serveapp;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAddCidadeCliente;
import br.com.serveapp.serveapp.dao.webService.ServiceListMunicipios;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CreateAddressCityAccount extends AppCompatActivity {

    public Button btn_cd_confirm_city;
    public Spinner spinner;
    public Spinner spinnerUf;
    public ProgressDialog mProgress;
    ArrayAdapter<String> spinnerArrayAdapter;
    ArrayAdapter<String> spinnerArrayAdapterUf;
    JSONArray municipios;

    Request serviceListMunicipios;
    Request add_cidade_cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_address_city_account);

        add_cidade_cliente = new Request(getApplicationContext(), new ServiceAddCidadeCliente(this));
        serviceListMunicipios = new Request(getApplicationContext(),new ServiceListMunicipios(this));

        spinnerUf = findViewById(R.id.spinnerCdUf);
        spinner = findViewById(R.id.spinnerCidade);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.entrer_from_right,
                        R.anim.exit_to_left);
            }
        });

        mProgress = new ProgressDialog(this);
        String titleId = "Enviando dados...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("Aguarde...");

        btn_cd_confirm_city = findViewById(R.id.btn_cd_confirm_city);
        btn_cd_confirm_city.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                btn_cd_confirm_city.setEnabled(false);
                mProgress.show();
                String idMunicipio =  getIdMunicipio(municipios, spinnerUf.getSelectedItemPosition(), spinner.getSelectedItemPosition());
                Map<String, String> params = new HashMap<>();
                params.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                params.put("id_municipio",idMunicipio);
                add_cidade_cliente.request(params);
             }
        });

        serviceListMunicipios.request();

    }

    public String getIdMunicipio(JSONArray result, int uf, int mun){

        try {
            return result.getJSONObject(uf).getJSONArray("municipios").getJSONObject(mun).getString("id");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<String> getMunicipio(JSONArray  jsonArray, int pos){
        List<String> listUf = new ArrayList<>();
        try {
            JSONArray result = jsonArray.getJSONObject(pos).optJSONArray("municipios");
            for(int i=0; i<result.length(); i++){
                listUf.add(result.getJSONObject(i).getString("cidade"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listUf;
    }

    public void initializeMun(final JSONArray result) {

        municipios = result;
        List<String> listUf = new ArrayList<>();
        for(int i=0; i<result.length(); i++){
            try {
                listUf.add(result.getJSONObject(i).getString("estado"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        spinnerArrayAdapterUf = new ArrayAdapter<String>(
                this, R.layout.spinner_item, listUf
        );

        spinnerUf.setAdapter(spinnerArrayAdapterUf);
        spinnerUf.setSelection(2);

        spinnerArrayAdapter = new ArrayAdapter<String>(
                this, R.layout.spinner_item, getMunicipio(result, 0)
        );

        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setSelection(0);


        spinnerUf.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                spinnerArrayAdapter = new ArrayAdapter<String>(
                        getApplicationContext(), R.layout.spinner_item, getMunicipio(result, i)
                );
                spinner.setAdapter(spinnerArrayAdapter);
                spinner.setSelection(0);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    public void startInitialActivities(){
        Intent intent = new Intent(getApplicationContext(), NavigationDrawerActivity.class);
        setResult(Activity.RESULT_OK);
        startActivity(intent);
        finish();
    }

}
