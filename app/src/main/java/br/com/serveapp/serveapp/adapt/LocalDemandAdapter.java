package br.com.serveapp.serveapp.adapt;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.entidades.Cliente;
import br.com.serveapp.serveapp.entidades.pedido.ItemPedido;
import br.com.serveapp.serveapp.entidades.pedido.Pedido;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class LocalDemandAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{


    Cliente cliente;
    LocalDemandAdapter.OnItemClickListner listener;
    LocalDemandAdapter.OnItemClickListner listener2;
    public JSONArray pedidoList;
    Context context;

    private final int VIEW_TYPE_DEMAND_NOT_FINISH = 1;
    private final int VIEW_TYPE_DEMAND_FINISH_NOT_LIKED =2;
    private final int VIEW_TYPE_DEMAND_FINISH_ALREADY_LIKED =3;



    public LocalDemandAdapter(Context context, List<Pedido> pedidos, Cliente cliente){
     //   pedidoList = pedidos;
        this.context = context;
        this.cliente = cliente;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder viewHolder = null;

        if(viewType ==VIEW_TYPE_DEMAND_NOT_FINISH) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_historic_demand_liked, parent, false);
            LineHistoricDemandLiked lineHistoricDemand = new LineHistoricDemandLiked(view);
            lineHistoricDemand.progressBar.setProgress(33);
            viewHolder = lineHistoricDemand;
        }
        else if(viewType ==VIEW_TYPE_DEMAND_FINISH_NOT_LIKED){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_promotion_home_estabeleciment, parent, false);
            LineHistoricDemand lineHistoricDemand = new LineHistoricDemand(view);
            lineHistoricDemand.progressBar.setProgress(100);
            lineHistoricDemand.ratingHistoric.setVisibility(View.GONE);
            viewHolder = lineHistoricDemand;
        }
        else if(viewType ==VIEW_TYPE_DEMAND_FINISH_ALREADY_LIKED){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_historic_demand_liked, parent, false);
            LineHistoricDemandLiked lineHistoricDemandLiked = new LineHistoricDemandLiked(view);
            lineHistoricDemandLiked.progressBar.setProgress(100);
            viewHolder = lineHistoricDemandLiked;
        }

        return viewHolder;
    }



    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {

        /*    ((LineHistoricDemandLiked)holder).textNameEstabeleciment.setText(pedidoList.get(position).getEstabelecimento().getNome().toString());
            ((LineHistoricDemandLiked)holder).textStatus.setText(pedidoList.get(position).getStatus().toString());

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        for(ItemPedido itemPedido: getSubItemPedido(pedidoList.get(position))){

                   View view = new View(context);
                    view = inflater.inflate(R.layout.line_demand_detail, null);
                    TextView itemDemand = new TextView(context);
                    itemDemand = view.findViewById(R.id.textItemDemand);
                    itemDemand.setText( itemPedido.getItem().getNome());
                    ((LineHistoricDemandLiked) holder).linearLayoutDetailDemand.addView(view);


                }

            ((LineHistoricDemandLiked)holder).cardPedido.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    collapseExpandTextView(((LineHistoricDemandLiked)holder).cardDetailPedido);
                }
            });
*/
        }



    @Override
    public int getItemViewType(int position) {

  /*      if ((pedidoList.get(position) != null) && (pedidoList.get(position).getStatus().equals(StatusPedido.ENTREGUE)) && AvalicaoDao.getAvaliacaoByPedido( cliente.getId(), pedidoList.get(position).getId())!=null){
           return VIEW_TYPE_DEMAND_FINISH_ALREADY_LIKED ;
        }
        else  if ( (pedidoList.get(position).getStatus().equals(StatusPedido.ENTREGUE)) && AvalicaoDao.getAvaliacaoByPedido( cliente.getId(), pedidoList.get(position).getId())==null) {
            return VIEW_TYPE_DEMAND_FINISH_NOT_LIKED ;
        }
        else if (pedidoList.get(position).getStatus().equals(StatusPedido.RECEBIDO)) {
            return VIEW_TYPE_DEMAND_NOT_FINISH;
        }
        else {
            System.out.println("00000000");
            return 0;
        }*/

  return 0;

    }



    void collapseExpandTextView(final CardView cardView) {
        if (cardView.getVisibility() == View.GONE) {

            cardView.setVisibility(View.VISIBLE);

            AlphaAnimation animation1 = new AlphaAnimation(0.2f, 1.0f);
            animation1.setDuration(500);
            cardView.setAlpha(1f);
            cardView.startAnimation(animation1);

        } else {

            cardView.setAlpha(0.5f);
            cardView.animate()
                    .alpha(0f)
                    .setDuration(500)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            cardView.setVisibility(View.GONE);
                        }
                    });

        }


    }

    @Override
    public int getItemCount() {
       return pedidoList.length();
    }


    public static class LineHistoricDemandLiked extends RecyclerView.ViewHolder {

        RatingBar ratingBarEvaluate;
        TextView textNameEstabeleciment;
        TextView textStatus;
        TextView textHorarioDemand;
        TextView textDateDemand;
        TextView textTotalDemand;
        TextView textNumPedido;
        CardView cardDetailPedido;
        CardView cardPedido;
        LinearLayout linearLayoutDetailDemand;
        ProgressBar progressBar;

        public LineHistoricDemandLiked(@NonNull View itemView) {
            super(itemView);

            textStatus = itemView.findViewById(R.id.textDemandStatus);
            ratingBarEvaluate = itemView.findViewById(R.id.ratingBarLikedListProduct3);
            textNameEstabeleciment = itemView.findViewById(R.id.textNameEstabeleciment);
            cardDetailPedido = itemView.findViewById(R.id.cardDetailDemand);
            cardPedido  = itemView.findViewById(R.id.cardRolation);
            linearLayoutDetailDemand = itemView.findViewById(R.id.linearLayoutCardDemand);
            progressBar = itemView.findViewById(R.id.progressBarDemandLiked);

           ratingBarEvaluate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                      }
            });

        }
    }





    public static class LineHistoricDemand extends RecyclerView.ViewHolder {

        Button btnAvaliar;
        TextView textNameEstabeleciment;
        TextView textStatus;

        TextView textHorarioDemand;
        TextView textDateDemand;
        TextView textTotalDemand;
        TextView textNumPedido;

        CardView cardDetailPedido;
        CardView cardPedido;
        ProgressBar progressBar;
        LinearLayout linearLayoutDetailDemand;
        RatingBar ratingHistoric;
        public LineHistoricDemand(@NonNull View itemView) {
            super(itemView);
            textStatus = itemView.findViewById(R.id.textDemandStatus);
            textDateDemand = itemView.findViewById(R.id.textDateDemand);
            textTotalDemand = itemView.findViewById(R.id.textTotalValueDemand);
            ratingHistoric = itemView.findViewById(R.id.ratting_avaliation);

            btnAvaliar = itemView.findViewById(R.id.btnAvaliateDemand);
            textNameEstabeleciment = itemView.findViewById(R.id.textNameEstabeleciment);
            cardDetailPedido = itemView.findViewById(R.id.cardDetailDemand);
            cardPedido  = itemView.findViewById(R.id.cardRolation);
            linearLayoutDetailDemand = itemView.findViewById(R.id.linearLayoutCardDemand);
            progressBar = itemView.findViewById(R.id.progressBarDemandLiked);
            progressBar.setProgressTintList(ColorStateList.valueOf(Color.GREEN));

            btnAvaliar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                   }
            });

        }
    }



    public interface OnItemClickListner{
        void onItemClick(Pedido pedido);
    }

    public void setOnItemClickListener(LocalDemandAdapter.OnItemClickListner listener){
        this.listener = listener;
    }


     private List<ItemPedido> getSubItemPedido(Pedido pedido){

        List<ItemPedido> itemPedidos = new ArrayList<>();

      for(ItemPedido item : pedido.getItens()){
          itemPedidos.add(item);
      }

        return itemPedidos;
    }


    private View buildViewItemPedido(){
        return  null;
    }


}
