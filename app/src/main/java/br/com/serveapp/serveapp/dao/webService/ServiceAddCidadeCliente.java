package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.CreateAddressCityAccount;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceAddCidadeCliente extends OnPostResponse {

    CreateAddressCityAccount createAddressCityAccount;

    public ServiceAddCidadeCliente(CreateAddressCityAccount createAddressCityAccount) {
        this.createAddressCityAccount = createAddressCityAccount;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "add_cidade_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        // loginActivity.mProgress.dismiss();
        Toast.makeText(createAddressCityAccount.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        createAddressCityAccount.btn_cd_confirm_city.setEnabled(true);
        createAddressCityAccount.mProgress.dismiss();
    }

    @Override
    public void onResponse(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("result")) {

                    Repository.getUser(createAddressCityAccount).put("id_municipio", "0");
                    Dao.save(Repository.getUser(createAddressCityAccount).toString(), FilesName.user, createAddressCityAccount.getApplicationContext());
                    createAddressCityAccount.startInitialActivities();

                }

            } catch (JSONException e) {

                e.printStackTrace();
                Toast.makeText(createAddressCityAccount.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
                createAddressCityAccount.btn_cd_confirm_city.setEnabled(true);
                createAddressCityAccount.mProgress.dismiss();

            }
        }else{
            Toast.makeText(createAddressCityAccount.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
            createAddressCityAccount.btn_cd_confirm_city.setEnabled(true);
            createAddressCityAccount.mProgress.dismiss();
        }
    }


}
