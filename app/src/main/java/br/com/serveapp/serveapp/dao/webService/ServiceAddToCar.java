package br.com.serveapp.serveapp.dao.webService;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.serveapp.serveapp.DetalheItemPopUpActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ServiceAddToCar extends OnPostResponse {

    DetalheItemPopUpActivity detalheItemPopUpActivity;
    public ServiceAddToCar(DetalheItemPopUpActivity detalheItemPopUpActivity){
        setId(Repository.idEstabelecimento);
        this.detalheItemPopUpActivity = detalheItemPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_produtos_do_carrinho_new.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
       // loginActivity.mProgress.dismiss();
        detalheItemPopUpActivity.receiveErroResponse();
     }

    @Override
    public void onResponse(String response) {
        if(!response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (!jsonObject.isNull("result")) {
                    JSONObject itemCar =  jsonObject.getJSONObject("result");
                   // String s = itemCar.getString("qtd");
                    Repository.qtdCart = itemCar.getJSONObject("cart").getInt("qtd");
                    detalheItemPopUpActivity.receiveSucessResponse(itemCar.getJSONObject("cart").getJSONObject("total").getString("label"), itemCar.getJSONObject("cart").getString("qtd"));
                  }
            } catch (JSONException e) {
                e.printStackTrace();
                detalheItemPopUpActivity.receiveErroResponse();
            }
    }

}