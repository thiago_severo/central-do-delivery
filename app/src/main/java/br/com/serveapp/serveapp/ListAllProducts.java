package br.com.serveapp.serveapp;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;

import br.com.serveapp.serveapp.adapt.LineGroupProduct;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListAllProducts;
import br.com.serveapp.serveapp.fragments.ifood.LineHorizontalItemAdapter;
import br.com.serveapp.serveapp.headerView.RecyclerHeaderView;
import br.com.serveapp.serveapp.repository.objects.Repository;

public class ListAllProducts extends AppCompatActivity {

    RecyclerView listProductsRecyclerView;
    RecyclerView headerProducts;
    RecyclerView destaque;
    Request requestLineProducts;
    RecyclerHeaderView recyclerHeaderView;
    JSONArray categorias;
    JSONArray jsonFilter;
    SearchView searchView;
    float minValueProducts;
    float maxValueProducts;
    float filterMinValueProducts;
    float filterMaxValueProducts;
    String filter;
    BottomNavigationView bottomNavigationView;
    ImageView imageButonFiltro;
    ImageView imageView20;
    ProgressBar progressBar;
    TextView textTitleEmptyListProducts;
    TextView textBodyEmptyListProducts;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        try {
            if (Repository.getUser(getApplicationContext()) == null) {
                startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));
                finish();
            }
        } catch (Exception e) {
            startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));
            finish();
        }


        setContentView(R.layout.activity_navigation_drawer_list_all_products);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        textBodyEmptyListProducts = findViewById(R.id.textBodyEmptyListProducts);
        textTitleEmptyListProducts = findViewById(R.id.textTitleEmptyListProducts);

        imageView20 = findViewById(R.id.serveLogo);
        progressBar = findViewById(R.id.progressBar2);
        destaque = findViewById(R.id.recyclerPromocoes);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        requestLineProducts = new Request(getApplicationContext(), new ServiceListAllProducts(progressBar, swipeRefreshLayout, this));
        listProductsRecyclerView = findViewById(R.id.recyclerProducts);
        headerProducts = findViewById(R.id.recyclerHeaderProducts);
        imageButonFiltro = findViewById(R.id.imageButonFiltro);
        searchView = findViewById(R.id.searchView);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        listProductsRecyclerView.setLayoutManager(linearLayoutManager);
        recyclerHeaderView = new RecyclerHeaderView(listProductsRecyclerView, headerProducts, this);
        progressBar.setVisibility(View.VISIBLE);
        requestLineProducts.request();
        filter = "";

        imageButonFiltro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buildFilterDialog();
            }
        });

       // new ListSetImage(getApplicationContext(), imageView20).execute("system/toolbar.png");

        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchView.setIconified(false);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                              @Override
                                              public boolean onQueryTextSubmit(String query) {
                                                  //Toast.makeText(getApplicationContext(), query, Toast.LENGTH_SHORT).show();
                                                  filterProducts(query);
                                                  return true;
                                              }

                                              @Override
                                              public boolean onQueryTextChange(String newText) {
                                                  filterProducts(newText);
                                                  return true;
                                              }
                                          }
        );

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        MyNavigationNew.setButtonsAction(this);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new MyNavigationMenu(this, navigationView));

        //NavigationView navigationView = findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(new MyNavigationMenu(this, navigationView));
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
//        bottomNavigationView = findViewById(R.id.bottom_navigation);
//        bottomNavigationView.setOnNavigationItemSelectedListener(new MyNavigation(this));
//        bottomNavigationView.setSelectedItemId(R.id.nav_fav_produtos);



        /*BottomNavigationMenuView bottomNavigationMenuView =
                (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        View v = bottomNavigationMenuView.getChildAt(4);
        BottomNavigationItemView itemView = (BottomNavigationItemView) v;

        View badge = LayoutInflater.from(this)
                .inflate(R.layout.badge, itemView, true);
        TextView tv = badge.findViewById(R.id.notifications_badge);
        tv.setText("6");*/
        MyNavigationMenu.a(bottomNavigationView, this);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            requestLineProducts.request();
                        } catch (NullPointerException e) {
                            requestLineProducts = new Request(getApplicationContext(), new ServiceListAllProducts(progressBar, swipeRefreshLayout, ListAllProducts.this));
                            requestLineProducts.request();
                        }
                    }
                }
        );

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);

    }

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            startActivity(new Intent(getApplicationContext() , NavigationDrawerActivity.class));
            finish();
        }

    };

    public void doRestart() {
        Intent mStartActivity = new Intent(getApplicationContext(), LoginActivity.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(getApplicationContext(), mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setQtdCart();
        if (bottomNavigationView != null)
            bottomNavigationView.setSelectedItemId(R.id.nav_fav_produtos);
    }

    private void filterProducts(String filter) {
        this.filter = filter;
        try {
            jsonFilter = new JSONArray();
            for (int i = 0; i < categorias.length(); i++) {
                JSONObject tempCat = categorias.getJSONObject(i);
                JSONArray products = tempCat.getJSONArray("produtos");
                JSONArray filterProducts = new JSONArray();
                for (int j = 0; j < products.length(); j++) {
                    JSONObject tempProduct = products.getJSONObject(j);
                    if (filter(filter, tempProduct, filterMinValueProducts, filterMaxValueProducts)) {
                        filterProducts.put(tempProduct);
                    }
                    /*double value = tempProduct.getDouble("value");
                    if ((tempProduct.getString("nome").contains(filter) || tempProduct.getString("cat_name").contains(filter))
                            && value >= filterMinValueProducts && value <= filterMaxValueProducts) {
                        filterProducts.put(tempProduct);
                    }*/
                }
                if (filterProducts.length() > 0) {
                    JSONObject categoria = new JSONObject();
                    categoria.put("nome", tempCat.getString("nome"));
                    categoria.put("_id", tempCat.getString("_id"));
                    categoria.put("produtos", filterProducts);
                    jsonFilter.put(categoria);
                }
            }
            updateList(jsonFilter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean filter(String filter, JSONObject product, float minValue, float maxValue) {
        double value = 0;
        try {
            filter = filter.toUpperCase();
            value = product.getDouble("value");
            String[] f = filter.split(" ");
            //String temp = (product.getString("nome") + " " + product.getString("cat_name")).toUpperCase();
            String temp = (product.getString("nome") + " "
                    + product.getString("cat_name") + " " +
                    product.getString("est_name")).toUpperCase();

            for (String s : f) {
                if (!temp.contains(s))
                    return false;
                else {
                    temp = temp.replaceFirst(s, "");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return (value >= minValue && value <= maxValue);
    }

    public void updateList(JSONArray categorias) {

        if (categorias.length() > 0) {
            textBodyEmptyListProducts.setVisibility(View.GONE);
            textTitleEmptyListProducts.setVisibility(View.GONE);
        } else {
            textBodyEmptyListProducts.setVisibility(View.VISIBLE);
            textTitleEmptyListProducts.setVisibility(View.VISIBLE);
        }

        recyclerHeaderView.setAdapter(categorias, "nome");
        LineGroupProduct lineGroupProduct = new LineGroupProduct(getApplicationContext(), categorias);

        lineGroupProduct.setOnItemClickListener(new LineGroupProduct.OnItemClickListner() {
            @Override
            public void onItemClick(JSONObject item) {
                Intent intent = new Intent(getApplicationContext(), DetalheItemPopUpActivity.class);
                try {

                    intent.putExtra("foto", item.getString("foto"));
                    intent.putExtra("value", item.getDouble("value"));
                    intent.putExtra("valor", item.getString("valor"));
                    String name = item.getString("cat_name") + "-" + item.getString("nome");
                    intent.putExtra("nome", name);
                    intent.putExtra("descricao", item.getString("descricao"));
                    intent.putExtra("id_produto", item.getString("_id"));
                    intent.putExtra("avaliacao", item.getString("avaliacao"));
                    intent.putExtra("tempoPreparo", item.getString("tempPreparo"));
                    if(item.has("fotos")){
                        intent.putExtra("fotos",  item.getString("fotos"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //startActivityForResult(intent, 1);
                new DetalheItemPopUpActivity(ListAllProducts.this,intent).show();

            }
        });

        listProductsRecyclerView.setAdapter(lineGroupProduct);
    }


    public void initializeListProducts(JSONObject result) {
        try {
            minValueProducts = (float) result.getJSONObject("faixa_preco").getDouble("min");
            maxValueProducts = (float) result.getJSONObject("faixa_preco").getDouble("max");
            filterMinValueProducts = minValueProducts;
            filterMaxValueProducts = maxValueProducts;
            categorias = result.getJSONArray("categorias");
            updateList(categorias);
            final LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
            destaque.setLayoutManager(layoutManager);

            layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
            destaque.setAdapter(new LineHorizontalItemAdapter(result.getJSONArray("destaque"), layoutManager, null, this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void buildFilterDialog() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(ListAllProducts.this);
        View mView = getLayoutInflater().inflate(R.layout.filter_dialog, null);

        mBuilder.setView(mView);

        final TextView textMin = mView.findViewById(R.id.textMin);
        final TextView textMax = mView.findViewById(R.id.textMax);
        final CrystalRangeSeekbar rangeSeekbar = mView.findViewById(R.id.rangeSeekBar);
        rangeSeekbar.setMaxValue(maxValueProducts);
        rangeSeekbar.setMinValue(minValueProducts);

        rangeSeekbar.setMinStartValue(filterMinValueProducts).setMaxStartValue(filterMaxValueProducts).apply();

        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                String min = NumberFormat.getCurrencyInstance().format(minValue);
                String max = NumberFormat.getCurrencyInstance().format(maxValue);
                textMin.setText(min);
                textMax.setText(max);
            }
        });
        final AlertDialog dialog = mBuilder.show();
        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        Button btnCancelMessage = mView.findViewById(R.id.cancellFilter);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                filterMinValueProducts = rangeSeekbar.getSelectedMinValue().floatValue();
                filterMaxValueProducts = rangeSeekbar.getSelectedMaxValue().floatValue();
                filterProducts(filter);
                dialog.dismiss();
            }
        });

        btnCancelMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public void setQtdCart() {
        TextView badge = findViewById(R.id.badge);

        if(Repository.getQtdCart()>0) {
            badge.setText(Repository.getQtdCart()+"");
            badge.setVisibility(View.VISIBLE);
        }else{
            badge.setText("");
            badge.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.overridePendingTransition(0, 0);
    }

}