package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;

import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;
import br.com.serveapp.serveapp.dao.web.Script;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceConfirmRecebimento extends OnPostResponse {

    DetailDemand detailDemand;
    ListProductsStablishmentDeliveryActivity activity;

    public ServiceConfirmRecebimento(DetailDemand detailDemand) {
        this.activity = activity;
        this.detailDemand = detailDemand;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "api/home/confirmar_recebimento";
    }

    public String getScript() {
        return Script.getUrl() + script;
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(detailDemand.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if (response != null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                detailDemand.updateStatus();
            //    detailDemand.confirmRecebimento=true;
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

}
