package br.com.serveapp.serveapp.dao.webService;

import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ServiceDealhesPedido extends OnPostResponse {

    DetailDemand detalhePedido;

    public ServiceDealhesPedido(DetailDemand detalhePedido, String idPedido) {
        setId(idPedido);
        this.detalhePedido = detalhePedido;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_detalhes_pedido.php";
    }

    @Override
    public Map<String, String> getParam() {

        final Map<String, String> paramLogin = new HashMap<>();
        paramLogin.put("id_pedido", getId());
        return paramLogin;
    }

    @Override
    public void onResponse(String response) {
        detalhePedido.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onCache(JSONObject jsonObject) {
        try {

            detalhePedido.initializeAdapter(jsonObject.getJSONObject("result"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        detalhePedido.progressBar.setVisibility(View.GONE);

        if(detalhePedido.dialog!=null) {
            detalhePedido.mProgressCancel.dismiss();
            detalhePedido.dialog.dismiss();
        }

        Toast.makeText(detalhePedido.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUpdate(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
            //    detalhePedido.noRefresh = true;
            //    detalhePedido.recreate();
                 detalhePedido.initializeAdapter(jsonObject.getJSONObject("result"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}