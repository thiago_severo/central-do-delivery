package br.com.serveapp.serveapp.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MyLocationListener implements LocationListener {

    boolean getLocation = false;
    Context context;
    CreateAddressAccountActivity createAddressAccountActivity;

    public MyLocationListener(Context context, CreateAddressAccountActivity createAddressAccountActivity){
        this.context = context;
        this.createAddressAccountActivity =createAddressAccountActivity;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onLocationChanged(Location loc) {


        if(getLocation==false && loc!=null) {

              Geocoder gcd = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = null;

            try {
                addresses = gcd.getFromLocation(loc.getLatitude(), loc.getLongitude(), 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(addresses!=null) {
                String addressLine = addresses.get(0).getAddressLine(0);
           //     String[] arrayAddressLine = addressLine.split("-");
                //   String uf = arrayAddressLine[2].split(",")[0];

                 createAddressAccountActivity.text_cd_rua.setText(addresses.get(0).getThoroughfare());
                createAddressAccountActivity.textInputCep.setText(addresses.get(0).getPostalCode());
                createAddressAccountActivity.text_cd_cidade.setText(addresses.get(0).getSubAdminArea());
                createAddressAccountActivity.text_cd_bairro.setText(addresses.get(0).getSubLocality());
                 createAddressAccountActivity.text_cd_numero.setText(addresses.get(0).getFeatureName());

    //            createAddressAccountActivity.setSelectSpiner(uf.trim());
                createAddressAccountActivity.mProgress.dismiss();

                getLocation = true;

            }
        }

    }

    @Override
    public void onProviderDisabled(String provider) {}

    @Override
    public void onProviderEnabled(String provider) {}

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {}
}
