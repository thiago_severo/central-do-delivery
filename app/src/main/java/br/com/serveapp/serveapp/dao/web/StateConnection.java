package br.com.serveapp.serveapp.dao.web;

public enum StateConnection {
    OFFLINE, RECONNECTED, ONLINE;
}
