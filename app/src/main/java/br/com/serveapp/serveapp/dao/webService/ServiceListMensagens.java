package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.ChatActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceListMensagens extends OnPostResponse {

    ChatActivity chatActivity;

    public ServiceListMensagens( String idPedido, ChatActivity changeAddressPopUpActivity){
        setId(idPedido);
        this.chatActivity = changeAddressPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(true);
        this.script = "select_message.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        Toast.makeText(chatActivity.getApplicationContext(), "Sem internet", Toast.LENGTH_LONG).show();
        chatActivity.erroEnvio();
    }

    @Override
    public void onCache(JSONObject jsonObject) {

            try {
                JSONObject result = jsonObject.getJSONObject("result");
                chatActivity.initializeChat(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    @Override
    public void onResponse(String response) {

        if(response!=null && !response.equals("null"))

            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                chatActivity.initializeChat(result);
                chatActivity. sucessSend();
               } catch (JSONException e) {
                e.printStackTrace();
            }

    }

    @Override
    public void onUpdate(String response) {
        if(response!=null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject result = jsonObject.getJSONObject("result");
                chatActivity.initializeChat(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
}