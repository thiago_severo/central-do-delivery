package br.com.serveapp.serveapp.adapt;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class LineGroupEvents extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public JSONArray listItensDemand;
    Context context;
    private OnItemClickListner listener;
    private LineAtracaoListAdapter.OnItemClickListnerViewImage listenerViewDetailImage;

    public LineGroupEvents(Context context, JSONArray listItensDemand) {
        this.listItensDemand = listItensDemand;
        this.context = context;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.group_products_establishment, parent, false);
        viewHolder = new ItemHeader(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        try {
            JSONObject jsonObject = listItensDemand.getJSONObject(position);

            if (holder instanceof ItemHeader) {
                ((ItemHeader) holder).title.setText(jsonObject.getString("data"));
                LineAtracaoListAdapter lineAtracaoListAdapter = new LineAtracaoListAdapter(context, jsonObject.getJSONArray("eventos"));
                lineAtracaoListAdapter.setListner(new LineAtracaoListAdapter.OnItemClickListnerAddress() {
                    @Override
                    public void onItemClick(JSONObject item) {
                        listener.onItemClick(item);
                    }

                    @Override
                    public void onItemClickRemove(JSONObject item) {
                        listener.onItemClickRemove(item);
                    }

                });

                lineAtracaoListAdapter.setListnerViewImage(new LineAtracaoListAdapter.OnItemClickListnerViewImage() {
                    @Override
                    public void onItemClickViewDetailImage(JSONObject item) {
                        listenerViewDetailImage.onItemClickViewDetailImage(item);
                    }
                });

                ((ItemHeader) holder).recyclerView.setAdapter(lineAtracaoListAdapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listItensDemand.length();
    }

    class ItemHeader extends RecyclerView.ViewHolder {
        public RecyclerView recyclerView;
        public TextView title;

        public ItemHeader(View itemView) {
            super(itemView);
            recyclerView = itemView.findViewById(R.id.groupList);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            recyclerView.setLayoutManager(linearLayoutManager);
            title = itemView.findViewById(R.id.groupTitle);
        }
    }

    public interface OnItemClickListner {
        void onItemClick(JSONObject itemCardapio);
        void onItemClickRemove(JSONObject itemCardapio);
    }

    public void setOnItemClickListener(OnItemClickListner listener) {
        this.listener = listener;
    }

    public void setOnItemClickListenerViewImage(LineAtracaoListAdapter.OnItemClickListnerViewImage listener) {
        this.listenerViewDetailImage = listener;
    }


}