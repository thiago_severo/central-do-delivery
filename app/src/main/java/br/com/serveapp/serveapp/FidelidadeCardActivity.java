package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import br.com.serveapp.serveapp.adapt.LineCartFidelity;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceListCartaoFidelidade;
import br.com.serveapp.serveapp.R;
import br.com.serveapp.serveapp.repository.objects.Repository;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FidelidadeCardActivity extends AppCompatActivity {

    RecyclerView recyclerViewFidelidadeCart;
    Request requestCartFidelidade;

    private Thread.UncaughtExceptionHandler onRuntimeError = new Thread.UncaughtExceptionHandler() {
        public void uncaughtException(Thread thread, Throwable ex) {
            //Try starting the service again
            //May be a pending intent might work
            finish();
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(Repository.getUser(getApplicationContext())==null){
            finish();
        }

        Thread.setDefaultUncaughtExceptionHandler(onRuntimeError);
        setContentView(R.layout.activity_fidelidade_card);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

        }

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(0,0);
            }
        });
        requestCartFidelidade = new Request(getApplicationContext(), new ServiceListCartaoFidelidade(this));
        recyclerViewFidelidadeCart = findViewById(R.id.recyclerFidelity);

        JSONArray jsonArray = new JSONArray();
        JSONObject object = new JSONObject();
        try {
            object.put("pontos","10");
            object.put("descFidelityEstabelecimento", "NA COMPRA DE 9 PRODUTOS O 10º É GRATUÍTO");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        jsonArray.put(object);

        recyclerViewFidelidadeCart = findViewById(R.id.recyclerFidelity);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerViewFidelidadeCart.setLayoutManager(linearLayoutManager);
        requestCartFidelidade.request();
    }

    public void initializeAdapter(JSONArray result) {
        LineCartFidelity lineCartFidelity = new LineCartFidelity(result, getApplicationContext());
        recyclerViewFidelidadeCart.setAdapter(lineCartFidelity);
    }
}
