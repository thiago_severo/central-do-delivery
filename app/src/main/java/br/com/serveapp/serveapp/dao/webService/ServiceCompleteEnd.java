package br.com.serveapp.serveapp.dao.webService;

import com.android.volley.VolleyError;

import br.com.serveapp.serveapp.ConfirmDataDemandActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

public class ServiceCompleteEnd extends OnPostResponse {

    ConfirmDataDemandActivity activity;

    public ServiceCompleteEnd(ConfirmDataDemandActivity activity) {
        this.activity = activity;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "complete_end.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        System.out.println("error address:"+error.networkResponse);

        activity.errorCompleteEnd();
    }



    @Override
    public void onResponse(String response) {
        activity.responseCompleteEnd();
    }

}