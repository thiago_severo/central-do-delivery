package br.com.serveapp.serveapp.dao.webService;

import android.widget.Toast;

import com.android.volley.VolleyError;
import br.com.serveapp.serveapp.ChangeAddressPopUpActivity;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServiceRemoveEndCliente extends OnPostResponse {

    ChangeAddressPopUpActivity changeAddressPopUpActivity;
    public ServiceRemoveEndCliente(ChangeAddressPopUpActivity changeAddressPopUpActivity){
        this.changeAddressPopUpActivity = changeAddressPopUpActivity;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "select_ends_cliente.php";
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        super.onErrorResponse(error);
        Toast.makeText(changeAddressPopUpActivity.getApplicationContext(), "verifique sua conexão", Toast.LENGTH_SHORT);
        changeAddressPopUpActivity.dimissProgress();
    }

    @Override
    public void onCache(JSONObject jsonObject) {
         /*   try {
                JSONArray result = jsonObject.getJSONArray("result");
                changeAddressPopUpActivity.initializeAdapterListAddress(result, jsonObject.getString("selected"));
                changeAddressPopUpActivity.dimissProgress();
            } catch (JSONException e) {
                e.printStackTrace();
            }*/
    }

    @Override
    public void onResponse(String response) {

    }

    @Override
    public void onUpdate(String response) {
        if(response!=null && !response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray result = jsonObject.getJSONArray("result");
                changeAddressPopUpActivity.initializeAdapterListAddress(result, jsonObject.getString("selected"));
                changeAddressPopUpActivity.dimissProgress();
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

}
