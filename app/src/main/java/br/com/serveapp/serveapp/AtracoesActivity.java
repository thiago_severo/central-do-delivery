package br.com.serveapp.serveapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.VolleyError;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import br.com.serveapp.serveapp.adapt.CarouselAdapter;
import br.com.serveapp.serveapp.adapt.LineAtracaoListAdapter;
import br.com.serveapp.serveapp.adapt.LineFavoritEstabelecimentAdapter;
import br.com.serveapp.serveapp.adapt.LineGroupEvents;
import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.web.RequestActivity;
import br.com.serveapp.serveapp.dao.webService.ServiceAddEventFavorit;
import br.com.serveapp.serveapp.dao.webService.ServiceListEvents;
import br.com.serveapp.serveapp.dao.webService.ServiceRemoveEventFavorit;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.headerView.RecyclerHeaderView;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.repository.objects.Variables;

import static android.graphics.Color.WHITE;

public class AtracoesActivity extends RequestActivity {


    CarouselView carouselView;
    CarouselAdapter carouselAdapter;
    //int[] sampleImages = {R.drawable.doceria, R.drawable.ic_burgeria, R.drawable.catule};
    int[] sampleImages = {R.drawable.doceria};

    CardView cardLocationEvent;
    Request serviceListEvents;
    Request serviceAddEventFavorit;
    Request serviceRemoveEventFavorit;
    private static RecyclerView listEvents;
    private static RecyclerView headerEvents;
    private TextView titleEndEvent;
    private RecyclerHeaderView recyclerHeaderView;
    private static LineFavoritEstabelecimentAdapter lineAdapter;
    LineAtracaoListAdapter lineAtracaoListAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView textEmptyEvent;
    ProgressBar progressBar;

    String idEndSelected;
    String descEndSelected;
    String idMunicipio;

    BottomNavigationView bottomNavigationView;

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer_atracoes);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        textEmptyEvent = findViewById(R.id.textEmptyEvent);
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        cardLocationEvent = findViewById(R.id.cardLocationEvent);
        listEvents = findViewById(R.id.recyclerEventos);
        headerEvents = findViewById(R.id.recyclerDatas);
        titleEndEvent = findViewById(R.id.titleEndEvent);
        progressBar = findViewById(R.id.progressBar4);
        recyclerHeaderView = new RecyclerHeaderView(listEvents, headerEvents, this);

        carouselView = (CarouselView) findViewById(R.id.carouselView);


        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            serviceListEvents.request();
                        } catch (NullPointerException e) {
                            serviceListEvents = new Request(getApplicationContext(), new ServiceListEvents(progressBar,AtracoesActivity.this, swipeRefreshLayout));
                            serviceListEvents.request();
                        }
                    }
                }
        );

        cardLocationEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(), ChangeAddressPopUpActivity.class), 1);
            }
        });

        serviceListEvents = new Request(getApplicationContext(), new ServiceListEvents(progressBar,this, swipeRefreshLayout));
        serviceListEvents.request();
        progressBar.setVisibility(View.VISIBLE);


        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        MyNavigationNew.setButtonsAction(this);
        toolbar.getNavigationIcon().setColorFilter(WHITE, PorterDuff.Mode.MULTIPLY);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new MyNavigationMenu(this, navigationView));



    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            this.overridePendingTransition(0, 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (bottomNavigationView != null)
            bottomNavigationView.setSelectedItemId(R.id.nav_atracoes);
    }


    @SuppressLint("MissingSuperCall")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Variables.EXIT_APPLICATION) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }

        if (resultCode == Variables.EXIT_APPLICATION) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        }

        if (resultCode == Activity.RESULT_OK) {

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(data.getStringExtra("endereco"));
                descEndSelected = jsonObject.getString("titulo");
                idEndSelected = jsonObject.getString("_id");

                if (!jsonObject.isNull("id_municipio")) {
                    idMunicipio = jsonObject.getString("id_municipio");
                } else {
                    idMunicipio = "0";
                }

                Map<String, String> param = new HashMap<>();

                param.put("id_municipio", idMunicipio);
                param.put("id_endereco", idEndSelected);
                param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                param.put("function_method", "set_end");

                serviceListEvents.request(param);
                //      titleEndEvent.setText(descEndSelected);

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

    }


    @Override
    public void onErrorResponse(VolleyError error) {
        System.console();
    }


    @Override
    public void onResponseServe(String response) {
        if (!response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                //check-in
                JSONObject result = jsonObject.getJSONObject("result");
                Repository.idEstabelecimento = result.getString("id_estabelecimento");
                Repository.atendimentoLocal = true;
                Intent intentOpenServiceLocal = new Intent(getApplicationContext(), SplashScreenEstabeleciment.class);
                intentOpenServiceLocal.putExtra("foto", result.getString("foto"));
                intentOpenServiceLocal.putExtra("slogan", result.getString("slogan"));
                intentOpenServiceLocal.putExtra("nome", result.getString("nome"));

                startActivity(intentOpenServiceLocal);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public void onCache(String response) {
        if (response != null && !response.equals("null")) {
            try {
                JSONObject jsonObject = new JSONObject(response);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_settings);
        item.setVisible(false);
        return true;
    }

    public void initializeAdapterListAddress(JSONObject result) {

        try{
            JSONArray banners = result.getJSONArray("banner");

            carouselView.setPageCount(banners.length());
            CarouselAdapter adapter = new CarouselAdapter(this, banners);
            carouselView.setViewListener(adapter);

            if(banners.length()>0){
                carouselView.setVisibility(View.VISIBLE);
            }else{
                carouselView.setVisibility(View.GONE);
            }
        }catch (Exception e){
            carouselView.setVisibility(View.GONE);
        }


        try {

            JSONArray eventos = result.getJSONArray("eventos");


            if(eventos.length()>0){
                textEmptyEvent.setVisibility(View.GONE);
            }else{
                textEmptyEvent.setVisibility(View.VISIBLE);
            }
            titleEndEvent.setText(result.getString("end"));
            recyclerHeaderView.setAdapter(eventos, "data");
            LineGroupEvents lineGroupEvents = new LineGroupEvents(this, eventos);

            lineGroupEvents.setOnItemClickListenerViewImage(new LineAtracaoListAdapter.OnItemClickListnerViewImage() {
                @Override
                public void onItemClickViewDetailImage(JSONObject item) {

                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(AtracoesActivity.this);
                    View mView = getLayoutInflater().inflate(R.layout.detail_event, null);
                    final ImageView imgEvent = mView.findViewById(R.id.imageEvent);
                    final TextView textViewAddressEvent = mView.findViewById(R.id.textViewAddressEvent);
                    final ImageButton imageButtonCloseEvent = mView.findViewById(R.id.imageButtonCloseEvent);
                    final Button buttonOkEvent = mView.findViewById(R.id.buttonOkEvent);

                    try {
                        new ListSetImage(getApplicationContext(), imgEvent).execute(item.getString("foto"));
                        textViewAddressEvent.setText(item.getString("endereco"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    mBuilder.setView(mView);
                    final AlertDialog dialog = mBuilder.create();
                    dialog.show();

                    imageButtonCloseEvent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                    buttonOkEvent.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                        }
                    });

                }
            });

            lineGroupEvents.setOnItemClickListener(new LineGroupEvents.OnItemClickListner() {
                @Override
                public void onItemClick(JSONObject evento) {
                    serviceAddEventFavorit = new Request(getApplicationContext(), new ServiceAddEventFavorit(AtracoesActivity.this));

                    Map<String, String> param = new HashMap<>();
                    try {
                        param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                        param.put("id_evento", evento.getString("_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    serviceAddEventFavorit.request(param);
                }

                @Override
                public void onItemClickRemove(JSONObject evento) {
                    serviceRemoveEventFavorit = new Request(getApplicationContext(), new ServiceRemoveEventFavorit(AtracoesActivity.this));

                    Map<String, String> param = new HashMap<>();
                    try {
                        param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                        param.put("id_evento", evento.getString("_id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    serviceRemoveEventFavorit.request(param);
                }
            });
            listEvents.setAdapter(lineGroupEvents);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}