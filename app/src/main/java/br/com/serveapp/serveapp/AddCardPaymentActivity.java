package br.com.serveapp.serveapp;


import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import br.com.serveapp.serveapp.dao.web.Request;
import br.com.serveapp.serveapp.dao.webService.ServiceAddCardCredit;
import br.com.serveapp.serveapp.dao.webService.ServiceAddressRegister;
import br.com.serveapp.serveapp.drawer.ListSetImage;
import br.com.serveapp.serveapp.repository.objects.Estado;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.util.CieloRequest;
import br.com.serveapp.serveapp.util.DownloadDados;
import br.com.serveapp.serveapp.util.DownloadDadosPOST;
import br.com.serveapp.serveapp.util.MaskEditUtil;
import cieloecommerce.sdk.Merchant;
import cieloecommerce.sdk.ecommerce.Address;
import cieloecommerce.sdk.ecommerce.Card;
import cieloecommerce.sdk.ecommerce.CieloEcommerce;
import cieloecommerce.sdk.ecommerce.Customer;
import cieloecommerce.sdk.ecommerce.Environment;
import cieloecommerce.sdk.ecommerce.Payment;
import cieloecommerce.sdk.ecommerce.Sale;
import cieloecommerce.sdk.ecommerce.request.CieloError;
import cieloecommerce.sdk.ecommerce.request.CieloRequestException;
import cieloecommerce.sdk.ecommerce.request.UpdateSaleResponse;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class AddCardPaymentActivity extends AppCompatActivity {

    private LocationManager locationManager;
    private Button btn;
    CardView btn_your_location;
    public Button btn_cadastrar;
    Button btn_pular;
    public static AddCardPaymentActivity refference;

    public TextInputEditText textInputEditText;
    public TextInputEditText textInputCep;
    public TextInputEditText text_cd_bairro;
    public TextInputEditText text_cd_card_number;
    public TextInputEditText cd_dgt_verificador;
    public TextInputLayout textInputLayoutCdVerificador;

    public TextInputEditText text_nome_titular;
    public TextInputEditText txtCdCpfUser;
    public TextInputEditText txtValidateCard;

    public TextInputLayout textInputLayoutCpfCnpjTitular;
    public TextInputLayout textInputLayoutCardNumber;
    public TextInputLayout textInputLayoutValidateCard;
    public TextInputLayout textInputLayoutNomeTitular;

    public Spinner spinner;
    public ProgressDialog mProgress;
    private Button btn_cd_create_account;
    private ImageView imgIconCard;

    public Button btnCleanCpf;
    public Button btnCleanName;
    public Button buttonCleanCard;
    public Button btnCleanValidade;
    public Button btnCleanCvv;
    public Button btnAddNewCard;

    public CardView cardSubTipoPagamentoTitleApp;
    public RecyclerView listMyCards;


    boolean cpfValid, cvvValid, cardValid, dateValid, nameValid;
    private String bandeira;

    private ImageView imgElo;
    private  ImageView imgVisa;
    private  ImageView imgMaster;
    private  ImageView imgAmex;
    private  ImageView imgDinners;

    private  ImageView imgHipercard;
    private  ImageView imgDiscover;
    private  ImageView imgJcb;

    private Request requestAddCard;
    public String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_card_payment);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);

        Toolbar toolbar = findViewById(
                R.id.toolbar1);
        imgIconCard = findViewById(R.id.imgIconCard);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        mProgress = new ProgressDialog(this);
        String titleId = "Adicionando um novo cartão...";
        mProgress.setTitle(titleId);
        mProgress.setMessage("por favor, aguarde...");

        textInputLayoutCpfCnpjTitular = findViewById(R.id.textInputLayoutCpfCnpjTitular);
        btnCleanCpf = findViewById(R.id.btnCleanCpf);
        btnAddNewCard = findViewById(R.id.end_add2);
        btnCleanCvv = findViewById(R.id.btnCleanCvv);
        btnCleanValidade = findViewById(R.id.btnCleanValidade);
        buttonCleanCard = findViewById(R.id.buttonCleanCard);
        txtCdCpfUser = findViewById(R.id.txtCdCpfUser);
        btn_cd_create_account = findViewById(R.id.btn_cd_create_account);
        btnCleanName = findViewById(R.id.btnCleanName);
        listMyCards = findViewById(R.id.listMyCards);

        imgMaster = findViewById(R.id.imgMaster);
        imgElo = findViewById(R.id.imgElo);
        imgVisa = findViewById(R.id.imgVisa);
        imgAmex = findViewById(R.id.imgAmex);
        imgDinners = findViewById(R.id.imgDinners);

        imgDiscover = findViewById(R.id.imgDiscover);
        imgHipercard = findViewById(R.id.imgHipercard);

        new ListSetImage(getApplicationContext(), imgElo).execute("card/" + "elo" + ".png");
        new ListSetImage(getApplicationContext(), imgVisa).execute("card/" + "visa" + ".png");
        new ListSetImage(getApplicationContext(), imgMaster).execute("card/" + "master" + ".png");
        new ListSetImage(getApplicationContext(), imgAmex).execute("card/" + "amex" + ".png");
        new ListSetImage(getApplicationContext(), imgDinners).execute("card/" + "diners" + ".png");

        new ListSetImage(getApplicationContext(), imgDiscover).execute("card/" + "discover" + ".png");
       new ListSetImage(getApplicationContext(), imgHipercard).execute("card/" + "hipercard" + ".png");

        textInputLayoutCardNumber = findViewById(R.id.textInputLayoutCardNumber);
        textInputLayoutValidateCard = findViewById(R.id.textInputLayoutValidateCard);
        textInputLayoutCdVerificador = findViewById(R.id.textInputLayoutCdVerificador);
        textInputLayoutNomeTitular = findViewById(R.id.textInputLayoutNomeTitular);

        requestAddCard = new Request(getApplicationContext(), new ServiceAddCardCredit(this));

        cd_dgt_verificador = findViewById(R.id.cd_dgt_verificador);
        cd_dgt_verificador.setOnFocusChangeListener(onFocusChangeListener);
        //   cd_dgt_verificador.addTextChangedListener(MaskEditUtil.mask(cd_dgt_verificador, MaskEditUtil.CODE_CARD));
        cd_dgt_verificador.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (cd_dgt_verificador.getText().length() > 0) {
                    btnCleanCvv.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        text_cd_card_number = findViewById(R.id.cardNumber);
        text_cd_card_number.addTextChangedListener(MaskEditUtil.maskCard(text_cd_card_number, getApplicationContext(), imgIconCard, buttonCleanCard, textInputLayoutCardNumber));
        text_cd_card_number.setOnFocusChangeListener(onFocusChangeListener);

        text_nome_titular = findViewById(R.id.text_nome_titular);
        text_nome_titular.setOnFocusChangeListener(onFocusChangeListener);

        cd_dgt_verificador.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});

        btnCleanCpf.setVisibility(View.GONE);
        btnCleanCvv.setVisibility(View.GONE);
        buttonCleanCard.setVisibility(View.GONE);
        btnCleanValidade.setVisibility(View.GONE);
        btnCleanName.setVisibility(View.GONE);

        btnCleanCpf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtCdCpfUser.setText("");
            }
        });

        btnCleanCvv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cd_dgt_verificador.setText("");
            }
        });

        btnCleanName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_nome_titular.setText("");
            }
        });

        btnCleanValidade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtValidateCard.setText("");
            }
        });

        buttonCleanCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                text_cd_card_number.setText("");
            }
        });

//         new ListSetImage(getApplicationContext(), imageView30).execute("card/master.png");

        MaskEditUtil.setValidCardData(new MaskEditUtil.validCardData() {
            @Override
            public void validCard(boolean isValidCard, String bandeira) {
                if (isValidCard) {

                    cardValid = true;
                    AddCardPaymentActivity.this.bandeira = bandeira;
                    if (bandeira != null) {
                        if (bandeira.equals("amex")) {
                            // Toast.makeText(getApplicationContext(), "cert", Toast.LENGTH_LONG).show();
                            cd_dgt_verificador = findViewById(R.id.cd_dgt_verificador);
                            cd_dgt_verificador.setFilters(new InputFilter[]{new InputFilter.LengthFilter(4)});

                        } else {
                            //Toast.makeText(getApplicationContext(), "err", Toast.LENGTH_LONG).show();
                            cd_dgt_verificador = findViewById(R.id.cd_dgt_verificador);
                            cd_dgt_verificador.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                        }
                    }

                } else {

                    cardValid = false;

                    if (cd_dgt_verificador.getText().toString().length() > 3) {
                        cd_dgt_verificador.setText(cd_dgt_verificador.getText().toString().substring(0, 3));
                    }
                    cd_dgt_verificador.setFilters(new InputFilter[]{new InputFilter.LengthFilter(3)});
                    //   btn_cd_create_account.setBackgroundColor(Color.GRAY);
                }
            }
        });

        txtValidateCard = findViewById(R.id.txtValidateCard);
        txtValidateCard.addTextChangedListener(MaskEditUtil.insert2(txtValidateCard, MaskEditUtil.FORMAT_VALID_DATE, btnCleanValidade, textInputLayoutValidateCard));
        txtValidateCard.setOnFocusChangeListener(onFocusChangeListener);

        txtCdCpfUser = findViewById(R.id.txtCdCpfUser);
        txtCdCpfUser.addTextChangedListener(MaskEditUtil.insert(txtCdCpfUser, btnCleanCpf));
        txtCdCpfUser.setOnFocusChangeListener(onFocusChangeListener);

        txtCdCpfUser.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutCpfCnpjTitular.setError(null);
            }
        });

        // text_nome_titular.addTextChangedListener(MaskEditUtil.insert2(text_nome_titular, MaskEditUtil.FORMAT_VALID_NAME, btnCleanName, textInputLayoutNomeTitular));
        text_nome_titular.setOnFocusChangeListener(onFocusChangeListener);
        text_nome_titular.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutNomeTitular.setError(null);
                if (text_nome_titular != null)
                    if (text_nome_titular.getText().length() > 0) {
                        btnCleanName.setVisibility(View.VISIBLE);
                    }
            }
        });

        cd_dgt_verificador.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                textInputLayoutCdVerificador.setError(null);
            }
        });


        btn_cd_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (requestToSave()) {

                    JSONObject card = new JSONObject();
                    try {
                        card.put("number", text_cd_card_number.getText().toString());
                        card.put("bandeira", bandeira);
                        //    card.put("cardType", )

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Repository.listCards.put(card);
                    setResult(1);
                    finish();

                }
            }
        });

        btn_cd_create_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (requestToSave()) {

                    mProgress.show();
                    final String cardNumber = text_cd_card_number.getText().toString().replace(" ", "").trim().substring(0, 6);
                    final DownloadDados downloadDados = new DownloadDados(DownloadDados.searchDataCard + cardNumber);
                    downloadDados.execute();

                    downloadDados.setRequestResponse(new DownloadDados.ResponseRequest() {
                        @Override
                        public void setResponse(String response) {

                            System.out.println("status: " + response);
                            mProgress.dismiss();

                            if (response == null || response.equals("null")) {
                                runOnUiThread(new Runnable() {
                                    public void run() {
                                        Toast.makeText(getApplicationContext(), "Erro ao conectar com a rede", Toast.LENGTH_LONG).show();

                                    }});

                            } else {
                                try {
                                    JSONObject obj = new JSONObject(response);
                                    if (!obj.has("Code") && obj.has("Status") && obj.getString("Status").equals("00")) {
                                        final JSONObject card = new JSONObject();
                                        try {
                                            card.put("number", text_cd_card_number.getText().toString());
                                            card.put("bandeira", obj.getString("Provider"));
                                            card.put("cvv", cd_dgt_verificador.getText().toString());
                                            card.put("cardType", obj.getString("CardType"));
                                            card.put("banco", obj.getString("Issuer"));
                                        } catch (JSONException e) {

                                        }

                                        String validDate = txtValidateCard.getText().toString().substring(0, 3) + "20" + txtValidateCard.getText().toString().substring(3, 5);
                                        MediaType mediaType = MediaType.parse("text/json");
                                        RequestBody body = RequestBody.create(mediaType, "{\r\n    \"CustomerName\" : \"" + text_nome_titular.getText().toString() + "\",\r\n    " +
                                                "   \"CardNumber\" : \"" + text_cd_card_number.getText().toString().replace(" ", "") + "\",\n" +
                                                "   \"Holder\" : \"" + text_nome_titular.getText().toString() + "\",\n" +
                                                "   \"ExpirationDate\" : \"" + validDate + "\",\n" +
                                                "   \"Brand\" : \"" + bandeira + "\"}\n");

                                        DownloadDadosPOST downloadDadosPOST = new DownloadDadosPOST(body);
                                        downloadDadosPOST.searchDataCard = "https://api.cieloecommerce.cielo.com.br/1/card/";
                                        downloadDadosPOST.execute();
                                        downloadDadosPOST.setRequestResponse(new DownloadDadosPOST.ResponseRequest() {
                                            @Override
                                            public void setResponse(String response) {

                                                try {
                                                    JSONObject obj = new JSONObject(response);
                                                    if (obj.has("CardToken")) {
                                                        String cardNumber = text_cd_card_number.getText().toString().replace("/[^0-9]+/g", "");

                                                        String CardToken = obj.getString("CardToken");
                                                        System.out.println("CardToken: " + CardToken);
                                                        card.put("CardToken", CardToken);
                                                        Map<String, String> param = new HashMap<>();
                                                        param.put("id_cliente", Repository.getIdUsuario(getApplicationContext()));
                                                        param.put("cpf", MaskEditUtil.unmask(txtCdCpfUser.getText().toString()));
                                                        param.put("bandeira", bandeira);
                                                        param.put("cvv", cd_dgt_verificador.getText().toString());
                                                        param.put("numero", cardNumber.substring(cardNumber.length() - 4, cardNumber.length()));
                                                        param.put("cliente_name", text_nome_titular.getText().toString());
                                                        param.put("token", CardToken);

                                                        requestAddCard.request(param);

                                                    } else {
                                                        runOnUiThread(new Runnable() {
                                                            public void run() {
                                                                mProgress.dismiss();

                                                                showMsg("Cartão não suportado", "verifique os dados informados do seu cartão");

                                                            }
                                                        });
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        });

                                    }
                                    Log.d("My App", obj.toString());

                                } catch (Throwable t) {
                                    t.printStackTrace();
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            mProgress.dismiss();
                                            showMsg("Cartão não suportado", "verifique os dados informados do seu cartão");
                                        }
                                    });
                                    Log.e("My App", "Could not parse malformed JSON: \"" + response + "\"");
                                }

                            }

                        }

                    });

                }

            }
        });
    }

    View.OnFocusChangeListener onFocusChangeListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (view.getId() == text_cd_card_number.getId()) {
                if (b == true && text_cd_card_number.getText().toString().length() > 0) {
                    buttonCleanCard.setVisibility(View.VISIBLE);
                } else {
                    if (cardValid == false && text_cd_card_number.getText().length() > 0) {
                        textInputLayoutCardNumber.setError("Informe um número de cartão válido.");
                    } else {
                        textInputLayoutValidateCard.setError(null);
                    }

                    buttonCleanCard.setVisibility(View.GONE);
                }
            } else if (view.getId() == txtValidateCard.getId()) {
                if (b == true && txtValidateCard.getText().toString().length() > 0) {
                    btnCleanValidade.setVisibility(View.VISIBLE);
                } else {

                    if (txtValidateCard.getText().length() > 0 && validDate(txtValidateCard.getText().toString()) == false) {
                        textInputLayoutValidateCard.setError("Informe uma data de vencimento válida.");
                    } else {
                        textInputLayoutValidateCard.setError(null);
                    }

                    btnCleanValidade.setVisibility(View.GONE);
                }
            } else if (view.getId() == cd_dgt_verificador.getId()) {
                if (b == true && cd_dgt_verificador.getText().toString().length() > 0) {
                    btnCleanCvv.setVisibility(View.VISIBLE);
                } else {

                    if (cd_dgt_verificador.getText().length() > 0 && !validCvv(cd_dgt_verificador.getText().toString())) {
                        textInputLayoutCdVerificador.setError("Insira um código válido");
                    } else {
                        textInputLayoutCdVerificador.setError(null);
                    }

                    btnCleanCvv.setVisibility(View.GONE);
                }
            } else if (view.getId() == text_nome_titular.getId()) {
                if (b == true && text_nome_titular.getText().toString().length() > 0) {
                    btnCleanName.setVisibility(View.VISIBLE);
                } else {
                    if (text_nome_titular.length() > 0 && validateName(text_nome_titular.getText().toString()) == false) {
                        textInputLayoutNomeTitular.setError("Informe nome e sobrenome.");
                    } else {
                        textInputLayoutNomeTitular.setError(null);
                    }
                    btnCleanName.setVisibility(View.GONE);
                }
            } else if (view.getId() == txtCdCpfUser.getId()) {
                if (b == true && txtCdCpfUser.getText().toString().length() > 0) {
                    btnCleanCpf.setVisibility(View.VISIBLE);
                } else {

                    if (txtCdCpfUser.getText().length() > 0 && !(MaskEditUtil.isCPF(txtCdCpfUser.getText().toString()) || MaskEditUtil.isCNPJ(txtCdCpfUser.getText().toString()))) {
                        textInputLayoutCpfCnpjTitular.setError("Informe um cpf/cnpj válido.");
                    } else {
                        textInputLayoutCpfCnpjTitular.setError(null);
                    }

                    btnCleanCpf.setVisibility(View.GONE);

                }
            }

        }
    };


    @Override
    public void onBackPressed() {

        super.onBackPressed();
        overridePendingTransition(R.anim.entrer_from_right,
                R.anim.exit_to_left);

    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    public void showMsg(String titulo, String mensagem) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.sucess_email_recuper, null);
        TextView textView = mView.findViewById(R.id.textMainMsg);
        TextView textViewSucess = mView.findViewById(R.id.textSucessMessage);

        textView.setText(titulo);
        textViewSucess.setText(mensagem);

        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();

        Button btnOkMessage = mView.findViewById(R.id.btnConfirmDeleteCardCredit);
        btnOkMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

            }
        });

    }

    private boolean validCvv(String cvv) {

        boolean ret = false;

        if (cvv.length() < 3) {
            ret = false;
        } else {
            ret = true;
        }

        return ret;
    }

    private boolean validateName(String name) {

        boolean res = false;

        Pattern amex = Pattern.compile("[a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ]* [a-zzéúíóáèùìòàõãñêûîôâëyüïöäA-ZÉÚÍÓÁÈÙÌÒÀÕÃÑÊÛÎÔÂËYÜÏÖÄ](.*)"); // ou ^[^\W_]*\d+[^\W_]*$ seguindo a ideia do mgibsonbr

        if (amex.matcher(name).matches()) {
            res = true;
        }

        return res;
    }

    private boolean validDate(String date) {

        boolean res = false;

        if (date.length() < 5) {
            res = false;
        } else {

            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            Integer mes = Integer.parseInt(date.substring(0, 2));
            Integer ano = Integer.parseInt("20" + date.substring(3, 5));


            if (mes > 0 && mes <= 12 && ((ano > year && mes > 0 && mes <= 12) || (year == ano && mes >= month && mes <= 12))) {
                res = true;
            } else {
                res = false;
            }
        }

        return res;

    }


    public boolean requestToSave() {

        if (cardValid == false) {
            textInputLayoutCardNumber.setError("Informe um número de cartão válido.");
            return false;
        } else if (validDate(txtValidateCard.getText().toString()) == false) {
            textInputLayoutValidateCard.setError("Informe uma data de vencimento válida.");
            return false;
        } else if (!validCvv(cd_dgt_verificador.getText().toString())) {
            textInputLayoutCdVerificador.setError("Insira um código válido");
            return false;
        } else if (validateName(text_nome_titular.getText().toString()) == false) {
            textInputLayoutNomeTitular.setError("Informe nome e sobrenome.");
            return false;
        } else if (!(MaskEditUtil.isCPF(txtCdCpfUser.getText().toString()) || MaskEditUtil.isCNPJ(txtCdCpfUser.getText().toString()))) {
            textInputLayoutCpfCnpjTitular.setError("Informe um cpf/cnpj válido.");
            return false;
        } else {
            return true;
        }

    }


}
