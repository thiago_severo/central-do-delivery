package br.com.serveapp.serveapp.util;

import android.net.Uri;
import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class DownloadDados extends AsyncTask<Void, Void, String> {

    public static String searchDataCard = "https://apiquery.cieloecommerce.cielo.com.br/1/cardBin/";
    public String url;


    public DownloadDados(String url){
      this.url = url;
    }

    ResponseRequest responseRequest;

    @Override
    protected String doInBackground(Void... params) {

        StringBuilder resposta = new StringBuilder();

        String response = null;
        OkHttpClient client = new OkHttpClient().newBuilder()
                .build();

        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .addHeader("MerchantId", "dfbe2589-9e71-499c-8669-ba9bd411e303")
                .addHeader("Content-Type", "text/json")
                .addHeader("MerchantKey", "7EyMYnvKHirU6Xq1mi4Wn4qcBkMWnt35qN6ktNfx")
                .build();

        try {
            Response response1 = client.newCall(request).execute();
            response = response1.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //URL url = new URL("https://reqres.in/api/users?page=2");

       /*     HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");

            connection.setRequestProperty("MerchantId", "f207cb7a-e1eb-4896-ac08-4a8ae717e061");
            connection.setRequestProperty("MerchantKey", "SKCGJKUWPZTDAPLUZNKQRSJLOXEJKXHGGBPMUACC");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.connect();

            Scanner scanner = new Scanner(url.openStream());
            while (scanner.hasNext()) {
                resposta.append(scanner.next());

                System.out.println("resposta: "+ resposta);

            }



        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */

        responseRequest.setResponse(response);

        return response;

    }
    @Override
    protected void onPostExecute(String dados) {
        // Faça alguma coisa com os dados
    }

    public interface ResponseRequest{
        public void setResponse(String response);
    }

    public void setRequestResponse(ResponseRequest responseRequest){
        this.responseRequest = responseRequest;
    }

}
