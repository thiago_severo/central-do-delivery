package br.com.serveapp.serveapp.drawer;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.ProgressBar;

import br.com.serveapp.serveapp.ListProductsStablishmentDeliveryActivity;
import br.com.serveapp.serveapp.SplashScreenEstabeleciment;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

public class SplashSetImageEstabeleciment extends SetImageWebBipMap {

    ImageView imageView;
    ProgressBar progressBar;
    Context context;
    SplashScreenEstabeleciment splashScreenHomeApp;
    public static String tipoAtendimento;
    public static String nomeEstabelecimento;
    public static String slogan;
    public static String distancia;
    public static String logo;
    public static int rating;
    public static String frete;
    public static boolean favorito;

    public SplashSetImageEstabeleciment(SplashScreenEstabeleciment splashScreenHomeApp, Context context, ImageView imageView, ProgressBar progressBar) {
        super(context);
        this.context = context;
        this.imageView = imageView;
        this.progressBar = progressBar;
        this.splashScreenHomeApp = splashScreenHomeApp;

    }

    @Override
    protected void onPostExecute(final Bitmap result) {

        final Intent intent = new Intent(context, ListProductsStablishmentDeliveryActivity.class);
        intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("nome", nomeEstabelecimento);
        intent.putExtra("slogan", slogan);
        intent.putExtra("distancia", distancia);
        intent.putExtra("logo", logo);
        intent.putExtra("rating", rating);
        intent.putExtra("frete", frete);
        intent.putExtra("fav", favorito);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                context.startActivity(intent);
                splashScreenHomeApp.finish();
            }
        }, 500);

        imageView.setImageBitmap(result);

        //context.startActivity(intent);
       // splashScreenHomeApp.finish();



    }

}

