package br.com.serveapp.serveapp.adapt;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import br.com.serveapp.serveapp.DetailDemand;
import br.com.serveapp.serveapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static com.facebook.FacebookSdk.getApplicationContext;

public class LineDemandGeral extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements View.OnCreateContextMenuListener {


    /*******/
    public onItemClickCancel onItemClickCancelListener;
    public OnItemClickAvaliate onItemClickAvaliate;
    LayoutInflater inflater;
    public JSONArray listItensDemand;
    Context context;
    public boolean cancel = true;
    Activity activity;
    ShowPopUpAvaliationListener showPopUpAvaliationListener;
    private OnItemClickListenerViewDetalhes onItemClickListenerViewDetalhes;

    public LineDemandGeral(Activity activity, Context context, JSONArray listItensDemand) {
        this.listItensDemand = listItensDemand;
        this.context = context;
        this.activity = activity;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = null;
        RecyclerView.ViewHolder viewHolder = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.line_demand, parent, false);
        viewHolder = new ItemDemand(view);
        return viewHolder;
    }

    private int getColorStatus(int status){
        switch (status){
            case 1: return Color.rgb(200,200,200);
            case 2: return Color.rgb(255,193,7);
            case 3: return Color.rgb(33,148,243);
            case 4: return Color.rgb(90,185,100);
            case 5: return Color.rgb(244,67,54);
        }
        return R.color.green;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        try {

            JSONObject jsonObject = listItensDemand.getJSONObject(position);

            ((ItemDemand) holder).viewBar.setBackgroundColor(getColorStatus(jsonObject.getInt("ped_status")));

            ((ItemDemand) holder).textSemDesconto.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
            ((ItemDemand) holder).textSemDesconto.setText(jsonObject.getString("sem_desconto"));

            ((ItemDemand) holder).valorPedido.setText(jsonObject.getString("ped_valor").replace(".", ","));
            ((ItemDemand) holder).codPedido.setText(jsonObject.getString("codigo").toUpperCase());

            if (!jsonObject.getString("estab_name").equals("null")) {
                ((ItemDemand) holder).nameEstabHistoric.setVisibility(View.VISIBLE);
                ((ItemDemand) holder).nameEstabHistoric.setText(jsonObject.getString("estab_name"));
            } else {
                ((ItemDemand) holder).nameEstabHistoric.setVisibility(View.GONE);
            }

            LineItemDemandEstabelishment lineItemDemandEstabelishment = new LineItemDemandEstabelishment(context, jsonObject.getJSONArray("produtos"));
            lineItemDemandEstabelishment.setOnClickAvaliationListener(new LineItemDemandEstabelishment.OnClickAvaliationListener() {
                @Override
                public void onClickAvaliationListener(JSONObject jsonObject) {
                    showPopUpAvaliationListener.showPopUpAvaliation(jsonObject, position + "");
                }
            });

            ((ItemDemand) holder).recyclerViewItensPedido.setAdapter(lineItemDemandEstabelishment);

            ((ItemDemand) holder).dataPedido.setText(jsonObject.getString("ped_data"));
            ((ItemDemand) holder).horaPedido.setText(jsonObject.getString("ped_hora"));
            if (jsonObject.isNull("frete")) {
                ((ItemDemand) holder).cardValDelivery.setVisibility(View.GONE);
            } else {
                ((ItemDemand) holder).cardValDelivery.setVisibility(View.VISIBLE);
                ((ItemDemand) holder).textValDelivery.setText(jsonObject.getString("frete"));
            }

            ((ItemDemand) holder).progressDemand.setProgress(jsonObject.getJSONObject("progress").getInt("percent"));
            ((ItemDemand) holder).pStatus = jsonObject.getJSONObject("progress").getInt("percent");
            ((ItemDemand) holder).time = jsonObject.getJSONObject("progress").getInt("time_up_percent");

            String str =  jsonObject.getJSONObject("progress").getString("time");
            ((ItemDemand) holder).dateResponse = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str).getTime();
            ((ItemDemand) holder).startProgressTime();
        } catch (JSONException | ParseException e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return listItensDemand.length();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        menu.setHeaderTitle("Select The Action");
        menu.add(0, v.getId(), 0, "Call");//groupId, itemId, order, title
        menu.add(0, v.getId(), 0, "SMS");

    }

    class ItemDemand extends RecyclerView.ViewHolder {

        public long dateResponse;
        public TextView codPedido;
        public TextView horaPedido;
        public TextView dataPedido;
        public TextView valorPedido;
        public TextView optionsToCancel;
        public TextView textValDelivery;
        public ProgressBar progressDemand;
        public CardView cardValDelivery;
        public TextView textSemDesconto;
        public TextView nameEstabHistoric;
        public View viewBar;
        Thread thread;
        int pStatus;
        int time;
        Handler handler = new Handler();
        public RecyclerView recyclerViewItensPedido;

        public ItemDemand(View itemView) {
            super(itemView);

            viewBar = (View) itemView.findViewById(R.id.viewBar);
            textSemDesconto = (TextView) itemView.findViewById(R.id.textSemDesconto);
            progressDemand = (ProgressBar) itemView.findViewById(R.id.progressDemand);
            optionsToCancel = (TextView) itemView.findViewById(R.id.optionsToCancel);
            codPedido = (TextView) itemView.findViewById(R.id.codPedido);
            horaPedido = (TextView) itemView.findViewById(R.id.horaPedido);
            dataPedido = (TextView) itemView.findViewById(R.id.dataPedido);
            textValDelivery = (TextView) itemView.findViewById(R.id.end_frete);
            cardValDelivery = (CardView) itemView.findViewById(R.id.cardValDelivery);
            valorPedido = (TextView) itemView.findViewById(R.id.valorPedido);
            nameEstabHistoric = (TextView) itemView.findViewById(R.id.nameEstabHistoric);
            recyclerViewItensPedido = (RecyclerView) itemView.findViewById(R.id.recyclerViewItensPedido);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            recyclerViewItensPedido.setLayoutManager(linearLayoutManager);
            textSemDesconto.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);

            itemView.setOnCreateContextMenuListener(new View.OnCreateContextMenuListener() {
                @Override
                public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                    menu.add("delete").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            //do what u want
                            return true;
                        }
                    });
                }
            });


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*try {
                        int pos = getAdapterPosition();
                        JSONObject itemCart = listItensDemand.getJSONObject(pos);
                        Toast.makeText(view.getContext(),"aqui: "+itemCart.getString("ped_id"), Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/

                    try {
                        int pos = getAdapterPosition();
                        JSONObject itemCart = listItensDemand.getJSONObject(pos);
                        DetailDemand.idPedido = itemCart.getString("ped_id");
                        DetailDemand.detailObject = itemCart;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    activity.startActivityForResult(new Intent(getApplicationContext(), DetailDemand.class), 1);
                }
            });

            optionsToCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    PopupMenu popupMenu = new PopupMenu(activity, v);
                    popupMenu.getMenuInflater().inflate(R.menu.menu_cancel_demand, popupMenu.getMenu());
                    popupMenu.show();
                    popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem menuItem) {
                            int pos = getAdapterPosition();

                            switch (menuItem.getItemId()) {

                                case R.id.navigation_avaliar:
                                    if (onItemClickCancelListener != null) {
                                        try {
                                            onItemClickAvaliate.openAvaliate(listItensDemand.getJSONObject(pos));
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    return true;

                                case R.id.navigation_ver_detalhes:
                                    if (onItemClickCancelListener != null) {
                                        try {
                                            onItemClickListenerViewDetalhes.onItemClickListenerViewDetalhes(listItensDemand.getJSONObject(pos), pos);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                    return true;

                                default:
                                    return false;
                            }

                        }
                    });
                }
            });

        }

        public void startProgressTime() {
            long temp = Calendar.getInstance().getTimeInMillis() - dateResponse;
            if (pStatus < 100) {
                pStatus += ((int) temp / time);
            }
            progressDemand.setProgress(pStatus);
            thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (pStatus < 100) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                progressDemand.setProgress(pStatus);
                            }
                        });
                        try {
                            Thread.sleep(time);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        pStatus++;
                    }

                    Drawable progressDrawable = progressDemand.getProgressDrawable().mutate();
                    progressDrawable.setColorFilter(Color.RED, android.graphics.PorterDuff.Mode.SRC_IN);
                    progressDemand.setProgressDrawable(progressDrawable);

                    try {
                        thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
            });

            thread.start();

        }
    }

    public interface onItemClickCancel {
        public void cancel(JSONObject jsonObject);
    }

    public void setOnItemClickCancelListener(onItemClickCancel onItemClickCancelListener) {
        this.onItemClickCancelListener = onItemClickCancelListener;
    }

    public interface OnItemClickAvaliate {
        public void openAvaliate(JSONObject jsonObject);
    }

    public void setOnItemClickAvaliate(OnItemClickAvaliate onItemClickAvaliate) {
        this.onItemClickAvaliate = onItemClickAvaliate;
    }

    public void setShowPopUpAvaliationListener(ShowPopUpAvaliationListener showPopUpAvaliationListener) {
        this.showPopUpAvaliationListener = showPopUpAvaliationListener;
    }

    public interface ShowPopUpAvaliationListener {
        public void showPopUpAvaliation(JSONObject jsonObject, String position);
    }


    public interface OnItemClickListenerViewDetalhes {
        public void onItemClickListenerViewDetalhes(JSONObject itemCart, int position);
    }

    public void setOnClickListenerViewDetalhes(OnItemClickListenerViewDetalhes onItemClickListenerViewDetalhes) {
        this.onItemClickListenerViewDetalhes = onItemClickListenerViewDetalhes;
    }

}