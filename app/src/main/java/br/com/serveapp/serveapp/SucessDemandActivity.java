package br.com.serveapp.serveapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import br.com.serveapp.serveapp.R;

public class SucessDemandActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sucess_demand_pop_up);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        TextView textViewNumPedido = findViewById(R.id.numPedido);
        textViewNumPedido.setText("Pedido "+  DetailDemand.idPedido);

        final Intent intent = new Intent(getApplicationContext(), DetailDemand.class);
        intent.putExtra("nome", getIntent().getStringExtra("nome"));
        intent.putExtra("demandFinalized", true);

        Button btnGoToHistoric = findViewById(R.id.btnGoToHistoric);
        btnGoToHistoric.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed() {
        final Intent intent = new Intent(getApplicationContext(), DetailDemand.class);
        intent.putExtra("nome", getIntent().getStringExtra("nome"));
        intent.putExtra("demandFinalized", true);
        ListProductsStablishmentDeliveryActivity.qtdItem = 0;
        startActivity(intent);
        finish();
    }
}