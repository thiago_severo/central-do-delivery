package br.com.serveapp.serveapp.util;

import android.content.Intent;

public class Address {

    private String rua;
    private String bairro;
    private String cep;
    private String cidade;
    private String uf;

    public Address(){
        this.rua = new String();
        this.bairro = new String();
        this.cep = new String();
        this.cidade = new String();
        this.uf = new String();
    }

    public Address(String rua, String bairro, String cep, String cidade, String uf){
        this.rua = rua;
        this.bairro = bairro;
        this.cep = cep;
        this.cidade = cidade;
        this.uf = uf;
    }

    private static boolean isCep(String cep){
        try{
            cep = cep.trim().replace("-","");
            Integer.parseInt(cep);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public static Address factory(String address){
        Address retorno = new Address();
        int i = 0;
        try{
            String[] lines = address.split(",");
            if(lines.length>1){
                i = lines.length-2;
                if(isCep(lines[i])){
                    retorno.setCep(lines[i]);
                    i--;
                }

                if(lines[i].trim().length()>2){
                    // trás a cidade junto com o estado
                    retorno.setCidade(lines[i].split("-")[0].trim());
                    retorno.setUf(lines[i].split("-")[1].trim());
                    String[] temp = lines[0].split("-");
                    if(temp.length == 1){
                        // tras só o bairro
                        retorno.setBairro(temp[0].trim());
                        retorno.setRua("");
                    }else{
                        // tras a rua junto com o bairro
                        retorno.setRua(temp[0].trim());
                        retorno.setBairro(temp[1].trim());
                    }
                }else{
                    // trás só o estado
                    retorno.setCidade(lines[0].trim());
                    retorno.setUf(lines[1].trim());
                }

            }else{
                return new Address(address,"","","","");
            }
        }catch (Exception e){
        }

        return retorno;
    }

    public String getRua() {
        return rua;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        if(cep.replace("-","").length()==8)
            this.cep = cep;
        else
            this.cep = "";
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }
}
