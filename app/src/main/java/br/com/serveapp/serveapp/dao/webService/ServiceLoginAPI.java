package br.com.serveapp.serveapp.dao.webService;

import android.content.Intent;
import android.widget.Toast;

import com.android.volley.VolleyError;

import br.com.serveapp.serveapp.ConfirmCodeSms;
import br.com.serveapp.serveapp.SearchAddressActivity;
import br.com.serveapp.serveapp.repository.objects.FilesName;
import br.com.serveapp.serveapp.repository.objects.Repository;
import br.com.serveapp.serveapp.CreateAddressAccountActivity;
import br.com.serveapp.serveapp.LoginActivity;
import br.com.serveapp.serveapp.NavigationDrawerActivity;
import br.com.serveapp.serveapp.dao.Dao;
import br.com.serveapp.serveapp.dao.web.OnPostResponse;

import com.google.android.gms.auth.api.signin.GoogleSignInResult;

import org.json.JSONException;
import org.json.JSONObject;

public class ServiceLoginAPI extends OnPostResponse {

    LoginActivity loginActivity;
    ConfirmCodeSms confirmCodeSms;

    GoogleSignInResult result;

    public ServiceLoginAPI(LoginActivity loginActivity, ConfirmCodeSms confirmCodeSms) {

        this.loginActivity = loginActivity;
        this.confirmCodeSms = confirmCodeSms;
        this.setAutoReconnect(false);
        this.setSaveCache(false);
        this.script = "login_api.php";

    }

    public void setResult(GoogleSignInResult result) {
        this.result = result;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(loginActivity!=null) {
            loginActivity.mProgress.dismiss();
            Toast.makeText(loginActivity.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
        }else if(confirmCodeSms!=null){
            confirmCodeSms.mProgressCreateAddress.dismiss();
          //Toast.makeText(confirmCodeSms.getApplicationContext(), "erro de conexão", Toast.LENGTH_LONG).show();
            confirmCodeSms.dimissAlertAlreadyCodeSms("erro de conexão");
        }
    }

    @Override
    public void onResponse(String response) {

        if (loginActivity != null) {
            loginSocialNetwork(response);
        }else if(confirmCodeSms !=null){
            loginPhone(response);
        }

        }

    public void loginPhone(String response) {

        if (!response.equals("null"))

            try {

                JSONObject jsonObject = new JSONObject(response);
                JSONObject user = null;

                if (!jsonObject.isNull("result")) {

                    user = jsonObject.getJSONObject("result");
                    Repository.setUser(user);
                    Intent intent;

                    if (Repository.getUser(confirmCodeSms).isNull("id_endereco")) {
                        intent = new Intent(confirmCodeSms.getApplicationContext(), SearchAddressActivity.class);
                        intent.putExtra("createUser", "y");
                    } else {
                        Dao.save(user.toString(), FilesName.user, confirmCodeSms.getApplicationContext());
                        intent = new Intent(confirmCodeSms.getApplicationContext(), NavigationDrawerActivity.class);
                    }

                    //confirmCodeSms.mProgressCreateAddress.dismiss();
                    confirmCodeSms.startActivity(intent);
                    confirmCodeSms.setResult(99);
                    confirmCodeSms.finish();

                } else {
                    confirmCodeSms.mProgressCreateAddress.dismiss();
                    //  loginActivity.buildConfirmDialog("Usuário ou senha incorreto", "");
                }

            } catch (JSONException e) {

                e.printStackTrace();
                confirmCodeSms.mProgressCreateAddress.dismiss();
                Toast.makeText(confirmCodeSms.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();

            }

    }

    public void loginSocialNetwork(String response) {

        if (!response.equals("null"))
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONObject user = null;

                if (!jsonObject.isNull("result")) {
                    user = jsonObject.getJSONObject("result");
                    Repository.setUser(user);
                    Intent intent;

                    if (Repository.getUser(loginActivity).isNull("id_endereco")) {
                        intent = new Intent(loginActivity.getApplicationContext(), SearchAddressActivity.class);
                        intent.putExtra("createUser", "y");
                    } else {

                        Dao.save(user.toString(), FilesName.user, loginActivity.getApplicationContext());
                       // loginActivity.mProgress.dismiss();
                        intent = new Intent(loginActivity.getApplicationContext(), NavigationDrawerActivity.class);
                    }
                    loginActivity.startActivity(intent);
                    loginActivity.finish();

                } else {
                    loginActivity.mProgress.dismiss();
                    loginActivity.buildConfirmDialog("Usuário ou senha incorreto", "");
                }

            } catch (JSONException e) {
                e.printStackTrace();
                loginActivity.mProgress.dismiss();
                Toast.makeText(loginActivity.getApplicationContext(), "algum erro aconteceu", Toast.LENGTH_LONG).show();
            }


    }


}
